//PRIVATE

var fs = require('fs');
var ok, helper, defaultEncoding; //objects that were init externally and passed to this file

var writeFullDirPath=function(path, endsWithFile){
    var ret={created_dirs:[], parent_dirs:''};

    var fileInPath=0;
    if(endsWithFile==undefined){ endsWithFile=false; }
    if(endsWithFile){ fileInPath=1; }
    
    var dirs=path.split('/');

    //first make sure all of the directories exists
    var parentDirs='';
    for(var d=0;d<dirs.length-fileInPath;d++){
        var dir=dirs[d];
        if(parentDirs.length>0){ parentDirs+='/'; }
        parentDirs+=dir;
        if(dir.length>0 && dir!=='.'){ //if this is not the first directory, ./
            if(path.indexOf('/')===0){
                if(parentDirs.indexOf('/')!==0){
                    parentDirs = '/' + parentDirs;
                }
            }
            //if this directory doesn't already exist
            if(!fs.existsSync(parentDirs)){
                fs.mkdirSync(parentDirs);
                ret['created_dirs'].push(parentDirs);
            }
        }
    }
    ret['parent_dirs']=parentDirs;
    return ret;
};

//create directories (if they don't already exist) and write the file
var writeFullFilePath=function(path, fileContent){
    var ret={write:false, created_dirs:[]};

    //create the directories if needed
    var dirRet=writeFullDirPath(path, true);
    ret['created_dirs']=dirRet['created_dirs'];
    parentDirs=dirRet['parent_dirs'];

    //if all parent directories exist, or there are no needed parent directories
    if(parentDirs.length<1 || fs.existsSync(parentDirs)){
        fs.writeFileSync(path, fileContent);
        ret['write']=true;
    }
    return ret;
};

//determine if this is a junk system file by its name
var isJunkFile=function(fDir, fName){
    var isJunk=false;
    fDir=helper.endPathWithSlash(fDir);
    try {
        var read=fs.accessSync(fDir+fName, fs.constants.R_OK); //readable?
        var isVis=fs.accessSync(fDir+fName, fs.constants.F_OK); //visible?
        
        if(!fs.lstatSync(fDir+fName).isDirectory() && !fs.lstatSync(fDir+fName).isFile()){
           isJunk=true;
        }
    } catch (err) {
        isJunk=true;
    }
    if(!isJunk){
        if(fName.indexOf('}')!=-1){
            isJunk=true;  
        }else if(fName.indexOf('{')!=-1){
            isJunk=true;  
        }else if(fName.toLowerCase().indexOf('ntuser.dat.log')!=-1){
            isJunk=true;  
        }else if(fName.toLowerCase()=='ntuser.ini'){
            isJunk=true;  
        }else if(fName.toLowerCase()=='.ds_store'){
            isJunk=true;  
        }
    }
    return isJunk;
};

//loop through each file and folder under a given path
var eachFile=function(path, itemCallback){
    var fIndex=0;
    var files;
    try{
        files=fs.readdirSync(path);
    }catch(err){  }
    if(files!=undefined){
        for(var f=0;f<files.length;f++){
            var file=files[f];
            if(!isJunkFile(path, file)){
                if(itemCallback!=undefined){
                    var ret=itemCallback(file, fIndex);
                    fIndex++;
                    if(ret!=undefined){
                        if(!ret){
                            break; //break this for loop if return=false from itemCallback
                        }else{
                            continue; //continue this for loop if return=true from itemCallback
                        }
                    }
                }
            }
        }
    }
};

//detect if a given path has child files or folders
var dirHasChildren=function(path){
    var ret=false;
    var fileRes=exports.isValidDir(path)
    if(fileRes.valid){
        eachFile(path, function(){
            ret=true; return false;
        });
    }
    return ret;
};

//PUBLIC 

exports.initExternal=function(args){ 
    ok=args['ok']; 
    helper=args['helper']; 
    defaultEncoding=args['default_encoding'];
}

exports.fileExists=function(path){
    return fs.existsSync(path);
};

/*
    isValidPath(path)
    
    return 
    
    {
        status:"ok",
        valid:true
    }
*/
exports.isValidPath=function(path){
    var res={status:ok, valid:false};
    if(path!=undefined){
        if(fs.existsSync(path)){
            var dir=helper.getParentDir(path);
            var name=helper.getNoParentDir(path);
            if(!isJunkFile(dir, name)){
                res.valid=true;
            }else{
                res.status=path+" designated as 'junk' file because of illegal name chars, permissions, or type";
            }
        }else{
            res.status=path+" doesn't exist";
        }
    }else{
        res.status="missing path argument, for isValidPath()";
    }
    return res;
}

/*
    isValidFile(path)
    
    return 
    
    {
        status:"ok",
        valid:true
    }
*/
exports.isValidFile=function(path){
    var res={status:ok, valid:false};
    var pathRes=exports.isValidPath(path);
    if(pathRes.valid){
        if(fs.lstatSync(path).isFile()){
            res.valid=true;
        }else{
            res.status=path+" not a file";
        }
    }else{
        res=pathRes;
    }
    return res;
}

/*
    isValidDir(path)
    
    return 
    
    {
        status:"ok",
        valid:true
    }
*/
exports.isValidDir=function(path){
    var res={status:ok, valid:false};
    var pathRes=exports.isValidPath(path);
    if(pathRes.valid){
        if(fs.lstatSync(path).isDirectory()){
            res.valid=true;
        }else{
            res.status=path+" not a directory";
        }
    }else{
        res=pathRes;
    }
    return res;
}

/*
    readFile({
        path:'some/path',
        encoding:'utf8' <optional>
    })
    
    return 
    
    {
        status:"ok",
        content:"file content"
    }
*/
exports.readFile=function(args){
    var res={status:ok,content:''};
    var path=helper.getArgVal(args, 'path');
    var fileRes=exports.isValidFile(path);
    if(fileRes.valid){
        var encoding=helper.getArgVal(args, 'encoding', defaultEncoding);
        res.content=fs.readFileSync(path, encoding);
    }else{
        res.status=fileRes.status;
    }
    return res;
}

/*
    deletePath({
        path:'some/path'
    })
    
    return 
    
    {
        status:"ok"
    }
*/
exports.deletePath=function(args){
    var res={status:ok};
    var path=helper.getArgVal(args, 'path');
    var fileRes=exports.isValidPath(path);
    if(fileRes.valid){
        fs.unlinkSync(path);
    }else{
        res.status=fileRes.status;
    }
    return res;
}

/*
    deleteFile({
        path:'some/path'
    })
    
    return 
    
    {
        status:"ok"
    }
*/
exports.deleteFile=function(args){
    var res={status:ok};
    var path=helper.getArgVal(args, 'path');
    var fileRes=exports.isValidFile(path);
    if(fileRes.valid){
        fs.unlinkSync(path);
    }else{
        res.status=fileRes.status;
    }
    return res;
}

/*
    dirHasChildren({
        path:'some/path'
    })
    
    return 
    
    {
        status:"ok",
        has_children:true
    }
*/
exports.dirHasChildren=function(args){
    var res={status:'error, missing path'};
    if(args.hasOwnProperty('path')){
        res['status']=ok;
        res['has_children']=dirHasChildren(args['path']);
    }
    return res;
}

/*
    deleteDir({
        path:'some/path'
    })
    
    return 
    
    {
        status:"ok"
    }
*/
exports.deleteDir=function(args){
    var res={status:ok};
    var path=helper.getArgVal(args, 'path');
    var fileRes=exports.isValidDir(path);
    if(fileRes.valid){
        fs.unlinkSync(path);
    }else{
        res.status=fileRes.status;
    }
    return res;
}

/*
    writeFile({
        path:'some/path',
        content:'what to write',
        allow:[ <optional>
            'create-file', <optional>
            'create-dirs', <optional>
            'modify-file' <optional>
        ],
        encoding:'utf8' <optional>
    })
    
    return 
    
    {
        status:"ok",
        <optional> created_dirs:["dir1", "dir2"],
        <optional> file_summary:"no-change / modified / modify-not-allowed / created"
    }
*/
exports.writeFile=function(args){
    var res={status:ok};
    var content=helper.getArgVal(args, 'content');
    if(content!=undefined){
        var path=helper.getArgVal(args, 'path');
        var allow=helper.getArgVal(args, 'allow', [
            'create-file',
            'create-dirs',
            'modify-file'
        ]);
        var resFile=exports.readFile(args);
        if(resFile.status==ok){ //could read existing file (it exists)
            //if there are changes
            if(!helper.isContentSame(resFile.content, content)){
                //if can make updates to existing file
                if(allow.indexOf('modify-file')!=-1){
                    fs.writeFileSync(path, content);
                    res['file_summary']="modified";
                }else{
                    res.status=path+" 'modify' not allowed";
                    res['file_summary']="modify-not-allowed";
                }
            }else{
                res['file_summary']="no-change";
            }
        }else{ //could not read file (doesn't exist?)
            res.status=resFile.status;
            if(path!=undefined){
                //if this file doesn't exist (not another issue, that caused the read to fail)
                if(res.status == path+" doesn't exist"){
                    //if allowed to create file
                    if(allow.indexOf('create-file')!=-1){
                        var doCreate=true;
                        //if not allowed to create directory
                        if(allow.indexOf('create-dirs')==-1){
                            var parentDir=helper.getParentDir(path);
                            var dirRes=exports.isValidDir(parentDir);
                            if(!dirRes.valid){
                                doCreate=false;
                                res.status=parentDir+" 'create' not allowed";
                            }
                        }
                        if(doCreate){
                            var writeRes=writeFullFilePath(path, content); //do the create/write
                            if(writeRes.created_dirs.length>0){
                                res['created_dirs']=writeRes.created_dirs;
                            }
                            res['file_summary']="created";
                            res.status=ok;
                        }
                    }else{
                        res.status=path+" 'create' not allowed";
                    }
                }
            }
        }
    }else{
        res.status="missing content argument, for writeFile()";
    }
    return res;
}

/*
    makeDirs({
        path:'some/path'
    })
    
    return 
    
    {
        status:"ok",
        created_dirs:['created', 'these', 'dirs'],
        parent_dirs:'existing/created/these/dirs'
        
    }
*/
exports.makeDirs=function(args){
    var ret={status:'error, no path specified'};
    if(args.hasOwnProperty('path')){
        var ret=writeFullDirPath(args['path'], false);
        ret['status']=ok;
    }
    return ret;
};

/*
    renamePath({
        old_path:'some/path',
        new_path:'some/path'
    })
    
    return 
    
    {
        status:"ok",
        old_path:{
            status:"ok"
        },
        new_path{
            status:"ok"
        }
        
    }
*/
exports.renamePath=function(args){
    var ret={status:'error, missing old_path'};
    if(args.hasOwnProperty('old_path')){
        ret['status']='error, missing new_path';
        if(args.hasOwnProperty('new_path')){
            var oldRet=exports.isValidPath(args['old_path']);
            ret['status']=oldRet.status;
            if(oldRet.valid){
                if(!fs.existsSync(args['new_path'])){
                    fs.renameSync(args['old_path'], args['new_path']);
                    ret['status']=ok;
                }else{
                    ret['status']='error, new_path already exists, ' + args['new_path'];
                }
            }
        }
    }
    return ret;
};

/*
    browse({
        path: "path/to/folder", 
        recursive: true/false,
        actions:{
            f:{
                include:{
                    all:true/false,
                    has_children:true/false,
                    ext:["js", "txt"], 
                    name_start:["file", "elif"], 
                    name_end:["file", "elif"], 
                    name_contains:["file", "elif"], 
                    name_is:["file", "elif"]
                },
                exclude:{
                    all:true/false,
                    has_children:true/false,
                    ext:["js", "txt"], 
                    name_start:["file", "elif"], 
                    name_end:["file", "elif"], 
                    name_contains:["file", "elif"], 
                    name_is:["file", "elif"]
                },
                label:[
                    {
                        all:true/false,
                        has_children:true/false,
                        ext:["js", "txt"], 
                        name_start:["file", "elif"], 
                        name_end:["file", "elif"], 
                        name_contains:["file", "elif"], 
                        name_is:["file", "elif"],
                        labels:['label1', 'label2']
                    },
                    { ... }
                ]
            },
            d:{
                ... same structure as that of 'f' file types ...
            }
        }
    })
    
    return 
    
    
    {
       status:"ok",
       matches:[
          {
             path:"rel/path",
             f:[
                {
                   name:"something.codegen", 
                   ext:"js", 
                   labels:["custom-label"]
                },
                {
                   name:"something.codegen", 
                   ext:"js", 
                   labels:["custom-label"]
                }
             ],
             d:[
                {
                   name:"somedir", 
                   labels:["custom-label"]
                },
                {
                   name:"somedir", 
                   labels:["custom-label"]
                }
             ]
          },
          { ... }
       ]
    }
*/
exports.browse=function(args){
    var res={status:ok, matches:[]};
    var path=helper.getArgVal(args, 'path');
    if(path!=undefined){
        var resDir=exports.isValidDir(path);
        if(resDir.valid){
            //args
            var actions=helper.getArgVal(args, 'actions', {});
            if(!actions.hasOwnProperty('f')){ actions['f']={}; }
            if(!actions.hasOwnProperty('d')){ actions['d']={}; }
            var recursive=helper.getArgVal(args, 'recursive', false);
            //function to eval ONE key/array OR-set condition
            var evalCondition=function(condKey, condVal, dPath, item, type){
                var ret=false;
                switch(type){
                    case 'f':
                        switch(condKey){
                            case 'ext': 
                                if(condVal.toLowerCase().trim()==item['ext'].toLowerCase().trim()){
                                    ret=true;
                                }else{
                                    ret=false;
                                }
                                break;
                        }
                    case 'd':
                        switch(condKey){
                            case 'has_children': 
                                dPath=helper.endPathWithSlash(dPath);
                                if(dirHasChildren(dPath+item['name'])){
                                    ret=condVal;
                                }else{
                                    ret=!condVal;
                                }
                                break;
                        }
                    default:
                        switch(condKey){
                            case 'all': ret=condVal; break;
                            case 'name_start': 
                                if(item['name'].indexOf(condVal)===0){
                                    ret=true;
                                }else{
                                    ret=false;
                                }
                                break;
                            case 'name_end': 
                                if(item['name'].indexOf(condVal)!=-1){
                                    if(item['name'].lastIndexOf(condVal)==item['name'].length-condVal.length){
                                        ret=true;
                                    }else{
                                        ret=false;
                                    }
                                }else{
                                    ret=false;
                                }
                                break;
                            case 'name_contains': 
                                if(item['name'].indexOf(condVal)!=-1){
                                    ret=true;
                                }else{
                                    ret=false;
                                }
                                break;
                            case 'name_is': 
                                if(item['name']==condVal){
                                    ret=true;
                                }else{
                                    ret=false;
                                }
                                break;
                        }
                        break;
                }
                return ret;
            };
            //function to eval all key/array conditions for an action
            var evalActionConditions=function(conditions, dPath, item, type){
                var ret=true;
                helper.eachKey(conditions, function(key){
                    var valArray=conditions[key]; var valArrayIsTrue=false;
                    if(typeof valArray=='boolean'){
                        valArrayIsTrue=evalCondition(key, valArray, dPath, item, type);
                    }else{
                        for(var v=0;v<valArray.length;v++){
                            var foundTrueVal=evalCondition(key, valArray[v], dPath, item, type);
                            if(foundTrueVal){ //if one of the array values eval true
                                valArrayIsTrue=true; break;
                            }
                        }
                    }
                    //all of the keys must contain at least one true array item, if not, then the entire condition set is false
                    if(!valArrayIsTrue){
                        ret=false; return false;
                    }
                }, {skip:'labels'});
                return ret;
            };
            //function to decide whether to include
            var yesInclude=function(dPath, item, type){
                var ret=true; //include, by default if actions[type].include is undefined
                if(actions[type]['include']!=undefined){
                    ret=evalActionConditions(actions[type]['include'], dPath, item, type);
                }
                return ret;
            };
            //function to decide whether to exclude
            var yesExclude=function(dPath, item, type){
                var ret=false; //do NOT exclude, by default if actions[type].exclude is undefined
                if(actions[type]['exclude']!=undefined){
                    ret=evalActionConditions(actions[type]['exclude'], dPath, item, type);
                }
                return ret;
            };
            //function to set labels, if any
            var setLabels=function(dPath, item, type){
                if(actions[type]['label']!=undefined){
                    //for each label set
                    for(var l=0;l<actions[type]['label'].length;l++){
                        var conditions=actions[type]['label'][l];
                        if(conditions.hasOwnProperty('labels')){
                            var doSetLabels=evalActionConditions(conditions, dPath, item, type);
                            if(doSetLabels){
                                for(var s=0;s<conditions.labels.length;s++){
                                    if(!item.hasOwnProperty('labels')){
                                        item['labels']=[];
                                    }
                                    if(item['labels'].indexOf(conditions.labels[s])==-1){
                                        item['labels'].push(conditions.labels[s]);
                                    }
                                }
                            }
                        }
                    }
                }
                return item;
            };
            //recursive function process one directory level
            var recursiveReadDir=function(dirPath){
                //init empty data for this directory level
                var matchData=undefined;
                //for each item under this directory path
                eachFile(dirPath, function(file){
                    var matchItem={}, type;
                    dirPath=helper.endPathWithSlash(dirPath);
                    if(fs.lstatSync(dirPath+file).isFile()){ //file
                        matchItem['name']=helper.getNoExt(file);
                        matchItem['ext']=helper.getExt(file);
                        type='f';
                    }else{ //directory
                        matchItem['name']=file;
                        type='d';   
                    }
                    //if this item matches the include rules
                    if(yesInclude(dirPath, matchItem, type)){
                        //if this item avoids the exclude rules
                        if(!yesExclude(dirPath, matchItem, type)){
                            //set labels on this item, if any
                            matchItem=setLabels(dirPath, matchItem, type);
                            //init matchData, for this level, if not already init
                            if(matchData==undefined){ matchData={path:dirPath, f:[], d:[]}; }
                            //add this item to the data
                            matchData[type].push(matchItem);
                        }
                    }
                });
                //add the matched child data to the data for this directory level
                if(matchData != undefined && (matchData.f.length>0 || matchData.d.length>0)){
                    res.matches.push(matchData);
                    //recursive calls to child levels, if recursive:true
                    if(recursive){
                        if(matchData.d.length>0){
                            for(var d=0;d<matchData.d.length;d++){
                                dirPath=helper.endPathWithSlash(dirPath);
                                //recursive call on sub folders
                                recursiveReadDir(dirPath+matchData.d[d]['name']);
                            }
                        }
                    }
                }
            };
            //initial directory level
            recursiveReadDir(path);
            
        }else{
            res.status=resDir.status;
        }
    }else{
        res.status="missing path argument, for browse()";
    }
    return res;
}