var fields = {
    "{checkbox}": {
        "label": "Demo Checkbox",
        "summary": "Demo Summary",
        "type": "checkbox"
    },
    "{dropdown}": {
        "type": "select",
        "values": [
            "A",
            "B",
            "C",
            "D",
            "E"
        ],
        "label": "Demo Dropdown ",
        "summary": "Demo Summary"
    },
    "{text}": {
        "label": "Demo Textbox",
        "summary": "Demo Summary",
        "values": [
            "Test Value 1",
            "Test Value 2",
            "Test Value 3"
        ]
    },
    "{variation_demo}": {
        "label": "Variation Demo",
        "summary": "Variation Demo",
        "values": [
            "dbname.1.2.3"
        ],
        "variations": {
            "nodots": "val=val.replace(/\\./g,'');",
            "dots_to_dash": "val=val.replace(/\\./g,'-');"
        }
    }
};