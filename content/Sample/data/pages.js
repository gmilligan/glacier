var pages = {
    "sample1": {
        "sections": [
            {
                "id": "s1",
                "title": "Welcome!"
            },
            {
                "id": "s2",
                "title": "Fields Types"
            },
            {
                "id": "s3",
                "title": "Field Scope"
            },
            {
                "id": "s4",
                "title": "Casing Variations"
            },
            {
                "id": "s5",
                "title": "Custom Variations"
            },
            {
                "id": "s6",
                "title": "Dynamic Conditions"
            }
        ],
        "title": "Sample 1.0",
        "outcome": "Demo Glacier features. Search as a launching point to create your first new page.",
        "search": [
            "Sample"
        ],
        "fields": [
            "{text}",
            "{dropdown}",
            "{checkbox}",
            "{variation_demo}"
        ]
    }
};