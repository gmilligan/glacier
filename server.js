var port = 4000;
var host = 'localhost';

var url='http://' + host + ':' + port;

//require
var eol = require('os').EOL;
var express = require('express'); //npm install --save express
var openBrowser = require('opn'); //npm install --save opn
var app = express();
var bodyParser = require('body-parser'); //npm install --save body-parser

//external server scripts
var fsHelper = require('./server/filesystem');
var helper = require('./lib/js/helper');

//constants
var ok='ok';
var defaultEncoding='utf8';
var contentRoot='./content/';

var dataFolder='data';
var fieldsFile='fields.js';
var pagesFile='pages.js';

var cssFolder='css';
var cssFile='css.css';

var imgFolder='img';

//pass values to the filesystem.js file
fsHelper.initExternal({
    ok:ok, helper:helper.helper, default_encoding:defaultEncoding
});

//root
app.use(express.static(__dirname+'/'));
app.use( bodyParser.json() ); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true
}));

var cleanDirScope=function(dir_scope){
    //make sure the dir_scope isn't surrounded by '/'
    if(dir_scope.indexOf('/')===0){
        dir_scope=dir_scope.substring('/'.length);
    }
    if(dir_scope.lastIndexOf('/')==dir_scope.length-'/'.length){
        dir_scope=dir_scope.substring(0, dir_scope.lastIndexOf('/'));
    }
    return dir_scope;
};

var getContentPath=function(dir_scope, page_id, file_key){
    var path=contentRoot;
    
    //make sure the dir_scope isn't surrounded by '/'
    dir_scope=cleanDirScope(dir_scope);
    
    path+=dir_scope;
    switch(file_key){
        case 'pages.js':
            path+='/'+dataFolder+'/'+pagesFile;
            break;
        case 'fields.js':
            path+='/'+dataFolder+'/'+fieldsFile;
            break;
        case 'index.html':
            path+='/'+page_id+'.html';
            break;
        case 'css.css':
            path+='/'+cssFolder+'/'+cssFile;
            break;
        default:
            path=undefined;
            break;
    }
    return path;
};

//ping server to check to see if it's up and functional
app.get('/is-server-up', function(req, res){
    res.send(JSON.stringify({status:ok, content_root:contentRoot, default_encoding:defaultEncoding}));
});

//load the content files for one level
app.post('/load-content-files',function(req,res){
    if(req.body.hasOwnProperty('path')){
        var path=req.body.path;
        
        if(path.indexOf(contentRoot)===0){
            path=path.substring(contentRoot.length);
        }
        
        var ret=fsHelper.browse({
            path:contentRoot+path,
            recursive:false,
            actions:{
                f:{
                    include:{
                        ext:["html"]
                    }
                },
                d:{
                    label:[{
                            has_children:true,
                            labels:['parent']
                    }],
                    exclude:{
                        name_is:['node_modules', '.git', 'css', 'data', 'img']
                    }
                }
            }
        });

        res.send(JSON.stringify(ret));
    }
});

//write save-changes to a file (never more than one at a time)
app.post('/write-to-file',function(req,res){
    var ret={status:'error, missing dir_scope'};
    if(req.body.hasOwnProperty('dir_scope')){
        ret['status']='error, missing page_id';
        if(req.body.hasOwnProperty('page_id')){
            ret['status']='error, missing file_key';
            if(req.body.hasOwnProperty('file_key')){
                ret['status']='error, missing content';
                if(req.body.hasOwnProperty('content')){
                    //get the arguments into variables
                    var dir_scope=req.body.dir_scope, page_id=req.body.page_id;
                    var file_key=req.body.file_key, content=req.body.content;
                    //get the file path
                    var path=getContentPath(dir_scope, page_id, file_key);
                    ret['status']='error, invalid file_key, '+file_key;
                    //if this is a valid file path
                    if(path!=undefined){
                        var shouldWrite=true;
                        //get existing file content, if exists
                        ret=fsHelper.readFile({
                            path:path,
                            encoding:defaultEncoding
                        });
                        //if file exists
                        if(ret.status==ok){
                            //if the content from the current file is the same as the new content
                            if(ret.content.length===content.length){
                                if(ret.content==content){
                                    shouldWrite=false;
                                    ret['status']='error, no new changes';
                                }
                            }
                        }
                        if(shouldWrite){
                            ret=fsHelper.writeFile({
                                path:path,
                                content:content,
                                encoding:defaultEncoding
                            });
                        }
                    }
                    ret['path']=path;
                }
            }
        }
    }
    res.send(JSON.stringify(ret));
});

//rename html content file
app.post('/rename-content-file',function(req,res){
    var ret={status:'error, missing dir_scope'};
    if(req.body.hasOwnProperty('dir_scope')){
        ret['status']='error, missing page_id';
        if(req.body.hasOwnProperty('page_id')){
            ret['status']='error, missing new_page_id';
            if(req.body.hasOwnProperty('new_page_id')){
                //get the arguments into variables
                var dir_scope=req.body.dir_scope, page_id=req.body.page_id;
                var file_key='index.html', new_page_id=req.body.new_page_id;
                //get the file path
                var path=getContentPath(dir_scope, page_id, file_key);
                var newPath=getContentPath(dir_scope, new_page_id, file_key);
                //if the file path is valid
                if(path!=undefined){
                    //get existing file content, if exists
                    ret=fsHelper.readFile({
                        path:path,
                        encoding:defaultEncoding
                    });
                    //if file to rename exists
                    if(ret.status==ok){
                        //if the new path doesn't already exist
                        ret['status']='error, cannot rename to path, already exists, '+newPath;
                        if(!fsHelper.fileExists(newPath)){
                            var content=ret.content;
                            //write the new name of the file
                            ret=fsHelper.writeFile({
                                path:newPath,
                                content:content,
                                encoding:defaultEncoding
                            });
                            //if copy was written
                            if(ret.status==ok){
                                //delete the original file
                                ret=fsHelper.deleteFile({
                                    path:path
                                });
                            }
                            
                        }
                    }
                }
                ret['path']=path;
                ret['new_path']=newPath;
            }
        }
    }
    res.send(JSON.stringify(ret));
});

//rename html content file
app.post('/delete-content-file',function(req,res){
    var ret={status:'error, missing dir_scope'};
    if(req.body.hasOwnProperty('dir_scope')){
        ret['status']='error, missing page_id';
        if(req.body.hasOwnProperty('page_id')){
            //get the arguments into variables
            var dir_scope=req.body.dir_scope, page_id=req.body.page_id;
            var file_key='index.html';
            //get the file path
            var path=getContentPath(dir_scope, page_id, file_key);
            //if the file path is valid
            if(path!=undefined){
                //delete html file
                ret=fsHelper.deleteFile({
                    path:path
                });
            }
            ret['path']=path;
        }
    }
    res.send(JSON.stringify(ret));
});

//figure out if a suggested file name already exists within a given scope, then return a unique name, if it already exists
app.post('/get-unique-content-file-name',function(req,res){
    var ret={status:'error, missing dir_scope'};
    if(req.body.hasOwnProperty('dir_scope')){
        //get optional suggest_name
        var suggest_name='page';
        if(req.body.hasOwnProperty('suggest_name')){
            suggest_name=req.body['suggest_name'];
        }
        //get optional start_with
        var start_with=suggest_name;
        if(req.body.hasOwnProperty('start_with')){
            start_with=req.body['start_with'];
        }
        var dir_scope=req.body.dir_scope;
        //make sure the dir_scope isn't surrounded by '/'
        dir_scope=cleanDirScope(dir_scope);
        //make the full path
        var path=contentRoot+dir_scope+'/';
        ret['path']=path;
        //see if the path exists
        ret=fsHelper.isValidDir(path);
        if(ret.valid){
            //if the suggested name is unique in this folder scope
            if(!fsHelper.fileExists(path+suggest_name+'.html')){
                ret['unique_name']=suggest_name;
                ret['status']=ok;
            }else{ //suggested name is not unique within this folder scope
                var num=1;
                while(fsHelper.fileExists(path+start_with+'_'+num+'.html')){
                    num++;
                }
                ret['unique_name']=start_with+'_'+num;
                ret['status']=ok;
            }
        }
    }
    res.send(JSON.stringify(ret));
});

//get the json data from a file that may not be in scope
app.post('/get-data-json',function(req,res){
    var ret={status:'error, missing dir_scope'};
    if(req.body.hasOwnProperty('dir_scope')){
        ret['status']='error, missing page_id';
        if(req.body.hasOwnProperty('page_id')){
            //get the arguments into variables
            var dir_scope=req.body.dir_scope, file_key=req.body.page_id;
            //get the path to this file
            var path=getContentPath(dir_scope, undefined, file_key);
            ret['path']=path;
            //read this file 
            ret=fsHelper.readFile({
                path:path,
                encoding:defaultEncoding
            });
            if(ret.status==ok){
                var content=ret.content;
                content=content.trim();
                //trim off everything left of the first {
                var indexOfStart=content.indexOf('{');
                if(indexOfStart!=-1){
                    content=content.substring(indexOfStart);
                }
                //trim off everything right of the last }
                var indexOfEnd=content.lastIndexOf('}');
                if(indexOfEnd!=-1){
                    content=content.substring(0, indexOfEnd+'}'.length);
                }
                //set this modified content on the return string
                ret.content=content;
            }
        }
    }
    res.send(JSON.stringify(ret));
});

//create a new page (and other files, like data/pages.js, data/fields.js, css/css.css, img) if they don't already exist
app.post('/create-new-page',function(req,res){
    var ret={status:'error, missing dir_scope'};
    if(req.body.hasOwnProperty('dir_scope')){
        ret['status']='error, missing page_id';
        if(req.body.hasOwnProperty('page_id')){
            ret['status']='error, missing content';
            if(req.body.hasOwnProperty('content')){
                ret['status']='error, missing pages_json';
                if(req.body.hasOwnProperty('pages_json')){
                    //get the arguments into variables
                    var dir_scope=req.body.dir_scope, page_id=req.body.page_id;
                    var content=req.body.content, pages_json=req.body.pages_json;
                    //get the file path

                    //function to create a file, if it doesn't exist
                    var createFileIfNotExist=function(fkey, str, allowModify){
                        if(allowModify==undefined){ allowModify=false; }
                        var path=getContentPath(dir_scope, page_id, fkey);
                        ret[fkey]={};
                        //if the file path is valid for index.html
                        if(path!=undefined){
                            var allow=[
                                'create-file',
                                'create-dirs'
                            ];
                            if(allowModify){
                                allow.push('modify-file');
                            }
                            ret[fkey]=fsHelper.writeFile({
                                path:path,
                                content:str,
                                encoding:defaultEncoding,
                                allow:allow
                            });
                            ret[fkey]['path']=path;
                        }
                    };
                    //create the index.html file
                    createFileIfNotExist('index.html', content);
                    ret['status']=ret['index.html']['status'];
                    //if the index.html file could be created
                    if(ret['status']==ok){
                        //create the fields.js, if needed
                        createFileIfNotExist('fields.js', 'var fields = { };');
                        //create the css.css, if needed
                        createFileIfNotExist('css.css', '/* your custom css for this folder scope */');
                        //create/update pages.js
                        createFileIfNotExist('pages.js', pages_json, true); //allow modify 
                        
                        //create the img folder, if not exist
                        var imgDir=contentRoot+cleanDirScope(dir_scope)+'/'+imgFolder;
                        fsHelper.makeDirs({
                            path:imgDir
                        });
                    }
                }
            }
        }
    }
    res.send(JSON.stringify(ret));
});

//create a content directory
app.post('/create-new-content-dir',function(req,res){
    var ret={status:'error, missing dir_scope'};
    if(req.body.hasOwnProperty('dir_scope')){
        //get the arguments into variables
        var dir_scope=req.body.dir_scope;
        //make the full path
        var path=contentRoot+cleanDirScope(dir_scope);
        ret['path']=path;
        //make the directory
        ret=fsHelper.makeDirs({
            path:path
        });
    }
    res.send(JSON.stringify(ret));
});

//rename a content folder
app.post('/rename-content-dir',function(req,res){
    var ret={status:'error, missing old_path'};
    if(req.body.hasOwnProperty('old_path')){
        var ret={status:'error, missing new_path'};
        if(req.body.hasOwnProperty('new_path')){
            //get the arguments into variables
            var old_path=req.body.old_path;
            var new_path=req.body.new_path;
            //make the full path
            old_path=contentRoot+cleanDirScope(old_path);
            new_path=contentRoot+cleanDirScope(new_path);
            ret['old_path']=old_path;
            ret['new_path']=new_path;
            //rename the directory
            ret=fsHelper.renamePath({
                old_path:old_path,
                new_path:new_path
            });
        }
    }
    res.send(JSON.stringify(ret));
});

//check if a root directory exists
app.post('/root-dir-exists',function(req,res){
    var ret={status:'error, missing path'};
    if(req.body.hasOwnProperty('path')){
        var path=req.body.path;
        ret=fsHelper.isValidDir(path);
    }
    res.send(JSON.stringify(ret));
});

//create a content directory
app.post('/write-output-file',function(req,res){
    var ret={status:'error, missing root'};
    if(req.body.hasOwnProperty('root')){
        //get the arguments into variables
        var root=req.body.root;
        ret['root']=root;
        ret['status']='error, missing path';
        if(req.body.hasOwnProperty('path')){
            var path=req.body.path;
            ret['path']=path;
            ret['status']='error, missing content';
            if(req.body.hasOwnProperty('content')){
                var content=req.body.content;
                //if the root is a valid directory
                ret=fsHelper.isValidDir(root);
                if(ret.valid){
                    ret=fsHelper.writeFile({
                        path:root+path,
                        content:content,
                        allow:[
                            'create-file',
                            'create-dirs'
                        ]
                    });
                    
                    ret['root']=root;
                    ret['path']=path;
                    ret['full_path']=root+path;
                }
            }
        }
    }
    res.send(JSON.stringify(ret));
});

//start up tab
var server = app.listen(port, function () {
    var hashLine='#######################';
    console.log('\n'+hashLine+'\n Welcome to Glacier! \n'+hashLine+'\n');
    openBrowser(url);
});