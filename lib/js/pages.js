var pagesHelper=(function(){
    // private

    var getDirectorySvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M416 480l96-256h-416l-96 256zM64 192l-64 288v-416h144l64 64h208v64z"></path></svg>';
    };

    var getHtmlFileSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024"><path d="M608 832v64.082c0 35.408-28.617 63.918-63.918 63.918h-480.165c-35.408 0-63.918-28.759-63.918-64.235v-735.531c0-35.488 28.776-64.235 64.273-64.235h319.727v192.061c0 35.565 28.739 63.939 64.189 63.939h159.811v64h-415.781c-53.14 0-96.219 42.952-96.219 95.961v224.078c0 52.998 42.752 95.961 96.219 95.961h415.781zM416 96v191.906c0 17.725 14.431 32.094 31.705 32.094h160.295l-192-224zM192.235 448h735.531c35.488 0 64.235 28.539 64.235 63.745v224.511c0 34.939-28.759 63.745-64.235 63.745h-735.531c-35.488 0-64.235-28.539-64.235-63.745v-224.511c0-34.939 28.759-63.745 64.235-63.745zM928 704h-128v-192h-32v224h160v-32zM448 544v192h32v-192h64v-32h-160v32h64zM320 608h-96v-96h-32v224h32v-96h96v96h32v-224h-32v96zM656 608l-48-96h-32v224h32v-160l32 64h32l32-64v160h32v-224h-32l-48 96z"></path></svg>';
    };

    var getTrashSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M64 160v320c0 17.6 14.4 32 32 32h288c17.6 0 32-14.4 32-32v-320h-352zM160 448h-32v-224h32v224zM224 448h-32v-224h32v224zM288 448h-32v-224h32v224zM352 448h-32v-224h32v224z"></path><path d="M424 64h-104v-40c0-13.2-10.8-24-24-24h-112c-13.2 0-24 10.8-24 24v40h-104c-13.2 0-24 10.8-24 24v40h416v-40c0-13.2-10.8-24-24-24zM288 64h-96v-31.599h96v31.599z"></path></svg>';
    };

    var getThumbDownSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 640"><path d="M352 640c-35.346 0-64-28.654-64-64v0-192h-224c-35.346 0-64-28.654-64-64v0-64l73.6-195.84c14.951-34.222 47.654-58.127 86.154-60.15l0.246-0.010h256c35.346 0 64 28.654 64 64v0 256l-96 224v96h-32zM544 320v-320h96v320h-96z"></path></svg>';
    }

    var getThumbUpSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 640"><path d="M352 0h32v96l96 224v256c0 35.346-28.654 64-64 64v0h-256c-35.2 0-73.92-26.88-86.4-60.16l-73.6-195.84v-64c0-35.346 28.654-64 64-64v0h224v-192c0-35.346 28.654-64 64-64v0zM544 320h96v320h-96v-320z"></path></svg>';
    }

    var getUrlParamString=function(){
        var paramsStr='';
        var href=document.location.href;
        var indexOfParams=href.indexOf('?');
        if(indexOfParams!=-1){
            paramsStr=href.substring(indexOfParams+'?'.length);
        }
        return paramsStr;
    };

    var getUrlParams=function(){
        var retParams={};
        var href=getUrlParamString();
        if(href.length>0){
            var kvArr=href.split('&');
            for(var kv=0;kv<kvArr.length;kv++){
                var keyVal=kvArr[kv];
                var keyValArr=keyVal.split('=');
                if(keyValArr.length==2){
                    retParams[keyValArr[0].trim()]=keyValArr[1].trim();
                }
            }
        }
        return retParams;
    };

    //restores params if page reload is canceled
    var restoreUrlParams=function(urlParams){
        var href=document.location.href;

        //remove changed params
        var currentParams=getUrlParamString();
        if(currentParams.length>0){
            href=href.substring(0, href.indexOf('?'+currentParams));
        }

        //add on the original url params
        href+='?'+urlParams;

        //set the url params
        document.location.href=href;
    };

    var reloadPage=function(){
        document.location.reload();
    }

    var setUrlParams=function(newParams){
        var anyChanges=false;
        var origParams=getUrlParamString();
        //put the new params into the current params
        var currentParams=getUrlParams();
        for(var p in newParams){
            if(newParams.hasOwnProperty(p)){
                //if the new param is NOT trying to remove a param
                if(typeof newParams[p] != 'boolean'){
                    //if this url param value is different than what is already there
                    if(!currentParams.hasOwnProperty(p) || currentParams[p]!=newParams[p]){
                        currentParams[p]=newParams[p];
                        anyChanges=true;
                    }
                }else if(!newParams[p]){ //new param = 'false' to remove
                    if(currentParams.hasOwnProperty(p)){
                        //remove this param
                        delete currentParams[p];
                        anyChanges=true;
                    }
                }
            }
        }
        if(anyChanges){
            //strip off current param string
            var href=document.location.href;
            var indexOfParams=href.indexOf('?');
            if(indexOfParams!=-1){
                href=href.substring(0, indexOfParams);
            }
            //append modified param string
            var newParamsStr='';
            for(var c in currentParams){
                if(currentParams.hasOwnProperty(c)){
                    if(newParamsStr.length>0){
                        newParamsStr+='&';
                    }else{
                        newParamsStr+='?';
                    }
                    newParamsStr+=c+'='+currentParams[c];
                }
            }

            var hashIndex=href.indexOf('#');
            if(hashIndex!=-1){
                href=href.substring(0, hashIndex);
            }

            document.location.href=href+'#'+newParamsStr;
            reloadPage();

            //since reload can be canceled, restore params if the reload is canceled
            setTimeout(function(){
                //this will never execute if the .reload() is NOT canceled
                restoreUrlParams(origParams);
            },100);
        }
        return anyChanges;
    };

    var setPageModeClass=function(){
        var mode=getPageMode();
        jQuery('body:first').addClass('mode-'+mode);
    };

    var getPageMode=function(){
        var mode='view';
        var params=getUrlParams();
        if(params.hasOwnProperty('mode')){
            mode=params['mode'];
        }
        return mode;
    };

    var getHomeUrl=function(){
        var url=document.location.origin;
        return url;
    };

    var goHome=function(){
        var url=getHomeUrl();
        document.location.href=url;
    };

    var getContentPageUrl=function(folderScope, pageId, mode){
        var url=document.location.origin;
        var contentRoot=pagesHelper.getRootContentDir();
        if(url.lastIndexOf('/')!=url.length-'/'.length){
            url+='/';
        }
        url+=contentRoot+'/'+folderScope+'/'+pageId+'.html';
        if(mode!=undefined && mode=='edit'){
            url+='#?mode='+mode;
        }
        return url;
    }

    var goToContentPage=function(folderScope, pageId, mode){
        var url=getContentPageUrl(folderScope, pageId, mode);
        document.location.href=url;
    };

    var isPageMode=function(mode){
        isMode=false;
        var urlParams=getUrlParams();
        if(urlParams.hasOwnProperty('mode')){
            if(urlParams['mode']=='edit'){
                isMode=true;
            }
        }else if(mode=='view'){
            isMode=true;
        }
        return isMode;
    };

    var setPageMode=function(mode){
        if(mode=='view'){ mode=false; }
        return setUrlParams({mode:mode});
    };

    var setPageId=function(newPageId){
        var pageId=pagesHelper.getPageId();
        var urlParams=getUrlParamString();
        if(urlParams.length>0){
            urlParams='#?'+urlParams;
        }
        var url=document.location.href;
        url=url.substring(0, url.lastIndexOf(pageId+'.html'));
        var newUrl=url+newPageId+'.html'+urlParams;
        if(newUrl!=document.location.href){
            document.location.href=newUrl;
        }
    };

    var dataToString=function(obj, prop, defaultVal){
        var theVal;
        if(typeof obj == 'string'){
            theVal = obj;
        }else if(obj.hasOwnProperty(prop)){
            theVal = obj[prop];
        }
        if(theVal==undefined){
            theVal=defaultVal;
        }
        return theVal;
    };

    //set page wrap
    var setPageTokenWrap=function(){
        var bod=jQuery('body:first');
        bod.append('<div class="token-wrap"></div>');
        var tokensWrap=bod.children('.token-wrap:first');
        bod.children().not('.token-wrap').each(function(){
            tokensWrap.append(jQuery(this));
        });
    };

    var getCodeElSaveContent=function(codeEl){
        var cloneEl=codeEl.clone();
        //remove <token> html markup from content
        var tokens=cloneEl.find('token');
        tokens.each(function(){
            var txt=jQuery(this).text();
            jQuery(this).after(txt);
        });
        tokens.remove();
        //get the content string
        var content=cloneEl.html();
        //replace char codes with string version
        content=content
            .replace(/&nbsp;/g, ' ')
            .replace(/<br \/>/g, '\n')
            .replace(/<br\/>/g, '\n')
            .replace(/<br>/g, '\n')
            .replace(/&gt;/g, '>')
            .replace(/&lt;/g, '<');
        return content;
    };

    var handleFileWrite=function(writeBtn){
        var outWrap=writeBtn.parents('.file-out-wrap:first');
        if(outWrap.hasClass('valid-root')){
            var rootInput=outWrap.find('.root-input:first');
            handleFileRootCheck(rootInput, function(){
                //set the root path in the cache to be reloaded next page-view
                var rootPath=rootInput.val();
                rootPath=helper.endPathWithSlash(rootPath);
                localStorage.setItem('glacier-output-root', rootPath);
                //for each output file
                var tokensWrap=jQuery('.token-wrap:first');
                tokensWrap.find('code[data-path]').each(function(){
                    var codeEl=jQuery(this);
                    var path=codeEl.attr('data-path');

                    var outFileName=outWrap.find('.out-file > .name[title="'+path+'"]:first');
                    var checkBox=outFileName.parent().find('input[type="checkbox"]:first');

                    if(checkBox.is(':checked')){

                        if(path.indexOf('/')===0){ path=path.substring(1); }
                        var content=getCodeElSaveContent(codeEl);
                        serverHelper.writeOutputFile(rootPath, path, content, function(res){
                            var outFileName=outWrap.find('.out-file > .name[title="'+res.path+'"]:first');
                            if(res.valid){
                                if(res.hasOwnProperty('file_summary')){
                                    outFileName.attr('data-result', res.file_summary);
                                }
                            }else{
                                var printConsoleError=true;
                                if(outFileName.length>0){
                                    if(res.hasOwnProperty('file_summary')){
                                        outFileName.attr('data-result', res.file_summary);
                                        printConsoleError=false;
                                    }else{
                                        outFileName.attr('data-result', 'error');
                                    }
                                }
                                if(printConsoleError){
                                    var msg=res.status;
                                    if(msg.indexOf(res.path)==-1){
                                      msg=res.path + ': '+msg;
                                    }
                                    console.error(msg);
                                }
                            }
                        });

                    }else{
                        outFileName.attr('data-result', 'deactivated');
                    }

                });
            });
        }
    };

    var root_check_timeout;
    var handleFileRootCheck=function(rootInput, callback){
        //clear result attributes
        var outWrap=rootInput.parents('.file-out-wrap:first');
        outWrap.find('[data-result]').removeAttr('data-result');
        //delay
        clearTimeout(root_check_timeout);
        root_check_timeout=setTimeout(function(){
            var rootPath=rootInput.val();
            if(rootPath.length>0){
                //auto-correct some path things
                var modifiedPath=false;
                if(rootPath.indexOf('/')!==0){
                    modifiedPath=true;
                    rootPath='/'+rootPath;
                }
                var lowerRootPath=rootPath.trim().toLowerCase();
                if(lowerRootPath.indexOf('/c/')===0){
                    modifiedPath=true;
                    rootPath=rootPath.substring('/c'.length);
                }else if(lowerRootPath=='/c'){
                    modifiedPath=true;
                    rootPath='/';
                }else if(lowerRootPath.indexOf('/c:/')===0){
                    modifiedPath=true;
                    rootPath=rootPath.substring('/c:'.length);
                }else if(lowerRootPath=='/c:'){
                    modifiedPath=true;
                    rootPath='/';
                }
                if(modifiedPath){
                    rootInput.val(rootPath);
                }
                //figure out if the corrected path exists
                serverHelper.rootDirExists(rootPath, function(res){
                    //the file root is valid
                    if(res.valid){
                        outWrap.addClass('valid-root');
                        if(callback!=undefined){
                            callback(rootInput);
                        }
                    }else{ //file root not valid
                        outWrap.removeClass('valid-root');
                    }
                })
            }
        },600);
    };

    var setFileOutputWrap=function(){
        if(!isPageMode('edit')){
            var bod=jQuery('body:first');
            var tokensWrap=bod.find('.token-wrap:first');
            tokensWrap.after('<div class="file-out-wrap"></div>');
            var fileOutWrap=tokensWrap.next('.file-out-wrap:first');
            var folderSvg=getDirectorySvg();
            fileOutWrap.append('<h2>'+folderSvg+'File Output</h2>');
            fileOutWrap.append('<div class="file-out-root"></div>');
            fileOutWrap.append('<div class="file-out-list"></div>');
            fileOutWrap.append('<div class="file-out-btns"></div>');
            var btnsWrap=fileOutWrap.children('.file-out-btns:last');
            btnsWrap.append('<p>File-writing disabled. No server connection.</p>');

            //if there are even any <code> elements with a write attribute
            var outCodeEls=tokensWrap.find('code[write]');
            if(outCodeEls.length>0){
                //if the server is up
                serverHelper.isServerUp(function(res){
                    if(res!=undefined){
                        //root path input
                        var rootWrap=fileOutWrap.children('.file-out-root:first');
                        rootWrap.append('<div class="root-path"></div>');
                        var rootPath=rootWrap.children('.root-path:first');
                        rootPath.append('<label>root:</label>');
                        rootPath.append('<input type="text" class="root-input" spellcheck="false" placeholder="?"  />');
                        rootPath.append('<span class="root-status"><span title="valid root" class="good">'+getThumbUpSvg()+'</span><span title="invalid root" class="bad">'+getThumbDownSvg()+'</span></span>');
                        var rootStatus=rootPath.children('.root-status:last');
                        rootStatus.click(function(e){
                            e.preventDefault(); e.stopPropagation();
                            jQuery(this).parent().children('.root-input:first').focus();
                        });
                        var rootInput=rootPath.children('.root-input:first');
                        rootInput.keydown(function(){
                            jQuery(this).parents('.file-out-wrap:first').removeClass('valid-root');
                        });
                        rootInput.keyup(function(){
                            handleFileRootCheck(jQuery(this));
                        });
                        rootInput.blur(function(){
                            handleFileRootCheck(jQuery(this));
                        });
                        //write button
                        btnsWrap.html('');
                        btnsWrap.append('<div class="write-out-btn"><span>Write Files</span></div>');
                        var writeBtn=btnsWrap.children('.write-out-btn:first');
                        writeBtn.click(function(e){
                            e.preventDefault(); e.stopPropagation();
                            handleFileWrite(jQuery(this));
                        });

                        //set a cached root
                        var cachedRoot=localStorage.getItem('glacier-output-root');
                        if(cachedRoot!=undefined){
                            rootInput.val(cachedRoot);
                        }

                        //check to see if the root is valid
                        handleFileRootCheck(rootInput);
                    }
                });
            }
        }
    };

    //set page title
    var setPageTitle=function(title){
        var theTitle = dataToString(title, 'title', '[My Page Title]');
        document.title=theTitle;
        var html='<h1 class="main-title">'+theTitle+'</h1>';
        if(isPageMode('edit')){
            var scopeFolder=pagesHelper.getPageScopeFolder();
            var fileId=pagesHelper.getPageId();
            var folderSvg=getDirectorySvg();
            var fileSvg=getHtmlFileSvg();

            html='<div class="save-as-wrap">'; //start save-as-wrap

            html+='<div class="folder-scope-wrap">'; //start folder-scope-wrap
            html+='<label>Scope'+folderSvg+'</label>';
            html+='<input readonly="readonly" spellcheck="false" type="text" data-edit="folder_scope_path" class="folder-scope edit-input" value="'+scopeFolder+'" />';
            html+='</div>'; //end folder-scope-wrap

            html+='<div class="file-id-wrap">'; //start file-id-wrap
            html+='<label>File'+fileSvg+'</label>';
            html+='<input readonly="readonly" spellcheck="false" type="text" data-edit="file_id" class="file-id edit-input" value="'+fileId+'" />';
            html+='</div>'; //end file-id-wrap

            html+='<div title="delete this page" class="delete-page-btn disabled"><span>'+getTrashSvg()+'delete page</span></div>';

            html+='</div>'; //end save-as-wrap

            html+='<textarea spellcheck="false" data-edit="title" class="main-title edit-textarea">'+theTitle+'</textarea>';
        }
        jQuery('body:first').prepend(html);
    };

    //set page outcome text
    var setOutcome=function(text){
        var isEditMode=isPageMode('edit');
        var theText = dataToString(text, 'outcome', 'Make what? Learn what? Document what? Do what? What is the result of using this page?');
        var moreDescription='';
        if(isEditMode){
            moreDescription=' (what will this page accomplish?)';
        }
        var html='<div class="outcome"><strong>Outcome'+moreDescription+':</strong> ';
        if(isEditMode){
            html+='<textarea spellcheck="false" class="edit-textarea" data-edit="outcome">'+theText+'</textarea>';
        }else{
            html+='<div>'+theText+'</div>';
        }
        html+='</div>'

        var afterEl=jQuery('.main-title:first');
        if(afterEl.length>0){
            afterEl.after(html);
        }else{
            jQuery('body:first').prepend(html);
        }
    };

    //set page search terms
    var setSearchTerms=function(data){
        var isEditMode=isPageMode('edit');
        //star the html
        var html='';
        var commaSeparated='';
        if(isEditMode){ commaSeparated=' for this walkthrough page (<em>comma separated</em>)'; }
        html+='<div class="search-terms"><p><strong>Search Terms'+commaSeparated+':</strong></p>';
        if(isEditMode){
            html+='<textarea spellcheck="false" data-edit="search" class="terms edit-textarea">';
        }else{
            html+='<p class="terms">';
        }
        //if there are any saved search terms
        if(data.hasOwnProperty('search')){
            var termsHtml='';
            for(var s=0;s<data.search.length;s++){
                var term=data.search[s];
                if(termsHtml.length<1){

                }else{
                    termsHtml+=", "
                }

                var startTerm='';
                var endTerm='';
                if(!isEditMode){
                    startTerm='<em>';
                    endTerm='</em>';
                }

                termsHtml+=startTerm+term+endTerm;
            }
            html+=termsHtml;
        }
        //end the html
        if(isEditMode){
            html+='</textarea>';
        }else{
            html+='</p>';
        }
        html+='</div>';
        //append the html
        var afterEl=jQuery('.outcome:first');
        if(afterEl.length<1){
            afterEl=jQuery('.main-title:first');
        }
        if(afterEl.length>0){
            afterEl.after(html);
        }else{
            jQuery('body:first').prepend(html);
        }
    };

    var getEditSectionLiHtml=function(sectionId, sectionTitle){
        var html='';
        html+='<li name="'+sectionId+'">';
        html+='<span class="section-btns">';
        html+='<a title="move" data-move="section" class="edit-btn move-section" href="#">&lt; &gt;</a>';
        html+='<input title="section id" spellcheck="false" type="text" data-edit="section_id" class="section-id edit-input" value="'+sectionId+'" />';
        html+='</span>';
        html+='<input title="section title" spellcheck="false" type="text" data-edit="section_title" class="section-title edit-input" value="'+sectionTitle+'" />';
        html+='<a href="#" title="delete section" data-delete="section" class="edit-btn delete-section-btn">X</a>';
        html+='</li>';
        return html;
    };

    var getEditSectionHtml=function(sectionTitle, sectionContent, sectionId){
        var html='';
        if(sectionId!=undefined){
            html+='<div id="'+sectionId+'" name="'+sectionId+'">';
        }
        html+='<input spellcheck="false" type="text" data-edit="section_title" class="section-title edit-input" value="'+sectionTitle+'" />';
        html+='<textarea data-edit="section_content" class="section-content edit-textarea">'+sectionContent+'</textarea>';
        if(sectionId!=undefined){
            html+='</div>';
        }
        return html;
    };

    //init sections
    var initSections=function(data){
        var isEditMode=isPageMode('edit');
        if(data.hasOwnProperty('sections')){
            var overviewLinks='';
            for(var s=0;s<data.sections.length;s++){
                var section=data.sections[s];
                if(section.hasOwnProperty('id')){
                    var sectionWrap=jQuery('#'+section.id+':first');
                    if(sectionWrap.length>0){
                        sectionWrap.attr('name', section.id);
                        if(section.hasOwnProperty('title')){
                            if(isEditMode){
                                //section content
                                var content=sectionWrap.html();
                                var sectionHtml=getEditSectionHtml(section.title, content);
                                sectionWrap.html(sectionHtml);

                                //sections list item
                                overviewLinks+=getEditSectionLiHtml(section.id, section.title);
                            }else{
                                sectionWrap.prepend('<h2 class="section-title">'+section.title+'</h2>');
                                overviewLinks+='<li><a href="#'+section.id+'">'+section.title+'</a></li>';
                            }
                        }
                    }
                }
            }
            var tokensWrap=jQuery('.token-wrap:first');
            var addBtnHtml='<a data-add="section" title="add section" class="edit-btn add-section" href="#">+ add section</a>';
            if(isEditMode){
                tokensWrap.append(addBtnHtml);
            }
            if(overviewLinks.length>0){
                var addSectionHtml='';
                if(isEditMode){
                    addSectionHtml=' ' + addBtnHtml;
                }
                overviewLinks='<div class="overview"><h2>Sections</h2><ul>'+overviewLinks+'</ul>'+addSectionHtml+'</div>';
                tokensWrap.prepend(overviewLinks);
            }
        }
    };

    //https://www.youtube.com/watch?v=qu577tNp1hA
    //https://www.youtube.com/watch?v=gPjqWwkAymg

    var initFieldWrap=function(){
        var html='<div class="fields"></div>';
        jQuery('.token-wrap:first').before(html);
    };

    var getCodeElemText=function(codeEl){
        var html=codeEl.html();
        html=html.trim();
        if(html.indexOf('<!--')===0){
            html=html.substring(html.indexOf('<!--')+'<!--'.length);
            if(html.lastIndexOf('-->')==html.length-'-->'.length){
                html=html.substring(0, html.lastIndexOf('-->'));
            }
        }
        while(html.indexOf('\n')===0){
            html=html.replace('\n', '');
        }
        return html;
    };

    //public
    return {
        getFileOutStoreKey:function() {
            //get the scope folder for this page
            var scopeFolder=pagesHelper.getPageScopeFolder();
            var pageId=pagesHelper.getPageId();
            //replace / with _
            scopeFolder=helper.replaceAll(scopeFolder, '/', '_');
            //assemble localstore key
            return 'fileout--' + escape(scopeFolder) + '--' + pageId;
        },
        getDirectorySvg:function(){
            return getDirectorySvg();
        },
        getHtmlFileSvg:function(){
            return getHtmlFileSvg();
        },
        //get the current page's id, "something" (from the url)
        getPageId:function(){
            var page=this.getPageName();
            if(page.indexOf('.html')!=-1){
                page=page.substring(0, page.lastIndexOf('.html'));
            }
            return page;
        },
        getRootContentDir:function(){
            return 'content'; //this should be the same root folder as listed in the server
        },
        //get the first page of the app from where you can open any page
        getHomeUrl:function(){
            return getHomeUrl();
        },
        //go to the home page of the application
        goHome:function(){
            goHome();
        },
        getContentPageUrl:function(folderScope, pageId, mode){
            return getContentPageUrl(folderScope, pageId, mode);
        },
        //go to a specific content page
        goToContentPage:function(folderScope, pageId, mode){
            goToContentPage(folderScope, pageId, mode);
        },
        //what type of walkthrough is this? Depends on the parent folder(s)
        getPageScopeFolder:function(){
            var scopeFolder='';
            var pageName=this.getPageName();
            var href=document.location.href;
            //strip off the ending page name
            scopeFolder=href.substring(0, href.lastIndexOf(pageName));
            //strip off the starting protocol, domain, and the root folder
            var rootDir='/'+this.getRootContentDir()+'/';
            if(scopeFolder.indexOf(rootDir)!=-1){
                scopeFolder=scopeFolder.substring(scopeFolder.indexOf(rootDir)+rootDir.length);
            }
            //make sure scope folder doesn't begin with /
            if(scopeFolder.indexOf('/')===0){
                scopeFolder=scopeFolder.substring('/'.length);
            }
            //make sure scope folder doesn't end with /
            if(scopeFolder.lastIndexOf('/')===scopeFolder.length-'/'.length){
                scopeFolder=scopeFolder.substring(0, scopeFolder.lastIndexOf('/'));
            }
            return scopeFolder;
        },
        //get the current page's name, "something.html" (from the url)
        getPageName:function(){
            var href=document.location.href;
            var page=href;
            if(href.indexOf('/')!=-1){
                page=href.substring(href.lastIndexOf('/')+'/'.length);
            }
            return page;
        },
        //replace the page data with different given data
        setPageData:function(data, pageId){
            var didSet=false;
            if(pageId==undefined){
                pageId=this.getPageId();
            }
            if(pages.hasOwnProperty(pageId)){
                pages[pageId]=data;
                didSet=true;
            }
        },
        //get a page's data from the ID in the URL or the parameter
        getPageData:function(pageId){
            var data;
            if(pageId==undefined){
                pageId=this.getPageId();
            }
            if(pages.hasOwnProperty(pageId)){
                data=pages[pageId];
                data['id']=pageId;
            }
            return data;
        },
        reloadPage:function(){
            reloadPage();
        },
        getUrlParams:function(){
            return getUrlParams();
        },
        isPageMode:function(mode){
            return isPageMode(mode);
        },
        getPageMode:function(){
            return getPageMode();
        },
        setPageMode:function(mode){
            return setPageMode(mode);
        },
        setPageId:function(newPageId){
            return setPageId(newPageId);
        },
        setUrlParams:function(insertParams){
            return setUrlParams(insertParams);
        },
        getEditSectionLiHtml:function(sectionId, sectionTitle){
            return getEditSectionLiHtml(sectionId, sectionTitle);
        },
        getEditSectionHtml:function(sectionTitle, sectionContent, sectionId){
            return getEditSectionHtml(sectionTitle, sectionContent, sectionId);
        },
        escapeInsideCode:function(){
            if(!isPageMode('edit')){
                var replaceThese = [
                    '&amp;','__^__',
                    '&','__^__',
                    '__^__','&amp;',
                    '<','&lt;',
                    '>','&gt;',
                    ' ','&nbsp;',
                    '\n','<br />'
                ];
                //for each code element
                jQuery('code').each(function(){
                    var html=getCodeElemText(jQuery(this));
                    //escape the <token> elements
                    var restoreTokens=[];
                    var tokenMatches=html.match(/<token data-key=.*<\/token>/g);
                    if(tokenMatches!=undefined){
                        for(var m=0;m<tokenMatches.length;m++){
                            var matchedLine=tokenMatches[m];
                            //split up the tokens if more than one <token> element was matched by the regex
                            var matches=matchedLine.split('</token>');
                            for(var s=0;s<matches.length;s++){
                                if(matches[s].length>0){
                                    if(matches[s].indexOf('<token data-key=')!=-1){
                                        var tokenMatch=matches[s]+'</token>';
                                        if(tokenMatch.indexOf('<token data-key=')!==0){
                                            tokenMatch=tokenMatch.substring(tokenMatch.indexOf('<token data-key='));
                                        }
                                        var tokenPh='[[TOKEN::'+m+':'+s+']]';
                                        html=html.replace(tokenMatch, tokenPh);
                                        restoreTokens.push({token:tokenMatch, ph:tokenPh});
                                    }
                                }
                            }
                        }
                    }
                    //replace inner html
                    for(var r=0;r<replaceThese.length;r+=2){
                        var re = new RegExp(replaceThese[r], 'g');
                        html=html.replace(re, replaceThese[r+1]);
                    }
                    //restore the <token> elements
                    for(p=0;p<restoreTokens.length;p++){
                        html=html.replace(restoreTokens[p]['ph'], restoreTokens[p]['token']);
                    }
                    //set the modified <code> inner html
                    jQuery(this).html(html);
                });
            }
        },
        //accurate reflect what paths will have file write output
        updateFileOutput:function(){
            var self=this;
            if(!isPageMode('edit')){
                var tokensWrap=jQuery('.token-wrap:first');
                var outCodeEls=tokensWrap.find('code[write]');
                var hasOutFiles=false;

                var fileOutWrap=jQuery('.file-out-wrap');
                var fileOutList=fileOutWrap.find('.file-out-list');
                var fileOutBtns=fileOutWrap.find('.file-out-btns');
                var fileOutRoot=fileOutWrap.find('.file-out-root');

                var outputPathObj={};

                if(outCodeEls.length>0){
                    //build the file path object
                    outCodeEls.each(function(){
                        var codeEl=jQuery(this);
                        var writeStr=codeEl.attr('write');
                        writeStr=writeStr.trim();
                        if(writeStr.length>0){
                            //switch single quotes to double quotes for JSON.parse
                            writeStr=writeStr.replace(/'/g,'"');
                            //if starts and ends with correct JSON syntax
                            if(writeStr.indexOf('{')===0 && writeStr.lastIndexOf('}')===writeStr.length-1){
                                var writeJson=JSON.parse(writeStr);
                                //if the write for this file is enabled
                                if(!writeJson.hasOwnProperty('enabled')){ writeJson['enabled']=true; }
                                if(writeJson.enabled){
                                    //if there is a path for this file
                                    if(writeJson.hasOwnProperty('path')){
                                        var path=writeJson.path;
                                        codeEl.attr('data-path', path);

                                        hasOutFiles=true;

                                        var pathArr=path.split('/');
                                        var currentObj=outputPathObj;

                                        for(var p=0;p<pathArr.length;p++){
                                            if(!currentObj.hasOwnProperty(pathArr[p])){
                                                if(p+1==pathArr.length){
                                                    currentObj['$'+pathArr[p]]=path;
                                                }else{
                                                    currentObj[pathArr[p]]={};
                                                }
                                            }
                                            currentObj=currentObj[pathArr[p]];
                                        }
                                    }
                                }
                            }
                        }
                    });
                }

                if(hasOutFiles){
                    fileOutList.html('');
                    fileOutBtns.css('display', '');
                    fileOutRoot.css('display', '');

                    var createOneFileLevel=function(obj, wrap){
                        Object.keys(obj).sort().forEach(function(key) {
                            var file=key;
                            var isFile=false;
                            var fileClass=' dir';
                            var titleAttr='';
                            var checkBox='';
                            if(file.indexOf('$')===0){
                                isFile=true;
                                file=file.substring(1);
                                fileClass=' file';
                                titleAttr=' title="'+obj[key]+'"';

                                var checkedAttr=' checked="checked"';
                                var storeKey=self.getFileOutStoreKey();
                                var fileoutCache=localStorage.getItem(storeKey);
                                if(fileoutCache!=undefined){
                                    var fileoutJson=JSON.parse(fileoutCache);
                                    if(fileoutJson.hasOwnProperty(obj[key])){
                                        if(!fileoutJson[obj[key]]){
                                            checkedAttr='';
                                        }
                                    }
                                }

                                checkBox='<input title="include in output" type="checkbox"'+checkedAttr+' />';
                            }
                            wrap.append('<div class="out-file'+fileClass+'">'+checkBox+'<span'+titleAttr+' class="name">'+file+'</span></div>');

                            if(!isFile){
                                //recursive next level
                                createOneFileLevel(obj[key], wrap.children('.out-file:last'));
                            }
                        });
                    };

                    createOneFileLevel(outputPathObj, fileOutList);

                    fileOutList.find('input[type="checkbox"]').not('.init').each(function(){
                        var checkEl=jQuery(this); checkEl.addClass('init');
                        checkEl.click(function(){
                            var storeKey=self.getFileOutStoreKey();
                            var isChecked=jQuery(this).is(':checked');
                            var checkPath=jQuery(this).parent().children('.name[title]:first').attr('title');
                            var fileoutCache=localStorage.getItem(storeKey);
                            if(fileoutCache==undefined){
                                if(!isChecked){
                                    localStorage.setItem(storeKey, '{"'+checkPath+'":'+isChecked+'}');
                                }
                            }else{
                                var cacheJson=JSON.parse(fileoutCache);

                                if(!isChecked){
                                    cacheJson[checkPath]=isChecked;
                                }else{
                                    if(cacheJson.hasOwnProperty(checkPath)){
                                        delete cacheJson[checkPath];
                                    }
                                }

                                localStorage.setItem(storeKey, JSON.stringify(cacheJson));
                            }
                        });
                    });

                }else{
                    fileOutList.html('<p>No file output.</p>');
                    fileOutBtns.css('display', 'none');
                    fileOutRoot.css('display', 'none');
                }
            }
        },
        //init a page
        init:function(){
            var self=this;
            var data=self.getPageData();

            setPageModeClass();

            //not home page
            setPageTokenWrap();
            setFileOutputWrap();
            setPageTitle(data);
            setOutcome(data);
            setSearchTerms(data);
            initSections(data);
            initFieldWrap();

            return data;
        }
    };
}());
