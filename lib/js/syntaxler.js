var syntaxlerHelper=(function(){
    // private

    var dataMesssagePrefix='<[msg]>::->';


    //get an arg value from a json and provide a default if the value doesn't exist
    var getArgVal=function(args, key, defaultVal){
        var ret=defaultVal;
        if(args!=undefined){
            if(args.hasOwnProperty(key)){
                ret=args[key];
            }
        }
        return ret;
    };

    //run a function on a separate worker thread to free up the main thread from hanging for large operations
    var runAsWorker=function(fn){
        return new Worker(URL.createObjectURL(new Blob(['('+fn+')()'])));
    };

    var getArg=function(argKey, txtElem){
        var args={};
        var val;
        if(txtElem[0].hasOwnProperty('syntaxler_args')){
            args=txtElem[0]['syntaxler_args'];
        }
        if(args.hasOwnProperty(argKey)){
            val=args[argKey];
        }
        return val;
    };

    //get the (number) of spaces per tab
    var hasLineNumbers=function(txtElem){
        var line_numbers=getArg('line_numbers', txtElem);
        if(line_numbers==undefined){
            line_numbers=true;
        }
        return line_numbers;
    };

    //get the (number) of spaces per tab
    var getSpacesPerTab=function(txtElem){
        var tabSpaces=getArg('tab_spaces', txtElem);
        if(tabSpaces==undefined){
            tabSpaces=3;
        }
        return tabSpaces;
    };

    //get the (string) tab
    var getSpacesForTab=function(txtElem){
        var tabSpaces=getSpacesPerTab(txtElem);
        var tab='';
        for(var t=0;t<tabSpaces;t++){
            tab+=' ';
        }
        return tab;
    }

    //should call when the size of the textarea changes
    var updateTextareaSize=function(txtElem){
        var wrap=txtElem.parent();
        var syntaxDiv=wrap.find('.syntax-div:first');
        var sizeDiv=wrap.find('.size-div:first');
        var height=syntaxDiv.children('.end-of-syntax-div:last').offset().top - syntaxDiv.offset().top;
        var lineHeight=parseFloat(txtElem.css('line-height'));
        height+=(lineHeight * 1); //add some blank space
        var width=wrap.outerWidth();
        sizeDiv.css({width:width+'px', height:height+'px'});
        //get the width for the textarea
        var lineWidth=getLongestLineWidth(txtElem);
        if(typeof lineWidth=='string'){ lineWidth=parseFloat(lineWidth); }
        var padLeft=parseFloat(txtElem.css('padding-left'))
        lineWidth+=padLeft+5;
        //set sizes
        txtElem.css({width:lineWidth+'px', height:height+'px'});
        syntaxDiv.css({width:width+'px', height:height+'px'});
        wrap.css({height:height+'px', "max-height":height+'px'});
    };

    var setArgsPropOnTxtElem=function(txtElem, args){
        if(!txtElem[0].hasOwnProperty('syntaxler_args')){
            txtElem[0]['syntaxler_args']={};
            for(var a in args){
                if(args.hasOwnProperty(a)){
                    if(a!='textareas'){
                        txtElem[0]['syntaxler_args'][a]=args[a];
                    }
                }
            }
        }
    };

    //init one textarea
    var initTextarea=function(txtElem, args){
        setArgsPropOnTxtElem(txtElem, args);

        //indicate this textarea has been init
        txtElem.addClass('init-syntaxler');
        txtElem.attr('spellcheck', false);
        //create wrap around textarea
        txtElem.before('<div class="syntaxler"></div>');
        var wrap=txtElem.prev('.syntaxler:first');
        wrap.append(txtElem);
        //set special classes for this init
        if(args.hasOwnProperty('classes')){
            for(var c=0;c<args['classes'].length;c++){
                wrap.addClass(args['classes'][c]);
            }
        }
        //create size div to simulate physical space of position:absolute floating syntax highlighter
        wrap.prepend('<div class="size-div"></div>');
        //create syntax div to contain the highlighted syntax
        wrap.prepend('<div class="syntax-div"></div>');
        //make sure the scroll for the textarea is at the top
        txtElem.scrollTop(0);
        //is read only?
        if(args.hasOwnProperty('readonly')){
            if(args['readonly']){
                txtElem.addClass('readonly');
            }
        }
        //line numbers?
        if(hasLineNumbers(txtElem)){
            wrap.addClass('has-line-numbers');
        }
        //events
        initTextareaEvents(txtElem);
        //refresh textarea view
        handleTextChange(txtElem);
        //scroll update
        handleScrollUpdate(txtElem);
    };

    var getLineEndOffset=function(lineEl){
        var eol=lineEl.children('.eol:last');
        if(eol.length<1){
            lineEl.append(getEndOfLineHtml());
            eol=lineEl.children('.eol:last');
        }
        var eolOffset=eol.offset().left + eol.outerWidth();
        return eolOffset;
    };

    var getLineWidth=function(lineEl){
        var eolOffset=getLineEndOffset(lineEl);
        var width=eolOffset - lineEl.offset().left;
        width=Math.round(width);
        return width;
    };

    var getLongestLineWidth=function(txtElem){
        var width=txtElem.attr('data-longest-line-width');
        if(width==undefined){
            var longestLine=getLongestLine(txtElem);
            width=getLineWidth(longestLine);
            txtElem.attr('data-longest-line-width', width);
        }
        return width;
    };

    var getLongestLine=function(txtElem){
        var wrap=txtElem.parent();
        var syntaxDiv=wrap.find('.syntax-div:first');
        var longestLine=syntaxDiv.children('.syn-line.longest');
        if(longestLine.length<1){
            var longestOffset=-1;
            syntaxDiv.children('.syn-line').each(function(){
                eolOffset=getLineEndOffset(jQuery(this));
                if(eolOffset>longestOffset){
                    longestOffset=eolOffset;
                   longestLine=jQuery(this);
                }
            });
            if(longestLine.length>0){
                syntaxDiv.children('.syn-line.longest').removeClass('longest');
                longestLine.addClass('longest');
            }
        }
        return longestLine;
    };

    var getMaxScrollLeft=function(txtElem){
        var overflowWidth=txtElem.attr('data-max-scroll-left');
        if(overflowWidth==undefined){
            var longestLineWidth=getLongestLineWidth(txtElem);
            overflowWidth=longestLineWidth - txtElem.outerWidth();
            overflowWidth=Math.round(overflowWidth);
            txtElem.attr('data-max-scroll-left', overflowWidth);
        }
        return overflowWidth;
    };

    var handleScrollUpdate=function(txtElem){
        var wrap=txtElem.parent();
        var syntaxDiv=wrap.find('.syntax-div:first');
        var scrollLeft=txtElem.scrollLeft();
        var newScrollLeft=scrollLeft;
        if(scrollLeft<=0){
            scrollLeft='';
        }else{
            var maxScrollLeft=getMaxScrollLeft(txtElem);
            var percent=scrollLeft / maxScrollLeft;
            newScrollLeft=scrollLeft + (maxScrollLeft * percent);

            newScrollLeft='-'+newScrollLeft+'px';
        }
        syntaxDiv.css({left:newScrollLeft});
    };

    //update things when the text changes
    var handleTextChange=function(txtElem, e){
        //function to wrap up the text change
        var finishHandleTextChange=function(){
            updateTextareaSize(txtElem);
            clearTextSizeCache(txtElem);
            handleScrollUpdate(txtElem);
        };
        //if there is already html in the syntax div
        var syntaxDiv=txtElem.parent().children('.syntax-div:first');
        if(syntaxDiv.html().length>0){
            if(partialUpdateSyntaxDiv(txtElem, e)){
                finishHandleTextChange();
                //when typing pauses, take the time to make sure the syntax highlight is updated
                clearTimeout(txtElem[0]['syntaxler_highlight_timeout']);
                txtElem[0]['syntaxler_highlight_timeout']=setTimeout(function(){
                    //async run the full syntax highlight update
                    fullUpdateSyntaxDiv(txtElem, e, function(){
                        finishHandleTextChange();
                        //suggest code
                        suggestSyntax(txtElem);
                    });
                },100);
            }
        }else{ //no html currently in the syntax div
            fullUpdateSyntaxDiv(txtElem, e, function(){
                finishHandleTextChange();
            });
        }
    };

    var clearTextSizeCache=function(txtElem){
        txtElem.removeAttr('data-max-scroll-left');
        txtElem.removeAttr('data-longest-line-width');
    };

    var handleTextResize=function(txtElem){
        clearTextSizeCache(txtElem);
        updateTextareaSize(txtElem);
        txtElem.scrollLeft(0);
    };

    var handleTabKeyPress=function(txtElem){
        var val=txtElem.val();
        //if there are any tab targets to select
        var nextTabTarget=getNextTabTarget(txtElem);
        if(nextTabTarget!=undefined){
            setTextSelectRange(txtElem, nextTabTarget.range.start, nextTabTarget.range.end);
        }else{ //no tab targets to select
            var txt=txtElem[0];

            //indexes of the selection range start==end when nothing is selected
            var start = txt.selectionStart;
            var end = txt.selectionEnd;

            //strings
            var tab=getSpacesForTab(txtElem);

            //handle tab when there is no selection
            var noSelection=function(){

                //get substrings before and after cursor
                var beforeCursor=val.substring(0, start);
                var afterCursor=val.substring(end);

                //insert the tab between the string before and after cursor
                txtElem.val(
                    beforeCursor
                    + tab
                    + afterCursor
                );

                //place the cursor right after the tab
                setTextSelectRange(jQuery(txt), (start + tab.length));
            };

            //handle tab when there is a selected range of text
            var hasSelection=function(){
                //indexes
                var firstNlBeforeSel=val.lastIndexOf('\n', start) + '\n'.length;
                var firstNlAfterSel=val.indexOf('\n', end);

                //get all of the complete lines that contain a full or partial selection
                var linesThatHaveSelection=val.substring(firstNlBeforeSel, firstNlAfterSel);
                //for each line, insert an additional tab before the line
                var insertedTabStr='';
                var lineArr=linesThatHaveSelection.split('\n');
                for(var a=0;a<lineArr.length;a++){
                    if(a!=0){ insertedTabStr+='\n'; }
                    insertedTabStr+=tab+lineArr[a];
                }
                //get the strings before and after modified blob
                var beforeLinesStr=val.substring(0, firstNlBeforeSel);
                var afterLinesStr=val.substring(firstNlAfterSel);
                //insert the insertedTabStr between the string before and after these lines
                txtElem.val(
                    beforeLinesStr
                    + insertedTabStr
                    + afterLinesStr
                );
                //set selection range to have insertedTabStr selected
                txt.setSelectionRange(firstNlBeforeSel, firstNlBeforeSel+insertedTabStr.length);
            };

            //if nothing is selected
            var inputType='';
            if(start==end){
                noSelection(); inputType='forwardNoSelect';
            }else{ //there is a selected range
                hasSelection(); inputType='forwardWithSelect';
            }

            txtElem[0].dispatchEvent(new InputEvent('input', {
              bubbles:true,
              data:dataMesssagePrefix+'[tab]'+inputType+';[inputType]insertText'
            }));
        }
    };

    //set the selection range of a textarea
    var setTextSelectRange=function(txtElem, selStart, selEnd){
        if(selEnd==undefined){ selEnd=selStart; }
        txtElem[0].selectionStart=selStart;
        txtElem[0].selectionEnd=selEnd;
        //for safari
        if(txtElem[0].setSelectionRange!=undefined){
            txtElem[0].setSelectionRange(selStart, selEnd);
        }
    };

    var handleShiftTabKeyPress=function(txtElem){
        var txt=txtElem[0];

        //indexes of the selection range start==end when nothing is selected
        var start = txt.selectionStart;
        var end = txt.selectionEnd;

        //strings
        var tab=getSpacesForTab(txtElem);
        var val=txtElem.val();

        //get the indexes of where the line(s) begin(s) and ends
        var firstNlBeforeSel=val.lastIndexOf('\n', start)+'\n'.length;
        var firstNlAfterSel=val.indexOf('\n', end);

        //get the strings before and after the blob to modify
        var beforeLinesStr=val.substring(0, firstNlBeforeSel);
        var afterLinesStr=val.substring(firstNlAfterSel);

        //get all of the complete lines that contain a full or partial selection
        var linesThatHaveSelection=val.substring(firstNlBeforeSel, firstNlAfterSel);

        var didRemoveTab='0';

        //handle tab when there is no selection
        var noSelection=function(){
            if(linesThatHaveSelection.indexOf(tab)===0){
                //remove one tab from the front of the string
                var newLine=linesThatHaveSelection.substring(tab.length);
                //insert the newLine between the string before and after this line
                txtElem.val(
                    beforeLinesStr
                    + newLine //shortened minus 1 tab
                    + afterLinesStr
                );
                //adjust the cursor back the length of the deleted tab
                setTextSelectRange(jQuery(txt), (start - tab.length));

                didRemoveTab='1';
            }
        };

        //handle tab when there is a selected range of text
        var hasSelection=function(){
            //for each line, insert an additional tab before the line
            var reducedTabStr='';
            var lineArr=linesThatHaveSelection.split('\n');
            for(var a=0;a<lineArr.length;a++){
                if(a!=0){ reducedTabStr+='\n'; }
                //remove a tab from the start of this line, if it begins with a tab
                if(lineArr[a].indexOf(tab)===0){
                    reducedTabStr+=lineArr[a].substring(tab.length);
                    didRemoveTab='1';
                }else{
                    reducedTabStr+=lineArr[a]; //otherwise, don't do anything to this line
                }
            }
            //insert the reducedTabStr between the string before and after these lines
            txtElem.val(
                beforeLinesStr
                + reducedTabStr
                + afterLinesStr
            );
            //set selection range to have reducedTabStr selected
            txt.setSelectionRange(firstNlBeforeSel, firstNlBeforeSel+reducedTabStr.length);
        };

        var evtInputType='';

        //if nothing is selected
        if(start==end){
            noSelection(); evtInputType='backwardNoSelect';
        }else{ //there is a selected range
            hasSelection(); evtInputType='backwardWithSelect';
        }

        txtElem[0].dispatchEvent(new InputEvent('input',{
          bubbles:true,
          data:dataMesssagePrefix+'[tab]'+evtInputType+';[remove_tab]'+didRemoveTab+';[inputType]insertText'
        }));
    };

    //redo
    var handleCtlYPress=function(txtElem){
        //TODO redo
    };

    //undo
    var handleCtlZPress=function(txtElem){
        //TODO undo
    };

    var handleBackspacePress=function(e, txtElem){
        var txt=txtElem[0];

        //indexes of the selection range start==end when nothing is selected
        var start = txt.selectionStart;
        var end = txt.selectionEnd;

        //if nothing is selected
        if(start==end){

            //strings
            var tab=getSpacesForTab(txtElem);
            var val=txtElem.val();

            //if there is a tab immediately to the left of the cursor
            if(val.lastIndexOf(tab, start)!=-1){
                var endTabBeforeSel=val.lastIndexOf(tab, start)+tab.length;
                if(endTabBeforeSel==start){

                    e.preventDefault(); //stop the single backspace

                    //get the strings before and after the tab
                    var beforeTabStr=val.substring(0, endTabBeforeSel - tab.length);
                    var afterTabStr=val.substring(endTabBeforeSel);

                    //remove the remainder of the tab that the backspace didn't do
                    txtElem.val(
                        beforeTabStr
                        + afterTabStr
                    );

                    //set cursor position
                    setTextSelectRange(jQuery(txt), (start - tab.length));

                    txtElem[0].dispatchEvent(new InputEvent('input', {
                      bubbles:true,
                      data:dataMesssagePrefix+'[inputType]deleteContentBackward'
                    }));
                }
            }
        }
    };

    var handleSpecialKeyPress=function(txtElem, e){
        var specialKeyPressed=false;
        var keyCode=e.keyCode || e.which;
        switch(keyCode){
            case 9: //tab key press
                e.preventDefault();
                specialKeyPressed=true;
                //if a float menu is open
                var floatMenu=getFirstOpenFloatMenu();
                if(floatMenu.length>0){
                    clickSelectedFloatMenuItem(floatMenu);
                }else{
                    if(e.shiftKey){
                        handleShiftTabKeyPress(txtElem);
                    }else{
                        handleTabKeyPress(txtElem);
                    }
                }
                break;
            case 8: //backspace key press
                specialKeyPressed=true;
                if(!txtElem.hasClass('no-hold-backspace')){
                    handleBackspacePress(e, txtElem);
                    txtElem.addClass('no-hold-backspace');
                }else{
                    e.preventDefault(); e.stopPropagation();
                }
                break;
            case 90: //Z key press
                if(e.ctrlKey){
                    e.preventDefault();
                    handleCtlZPress(txtElem);
                }
                break;
            case 89: //Y key press
                if(e.ctrlKey){
                    e.preventDefault();
                    handleCtlYPress(txtElem);
                }
                break;
        }
        return specialKeyPressed;
    };

    //GENERIC

    //generic get cache
    var getCache=function(txtElem, key, defaultVal){
        var ret=defaultVal;
        if(txtElem[0].hasOwnProperty('syntaxler_cache')){
            if(txtElem[0]['syntaxler_cache'].hasOwnProperty(key)){
                ret=txtElem[0]['syntaxler_cache'][key];
            }
        }
        return ret;
    };

    //generic set cache
    var setCache=function(txtElem, key, getFunc){
        //set cache
        if(!txtElem[0].hasOwnProperty('syntaxler_cache')){
            txtElem[0]['syntaxler_cache']={};
        }
        txtElem[0]['syntaxler_cache'][key]=getFunc(txtElem);
        return txtElem[0]['syntaxler_cache'];
    };

    //RANGE

    //get the current select range
    var getSelectRange=function(txtElem, addLines){
        if(addLines==undefined){ addLines=true; }
        //get full value
        var val=txtElem.val();
        var ret={val:val};
        //get syntax
        var syntaxDiv=txtElem.parent().children('.syntax-div:first');
        ret['syntax']=syntaxDiv.html();
        //get start and end
        var sStart=txtElem[0].selectionStart, sEnd=txtElem[0].selectionEnd;
        ret['ch']={start:sStart, end:sEnd};
        if(sStart==sEnd){
            ret['selected']='';
        }else{
            val=val.substring(sStart, sEnd);
            ret['selected']=val;
        }
        //add line and position data
        if(addLines){ ret=addLinesToRange(ret, ret['val']); }
        return ret;
    };

    var hasSelectedText=function(txtElem, range){
        if(range==undefined){ range=getSelectRange(txtElem, false); }
        return (range.selected.length>0);
    };

    //add the line numbers to the range data
    var addLinesToRange=function(range, val){
        if(!range.hasOwnProperty('line')){
            //get the start line
            var beforeStart=val.substring(0, range.ch.start);
            var beforeStartLines=beforeStart.split('\n');
            var startLine=beforeStartLines.length;
            //get the start position
            range['pos']={
                start:{
                    line:startLine,
                    ch:beforeStartLines[beforeStartLines.length-1].length
                }
            };
            //get the end line
            var endLine=startLine;
            range['line']={start:startLine};
            if(range.ch.start!=range.ch.end){
                //split the selected text into an array of lines
                var selectedLines=range.selected.split('\n');
                //add the selected lines to the end line number
                endLine+=selectedLines.length-1;
                range['line']['end']=endLine;
                //get the last selected line text
                var lastSelectedLine=range.selected;
                if(selectedLines.length>1){
                    lastSelectedLine=selectedLines[selectedLines.length-1];
                }
                //get the character index for the end of the last selected line
                var lastLineChar=lastSelectedLine.length;
                if(selectedLines.length==1){
                    lastLineChar+=range['pos']['start']['ch'];
                }
                //get the end position
                range['pos']['end']={
                    line:endLine,
                    ch:lastLineChar
                };
            }
        } return range;
    };

    //get the previous cached selection range
    var getCacheRange=function(txtElem){ return getCache(txtElem, 'range', undefined); };

    //set the selection range
    var setCacheRange=function(txtElem){
        var cache=setCache(txtElem, 'range', getSelectRange);
        return cache;
    };

    //GET CHANGED TEXT AFTER INPUT

    //get one or more line <pre> elements depending on the given position
    var getSyntaxLines=function(txtElem, pos){
        var lineSpans;
        var syntaxDiv=txtElem.parent().children('.syntax-div:first');
        var lineRange=1;
        if(pos.hasOwnProperty('end')){
            lineRange=pos.end.line - pos.start.line + 1;
            for(var r=0;r<lineRange;r++){
                syntaxDiv.children('.syn-line').eq((pos.start.line-1)+r).addClass('select-line');
            }
            lineSpans=syntaxDiv.children('.syn-line.select-line').removeClass('select-line');
        }else{
            lineSpans=syntaxDiv.children('.syn-line').eq(pos.start.line-1);
        }
        return lineSpans;
    };

    var getReplaceChars=function(str){
        if(str.length>0){
            str=str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }
        return str;
    };

    //escape regex
    var escReg=function(st){
        return st.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    };

    //this is where all the magic happens for syntax highlighting
    var getHighlightChars=function(highlights, str, data, callback){
        var placeholders=[], lvlIndexes={};
        //is array function
        var isArray=function(obj){
            var isArr = false;
            if(Array.isArray!=undefined){
                isArr=Array.isArray(obj);
            }else{
                isArr=(obj && typeof obj === 'object' && obj.constructor === Array);
            }
            return isArr;
        };
        //loop through highlights with a callback
        var nextHighlight=function(hls, st, recLevel, h, finishCallback){
            if(recLevel==undefined){ recLevel=0; }
            if(h==undefined){ h=0; }
            if(h<hls.length){
                getStrForHighlight(hls[h], st, recLevel, function(s){
                    //recursive move to the next highlight
                    nextHighlight(hls, s, recLevel, h+1, finishCallback);
                });
            }else{ //finish up
                if(finishCallback!=undefined){
                    finishCallback(st);
                }
            }
        };
        //function to process a highlight
        var getStrForHighlight=function(hl, theStr, recLevel, hlCallback){
            //function executed after hl is either executed as a function or isn't a function
            var afterFunction=function(highlight){
                //if there is a match regex for this highlight
                if(highlight.hasOwnProperty('match')){
                    //init some values
                    var usePlaceholder=getArgVal(highlight, 'placeholders', false);
                    var regFlags=getArgVal(highlight, 'reg_flags');
                    var matchIndex=getArgVal(highlight, 'index');
                    var wrapRedraw=getArgVal(highlight, 'redraw', false);
                    var consumeDirs=getArgVal(highlight, 'consume', []);
                    var debug=getArgVal(highlight, 'debug', false);
                    if(!lvlIndexes.hasOwnProperty(recLevel)){
                        lvlIndexes[recLevel]=0;
                    }
                    //classes
                    var classesArray=[]; var classesAttr=''; var classesList='';
                    if(wrapRedraw){ classesArray.push('redraw'); }
                    if(consumeDirs.indexOf('right')!=-1){ classesArray.push('consume-right'); }
                    if(consumeDirs.indexOf('left')!=-1){ classesArray.push('consume-left'); }
                    //put classes together
                    for(var c=0;c<classesArray.length;c++){
                        if(c==0){ classesAttr+=' class="'; }
                        else{ classesAttr+=' '; }
                        classesList+=' ';
                        classesAttr+=classesArray[c];
                        classesList+=classesArray[c];
                        if(c+1==classesArray.length){ classesAttr+='"'; }
                    }
                    //should get multiple matches to select an index from, if using an index
                    if(matchIndex!=undefined){
                        if(matchIndex!=0){
                            //make sure this regex is global, (if this is a regex string), if selecting the match by its index
                            if(regFlags==undefined){ regFlags='g'; }
                            else if(regFlags.indexOf('g')==-1){ regFlags+='g'; }
                        }
                    }
                    var wrap=undefined;
                    //get the matches specified by the regex, regex-string, or function
                    var matches=undefined;
                    if(debug){ debugger; } //you can set a debugger flag to drop breakpoint here
                    switch(typeof highlight.match){
                        case 'string':
                            var regStr=highlight.match;
                            var reg=new RegExp(escReg(regStr), regFlags);
                            matches=theStr.match(reg);
                            break;
                        default:
                            if(matchIndex!=undefined){
                                if(matchIndex!=0){
                                    if(highlight.match.flags.indexOf('g')==-1){
                                        //make sure this regex is global, if selecting the match by its index
                                        highlight.match=new RegExp(highlight.match, 'g'+highlight.match.flags);
                                    }
                                }
                            }
                            matches=theStr.match(highlight.match);
                            break;
                    }
                    //if matches is an array (not undefined)
                    if(matches!=undefined && matches.length > 0){
                        //get an array of unique matches
                        var uniqueMatches=[]; var getMatchIndex=undefined;
                        //if selecting a match at a specific index
                        if(matchIndex!=undefined){
                            //if selecting, counting backwards from the end (negative index)
                            if(matchIndex<0){
                                getMatchIndex=matches.length+matchIndex;
                            }else{ //selecting with positive index
                                getMatchIndex=matchIndex;
                            }
                            //if the index is within range
                            if(getMatchIndex>-1 && getMatchIndex<matches.length){
                                uniqueMatches.push(matches[getMatchIndex]);
                            }
                        }else{ //selecting all matches (not just at a specific index)
                            for(var m=0;m<matches.length;m++){
                                if(uniqueMatches.indexOf(matches[m])==-1){
                                    uniqueMatches.push(matches[m]);
                                }
                            }
                        }
                        if(uniqueMatches.length>0){
                            //function to count the number of starts and ends
                            var setStartsEnds=function(){
                                var startsEndsAttr='';
                                if(wrap.indexOf('start_')===0 || wrap.indexOf('end_')===0){
                                    var startOrEnd='end';
                                    if(wrap.indexOf('start_')===0){ startOrEnd='start'; }
                                    var wrapType=wrap.substring(wrap.indexOf('_')+1);
                                    startsEndsAttr=' data-'+startOrEnd+'="'+wrapType+'"';
                                } return startsEndsAttr;
                            };
                            //get wrap
                            var wrap='', startWrap='', endWrap='', startEndAttr='';
                            if(highlight.hasOwnProperty('wrap')){
                                wrap=highlight.wrap;
                                startEndAttr=setStartsEnds();
                                //create start and end wrap markup
                                startWrap='<'+wrap+classesAttr+startEndAttr+'>';
                                endWrap='</'+wrap+'>';
                            }
                            //get recursive
                            var recursive=undefined;
                            if(highlight.hasOwnProperty('recursive')){ recursive=highlight.recursive; }
                            //for each unique matched
                            for(var m=0;m<uniqueMatches.length;m++){
                                var match=uniqueMatches[m];
                                //get the regex to replace, if not only replacing one match (at index)
                                var regx=undefined;
                                if(getMatchIndex==undefined){
                                    var escMatch=escReg(match);
                                    regx=new RegExp(escMatch, 'g');
                                }
                                //if there is a recursive matches to do on this matched string
                                if(recursive!=undefined){
                                    //for each recursive highlight
                                    for(var r=0;r<recursive.length;r++){
                                        //get the string
                                        match=getStrForHighlight(recursive[r], match, (recLevel+1));
                                    }
                                }
                                //ph postfix
                                var uniquePh=';;;;'+recLevel+';;;;'+lvlIndexes[recLevel]+';;;;';
                                lvlIndexes[recLevel]+=1;
                                //next, figure out replaceWith
                                var replaceWith=undefined;
                                //if the match contains any newlines
                                if(match.indexOf('\n')!=-1){
                                    var multiLineMatch='';
                                    //if there is anything with which to wrap each line
                                    if(wrap.length>0){
                                        var splitLines=match.split('\n');
                                        //surround each line in the match with the startWrap and endWrap
                                        for(var s=0;s<splitLines.length;s++){
                                            if(s!=0){ multiLineMatch+='\n'; }
                                            //do not use this start end block if the contents are blank
                                            var startEnd=startEndAttr;
                                            if(startEnd.length>0){
                                                if(splitLines[s].trim().length<1){
                                                    startEnd='';
                                                }
                                            }
                                            //figure out the class for the wrap
                                            if(s==0){
                                                startWrap='<'+wrap+' class="multi-line first-ml'+classesList+'"'+startEnd+'>'; }
                                            else if(s+1==splitLines.length){
                                                startWrap='<'+wrap+' class="multi-line last-ml'+classesList+'"'+startEnd+'>'; }
                                            else{
                                                startWrap='<'+wrap+' class="multi-line inner-ml'+classesList+'"'+startEnd+'>';
                                            }
                                            //add to the multi-line match string
                                            multiLineMatch+=startWrap+splitLines[s]+endWrap;
                                        }
                                    }else{ //no need to wrap lines with anything
                                        multiLineMatch=match;
                                    }
                                    //if using a temporary placeholder to remove this sub-string from the full string
                                    replaceWith=multiLineMatch;
                                    if(usePlaceholder){
                                        replaceWith=uniquePh;
                                        placeholders.push({ph:replaceWith, value:multiLineMatch});
                                    }
                                }else{ //the match doesn't contain newlines
                                    //if using a temporary placeholder to remove this sub-string from the full string
                                    replaceWith=startWrap+match+endWrap;
                                    if(usePlaceholder){
                                        replaceWith=uniquePh;
                                        placeholders.push({ph:replaceWith, value:startWrap+match+endWrap});
                                    }
                                }
                                //replace using the regex
                                if(getMatchIndex==undefined){
                                    theStr=theStr.replace(regx, replaceWith);
                                }else{ //replace the nth occurance of the match
                                    switch(getMatchIndex){
                                        case 0: theStr=theStr.replace(match, replaceWith); break;
                                        default:
                                            theStr='_ '+theStr+' _';
                                            var strArr=theStr.split(match);
                                            var newStr='';
                                            for(var s=0;s<strArr.length;s++){
                                                newStr+=strArr[s];
                                                if(s==getMatchIndex){
                                                    newStr+=replaceWith;
                                                }else if(s+1<strArr.length){
                                                    newStr+=match;
                                                }
                                            }
                                            newStr=newStr.substring('_ '.length);
                                            newStr=newStr.substring(0, newStr.length-' _'.length);
                                            theStr=newStr;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }else if(isArray(highlight)){
                    for(var a=0;a<highlight.length;a++){
                        theStr=getStrForHighlight(highlight[a], theStr, recLevel);
                    }
                }
                //callback
                if(hlCallback!=undefined){
                    hlCallback(theStr);
                }
            };
            //get the highlight as dynamic if typeof is not a json object
            switch(typeof hl){
                case 'string':
                    hl={match:hl, reg_flags:'g', wrap:'highlight'};
                    afterFunction(hl);
                    break;
                case 'function':
                    hl=hl(theStr, data);
                    afterFunction(hl);
                    break;
                default:
                    //if this is a function call sent to the worker
                    if(hl.hasOwnProperty('call')){
                        hl=this[hl['call']](theStr, data);
                        afterFunction(hl);
                    }else{ //probably just a normal json object with match, wrap, etc...
                        afterFunction(hl);
                    }
                    break;
            }
            return theStr;
        };
        //start processing highlights
        nextHighlight(highlights, str, 0, 0, function(st){
            //for each placeholder to restore
            for(var p=placeholders.length-1;p>-1;p--){
                //get the regex to replace
                var re=new RegExp(escReg(placeholders[p]['ph']), 'g');
                //replace this placeholder
                st=st.replace(re, placeholders[p]['value']);
            }
            //callback
            if(callback!=undefined){
                callback(st); //this is the end of this function
            }
        });
    };

    //function to convert function to string so that it can be passed to the worker
    var getFuncAsString=function(func){
        var funcStr=func.toString();
        //get function args
        var argsStr=funcStr;
        var startBrackIndex=funcStr.indexOf('{');
        argsStr=argsStr.substring(0, startBrackIndex);
        var startParenIndex=argsStr.indexOf('(');
        if(startParenIndex!=-1){ argsStr=argsStr.substring(startParenIndex+1); }
        var endParenIndex=argsStr.indexOf(')');
        if(endParenIndex!=-1){ argsStr=argsStr.substring(0, endParenIndex); }
        argsStr=argsStr.trim();
        //get function body
        if(startBrackIndex!=-1){ funcStr=funcStr.substring(startBrackIndex+1); }
        var endBrackIndex=funcStr.lastIndexOf('}');
        if(endBrackIndex!=-1){ funcStr=funcStr.substring(0, endBrackIndex); }
        return argsStr+'>>'+funcStr.trim();
    };

    //this function uses a web worker to run on a separate thread, responsible for updating the syntax highlights
    var insertHtmlLines=function(txtElem, str, callback){
        //create the worker if it doesn't already exist
        if(!txtElem[0].hasOwnProperty('syntaxler_worker_threads')){ txtElem[0]['syntaxler_worker_threads']={}; }
        if(!txtElem[0]['syntaxler_worker_threads'].hasOwnProperty('highlights')){
            //define the worker thread to highlight syntax
            txtElem[0]['syntaxler_worker_threads']['highlights']=runAsWorker(function(){
                onmessage=function(w) {
                    if(w.data.hasOwnProperty('funcs')){
                        //for each function
                        for(var f in w.data.funcs){
                            if(w.data.funcs.hasOwnProperty(f)){
                                //if this function isn't already a property of "this"
                                if(!this.hasOwnProperty(f)){
                                    //convert the function string into a real function
                                    var funcStr=w.data.funcs[f];
                                    if(funcStr.indexOf('>>')!=-1){
                                        var args=funcStr.substring(0, funcStr.indexOf('>>')); args=args.trim();
                                        var funcStr=funcStr.substring(funcStr.indexOf('>>')+2);
                                        //convert the helper to a string function
                                        if(args.length>0){
                                            this[f]=new Function(args, funcStr);
                                        }else{
                                            this[f]=new Function(funcStr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(w.data.hasOwnProperty('str')){
                        //replace < and > with escaped chars
                        str=this.getReplaceChars(w.data.str);
                        //run after highlights finish
                        var afterHighlights=function(st){
                            var html='';
                            //create the <pre> lines
                            var lines=st.split('\n');
                            for(var n=0;n<lines.length;n++){
                                html+=getLineHtml(lines[n]);
                            }
                            //div that signifies the end of the content
                            html+='<div class="end-of-syntax-div"></div>';
                            //return value from this separate thread
                            postMessage(html);
                        };
                        //handle the hightlights
                        if(w.data.highlights.length>0){
                            //generate the syntax highlights
                            this.getHighlightChars(w.data.highlights, str, w.data.data, function(st){
                                afterHighlights(st);
                            });
                        }else{
                            afterHighlights(str);
                        }
                    }
                }
            });
            //convert the needed functions (inside the helper thread) to a string
            var funcs={};
            funcs['getArgVal']=getFuncAsString(getArgVal);
            funcs['getReplaceChars']=getFuncAsString(getReplaceChars);
            funcs['getHighlightChars']=getFuncAsString(getHighlightChars);
            funcs['escReg']=getFuncAsString(escReg);
            funcs['getLineHtml']=getFuncAsString(getLineHtml);
            funcs['getEndOfLineHtml']=getFuncAsString(getEndOfLineHtml);
            funcs['getSyntaxLineHtml']=getFuncAsString(getSyntaxLineHtml);
            funcs['getSyntaxLineHtmlWithoutEol']=getFuncAsString(getSyntaxLineHtmlWithoutEol);
            //send task to worker
            txtElem[0]['syntaxler_worker_threads']['highlights'].postMessage({
                funcs:funcs
            });
        }
        //get data from worker
        txtElem[0]['syntaxler_worker_threads']['highlights'].onmessage=function(w){
            str=w.data;
            if(callback!=undefined){
                callback(str); //this is the end of insertHtmlLines()
            }
        };
        //prepare highlights to send to the worker
        var hls=[];
        if(txtElem[0]['syntaxler_args'].hasOwnProperty('highlights')){
            hls=txtElem[0]['syntaxler_args']['highlights'];
        }
        //prepare functions to send to the worker
        var funcs={};
        if(txtElem[0]['syntaxler_args'].hasOwnProperty('highlight_functions')){
            var hlFuncs=txtElem[0]['syntaxler_args']['highlight_functions'];
            for(var f in hlFuncs){
                if(hlFuncs.hasOwnProperty(f)){
                    if(hlFuncs[f]!=undefined){
                        if(typeof hlFuncs[f]=='function'){
                            funcs[f]=getFuncAsString(hlFuncs[f]);
                        }
                    }
                }
            }
            //prevent having to send functions to the worker again
            delete txtElem[0]['syntaxler_args']['highlight_functions'];
        }
        //prepare data to send to the worker
        var data={};
        if(txtElem[0]['syntaxler_args'].hasOwnProperty('highlight_data')){
            var hlData=txtElem[0]['syntaxler_args']['highlight_data'];
            for(var d in hlData){
                if(hlData.hasOwnProperty(d)){
                    if(hlData[d]!=undefined){
                        if(typeof hlData[d]=='function'){
                            data[d]=hlData[d]();
                        }
                    }
                }
            }
        }
        //send task to worker
        txtElem[0]['syntaxler_worker_threads']['highlights'].postMessage({
            str:str,
            highlights:hls,
            funcs:funcs,
            data:data
        });
    };

    //show a popup with code suggestings, depending on the current input
    var suggestSyntax=function(txtElem){
        //if there is any suggest data
        var suggestData=getArg('suggest', txtElem);
        if(suggestData!=undefined){
            clearTimeout(txtElem[0]['syntaxler_suggest_timeout']);
            txtElem[0]['syntaxler_suggest_timeout']=setTimeout(function(){
                var range=getSelectRange(txtElem);
                //if there is no selection
                if(range.selected.length<1){
                    //figures out if an option should be suggested based on the match rules
                    var canSuggestOption=function(suggestOption){
                        var ret=undefined;
                        if(suggestOption.hasOwnProperty('label') && suggestOption.label.length>0){
                            //get the textarea text, before and after the cursor
                            var beforeStr=range.val.substring(0, range.ch.start);
                            var afterStr=range.val.substring(beforeStr.length);
                            //get the match rules from the option item
                            var minCharsLeft=getArgVal(suggestOption, 'min_chars_left', 2);
                            var matchLeft=getArgVal(suggestOption, 'match_left', []);
                            if(matchLeft.indexOf(suggestOption.label)==-1){
                                matchLeft.push(suggestOption.label);
                            }
                            //make sure the matchLeft array is in order from longest to shortest
                            if(matchLeft.length>1){
                                matchLeft.sort(function(a,b){ return b.length-a.length; });
                            }
                            //see if any matches are found
                            for(var ml=0;ml<matchLeft.length;ml++){
                                if(matchLeft[ml].length>=minCharsLeft){
                                    //get the minimum correct chars to match
                                    var minMatch=matchLeft[ml].substring(0, minCharsLeft);
                                    //if the minimum number of chars actually exists in beforeStr
                                    var lastIndexOfMin=beforeStr.lastIndexOf(minMatch);
                                    if(lastIndexOfMin!=-1){
                                        //if the word to match isn't too far to the left
                                        var matchBehind=beforeStr.substring(lastIndexOfMin);
                                        if(matchBehind.length<=matchLeft[ml].length){
                                            //if the partial word is matched to the left
                                            if(matchLeft[ml].indexOf(matchBehind)===0 && matchLeft[ml].length>matchBehind.length){
                                                ret={replace_left:matchBehind, match:matchLeft[ml]};
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return ret;
                    };
                    //build up the suggestion options
                    var menuId='suggest-menu';
                    var suggestThese={
                        id:menuId,
                        position:'text-cursor',
                        close_on_click:true,
                        layout:{rows:5, columns:2},
                        options:[]
                    };
                    //for each suggest group
                    var doBreak=false;
                    for(var g=0;g<suggestData.length;g++){
                        var suggestGroup=suggestData[g];
                        //if this suggest group has any options
                        if(suggestGroup.hasOwnProperty('options')){
                            //default group values
                            var defaultGroupSvg=getArgVal(suggestGroup, 'svg', false);
                            var defaultGroupCharsLeft=getArgVal(suggestGroup, 'min_chars_left', 2);
                            doBreak=getArgVal(suggestGroup, 'break', false);
                            //for each option in this group
                            for(var p=0;p<suggestGroup.options.length;p++){
                                var suggestOption=suggestGroup.options[p];
                                //set group defaults
                                suggestOption['min_chars_left']=getArgVal(suggestOption, 'min_chars_left', defaultGroupCharsLeft);
                                suggestOption['break']=getArgVal(suggestOption, 'break', doBreak);
                                //figure out if there is a found match for this suggestion
                                var suggestHit=canSuggestOption(suggestOption);
                                if(suggestHit!=undefined){
                                    //break searching for more suggestions after this?
                                    doBreak=getArgVal(suggestOption, 'break', false);
                                    var writeStr=getArgVal(suggestOption, 'write', suggestOption.label);
                                    var svg=getArgVal(suggestOption, 'svg', defaultGroupSvg);
                                    var key=getArgVal(suggestOption, 'key', suggestOption.label);
                                    //create the data for this suggestion
                                    suggestThese.options.push({
                                        svg:svg,
                                        key:key,
                                        label:suggestOption.label,
                                        suggest:{
                                            write:writeStr,
                                            match:suggestHit.match,
                                            replace_left:suggestHit.replace_left
                                        }
                                    });
                                    if(doBreak){ break; }
                                }
                            }
                        }
                        if(doBreak){ break; }
                    }
                    //if any suggestions are available
                    if(suggestThese.options.length>0){
                        //show the float menu with the options
                        openFloatMenu(suggestThese);
                    }else{ //no suggestions, make sure float menu is closed
                        closeFloatMenu(menuId);
                    }
                }else{ //there is a selection
                    //TODO, show all options with a ${select in the "write"
                }
            },300);
        }
    };

    //show error style if a start_ or end_ wrap doesn't have it's twin paired wrap
    var lintSyntax=function(syntaxDiv){
        clearTimeout(syntaxDiv[0]['syntaxler_lint_timeout']);
        syntaxDiv[0]['syntaxler_lint_timeout']=setTimeout(function(){
            var countAttr={start:{}, end:{}};
            //function to set the wrap type
            var setUpdatedAttr=function(attrKey, typeData, wrap, startOrEnd){
                var attr=wrap.attr(attrKey);
                //add the attr type to the attr
                if(typeData.type.length>0){
                    attr+='-'+typeData.type;
                }
                //if not self-closing
                if(!typeData.self_close){
                    //init the counter for this attr
                    if(!countAttr.hasOwnProperty(startOrEnd+'-'+attr)){
                        countAttr[startOrEnd+'-'+attr]=0;
                    }
                    //increment counter
                    countAttr[startOrEnd+'-'+attr]+=1;
                    //add counter number to attribute
                    attr+='-'+countAttr[startOrEnd+'-'+attr];
                }else{
                    wrap.addClass('self-closing');
                }
                //set the updated attr
                wrap.attr(attrKey, attr);
            };
            //function to get the wrap type
            var getWrapType=function(wStr){
                var selfClose=false;
                if(wStr.indexOf('<')===0){
                    if(wStr.indexOf('</')===0){
                        wStr=wStr.replace('</', '');
                    }else{
                        wStr=wStr.replace('<', '');
                        if(wStr.lastIndexOf('/>')==wStr.length-'/>'.length){
                            selfClose=true;
                        }
                    }
                }
                var spaceIndex=wStr.indexOf(' ');
                if(spaceIndex!=-1){
                    wStr=wStr.substring(0, spaceIndex);
                }
                var match=wStr.match(/\w+/);
                if(match!=undefined){
                    wStr=match[0];
                }else{
                    wStr='';
                }
                return {type:wStr, self_close:selfClose};
            };
            var hasStartEnd=false;
            //each data-start
            syntaxDiv.find('[data-start]').each(function(){
                hasStartEnd=true;
                var wrap=jQuery(this);
                var typeData=getWrapType(wrap.text());
                setUpdatedAttr('data-start', typeData, wrap, 'start');
            });
            //each data-end
            syntaxDiv.find('[data-end]').each(function(){
                hasStartEnd=true;
                var wrap=jQuery(this);
                var typeData=getWrapType(wrap.text());
                setUpdatedAttr('data-end', typeData, wrap, 'end');
            });
            if(hasStartEnd){
                var hasTwin=function(wrap, startOrEnd){
                    var has=true;
                    var other='end';
                    if(startOrEnd=='end'){ other='start'; }
                    var attr=wrap.attr('data-'+startOrEnd);
                    var otherWrap=syntaxDiv.find('[data-'+other+'="'+attr+'"]:first');
                    if(otherWrap.length<1){
                        has=false;
                    }
                    return has;
                };
                //each data-start
                syntaxDiv.find('[data-start]').not('.self-closing').each(function(){
                    if(!hasTwin(jQuery(this), 'start')){
                        jQuery(this).addClass('no-end');
                    }
                });
                //each data-start
                syntaxDiv.find('[data-end]').not('.self-closing').each(function(){
                    if(!hasTwin(jQuery(this), 'end')){
                        jQuery(this).addClass('no-start');
                    }
                });
            }
        },100);
    };

    var fullUpdateSyntaxDiv=function(txtElem, e, callback){
        //full syntax highlights
        insertHtmlLines(txtElem, txtElem.val(), function(html){
            //insert the full syntax highlights into the div
            var syntaxDiv=txtElem.parent().children('.syntax-div:first');
            syntaxDiv.html(html);
            //lint html
            lintSyntax(syntaxDiv);
            //callback
            if(callback!=undefined){
                callback(html);
            }
        });
    };

    var getLineHtml=function(content){
        if(content==undefined){ content=''; }
        else{ content=getSyntaxLineHtmlWithoutEol(content); }
        var eol=getEndOfLineHtml();
        return '<pre class="syn-line">'+content+eol+'</pre>';
    };

    var getEndOfLineHtml=function(){
        return '<span class="eol"></span>';
    };

    var getSyntaxLineHtmlWithoutEol=function(preLine){
        //get html
        var lineHtml=getSyntaxLineHtml(preLine);
        //strip off eol
        var eol=getEndOfLineHtml();
        if(lineHtml.indexOf(eol)!=-1){
            if(lineHtml.lastIndexOf(eol)===lineHtml.length-eol.length){
                lineHtml=lineHtml.substring(0, lineHtml.lastIndexOf(eol));
            }
        }
        return lineHtml;
    };

    var getSyntaxLineHtml=function(preLine){
        //get html
        var lineHtml=preLine;
        if(typeof lineHtml!='string'){ lineHtml=lineHtml.html(); }
        return lineHtml;
    };

    //get the properly closed markup for html string that has been split in half
    var closeMarkupTagsInSplit=function(beforeSplitStr, afterSplitStr, insertSplitElem){
        if(insertSplitElem==undefined){ insertSplitElem=false; }
        var divider='<div class="split-here"></div>';

        var bodEl=jQuery('body:first');
        bodEl.append('<div style="display:none;" class="temporary-wrap syn-line"></div>');
        var tempDiv=bodEl.children('.temporary-wrap:last');
        tempDiv.html(beforeSplitStr+divider+afterSplitStr);
        var splitEl=tempDiv.find('.split-here:first');

        //move divider in or out of markup wrappers
        moveInsertOutOfMarkup(splitEl); moveInsertIntoMarkup(splitEl);

        var tempDivClone=tempDiv.clone();
        tempDiv.before(tempDivClone);

        var splitParentClone=tempDivClone.find('.split-here:first').parent();
        var splitParent=splitEl.parent();

        var splitHtmlClone=splitParentClone.html();
        var splitHtml=splitParent.html();

        splitHtmlClone=splitHtmlClone.substring(0, splitHtmlClone.indexOf(divider)+divider.length);
        splitHtml=splitHtml.substring(splitHtml.indexOf(divider));

        splitParentClone.html(splitHtmlClone);
        splitParent.html(splitHtml);

        beforeHml=tempDivClone.html();
        afterHtml=tempDiv.html();

        var splitElem='';
        if(insertSplitElem){ splitElem='<divide></divide>'; }

        //strip out non-html
        var beforeHtml1=beforeHml.substring(0, beforeHml.indexOf(divider));
        var beforeHtml2=beforeHml.substring(beforeHtml1.length+divider.length);
        var splitBeforeHtml2=getHtmlSplit(beforeHtml2);
        beforeHtml1+=splitElem;
        for(var s=0;s<splitBeforeHtml2.length;s++){
            if(splitBeforeHtml2[s].indexOf('<')===0){
                beforeHtml1+=splitBeforeHtml2[s];
            }
        }
        beforeSplitStr=beforeHtml1;

        //strip out non-html
        var afterHtml1=afterHtml.substring(0, afterHtml.indexOf(divider));
        var afterHtml2=afterHtml.substring(afterHtml1.length+divider.length);
        var splitAfterHtml1=getHtmlSplit(afterHtml1);
        afterHtml1='';
        for(var s=0;s<splitAfterHtml1.length;s++){
            if(splitAfterHtml1[s].indexOf('<')===0){
                afterHtml1+=splitAfterHtml1[s];
            }
        }
        afterSplitStr=afterHtml1+splitElem+afterHtml2;

        //cleanup
        tempDivClone.remove();
        tempDiv.remove();

        return {before:beforeSplitStr, after:afterSplitStr};
    };

    //get an array where every other item is a markup element
    var getHtmlSplit=function(str){
        var htmlSplit=[];
        while(str.indexOf('<')!=-1){
            var beforeMarkup=str.substring(0, str.indexOf('<'));
            //get the before markup
            if(beforeMarkup.length>0){
                htmlSplit.push(beforeMarkup);
                str=str.substring(str.indexOf('<'));
            }
            //get the markup element
            var markEl=str.substring(0, str.indexOf('>')+1);
            str=str.substring(markEl.length);
            htmlSplit.push(markEl);
        }
        if(str.length>0){
            htmlSplit.push(str);
        }
        return htmlSplit;
    };

    //get the before and after string for a syntax line, split at a character index
    var getSyntaxSplitLine=function(preLine, startSplitAt, endSplitAt){
        if(endSplitAt==undefined){ endSplitAt=startSplitAt; }
        //get the plain line text
        var lineStr=preLine.text();
        //get the plain text before and after the split
        var beforeSplit=getReplaceChars(lineStr.substring(0, startSplitAt));
        var insideSplit='';
        if(startSplitAt!=endSplitAt){
            insideSplit=getReplaceChars(lineStr.substring(startSplitAt, endSplitAt));
        }
        var afterSplit=getReplaceChars(lineStr.substring(endSplitAt));
        var ret={before:beforeSplit, inside:insideSplit, after:afterSplit};
        //get the html line text
        var lineHtml=getSyntaxLineHtmlWithoutEol(preLine);
        var eol=getEndOfLineHtml();
        //if the line contains html markup
        if(lineHtml.indexOf('<')!=-1){
            var htmlBeforeSplit='';
            var htmlInsideSplit='';
            var htmlAfterSplit='';
            var beforeLen=0; var insideLen=0;
            var htmlMarkup=lineHtml;
            //get the html split
            var htmlSplit=getHtmlSplit(lineHtml);
            if(htmlSplit.length>0){
                //function to handle one iteration
                var handleSplitItem=function(html, txt, txtLen, nextChunk){
                    var carry_over='';
                    //if this is a markup element
                    if(nextChunk.indexOf('<')===0){
                        //if this is an ending element
                        if(nextChunk.indexOf('</')===0){
                            //add the next chunk
                            html+=nextChunk;
                        }else{ //this is a starting element
                            //add the next chunk
                            html+=nextChunk;
                        }
                    }else{ //not a markup element
                        //if overshooting txt
                        if(nextChunk.length+txtLen>txt.length){
                            //figure out how much to shorten the next chunk
                            var diff=(nextChunk.length+txtLen)-txt.length;
                            var shorterLength=nextChunk.length-diff;
                            var shorterChunk=nextChunk.substring(0, shorterLength);
                            carry_over=nextChunk.substring(shorterChunk.length);
                            nextChunk=shorterChunk;
                        }
                        //accumulate the length
                        txtLen+=nextChunk.length;
                        //add the next chunk
                        html+=nextChunk;
                    }
                    return {html:html, txt:txt, txtLen:txtLen, carry_over:carry_over};
                };
                //for each item in htmlSplit
                for(var h=0;h<htmlSplit.length;h++){
                    //if not already overshot before text
                    if(beforeLen<beforeSplit.length){
                        var splitData=handleSplitItem(htmlBeforeSplit, beforeSplit, beforeLen, htmlSplit[h]);
                        htmlBeforeSplit=splitData.html;
                        beforeSplit=splitData.txt;
                        beforeLen=splitData.txtLen;
                        if(splitData.carry_over.length>0){
                            if(insideSplit.length>0){ //if there is an endSplitAt
                                var splitData=handleSplitItem(htmlInsideSplit, insideSplit, insideLen, splitData.carry_over);
                                htmlInsideSplit=splitData.html;
                                insideSplit=splitData.txt;
                                insideLen=splitData.txtLen;
                            }
                        }
                    }else if(insideSplit.length>0){ //if there is an endSplitAt
                        if(insideLen<insideSplit.length){
                            var splitData=handleSplitItem(htmlInsideSplit, insideSplit, insideLen, htmlSplit[h]);
                            htmlInsideSplit=splitData.html;
                            insideSplit=splitData.txt;
                            insideLen=splitData.txtLen;
                        }else{
                            break;
                        }
                    }else{
                        break;
                    }
                }
            }
            //set the after split string
            htmlAfterSplit=lineHtml.substring(htmlBeforeSplit.length+htmlInsideSplit.length);
            //set the return strings
            ret['before']=htmlBeforeSplit;
            ret['inside']=htmlInsideSplit;
            ret['after']=htmlAfterSplit+eol;
        }
        return ret;
    };

    //function to bounce out inserted text, if it isn't supposed to be within its markup parent
    var moveInsertOutOfMarkup=function(insertedEl){
        var didMoveOut=false;
        var parentEl=insertedEl.parent();
        //if the inserted element is not a child of the line <pre>
        if(!parentEl.hasClass('syn-line')){
            //if the inserted element is not a child of an inner line within multi-line
            if(!parentEl.hasClass('inner-ml')){
                var insertHtml=insertedEl[0].outerHTML;
                var innerHtml=parentEl.html();
                var isAtEnd=false;
                //if this insertedEl is currently the first inside the markup element
                if(innerHtml.indexOf(insertHtml)===0){
                    //move the insertedEl before parentEl
                    parentEl.before(insertedEl);
                    isAtEnd=true;
                }else if(innerHtml.lastIndexOf(insertHtml)===innerHtml.length-insertHtml.length){
                    //move the insertedEl after parentEl
                    parentEl.after(insertedEl);
                    isAtEnd=true;
                }
                //if insertedEl as at the end of a markup element
                if(isAtEnd){
                    //recursive move again if needed
                    moveInsertOutOfMarkup(insertedEl);
                }
                didMoveOut=isAtEnd;
            }
        } return didMoveOut;
    };

    //function to pull inserted text into markup element, if inserted text is supposed to be inside any adjacent markup
    var moveInsertIntoMarkup=function(insertedEl){
        var didMoveIn=false;
        //get the html string to figure out if there are any html elements directly touching insertedEl
        var parentEl=insertedEl.parent();
        var insertHtml=insertedEl[0].outerHTML;
        var innerHtml=parentEl.html();
        //if there is another element directly before insertedEl
        var elBefore=false; if(innerHtml.indexOf('>'+insertHtml)!=-1){ elBefore=true; }
        var elAfter=false; if(innerHtml.indexOf(insertHtml+'<')!=-1){ elAfter=true; }
        //handle element directly before
        if(elBefore){
            var prevEl=insertedEl.prev();
            if(prevEl.length>0){
                //if this is a part of a multi-line
                if(prevEl.hasClass('inner-ml') || prevEl.hasClass('first-ml') || prevEl.hasClass('consume-right')){
                    prevEl.append(insertedEl);
                    didMoveIn=true;
                }
            }
        }
        //handle element directly after
        if(!didMoveIn && elAfter){
            var nextEl=insertedEl.next();
            if(nextEl.length>0){
                //if this is a part of a multi-line
                if(nextEl.hasClass('inner-ml') || nextEl.hasClass('last-ml') || nextEl.hasClass('consume-left')){
                    nextEl.prepend(insertedEl);
                    didMoveIn=true;
                }
            }
        }
        //if the insertedEl was moved
        if(didMoveIn){
            //recursive move again if needed
            moveInsertIntoMarkup(insertedEl);
        }
        return didMoveIn;
    };

    //used to give range caching time to catch up
    var pauseInput=function(txtEl){
        txtEl.addClass('pause-input');
    };
    var resumeInput=function(txtEl){
        //give the cache time to update so that it reflects the proper selection
        setTimeout(function(){
            txtEl.removeClass('pause-input');
        },100);
    };

    //inserted str is wrapped in <insert>string</insert>... remove outer <insert>
    var unwrapChars=function(txtElem, type){
        var syntaxDiv=txtElem.parent().children('.syntax-div:first');
        var charEls=syntaxDiv.find('.syn-line '+type);
        charEls.each(function(){
            var html=jQuery(this).html();
            jQuery(this).after(html);
        });
        charEls.remove();
    };

    //merge two lines together
    var mergeSyntaxLines=function(linePre1, linePre2){
        //linePre1 must come before linePre1 and they must be adjacent
        if(linePre1.next('.syn-line:first').is(linePre2)){
            //remove eol from linePre1
            linePre1.children('.eol:last').remove();
            //append the html from linePre2 to linePre1
            var line1Html=linePre1.html();
            var line2Html=linePre2.html();
            linePre1.html(line1Html+line2Html);
            //remove linePre2
            linePre2.remove();
            //merge adjacent, multi-line syntax markup
            linePre1.children('.multi-line').not('span.eol').each(function(){
                var tagName=jQuery(this)[0].tagName.toLowerCase();
                var firstEl=linePre1.children(tagName+':first');
                var adjacent=jQuery(this).next(tagName+':first');
                if(adjacent.length>0){
                    //mark this adjacent element for deletion
                    adjacent.addClass('merged-adjacent');
                    //get the html to merge together
                    var firstHtml=firstEl.html();
                    var adjacentHtml=adjacent.html();
                    //merge the html into the first element
                    firstEl.html(firstHtml+adjacentHtml);
                    //clear out the adjacent
                    adjacent.html('');
                }
            });
            //remove merged adjacent
            linePre1.children('.merged-adjacent').remove();
        }
    };

    var backspaceSyntaxRange=function(txtElem, oldRange, newRange){
        var deletedChar=oldRange.val.substring(newRange.ch.start, oldRange.ch.start);
        //simulate the deletedChar as if it was selected
        var rangeToDelete={
            ch:{
                start:newRange.ch.start, end:oldRange.ch.start
            },
            line:{
                start:newRange.line.start, end:oldRange.line.start
            },
            pos:{
                start:{
                    ch:newRange.pos.start.ch, line:newRange.pos.start.line
                },
                end:{
                    ch:oldRange.pos.start.ch, line:oldRange.pos.start.line
                }
            },
            selected:deletedChar, syntax:oldRange.syntax, val:oldRange.val
        };
        //fallback on this other function
        removeSyntaxRange(txtElem, rangeToDelete);
    };

    //remove the given range of syntax
    var removeSyntaxRange=function(txtElem, range){
        //get syntax lines
        var preLines=getSyntaxLines(txtElem, range.pos);
        if(preLines.length>0){
            var partialFirstLine=undefined;
            var partialLastLine=undefined;
            //function to insert <remove> elements around markup
            var insertRemoveBetweenMarkup=function(htmlSplit){
                var str='';
                for(var h=0;h<htmlSplit.length;h++){
                    var splitItem=htmlSplit[h];
                    if(splitItem.indexOf('<')===0){
                        str+=splitItem;
                    }else{
                        str+='<remove>'+splitItem+'</remove>';
                    }
                }
                return str;
            };
            //restore eol 
            var restoreEol=function(lineEl){
                if(lineEl.children('.eol:last').length<1){
                    var eol=getEndOfLineHtml();
                    lineEl.append(eol);
                }
            };
            //if more than one line
            if(preLines.length>1){
                //for each line
                preLines.each(function(i){
                    var isFirst=false; var isLast=false;
                    if(i==0){ isFirst=true; }
                    if(i+1==preLines.length){ isLast=true; }
                    //is first line
                    if(isFirst){
                        var startCh=range.pos.start.ch;
                        if(startCh>0){ //sub-string starts after the start of the first line
                            var splitLine=getSyntaxSplitLine(jQuery(this), startCh);
                            //if any highlight markup was removed
                            if(splitLine.after.indexOf('>')!=-1 || splitLine.after.indexOf('<')!=-1){
                                //get the line html markup split
                                var htmlSplit=getHtmlSplit(splitLine.after);
                                //insert the <remove> elements between the markup elements
                                splitLine.after=insertRemoveBetweenMarkup(htmlSplit);
                                //set the html with the <remove> text properly identified
                                jQuery(this).html(splitLine.before+splitLine.after);
                            }else{ //no markup was disturbed
                                jQuery(this).html(splitLine.before+'<remove>'+splitLine.after+'</remove>');
                            }
                            //indicate the first line was a partial (sub-string) removal (not entire line)
                            partialFirstLine=jQuery(this);
                            restoreEol(partialFirstLine);
                        }else{ //the entire line
                            jQuery(this).addClass('remove');
                        }
                    }else if(isLast){ //is last line
                        var lineTxt=jQuery(this).text();
                        var endCh=range.pos.end.ch;
                        if(endCh<=lineTxt.length){ //sub-string ends before the end of the last line
                            var splitLine=getSyntaxSplitLine(jQuery(this), endCh);
                            //if any highlight markup was removed
                            if(splitLine.before.indexOf('>')!=-1 || splitLine.before.indexOf('<')!=-1){
                                //get the line html markup split
                                var htmlSplit=getHtmlSplit(splitLine.before);
                                //insert the <remove> elements between the markup elements
                                splitLine.before=insertRemoveBetweenMarkup(htmlSplit);
                                //set the html with the <remove> text properly identified
                                jQuery(this).html(splitLine.before+splitLine.after);
                            }else{ //no markup was disturbed
                                jQuery(this).html('<remove>'+splitLine.before+'</remove>'+splitLine.after);
                            }
                            //indicate the first line was a partial (sub-string) removal (not entire line)
                            partialLastLine=jQuery(this);
                            restoreEol(partialLastLine);
                        }else{ //the entire line
                            jQuery(this).addClass('remove');
                        }
                    }else{ //is between first and last line
                        jQuery(this).addClass('remove');
                    }
                });
            }else{ //only one line
                var preLine=preLines;
                var partialLineSelect=false;
                //handle start of the range
                var startCh=range.pos.start.ch;
                if(startCh>0){ //sub-string starts after the start of the line
                    partialLineSelect=true;
                }
                //handle end of the range
                var lineTxt=preLine.text();
                var endCh=range.pos.end.ch;
                if(endCh<=lineTxt.length){ //sub-string ends before the end of the line
                    partialLineSelect=true;
                }
                //a substring in the line was selected
                if(partialLineSelect){
                    var splitLine=getSyntaxSplitLine(preLine, startCh, endCh);
                    //if any highlight markup was removed
                    if(splitLine.inside.indexOf('>')!=-1 || splitLine.inside.indexOf('<')!=-1){
                        //get the line html markup split
                        var htmlSplit=getHtmlSplit(splitLine.inside);
                        //insert the <remove> elements between the markup elements
                        splitLine.inside=insertRemoveBetweenMarkup(htmlSplit);
                        //set the html with the <remove> text properly identified
                        preLine.html(splitLine.before+splitLine.inside+splitLine.after);
                    }else{ //no markup was disturbed
                        preLine.html(splitLine.before+'<remove>'+splitLine.inside+'</remove>'+splitLine.after);
                    }
                }else{ //the entire line is selected
                    preLine.addClass('remove');
                }
            }
            //remove the elements
            var syntaxDiv=txtElem.parent().children('.syntax-div:first');
            var linesToRemove=syntaxDiv.find('.remove');
            var textToRemove=syntaxDiv.find('remove');
            //remove lines
            linesToRemove.remove();
            //remove part of lines
            textToRemove.remove();
            //if the first and last line should be merged
            if(partialFirstLine!=undefined && partialLastLine!=undefined){
               mergeSyntaxLines(partialFirstLine, partialLastLine);
            }
        }
    };

    //if one or more lines touch selection, or contain cursor, when hitting tab, then remove a tab at the beginning of each line
    var backwardTabSelectedSyntax=function(txtElem, pos){
        //get syntax lines
        var preLines=getSyntaxLines(txtElem, pos);
        if(preLines.length>0){
            var tab=getSpacesForTab(txtElem);
            //remove a tab from the beginning of each of the lines
            preLines.each(function(){
                var html=jQuery(this).html();
                if(html.indexOf(tab)!=-1){
                    html=html.replace(tab, '<remove>'+tab+'</remove>');
                    jQuery(this).html(html);
                    var removeEl=jQuery(this).find('remove:first');
                }
            });
            //remove tabs
            preLines.find('remove').remove();
        }
    };

    //if one or more lines touch selection when hitting tab, then insert a tab at the beginning of each line
    var forwardTabSelectedSyntax=function(txtElem, pos){
        //get syntax lines
        var preLines=getSyntaxLines(txtElem, pos);
        if(preLines.length>0){
            var tab=getSpacesForTab(txtElem);
            //insert a tab at the beginning of each of the lines
            preLines.each(function(){
                jQuery(this).prepend('<insert>'+tab+'</insert>');
                var inserted=jQuery(this).children('insert:first');
                //move the inserted str out of any markup element, if it's inside the markup at either the start or end edge
                moveInsertOutOfMarkup(inserted);
                //move the inserted str into adjacent markup element, if it's supposed to fall into this element
                moveInsertIntoMarkup(inserted);
            });
            //clean up insert elements
            unwrapChars(txtElem, 'insert');
        }
    };

    //handle pasting (setTimeout to wait for pasted string to be available)
    var pasteSyntaxStr=function(txtElem, oldRange){
        setTimeout(function(){
            //get new ranges
            var newRange=getSelectRange(txtElem);
            //get the pasted string
            var pastedStr=newRange.val.substring(oldRange.ch.start, newRange.ch.start);
            if(pastedStr.length>0){
                //handle inserting the string
                insertSyntaxStr(txtElem, pastedStr, oldRange.pos);
                //update textarea size
                updateTextareaSize(txtElem);
            }
        }, 0);
    };

    //insert characters through typing OR through pasting
    var insertSyntaxStr=function(txtElem, str, pos){
        //get syntax line
        var preLine=getSyntaxLines(txtElem, pos);
        if(preLine.length>0){
            //should only ever be one preLine since there can't be more than one insert target position (in more than one line)
            if(preLine.length===1){
                //if str is only one line
                if(str.indexOf('\n')==-1){
                    //add inserted string
                    var splitLine=getSyntaxSplitLine(preLine, pos.start.ch);
                    str=getReplaceChars(str);
                    preLine.html(splitLine.before+'<insert>'+str+'</insert>'+splitLine.after);
                    //make sure there is an eol element at the end of the line
                    if(preLine.children('.eol:last').length<1){
                        var eol=getEndOfLineHtml();
                        preLine.append(eol);
                    }
                    //fix the syntax highlight of this line
                    var inserted=preLine.find('insert:first');
                    //move the inserted str out of any markup element, if it's inside the markup at either the start or end edge
                    moveInsertOutOfMarkup(inserted);
                    //move the inserted str into adjacent markup element, if it's supposed to fall into this element
                    moveInsertIntoMarkup(inserted);
                    //remove <insert> element around the string
                    unwrapChars(txtElem, 'insert');
                }else{ //str is 2 or more lines (pasted string)
                    //split at where the new str should be inserted
                    var bookendLines=addSyntaxLineAfter(txtElem, pos, false);
                    var divide1=bookendLines[0].find('divide:first');
                    var divide2=bookendLines[1].find('divide:first');
                    str=getReplaceChars(str);
                    var strLines=str.split('\n');
                    //insert the first new line txt
                    divide1.append(strLines[0]);
                    //insert the last new line txt
                    divide2.append(strLines[strLines.length-1]);
                    //for each line between the first and last lines
                    if(strLines.length>2){
                        var afterPos=getEndOfLinePos(bookendLines[0]);
                        for(var s=1;s<strLines.length-1;s++){
                            var lineStr=strLines[s];
                            //create another line
                            var lines=addSyntaxLineAfter(txtElem, afterPos, false);
                            //add the string to that line
                            var lDivide=lines[1].find('divide:first');
                            lDivide.append(lineStr);
                            if(s+1<strLines.length){
                                //get the end of line position for this new line
                                afterPos=getEndOfLinePos(lines[1]);
                            }
                        }
                    }
                    //remove <divide> element around the string
                    unwrapChars(txtElem, 'divide');
                }
            }
        }
    };

    var getEndOfLinePos=function(lineEl){
        var pos;
        if(lineEl.hasClass('syn-line')){
            var lineIndex=lineEl.index();
            var lineTxt=lineEl.text();
            pos={start:{line:(lineIndex+1), ch:lineTxt.length}};
        }
        return pos;
    };

    //triggered when hitting return, or when pasting into a line at a position that will split into another line
    var addSyntaxLineAfter=function(txtElem, pos, removeDivide){
        if(removeDivide==undefined){ removeDivide=true; }
        //get the last line element
        var preLine=getSyntaxLines(txtElem, pos);
        if(preLine.length>1){ preLine=preLine.eq(preLine.length-1); }
        //if creating newline before the end of the line
        var splitLine=getSyntaxSplitLine(preLine, pos.start.ch);
        //make sure all of the markup tags are closed in this before string
        var splitUpdate=closeMarkupTagsInSplit(splitLine.before, splitLine.after, true);
        splitLine.before=splitUpdate.before;
        splitLine.after=splitUpdate.after;
        //remove the after-string html from the preLine
        preLine.html(splitLine.before);
        //create the new line
        var newLineHtml=getLineHtml(splitLine.after);
        preLine.after(newLineHtml);
        var newLine=preLine.next('.syn-line:first');
        if(removeDivide){
            //clean up divide elements
            unwrapChars(txtElem, 'divide');
        }
        //make sure multi-line classes get updated
        newLine.find('.first-ml').removeClass('first-ml').addClass('inner-ml');
        //return
        return [preLine, newLine];
    };

    //instead of a full redraw for the entire textarea, try to figure out what smaller difference needs to change
    var partialUpdateSyntaxDiv=function(txtElem, e){
        //pause input until syntax update is over
        var hadPausedInput=txtElem.hasClass('pause-input');
        if(!hadPausedInput){ txtElem.addClass('pause-input'); }
        //return did update
        var didUpdate=true;
        //get old/new ranges
        var oldRange=getCacheRange(txtElem);
        var newRange=getSelectRange(txtElem);
        //figure out if there was selected text when the input happened
        var hasSelected=hasSelectedText(txtElem, oldRange);

        //function to get discrete values from e.data
        var getDataJson=function(evData){
            var ret={};
            if(evData!=undefined){
              if(typeof evData=='string'){
                  if(evData.indexOf(dataMesssagePrefix)===0){
                      evData=evData.substring(dataMesssagePrefix.length);
                      if(evData.indexOf(']')!=-1){
                          //function to get the next key out of the e.data string
                          var setNextKey=function(){
                              var setNext=false;
                              if(evData.indexOf('[')===0){
                                  var key=''; var val='';

                                  //get the key
                                  key=evData.substring(0, evData.indexOf(']')+1);
                                  evData=evData.substring(key.length);
                                  key=key.substring(1);
                                  key=key.substring(0, key.lastIndexOf(']'));

                                  //get the val
                                  var nextMatch=evData.match(/;\[\w+\]/);
                                  if(nextMatch!=undefined && nextMatch.length>0){
                                      var indexOfNext=evData.indexOf(nextMatch[0]);
                                      val=evData.substring(0, indexOfNext);
                                      evData=evData.substring(val.length+1);
                                  }else{
                                      val=evData;
                                  }

                                  //set the new key/value
                                  ret[key]=val;

                                  setNext=true;
                              } return setNext;
                          };
                          //get all of the key/vals out of the e.data string
                          var next=setNextKey();
                          while(next){
                              next=setNextKey();
                          }
                      }
                  }
              }
            } return ret;
        };

        //convert event data to JSON
        var evtData=getDataJson(e.data);

        //function to get the input character
        var getChar=function(){
            var ch=undefined;
            if(e.keyCode!=undefined){
                ch=String.fromCharCode(e.keyCode);
            }else if(e.data!=undefined){
                if(e.data.indexOf(dataMesssagePrefix)===0){
                    if(evtData.hasOwnProperty('write')){
                        ch=evtData.write;
                    }else{
                        ch='';
                    }
                }else{
                    ch=e.data;
                }
            } return ch;
        };

        //try to get the input type from some property in the event
        var iType;
        if(evtData.hasOwnProperty('inputType')){
            iType=evtData.inputType;
        }else{
            iType=e.inputType;
            if(iType==undefined || iType.length<1){
                iType=e.type;
            }
        }

        //handle the input type
        var inputType=iType.toLowerCase();
        switch(inputType){
            case 'inserttext':
                //if this is a tab key press
                if(evtData.hasOwnProperty('tab')){
                    var tab=getSpacesForTab(txtElem);
                    var tabType=evtData.tab;
                    var didRemoveTab='0';
                    if(evtData.hasOwnProperty('remove_tab')){
                        didRemoveTab=evtData.remove_tab;
                    }
                    tabType=tabType.toLowerCase();
                    switch(tabType){
                        case 'forwardnoselect':
                            insertSyntaxStr(txtElem, tab, oldRange.pos);
                            break;
                        case 'forwardwithselect':
                            forwardTabSelectedSyntax(txtElem, newRange.pos);
                            break;
                        case 'backwardnoselect':
                            if(didRemoveTab=='1'){
                                backwardTabSelectedSyntax(txtElem, newRange.pos);
                            }
                            break;
                        case 'backwardwithselect':
                            if(didRemoveTab=='1'){
                                backwardTabSelectedSyntax(txtElem, newRange.pos);
                            }
                            break;
                    }
                }else{ //not a tab key press
                    if(hasSelected){ //text was selected, and will be replaced by the new character
                        //remove the selected text from the syntax
                        removeSyntaxRange(txtElem, oldRange);
                    }
                    //add the character in
                    var ch=getChar();
                    if(ch.length>0){
                        insertSyntaxStr(txtElem, ch, oldRange.pos);
                    }
                }
                break;
            case 'insertlinebreak':
                if(hasSelected){ //text was selected, and will be replaced by the new character
                    //remove the selected text from the syntax
                    removeSyntaxRange(txtElem, oldRange);
                }
                addSyntaxLineAfter(txtElem, oldRange.pos);
                break;
            case 'deletecontentbackward':
                if(hasSelected){ //text was selected, and will all be deleted
                    //remove the selected text from the syntax
                    removeSyntaxRange(txtElem, oldRange);
                }else{
                    //handle backspace press
                    backspaceSyntaxRange(txtElem, oldRange, newRange);
                }
                break;
            case 'deletewordbackward':
                if(hasSelected){ //text was selected, and will all be deleted
                    //remove the selected text from the syntax
                    removeSyntaxRange(txtElem, oldRange);
                }else{
                    //handle backspace press
                    backspaceSyntaxRange(txtElem, oldRange, newRange);
                }
                break;
            /*case 'deletebydrag':
                break;
            case 'insertfromdrop':
                break;*/
            case 'insertfrompaste':
                if(hasSelected){ //text was selected, and will be replaced by the new character
                    //remove the selected text from the syntax
                    removeSyntaxRange(txtElem, oldRange);
                }
                pasteSyntaxStr(txtElem, oldRange);
                break;
            case 'deletebycut':
                //remove the selected text from the syntax
                removeSyntaxRange(txtElem, oldRange);
                break;
            default:
                //event not handled, prevent changes to make sure textarea remains aligned with syntax html
                e.preventDefault(); e.stopPropagation();
                didUpdate=false;
                //restore value
                txtElem.val(oldRange.val);
                //restore selection / cursor position
                setTextSelectRange(txtElem, oldRange.ch.start, oldRange.ch.end);

                console.log(inputType); //****
                break;
        }
        //update the textarea size
        updateTextareaSize(txtElem);
        //remove pause
        if(!hadPausedInput){ txtElem.removeClass('pause-input'); }
        return didUpdate;
    };

    //init the events for the textarea
    var initTextareaEvents=function(txtElem){
        if(!txtElem.hasClass('init-events')){
            txtElem.addClass('init-events');

            //is this textarea allowed to have text changes?
            var allowWrite=function(e, txtE){
                var allowKeyDown=false;
                if(!txtE.hasClass('pause-input')){
                    allowKeyDown=!txtE.hasClass('readonly');
                    //if this is readonly... some key presses may still be allowed to select etc...
                    if(!allowKeyDown){
                        if(e.ctrlKey){
                            var charPressed=String.fromCharCode(e.which).toLowerCase();
                            switch(charPressed){
                                case 'c': allowKeyDown=true; break; //allow ctl+c to copy
                                case 'a': allowKeyDown=true; break; //allow ctl+a to select all
                                default:
                                    switch(e.keyCode){
                                        case 37: allowKeyDown=true; break; //left + ctl
                                        case 38: allowKeyDown=true; break; //up + ctl
                                        case 39: allowKeyDown=true; break; //right + ctl
                                        case 40: allowKeyDown=true; break; //down + ctl
                                    }
                                    break;
                            }
                        }else if(e.shiftKey){
                            switch(e.keyCode){
                                case 37: allowKeyDown=true; break; //left + shift
                                case 38: allowKeyDown=true; break; //up + shift
                                case 39: allowKeyDown=true; break; //right + shift
                                case 40: allowKeyDown=true; break; //down + shift
                            }
                        }else{
                            switch(e.keyCode){
                                case 37: allowKeyDown=true; break; //left
                                case 38: allowKeyDown=true; break; //up
                                case 39: allowKeyDown=true; break; //right
                                case 40: allowKeyDown=true; break; //down
                            }
                        }
                    }
                }
                return allowKeyDown;
            };

            //handle special key presses like tab that doesn't natively tab
            txtElem.keydown(function(e){
                //if allowed to keypress (not blocked by readonly)
                if(allowWrite(e, jQuery(this))){
                    handleSpecialKeyPress(jQuery(this), e);
                }else{
                    e.preventDefault(); e.stopPropagation();
                }
            });

            //detect non-text changing things
            txtElem.keyup(function(e){
                //if allowed to keypress (not blocked by readonly)
                if(allowWrite(e, jQuery(this))){
                    if(!e.shiftKey && !e.ctrlKey){
                        setCacheRange(txtElem);
                    }
                    //end the backspace hold restriction
                    var keyCode=e.keyCode || e.which;
                    switch(keyCode){
                        case 8:
                            jQuery(this).removeClass('no-hold-backspace'); break;
                    }
                }else{
                    e.preventDefault(); e.stopPropagation();
                }
            });

            //detect non-text changing things
            txtElem.focus(function(e){
                if(!jQuery(this).hasClass('readonly')){
                    setCacheRange(jQuery(this));
                    if(!jQuery(this).hasClass('focus')){
                        jQuery('body:first').find('.init-syntaxler.focus').removeClass('focus');
                        jQuery(this).addClass('focus');
                    }
                }
            });

            //detect non-text changing things
            txtElem.mousedown(function(e){
                if(!jQuery(this).hasClass('readonly')){
                    pauseInput(txtElem);
                }
            });

            //detect non-text changing things
            txtElem.mouseup(function(e){
                if(!jQuery(this).hasClass('readonly')){
                    setCacheRange(txtElem);
                    resumeInput(txtElem);
                }
            });

            document.addEventListener('selectionchange', function() {
                setCacheRange(txtElem);
            });

            //detect non-text changing things
            txtElem.click(function(e){
                if(!jQuery(this).hasClass('readonly')){
                    setCacheRange(txtElem);
                }
            });

            //detect non-text changing things
            txtElem.mouseleave(function(e){
                if(!jQuery(this).hasClass('readonly')){
                    setCacheRange(txtElem);
                    resumeInput(txtElem);
                }
            });

            //detect text-changing things
            if (txtElem[0].addEventListener) {
                txtElem[0].addEventListener('input', function(e) {
                    //if allowed to keypress (not blocked by readonly)
                    if(allowWrite(e, jQuery(this))){
                        handleTextChange(jQuery(this), e);
                    }else{
                        e.preventDefault(); e.stopPropagation();
                    }
                }, false);
            }/* else if (txtElem[0].attachEvent) {
                txtElem[0].attachEvent('onpropertychange', function(e) {
                    //if allowed to keypress (not blocked by readonly)
                    if(allowWrite(e, jQuery(this))){
                        handleTextChange(jQuery(this), e);
                    }else{
                        e.preventDefault(); e.stopPropagation();
                    }
                });
            }*/

            //detect text-changing things
            txtElem.bind('drop',function(e){
                if(!jQuery(this).hasClass('readonly')){
                    //handleTextChange(jQuery(this), e);
                    e.preventDefault(); e.stopPropagation();
                }else{
                    e.preventDefault(); e.stopPropagation();
                }
            });

            /*//detect text-changing things
            txtElem.bind('paste',function(e){
                if(!jQuery(this).hasClass('readonly')){
                    handleTextChange(jQuery(this), e);
                }else{
                    e.preventDefault(); e.stopPropagation();
                }
            });*/

            //detect scrolling
            txtElem.scroll(function(){
                handleScrollUpdate(jQuery(this));
            });
        }
    };

    var initOnce=function(){
        if(!document.hasOwnProperty('syntaxler_init_once')){
            document['syntaxler_init_once']=true;
            jQuery(window).resize(function(){
                clearTimeout(document['syntaxler_resize_timeout']);
                document['syntaxler_resize_timeout']=setTimeout(function(){
                    jQuery('textarea.init-syntaxler').each(function(){
                        handleTextResize(jQuery(this));
                    });
                },100);
            });
            jQuery(document).click(function(e){
                if(closeFloatMenus()){
                    e.preventDefault(); e.stopPropagation();
                }
            });
            //handle open float menus
            jQuery(document).keydown(function(e){
                //if there is an open float menu
                var floatMenu=getFirstOpenFloatMenu();
                if(floatMenu.length>0){
                    switch (e.keyCode) {
                        case 13: //enter key press
                            e.preventDefault(); e.stopPropagation();
                            clickSelectedFloatMenuItem(floatMenu);
                            break;
                        case 27: //escape key press
                            e.preventDefault(); e.stopPropagation();
                            closeFloatMenu(floatMenu);
                            break;
                        case 37: //left
                            e.preventDefault(); e.stopPropagation();
                            selectNextFloatMenuItem(floatMenu, 'left');
                            break;
                        case 38: //up
                            e.preventDefault(); e.stopPropagation();
                            selectNextFloatMenuItem(floatMenu, 'up');
                            break;
                        case 39: //right
                            e.preventDefault(); e.stopPropagation();
                            selectNextFloatMenuItem(floatMenu, 'right');
                            break;
                        case 40: //down
                            e.preventDefault(); e.stopPropagation();
                            selectNextFloatMenuItem(floatMenu, 'down');
                            break;
                    }
                }
            });
        }
    };

    //init multiple textareas
    var initTextareas=function(args){
        if(args.hasOwnProperty('textareas')){
            var txtElems;
            if(typeof args['textareas']=='string'){
                txtElems=jQuery(args['textareas']);
            }else{
                txtElems=args['textareas'];
            }
            if(txtElems.length>0){
                txtElems.not('.init-syntaxler').each(function(){
                    initTextarea(jQuery(this), args);
                });
            }
        }
    };

    var getFirstOpenFloatMenu=function(){
        return jQuery('body:first').children('.syn-float-menu:first');
    };

    var getFloatMenu=function(id){
        var menuWrap;
        if(id==undefined){
            menuWrap=getFirstOpenFloatMenu();
        }else{
            if(typeof id=='string'){
                var bodEl=jQuery('body:first');
                menuWrap=bodEl.children('.syn-float-menu[data-id="'+id+'"]:first');
            }else if(id.hasClass('syn-float-menu')){
                menuWrap=id;
            }
        }
        return menuWrap;
    };

    var getSelectedFloatMenuItem=function(id){
        var item, wrap=getFloatMenu(id);
        if(wrap!=undefined && wrap.length>0){
            item=wrap.find('.float-content [data-key].selected:first');
        }
        return item;
    };

    var clickSelectedFloatMenuItem=function(id){
        var item=getSelectedFloatMenuItem(id);
        if(item!=undefined && item.length>0){
            var labelSpan=item.find('.option-label:first');
            labelSpan.click();
        }
    };

    var closeFloatMenus=function(){
        var didClose=false;
        jQuery('body:first').children('.syn-float-menu').not('.hover').each(function(){
            didClose=true;
            closeFloatMenu(jQuery(this));
        });
        return didClose;
    };

    var closeFloatMenu=function(id){
        var wrap=getFloatMenu(id);
        if(wrap!=undefined && wrap.length>0){
            wrap.remove();
        }
    };

    var getFloatMenuItem=function(id, key){
        var item, wrap=getFloatMenu(id);
        switch(typeof key){
            case 'number': //get item by index
                item=wrap.find('.float-content [data-key]').eq(key);
                break;
            case 'string': //get item by key
                item=wrap.find('.float-content [data-key="'+key+'"]:first').eq(key);
                break;
            default: //key is the html element
                if(key.attr('data-key')!=undefined){
                    item=key;
                }
                break;
        } return item;
    };

    var selectNextFloatMenuItem=function(id, direction){
        var item=getSelectedFloatMenuItem(id);
        if(item!=undefined && item.length>0){
            var nextItem;
            switch(direction){
                case 'left':
                    nextItem=item.prev('[data-key]:first');
                    if(nextItem.length<1){
                        var nextRow=item.parent().prev('.option-row:first');
                        if(nextRow.length>0){
                            nextItem=nextRow.children('[data-key]:last');
                        }
                    }
                    break;
                case 'right':
                    nextItem=item.next('[data-key]:first');
                    if(nextItem.length<1){
                        var nextRow=item.parent().next('.option-row:first');
                        if(nextRow.length>0){
                            nextItem=nextRow.children('[data-key]:first');
                        }
                    }
                    break;
                case 'up':
                    var nextRow=item.parent().prev('.option-row:first');
                    if(nextRow.length>0){
                        nextItem=nextRow.children('[data-key]').eq(item.index());
                    }
                    break;
                case 'down':
                    var nextRow=item.parent().next('.option-row:first');
                    if(nextRow.length>0){
                        nextItem=nextRow.children('[data-key]').eq(item.index());
                    }
                    break;
            }
            if(nextItem!=undefined && nextItem.length>0){
                selectFloatMenuItem(id, nextItem);
            }
        }
    };

    var selectFloatMenuItem=function(id, key){
        var item=getFloatMenuItem(id, key);
        if(item!=undefined && item.length>0){
            if(!item.hasClass('selected')){
                var contentWrap=item.parents('.float-content:first');
                //clear any other selected item
                contentWrap.find('[data-key].selected').removeClass('selected');
                //select this item
                item.addClass('selected');
            }
        }
    };

    var getBuildFloatMenuWrap=function(id){
        var wrap=getFloatMenu(id);
        if(wrap!=undefined || wrap.length>0){
            var bodEl=jQuery('body:first');
            bodEl.append('<div data-id="'+id+'" class="syn-float-menu"><div class="close-btn"></div></div>');
            var wrap=getFloatMenu(id);
            wrap.append('<div class="float-content"></div>');
        }
        return wrap;
    };

    var getFocusTxtElem=function(){
        var txtElem=jQuery('textarea.init-syntaxler:focus').eq(0);
        if(txtElem.length<1){ txtElem=jQuery('textarea.init-syntaxler.focus:first'); }
        return txtElem;
    };

    //gets the x/y coordinates of the caret inside a syntaxler textarea
    var getCaretPosXY=function(){
        var ret={x:-1, y:-1};
        var txtElem=getFocusTxtElem();
        if(txtElem.length>0){
            //get syntax div
            var syntaxDiv=txtElem.parent().children('.syntax-div:first');
            //get range
            var range=getSelectRange(txtElem);
            //get the syntax line element
            var synLine=syntaxDiv.children('.syn-line').eq(range.pos.start.line-1);
            //get the y-position of the syntax element
            ret.y=synLine.offset().top - window.scrollY;
            //insert a caret position into the line
            var splitStr=getSyntaxSplitLine(synLine, range.pos.start.ch);
            synLine.html(splitStr.before+'<caret_position></caret_position>'+splitStr.after);
            var caret=synLine.find('caret_position:first');
            //get the x-position
            ret.x=caret.offset().left - window.scrollX;
            //cleanup remove the caret element
            caret.remove();
        }
        return ret;
    };

    //get the next tab target select range
    var getNextTabTarget=function(txtElem){
        var nextTarget;
        var val=txtElem.val();
        var tabTargets=getTabTargets(val);
        if(tabTargets!=undefined){
            //current selection start and end
            var selStart=txtElem[0].selectionStart, selEnd=txtElem[0].selectionEnd;
            //get an ordered list of indexes
            var indexes=[];
            for(var i in tabTargets){
                if(tabTargets.hasOwnProperty(i)){
                    indexes.push(i);
                }
            }
            //sort the indexes in correct order
            indexes.sort();
            //for each tabTarget, in order of index
            for(var i=0; i<indexes.length;i++){
                if(nextTarget==undefined){
                    var index=indexes[i];
                    var tabTarget=tabTargets[index];
                    for(var s=0;s<tabTarget.length;s++){
                        var str=tabTarget[s];
                        //get the index of this tab start string
                        var startStr=val.indexOf(str);
                        var endStr=startStr+str.length;
                        //if this tab target string is not currently selected
                        if(selStart!=startStr && selEnd!=endStr){
                            //mark this tab target string's range as the next range to select
                            nextTarget={
                                str:str,
                                range:{
                                    start:startStr,
                                    end:endStr
                                }
                            };
                            break;
                        }else{ //this tab target is already selected
                            //escape this tab target string
                            val=val.replace(str, str.replace('{', '_'));
                        }
                    }
                }else{
                    break;
                }
            }
        }
        return nextTarget;
    };

    //get the next ${tabTarget:index} based on the index
    var getTabTargets=function(inStr){
        var tabTargets;
        //figure out where to place the cursor after the suggestion was written into the textarea
        var matchPoses=inStr.match(/\$\{\w+:\d+\}/g);
        if(matchPoses!=undefined && matchPoses.length>0){
            //for each regex match
            for(var m=0;m<matchPoses.length;m++){
                //get the index value for this regex match
                var index=matchPoses[m].substring(matchPoses[m].indexOf(':')+1);
                index=index.substring(0, index.lastIndexOf('}'));
                index=parseInt(index);
                if(!isNaN(index)){
                    if(tabTargets==undefined){
                        tabTargets={};
                    }
                    if(!tabTargets.hasOwnProperty(index)){
                        tabTargets[index]=[];
                    }
                    tabTargets[index].push(matchPoses[m]);
                }
            }
        }
        return tabTargets;
    };

    //open a float context menu containing options of your choosing
    var openFloatMenu=function(args){
        //get values used to build the float menu
        var id=getArgVal(args, 'id', 'example-float-menu'); args['id']=id;
        var pos=getArgVal(args, 'position', 'mouse-cursor'); args['position']=pos; //"mouse-cursor" | "text-cursor" | {x:num, y:num}
        var closeOnClick=getArgVal(args, 'close_on_click', true); args['close_on_click']=closeOnClick; //"mouse-cursor" | "text-cursor" | {x:num, y:num}
        var layout=getArgVal(args, 'layout', {}); //{rows:num, columns:num}
        layout['columns']=getArgVal(layout, 'columns', 1);
        layout['rows']=getArgVal(layout, 'rows', 5);
        args['layout']=layout;
        var trigger=getArgVal(args, 'trigger'); //the triggering element
        var event=getArgVal(args, 'event'); //the triggering event

        var optionClick=function(option){
            console.log('Clicked: '+option.label+' ('+option.key+')');
        };
        var options=getArgVal(args, 'options', [
            {
                svg:false,
                key:'1',
                label:'[Option 1]',
                click:optionClick
            },
            {
                svg:false,
                key:'2',
                label:'[Option 2]',
                click:optionClick
            }
        ]);
        //close this float menu if it exists
        closeFloatMenu(id);
        //build the float menu
        var wrap=getBuildFloatMenuWrap(id);
        var contentWrap=wrap.find('.float-content');
        //add the options to the menu HTML
        var currentCol=1, optionHtml='';
        for(var o=0;o<options.length;o++){
            var option=options[o];
            if(currentCol==1){
                optionHtml+='<div class="option-row">'; //start option-row
            }

            optionHtml+='<div data-key="'+option.key+'" class="option-wrap">'; //start option-wrap
            optionHtml+='<span class="option-item">'; //start option-item
            if(option.hasOwnProperty('svg')){
                if(typeof option.svg=='string'){
                    optionHtml+='<span class="option-icon">';
                    optionHtml+=option.svg;
                    optionHtml+='</span>';
                }
            }
            optionHtml+='<span class="option-label">';
            optionHtml+=option.label.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            optionHtml+='</span>';
            optionHtml+='</span>'; //end option-item
            optionHtml+='</div>'; //end option-wrap

            if(currentCol==layout.columns || o+1==options.length){
                optionHtml+='</div>'; //end option-row
                currentCol=1;
            }else{
                currentCol++;
            }
        }
        contentWrap.html(optionHtml);
        //add the option hover events
        contentWrap.find('[data-key]').hover(function(){
            selectFloatMenuItem(jQuery(this).parents('.syn-float-menu:first'), jQuery(this));
        });
        //handles writing a suggestion into the textarea
        var writeSuggestion=function(copyArgs, settings, labelSpan, e){
            if(copyArgs.hasOwnProperty('suggest')){
                var txtElem=getFocusTxtElem();
                if(txtElem.length>0){
                    var write=copyArgs.suggest.write;
                    if(write!=undefined){
                        var selStart=txtElem[0].selectionStart, selEnd=txtElem[0].selectionEnd;
                        var val=txtElem.val();
                        //if there is no selection
                        if(selStart==selEnd){
                            //get before and after cursor position
                            var beforeSel=val.substring(0, selStart);
                            var afterSel=val.substring(selEnd);
                            //update select start to remove the replace_left string
                            selStart-=copyArgs.suggest.replace_left.length;
                            setTextSelectRange(txtElem, selStart, selEnd);
                            //cache the updated "old" range
                            setCacheRange(txtElem);
                            //set the new textarea string
                            beforeSel=beforeSel.substring(0, beforeSel.lastIndexOf(copyArgs.suggest.replace_left));
                            txtElem.val(beforeSel+write+afterSel);
                            //update the selection and cursor placement
                            setTextSelectRange(txtElem, (beforeSel.length+write.length));

                            //trigger insert event
                            txtElem[0].dispatchEvent(new InputEvent('input',{
                                bubbles:true,
                                data:dataMesssagePrefix+'[inputType]insertText;[write]'+write
                            }));
                        }else{ //there is a selection
                            //TODO *** allow replace selection
                        }
                        //figure out where to place the cursor after the suggestion was written into the textarea
                        var tabTargets=getTabTargets(write);
                        if(tabTargets!=undefined){
                            var firstPose=tabTargets[0][0];
                            //select the string
                            var val=txtElem.val();
                            var poseIndex=val.lastIndexOf(firstPose, selStart+write.length);
                            setTextSelectRange(txtElem, poseIndex, (poseIndex+firstPose.length));
                        }
                    }
                }
            }
        };
        //add the options click events
        for(var o=0;o<options.length;o++){
            var option=options[o];
            var optionWrap=contentWrap.find('[data-key="'+option.key+'"]:first');
            var labelSpan=optionWrap.find('.option-label:first');
            labelSpan[0]['option_args']=option;
            labelSpan[0]['float_menu_args']=args;
            labelSpan.click(function(e){
                var optionArgs=jQuery(this)[0]['option_args'];
                var settings=jQuery(this)[0]['float_menu_args'];
                //create a copy of the args to send to the click events
                var copyArgs={};
                for(var a in optionArgs){
                    if(optionArgs.hasOwnProperty(a)){
                        if(a!='click'){
                            copyArgs[a]=optionArgs[a];
                        }
                    }
                }
                //write the suggestion if this is an autocomplete menu
                writeSuggestion(copyArgs, settings, jQuery(this), e);
                //if this item has a custom click action
                if(optionArgs.hasOwnProperty('click')){
                    optionArgs['click'](copyArgs, settings, jQuery(this), e);
                }
                //if this item should close the float menu when clicked
                if(labelSpan[0]['float_menu_args']['close_on_click']){
                    closeFloatMenu(jQuery(this).parents('[data-id]:first'));
                }
            });
        }
        //find the widest row and its right value
        var farthestRight=-999, rowHeight=-1, rowCount=0;
        contentWrap.find('.option-row').each(function(){
            if(rowHeight<0){
                rowHeight=jQuery(this).outerHeight();
            }
            var lastOptionWrap=jQuery(this).find('.option-wrap:last');
            var optionRight=lastOptionWrap.outerWidth()+lastOptionWrap.offset().left;
            if(optionRight>farthestRight){
                farthestRight=optionRight;
                jQuery(this).parent().children('.widest').removeClass('widest');
                jQuery(this).addClass('widest');
            }
            rowCount++;
        });
        //if the contentWrap is not matching the widest row width
        var contentWrapRight=contentWrap.outerWidth()+contentWrap.offset().left;
        if(contentWrapRight!=farthestRight){
            var wrapWidth=wrap.outerWidth();
            var diff=farthestRight-contentWrapRight;
            var newWrapWidth=wrapWidth+diff;
            //adjust the menu width to fit the contents
            wrap.css({width:newWrapWidth+'px'});
        }
        //figure out the correct height for the number of rows
        var maxHeight=-1;
        if(rowCount>=layout.rows){
            maxHeight=rowHeight*layout.rows;
        }else{ //if the wrap should be shorter than the max
            maxHeight=rowHeight*rowCount;
        }
        //if the height needs to be adjusted for the number of rows
        var contentWrapHeight=contentWrap.outerHeight();
        if(contentWrapHeight!=maxHeight){
            var diff=maxHeight-contentWrapHeight;
            var newWrapHeight=wrap.outerHeight()+diff;
            //adjust the menu width to fit the contents
            wrap.css({height:newWrapHeight+'px'});
        }
        //get the x/y
        var x=-1, y=-1;
        switch(pos){
            case 'mouse-cursor':
                if(event!=undefined){
                    x=event.clientX;
                    y=event.clientY;
                }
                break;
            case 'text-cursor':
                var xy=getCaretPosXY();
                x=xy.x; y=xy.y;
                break;
            default: // {x:num, y:num}
                break;
        }
        //move the float menu to the correct position
        if(x>-1 && y>-1){
            y-=wrap.outerHeight();
            wrap.css({left:x+'px', top:y+'px'});
            //if hanging off the right of the screen
            var wrapRight=wrap.outerWidth()+wrap.offset().left;
            if(wrapRight>window.innerWidth){
                var diff=wrapRight-window.innerWidth;
                var newLeft=x-diff;
                wrap.css({left:newLeft+'px'});
            }
        }
        //add close button event
        var closeBtn=wrap.find('.close-btn:first');
        closeBtn.click(function(e){
            e.preventDefault(); e.stopPropagation();
            closeFloatMenu(jQuery(this).parents('[data-id]:first'));
        });
        //add mouse leave close event
        wrap.mouseleave(function(){
            closeFloatMenu(jQuery(this));
        });
        //add hover class
        wrap.hover(function(){
            jQuery(this).addClass('hover');
        },function(){
            jQuery(this).removeClass('hover');
        });
        //select first menu item
        selectFloatMenuItem(wrap, 0);
    };

    //public
    return {
        closeFloatMenu:function(id){
            closeFloatMenu(id);
        },
        openFloatMenu:function(args){
            openFloatMenu(args);
        },
        init:function(args){
            initTextareas(args);
            initOnce();
        }
    };
}());
