var editHelper=(function(){
    
    var tabSpaces=4; //syntaxler tab spaces
    var revertDuration=500; //used in animation for field dropping

    /* document['edit_helper_data_history']
    {
        add_edit_field:{
            backup_page:{...}, //only stored for the original data
            backup_fields:{...}, //only stored for the original data
        },
        ...
    }
    */
    
    //HANDLE DATA EDITS
    
    var setData=function(changeType, whichObject, propPath, changeCallback){
        //get the curent screen ID
        var screenId=getCurrentScreenId();

        //backup the data in case it has to be reverted on "cancel" button
        if(screenId!='home'){
            var backupDataForScreen=function(sid){
                //init data object
                if(!document.hasOwnProperty('edit_helper_data_history')){
                    document['edit_helper_data_history']={};
                }
                //if this screen doesn't have init backup data
                if(!document['edit_helper_data_history'].hasOwnProperty(sid)){
                    document['edit_helper_data_history'][sid]={};
                }
                //backup original data for this screen
                if(!document['edit_helper_data_history'][sid].hasOwnProperty('backup_page')){
                    document['edit_helper_data_history'][sid]['backup_page']=getClonePageJson();
                }
                if(!document['edit_helper_data_history'][sid].hasOwnProperty('backup_fields')){
                    document['edit_helper_data_history'][sid]['backup_fields']=getCloneFieldsJson();
                }
            };
            backupDataForScreen(screenId);
            //backup the same data to the referrer screen
            var referrerScreenId=getCurrentReferrerScreenId();
            if(referrerScreenId!=screenId){
                backupDataForScreen(referrerScreenId);
            }
        }
        //change the data
        changeCallback();
        //set save pending indication
        setSavePendingIndicator();

        //console.log(changeType+' '+whichObject+' '+propPath);
    };
    
    //data-add
    var addData=function(htmlField){
        var dataKey=htmlField.attr('data-add');
        if(dataKey!=undefined){
            switch(dataKey){
                case 'field':
                    addField(htmlField);
                    break;
                case 'plain_html_field':
                    addHtmlField(htmlField);
                    break;
                case 'variation':
                    addVariation(htmlField);
                    break;
                case 'section':
                    addSection();
                    break;
            }
        }
    };
    
    //copy a field from one scope to the other (already determined this field is new to the copy-to-scope)
    var copyFieldData=function(fromScope, fieldItemWrap){
        switch(fromScope){
            case 'local': //copy from local into global
                copyFieldIntoGlobal(fieldItemWrap);
                break;
            case 'global': //copy from global into local
                copyFieldIntoLocal(fieldItemWrap);
                break;
        }
    };
    
    //data-delete
    var deleteData=function(htmlField){
        var dataKey=htmlField.attr('data-delete');
        if(dataKey!=undefined){
            var doDelete=false;
            switch(dataKey){
                case 'field':
                    deleteField(htmlField)
                    break;
                case 'variation':
                    deleteVariation(htmlField)
                    break;
                case 'section':
                    deleteSection(htmlField);
                    break;
            }
        }
    };
    
    //data-edit
    var editData=function(htmlField){
        var dataKey=htmlField.attr('data-edit');
        if(dataKey!=undefined){
            //if the value changed since last time
            if(htmlField.val()!=htmlField[0]['latest_value']){
                switch(dataKey){
                    case 'file_id': //page file name/ID
                        editPageId(htmlField); 
                        break;
                    case 'title': //page title
                        editPageTitle(htmlField); 
                        break;
                    case 'outcome': //page outcome
                        editPageOutcome(htmlField);
                        break;
                    case 'search': //page search-terms
                        editPageSearch(htmlField);
                        break;
                    case 'plain_html_field': //field plain-html
                        editPlainHtmlField(htmlField);
                        break;
                    case 'field_id': //field id
                        editFieldId(htmlField);
                        break;
                    case 'field_type': //field type
                        editFieldType(htmlField); 
                        break;
                    case 'field_label': //field label
                        editFieldLabel(htmlField);
                        break;
                    case 'field_class': //field class
                        editFieldClass(htmlField);
                        break;
                    case 'field_summary': //field summary
                        editFieldSummary(htmlField);
                        break;
                    case 'field_values': //field values
                        editFieldValues(htmlField);
                        break;
                    case 'field_variation_id': //field variation id
                        editFieldVariationId(htmlField);
                        break;
                    case 'field_variation': //field variation
                        editFieldVariation(htmlField);
                        break;
                    case 'section_id': //section id
                        editSectionId(htmlField);
                        break;
                    case 'section_title': //section title
                        editSectionTitle(htmlField);
                        break;
                    case 'section_content': //section content
                        editSectionContent(htmlField);
                        break;
                }
                
                //update the latest value cache property
                jQuery('[data-edit="'+dataKey+'"]').each(function(){
                    var latestVal=jQuery(this).val();
                    jQuery(this)[0]['latest_value']=latestVal; //update to the latest value
                });
            }
        }
    };
    
    //data-move
    var moveData=function(htmlField, parentItemSelector){
        var dataKey=htmlField.attr('data-move');
        if(dataKey!=undefined){
            //if the position changed since last time
            var currentIndex=htmlField.parents(parentItemSelector).index();
            var latestIndex=htmlField[0]['latest_index'];
            if(latestIndex!=currentIndex){
                switch(dataKey){
                    case 'field':
                        moveField(htmlField);
                        break;
                    case 'section':
                        moveSection(htmlField);
                        break;
                }
                
                //update the latest position cache property
                jQuery('[data-move="'+dataKey+'"]').each(function(){
                    var latestIndex=jQuery(this).parents(parentItemSelector).index();
                    jQuery(this)[0]['latest_index']=latestIndex; //update to the latest position
                });
                
                //cleanup after move
                switch(dataKey){
                    case 'section':
                        updateSectionNumbering();
                        break;
                }
            }
        }
    };
    
    //HTML
    
    //cancel the data changes made on the current screen, restore history data to previous state
    var revertDataChanges=function(){
        if(document.hasOwnProperty('edit_helper_data_history')){
            var screenId=getCurrentScreenId();
            //if this screen had any back up data
            if(document['edit_helper_data_history'].hasOwnProperty(screenId)){
                if(document['edit_helper_data_history'][screenId].hasOwnProperty('backup_fields')){
                    //just replace the fields data with the backed up data
                    fields=document['edit_helper_data_history'][screenId]['backup_fields'];
                }
                if(document['edit_helper_data_history'][screenId].hasOwnProperty('backup_page')){
                    //just replace the page data with the backed up data
                    pagesHelper.setPageData(document['edit_helper_data_history'][screenId]['backup_page']);
                }
                //clear screen history
                clearDataChanges();
            }
        }
    };
    
    //drop the history of changes made on a screen, for example, when that screen is closed
    var clearDataChanges=function(){
        if(document.hasOwnProperty('edit_helper_data_history')){
            var screenId=getCurrentScreenId();
            //if this screen had any back up data
            if(document['edit_helper_data_history'].hasOwnProperty(screenId)){
                delete document['edit_helper_data_history'][screenId];
            }
        }
    };
    
    //get a backup clone of the current page json data
    var getClonePageJson=function(){
        var pageData=pagesHelper.getPageData();
        return helper.copyJson(pageData);
    };
    
    //get a backup clone of pages json data
    var getClonePagesJson=function(){
        return helper.copyJson(pages);
    }
    
    //get a backup clone of the global fields json data
    var getCloneFieldsJson=function(){
        return helper.copyJson(fields);
    };
    
    var getScopeIcon=function(scope){
        var scopeIcon='';
        switch(scope){
            case 'global': scopeIcon=getGlobalSvg(); break;
            case 'local': scopeIcon=getLocalSvg(); break;
        }
        return scopeIcon;
    };
    
    var getCheckSvg=function(){
        return '<svg class="check" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M432 64l-240 240-112-112-80 80 192 192 320-320z"></path></svg>';
    };
    
    var getLocalSvg=function(){
        return '<svg class="local" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 0c-88.366 0-160 71.634-160 160 0 160 160 352 160 352s160-192 160-352c0-88.366-71.635-160-160-160zM256 256c-53.020 0-96-42.98-96-96s42.98-96 96-96 96 42.98 96 96-42.98 96-96 96z"></path></svg>';
    };
    
    var getGlobalSvg=function(){
        return '<svg class="global" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 448"><path d="M192 32c106 0 192 86 192 192s-86 192-192 192-192-86-192-192 86-192 192-192zM260.5 162.25c-2 1.5-3.25 4.25-5.75 4.75 1.25-0.25 2.5-4.75 3.25-5.75 1.5-1.75 3.5-2.75 5.5-3.75 4.25-1.75 8.5-2.25 13-3 4.25-1 9.5-1 12.75 2.75-0.75-0.75 5.25-6 6-6.25 2.25-1.25 6-0.75 7.5-3 0.5-0.75 0.5-5.5 0.5-5.5-4.25 0.5-5.75-3.5-6-7 0 0.25-0.5 1-1.5 2 0.25-3.75-4.5-1-6.25-1.5-5.75-1.5-5-5.5-6.75-9.75-1-2.25-3.75-3-4.75-5.25-1-1.5-1.5-4.75-3.75-5-1.5-0.25-4.25 5.25-4.75 5-2.25-1.25-3.25 0.5-5 1.5-1.5 1-2.75 0.5-4.25 1.25 4.5-1.5-2-4-4.25-3.5 3.5-1 1.75-4.75-0.25-6.5h1.25c-0.5-2.25-7.5-4.25-9.75-5.75s-14.25-4-16.75-2.5c-3 1.75 0.75 6.75 0.75 9.25 0.25 3-3 3.75-3 6.25 0 4.25 8 3.5 6 9.25-1.25 3.5-6 4.25-8 7-2 2.5 0.25 7 2.25 8.75 2 1.5-3.5 4-4.25 4.5-4.25 2-7.5-4.25-8.5-8-0.75-2.75-1-6-4-7.5-1.5-0.5-6.25-1.25-7.25 0.25-1.5-3.75-6.75-5.25-10.25-6.5-5-1.75-9.25-1.75-14.5-1 1.75-0.25-0.5-8-4.75-6.75 1.25-2.5 0.75-5.25 1.25-7.75 0.5-2 1.5-4 3-5.75 0.5-1 6-6.75 4.25-7 4.25 0.5 9 0.75 12.5-2.75 2.25-2.25 3.25-6 5.5-8.5 3.25-3.75 7.25 1 10.75 1.25 5 0.25 4.75-5.25 2-7.75 3.25 0.25 0.5-5.75-1.25-6.5-2.25-0.75-10.75 1.5-6.25 3.25-1-0.5-7 12-10.5 5.75-1-1.25-1.5-6.5-3.75-6.75-2 0-3.25 2.25-4 3.75 1.25-3.25-7-5.5-8.75-5.75 3.75-2.5 0.75-5.25-2-6.75-2-1.25-8.25-2.25-10-0.25-4.75 5.75 5 6.5 7.5 8 0.75 0.5 3.75 2.25 2 3.5-1.5 0.75-6 2-6.5 3-1.5 2.25 1.75 4.75-0.5 7-2.25-2.25-2.25-6-4-8.5 2.25 2.75-9 1.25-8.75 1.25-3.75 0-9.75 2.5-12.5-1.25-0.5-1-0.5-6.75 1-5.5-2.25-1.75-3.75-3.5-5.25-4.5-8.25 2.75-16 6.25-23.5 10.25 1 0.25 1.75 0.25 3-0.25 2-0.75 3.75-2 5.75-3 2.5-1 7.75-4 10.5-1.75 0.25-0.5 1-1 1.25-1.25 1.75 2 3.5 4 5 6.25-2-1-5.25-0.5-7.5-0.25-1.75 0.5-4.75 1-5.5 3 0.75 1.25 1.75 3.25 1.25 4.5-3.25-2.25-5.75-6-10.25-6.5-2 0-4 0-5.5 0.25-24 13.25-44.25 32.5-58.75 55.5 1 1 2 1.75 3 2 2.5 0.75 0 8 4.75 4.25 1.5 1.25 1.75 3 0.75 4.75 0.25-0.25 10.25 6.25 11 6.75 1.75 1.5 4.5 3.25 5.25 5.25 0.5 1.75-1 3.75-2.5 4.5-0.25-0.5-4-4.25-4.5-3.25-0.75 1.25 0 8 2.75 7.75-4 0.25-2.25 15.75-3.25 18.75 0 0.25 0.5 0.25 0.5 0.25-0.75 3 1.75 14.75 6.75 13.5-3.25 0.75 5.75 12.25 7 13 3.25 2.25 7 3.75 9.25 7 2.5 3.5 2.5 8.75 6 11.5-1 3 5.25 6.5 5 10.75-0.5 0.25-0.75 0.25-1.25 0.5 1.25 3.5 6 3.5 7.75 6.75 1 2 0 6.75 3.25 5.75 0.5-5.5-3.25-11-6-15.5-1.5-2.5-3-4.75-4.25-7.25-1.25-2.25-1.5-5-2.5-7.5 1 0.25 6.5 2.25 6 3-2 5 8 13.75 10.75 17 0.75 0.75 6.5 8.25 3.5 8.25 3.25 0 7.75 5 9.25 7.5 2.25 3.75 1.75 8.5 3.25 12.5 1.5 5 8.5 7.25 12.5 9.5 3.5 1.75 6.5 4.25 10 5.5 5.25 2 6.5 0.25 11-0.5 6.5-1 7.25 6.25 12.5 9 3.25 1.75 10.25 4.25 13.75 2.75-1.5 0.5 5.25 10.75 5.75 11.5 2.25 3 6.5 4.5 9 7.5 0.75-0.5 1.5-1.25 1.75-2.25-1 2.75 3.75 8 6.25 7.5 2.75-0.5 3.5-6 3.5-8-5 2.5-9.5 0.5-12.25-4.5-0.5-1.25-4.5-8.25-1-8.25 4.75 0 1.5-3.75 1-7.25s-4-5.75-5.75-8.75c-1.5 3-6.5 2.25-8-0.25 0 0.75-0.75 2-0.75 3-1.25 0-2.5 0.25-3.75-0.25 0.5-3 0.75-6.75 1.5-10 1.25-4.5 9.5-13.25-1.25-12.75-3.75 0.25-5.25 1.75-6.5 5-1.25 3-0.75 5.75-4.25 7.25-2.25 1-9.75 0.5-12-0.75-4.75-2.75-8-11.5-8-16.5-0.25-6.75 3.25-12.75 0-19 1.5-1.25 3-3.75 4.75-5 1.5-1 3.25 0.75 4-2.25-0.75-0.5-1.75-1.5-2-1.5 3.75 1.75 10.75-2.5 14 0 2 1.5 4.25 2 5.5-0.5 0.25-0.75-1.75-3.75-0.75-5.75 0.75 4.25 3.5 5 7.25 2.25 1.5 1.5 5.5 1 8.25 2.5 2.75 1.75 3.25 4.5 6.5 0.75 2 3 2.25 3 3 6 0.75 2.75 2.25 9.75 4.75 11 5.25 3.25 4-5.5 3.5-8.5-0.25-0.25-0.25-8.5-0.5-8.5-8-1.75-5-8-0.5-12.25 0.75-0.5 6.5-2.5 9-4.5 2.25-2 5-5.5 3.75-8.75 1.25 0 2.25-1 2.75-2.25-0.75-0.25-3.75-2.75-4.25-2.5 1.75-1 1.5-2.5 0.5-4 2.5-1.5 1.25-4.25 3.75-5.25 2.75 3.75 8.25-0.5 5.5-3.5 2.5-3.5 8.25-1.75 9.75-5 3.75 1 1-3.75 3-6.5 1.75-2.25 4.75-2.25 7-3.5 0 0.25 6.25-3.5 4.25-3.75 4.25 0.5 12.75-4 6.25-7.75 1-2.25-2.25-3.25-4.5-3.75 1.75-0.5 4 0.5 5.5-0.5 3.25-2.25 1-3.25-1.75-4-3.5-1-8 1.25-10.75 3zM219.75 381.5c34.25-6 64.75-23 87.75-47.25-1.5-1.5-4.25-1-6.25-2-2-0.75-3.5-1.5-6-2 0.5-5-5-6.75-8.5-9.25-3.25-2.5-5.25-5.25-10-4.25-0.5 0.25-5.5 2-4.5 3-3.25-2.75-4.75-4.25-9-5.5-4-1.25-6.75-6.25-10.75-1.75-2 2-1 5-2 7-3.25-2.75 3-6 0.5-9-3-3.5-8.25 2.25-10.75 3.75-1.5 1.25-3.25 1.75-4.25 3.25-1.25 1.75-1.75 4-2.75 5.75-0.75-2-5-1.5-5.25-3 1 6 1 12.25 2.25 18.25 0.75 3.5 0 9.25-3 12s-6.75 5.75-7.25 10c-0.5 3 0.25 5.75 3 6.5 0.25 3.75-4 6.5-3.75 10.5 0 0.25 0.25 2.75 0.5 4z"></path></svg>'
    };
    
    //set the save-pending class on the current screen(s)
    var setSavePendingIndicator=function(screenIds, setOn){
        if(screenIds==undefined){ screenIds=getCurrentScreenId(); }
        if(setOn==undefined){ setOn=true; }
        var setSavePendingOnScreen=function(screenId){
            var wrap;
            if(screenId=='home'){
                wrap=jQuery('body:first');
            }else{ //is popup screen
                var popupWrap=getPopupWrap(screenId);
                wrap=popupWrap;
            }
            if(setOn){
                wrap.addClass('save-pending');
            }else{
                wrap.removeClass('save-pending');
            }
        };
        if(typeof screenIds=='string'){
            setSavePendingOnScreen(screenIds);
        }else{
            for(var s=0;s<screenIds.length;s++){
                setSavePendingOnScreen(screenIds[s]);
            }
        }
    };
    
    var getNextHtmlFieldId=function(){
        var localWrap=getScopeWrap('local');
        var plainHtmlFields=localWrap.find('.field-items .field-item.plain-html');
        var plainHtmlId=plainHtmlFields.length;
        return plainHtmlId;
    };

    var getEditFieldItemHtml=function(fieldObj){
        var retHtml='';
        var fieldKey=''; var plainHtmlClass=''; var plainHtmlIdAttr=''; var plainHtmlId=-1;
        //if plain html field
        if((typeof fieldObj=='string' && fieldObj.indexOf('html:')==0) || fieldObj.hasOwnProperty('html')){
            plainHtmlClass=' plain-html';
            if(fieldObj.hasOwnProperty('id')){
                plainHtmlId=fieldObj['id'];
            }else{
                plainHtmlId=getNextHtmlFieldId();
            }
            plainHtmlIdAttr=' data-html-id="'+plainHtmlId+'"';
            fieldKey='__html_'+plainHtmlId+'__';
        }else if(fieldObj.hasOwnProperty('id')){ //normal json-format field
            fieldKey=fieldObj['id'];
        }else if(typeof fieldObj=='string'){ //normal string-format id field
            fieldKey=fieldObj; 
        }
        retHtml+='<div class="field-item'+plainHtmlClass+'" data-id="'+fieldKey+'"'+plainHtmlIdAttr+'>';
        retHtml+='<div class="field-id">'; //start .field-id
        if(plainHtmlClass.length<1){
            retHtml+='<span class="accordian-btn"></span>';
            retHtml+='<span data-move="field" class="id">'+fieldKey+'</span>';
        }else{
            retHtml+='<div data-move="field" class="move-html">&lt; &gt; HTML <strong>['+plainHtmlId+']</strong></div>';
        }
        retHtml+='<span data-delete="field" class="delete-btn" title="delete '+fieldKey+'"></span>';
        retHtml+='</div>'; //end .field-id

        retHtml+='</div>';
        return retHtml;
    };
    
    //get the html for the "+ add/edit fields" page
    var getAddEditField_HTML=function(fieldId){
        var html='';
        
        var pageData=pagesHelper.getPageData();
        
        var getAddFieldBtnHtml=function(newFieldPh){
            var addBtnHtml='';
            addBtnHtml+='<div class="new-field-wrap"><input data-add="field" class="add-new-field" type="text" spellcheck="false" placeholder="['+newFieldPh+']"></div>';
            return addBtnHtml;
        };
        
        html+='<div class="editFieldsWrap">'; //start .editFieldsWrap
        
        html+='<div data-scope="global" class="globalWrap">'; //start .globalWrap
        html+='<div class="titleWrap">'; //start .titleWrap
        html+=getGlobalSvg();
        html+='<span>Global</span>';
        html+='</div>'; //end .titleWrap
        html+='<div class="contentWrap">'; //start .contentWrap
        html+='<div class="field-items">'; //start .field-items
        //if add/edit all fields
        if(fieldId==undefined){
            for(var f in fields){
                if(fields.hasOwnProperty(f)){
                    var fieldData=fields[f];
                    fieldData['id']=f;
                    html+=getEditFieldItemHtml(fieldData);
                }
            }
        }else{ //edit screen for only one field
            if(fields.hasOwnProperty(fieldId)){
                fields[fieldId]['id']=fieldId;
                html+=getEditFieldItemHtml(fields[fieldId]);
            }
        }
        html+='</div>'; //end .field-items
        if(fieldId==undefined){
            html+=getAddFieldBtnHtml('new global field');
        }
        html+='</div>'; //end .contentWrap
        html+='</div>'; //end .globalWrap
        
        html+='<div data-scope="local" class="localWrap">'; //start .localWrap
        html+='<div class="titleWrap">'; //start .titleWrap
        html+=getLocalSvg();
        html+='<span>This Page</span>';
        html+='</div>'; //end .titleWrap
        html+='<div class="contentWrap">'; //start .contentWrap
        html+='<div class="field-items">'; //start .field-items
        if(pageData.hasOwnProperty('fields')){
            //if add/edit all fields
            if(fieldId==undefined){
                for(var f=0;f<pageData['fields'].length;f++){
                    html+=getEditFieldItemHtml(pageData['fields'][f]);
                }
            }else{ //edit screen for only one field
                var fieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
                if(fieldIndex!=-1){
                    html+=getEditFieldItemHtml(pageData['fields'][fieldIndex]);
                }
            }
        }
        html+='</div>'; //end .field-items
        if(fieldId==undefined){
            html+=getAddFieldBtnHtml('new page field');
            html+='<div data-add="plain_html_field" class="add-html-btn">add HTML</div>';
        }
        html+='</div>'; //end .contentWrap
        html+='</div>'; //end .localWrap
        
        html+='</div>'; //end .editFieldsWrap
        
        return html;
    };
    
    //get one item for the left nav of the variation edit screen
    var getVariationListItem_HTML=function(variationId, isActive){
        var html='';
        if(isActive==undefined){ isActive=false; }
        var activeClass='';
        if(isActive){
            activeClass=' active';
        }
        html+='<div data-id="'+variationId+'" class="variation-list-btn'+activeClass+'"><span>'+variationId+'</span></div>';
        return html;
    };
    
    var getDefaultVariationContent=function(){
        return '/* ... your modifications to val ... */';
    };
    
    //get one screen for the variation admin
    var getVariationScreen_HTML=function(fieldId, scope, variationId, isActive){
        var html='';
        if(isActive==undefined){ isActive=false; }
        var activeClass='';
        if(isActive){
            activeClass=' active';
        }
        //get all possible values assigned to either global OR local data
        var pageFieldData=fieldsHelper.getFieldData(fieldId, {global:false, page:true, default:true});
        var globalFieldData=fieldsHelper.getFieldData(fieldId, {global:true, page:false, default:true});
        var valsArray=[]; var inheritedClass='';
        if(pageFieldData!=undefined){
            if(pageFieldData.hasOwnProperty('values')){
                for(var v=0;v<pageFieldData['values'].length;v++){
                    var val=pageFieldData['values'][v];
                    if(valsArray.indexOf(val)==-1){
                        valsArray.push(val);
                    }
                }
            }
            if(scope=='local'){
                if(!pageFieldData.hasOwnProperty('variations')){
                    inheritedClass=' inherited-variation';
                }
            }
        }
        if(globalFieldData!=undefined){
            if(globalFieldData.hasOwnProperty('values')){
                for(var v=0;v<globalFieldData['values'].length;v++){
                    var val=globalFieldData['values'][v];
                    if(valsArray.indexOf(val)==-1){
                        valsArray.push(val);
                    }
                }
            }
        }
        var testVal='test';
        if(valsArray.length>0){
            testVal=valsArray[0];
        }
        
        html+='<div data-id="'+variationId+'" class="var-screen'+inheritedClass+activeClass+'">'; //start var-screen
        
        html+='<div class="top-fields">'; //start top-fields
        
        html+='<div class="var-field var-id">'; //start var-id
        html+='<label>ID</label>'; 
        html+='<input type="text" data-edit="field_variation_id" spellcheck="false" value="'+variationId+'" />'; 
        html+='</div>'; //end var-id
        
        html+='<div class="var-field var-val">'; //start var-val
        html+='<label>test val</label>'; 
        var listAttr=fieldId+'_'+variationId+'_values';
        html+='<input list="'+listAttr+'" type="text" spellcheck="false" value="'+testVal+'" />'; 
        html+='<datalist id="'+listAttr+'">';
        if(valsArray.length>0){
            valsArray.sort();
            for(var v=0;v<valsArray.length;v++){
                html+='<option>'+valsArray[v]+'</option>';
            }
        }
        html+='</datalist>';
        html+='</div>'; //end var-val
        
        html+='<div class="var-field var-delete">';
        html+='<span data-delete="variation" class="delete-variation-btn" title="delete this entire variation"></span>';
        html+='</div>';
        
        html+='</div>'; //end top-fields
        
        html+='<div class="code-wrap">'; //start code-wrap
        html+='<div class="code-head"><span class="var-id">'+variationId+'</span>: function ( val ) { </div>';
        html+='<div class="code-scroll">'; //start code-scroll
        
        var locAndGlobData=getLocalAndGlobalVals(fieldId, 'variations');
        var pageVal=locAndGlobData['local_page'];
        var globalVal=locAndGlobData['global'];
        var ctlClass=getFieldCtlClass(scope, fieldId, 'variations', pageVal, globalVal);
        //figure out the function content for this variation
        var setVal=getSetVal(ctlClass, pageVal, globalVal);
        var varString=getDefaultVariationContent();
        if(setVal!=undefined){
            var varFunction=setVal[variationId];
            if(varFunction!=undefined){
                varString=varFunction.toString();
            }
        }
        //clean up the function content
        varString=varString.trim();
        if(varString.indexOf('function')===0){
            varString=varString.substring(varString.indexOf('{')+'{'.length);
            var endBrackIndex=varString.lastIndexOf('}');
            if(endBrackIndex!=-1){
                varString=varString.substring(0, endBrackIndex);
            }
        }
        html+='<textarea data-edit="field_variation">'+varString+'</textarea>';
        
        html+='</div>'; //end code-scroll
        html+='<div class="code-foot">return val; }</div>';
        html+='</div>'; //end code-wrap
        
        html+='<div class="preview-wrap">'; //start .preview-wrap
        html+='<div class="preview-head"><label>Preview Output</label><div class="update-preview-btn">Update Output</div></div>';
        html+='<div class="preview-content"><textarea readonly="readonly" spellcheck="false"></textarea></div>';
        html+='</div>'; //end .preview-wrap
        
        html+='</div>'; //end var-screen
        
        return html;
    };
    
    //get the html for the "+ add/edit variations" page
    var getAddEditVariations_HTML=function(fieldId, scope){
        var html='';
        
        var scopeIcon=getScopeIcon(scope);
        var locAndGlobData=getLocalAndGlobalVals(fieldId, 'variations');
        var pageVal=locAndGlobData['local_page'];
        var globalVal=locAndGlobData['global'];
        var ctlClass=getFieldCtlClass(scope, fieldId, 'variations', pageVal, globalVal);
        var setVal=getSetVal(ctlClass, pageVal, globalVal);
        
        var varIdArray=[];
        for(var v in setVal){
            if(setVal.hasOwnProperty(v)){
                varIdArray.push(v); //add the variation's id to the array
            }
        }
    
        var firstVarId='', noVarsClass='';
        var itemsHtml=''; var screensHtml='<p class="no-variations-msg">No variations.</p>';
        if(varIdArray.length>0){
            varIdArray.sort();
            firstVarId=varIdArray[0];
        }else{
            noVarsClass=' no-variations';
        }

        for(var v=0;v<varIdArray.length;v++){
            var isActive=false;
            if(v==0){isActive=true;}
            itemsHtml+=getVariationListItem_HTML(varIdArray[v], isActive);
            screensHtml+=getVariationScreen_HTML(fieldId, scope, varIdArray[v], isActive);
        }
        
        var varPrefix=tokensHelper.getVariationPrefix();

        html+='<div data-scope="'+scope+'" class="editVariationsWrap'+noVarsClass+'">'; //start outer wrap
        html+='<div class="titleWrap" title="variation token">'+scopeIcon+'<span class="field-id">'+fieldId+'</span><span class="prefix">'+varPrefix+'</span><span class="variation-key">'+firstVarId+'</span></div>';
        html+='<div class="contentWrap">'; //start content wrap
        
        html+='<div class="var-screens">'; //start var-screens
        html+=screensHtml;
        html+='</div>'; //end var-screens
        
        html+='<div class="var-listing">'; //start var listing
        
        html+='<div class="variation-list-btns">'; //start variation-list-btns
        html+=itemsHtml;
        html+='</div>'; //end variation-list-btns
        html+='<input data-add="variation" class="add-new-variation" type="text" spellcheck="false" placeholder="[new variation id]" />';
        html+='</div>'; //end var listing
        
        html+='</div>'; //end content wrap
        html+='</div>'; //end outer wrap
        
        return html;
    };
    
    //remove the scope classes from the property field
    var removeFieldCtlClasses=function(propWrap){
        var classNames=['local', 'global', 'not-set', 'override'];
        var removedNames=[];
        for(var c=0;c<classNames.length;c++){
            if(propWrap.hasClass(classNames[c])){
                propWrap.removeClass(classNames[c]);
                removedNames.push(classNames[c]);
            }
        }
        return removedNames;
    };
    
    //decide what class should be given to a config property, given its scope and value fall-back
    var getFieldCtlClass=function(scope, fieldId, propName, pageVal, globalVal){
        var ctlClass='';

        if(pageVal==undefined && globalVal==undefined){ //neither local nor global
            ctlClass='not-set'; 
        }else if(pageVal!=undefined && globalVal==undefined){ //only local
            switch(scope){
                case 'local': ctlClass='local'; break;
                case 'global': ctlClass='not-set override'; break;
            }
        }else if(pageVal==undefined && globalVal!=undefined){ //only global
            switch(scope){
                case 'local': ctlClass='global'; break;
                case 'global': ctlClass='global'; break;
            }
        }else if(pageVal!=undefined && globalVal!=undefined){ //both local and global
            switch(scope){
                case 'local': ctlClass='local'; break;
                case 'global': ctlClass='global override'; break;
            }
        }

        return ctlClass;
    }
    
    //returns the id of a popup, if open, or "home" if no popup is open
    var getCurrentScreenId=function(){
        var id='home';
        var openPopupWrap=jQuery('.popups .popup.open').not('[data-referrer]');
        if(openPopupWrap.length>0){
            id=openPopupWrap.attr('popup-id');
        }
        return id;
    };
    
    //disable the ok button on the given screen
    var disableOkForSreen=function(screenId){
        if(screenId==undefined){ screenId=getCurrentScreenId(); }
        var screenWrap=jQuery('.popups [popup-id="'+screenId+'"]:first');
        if(screenWrap.length>0){
            screenWrap.find('.popup-btns .btn-ok').addClass('disabled');
        }
    };
    
    //enable the ok button on the given screen
    var enableOkForSreen=function(screenId){
        if(screenId==undefined){ screenId=getCurrentScreenId(); }
        var screenWrap=jQuery('.popups [popup-id="'+screenId+'"]:first');
        if(screenWrap.length>0){
            screenWrap.find('.popup-btns .btn-ok').removeClass('disabled');
        }
    };
    
    //return the current referring screen ID, or the current screen ID, if no referrer
    var getCurrentReferrerScreenId=function(){
        var refId=undefined;
        var screenId=getCurrentScreenId();
        var referWrap=jQuery('.popups .popup[data-referrer="'+screenId+'"]:first');
        if(referWrap.length>0){
            refId=referWrap.attr('popup-id'); //referring screen ID
        }else{
            refId=screenId; //current screen ID
        }
        return refId;
    };
    
    //get the value that will be used, given the value that's set locally and globally
    var getSetVal=function(ctlClass, pageVal, globalVal){
        var setVal;
        switch(ctlClass){
            case 'local': setVal=pageVal; break;
            case 'global': setVal=globalVal; break;
            case 'global override': setVal=globalVal; break;
            case 'not-set': setVal=''; break;
        }
        return setVal;
    };
    
    var getHtmlSaveContentHead=function(folderScope){
        var html='';
        var headEl=jQuery('head:first');
        var cloneHeadEl=headEl.clone();
        var titleEl=cloneHeadEl.find('title:first');
        //make title element empty
        titleEl.remove(); //no need to capture dynamic title
        //remove any dynamic inserted elements for the server
        cloneHeadEl.find('[id]').remove();
        //if the folder scope for this head section is given
        if(folderScope!=undefined){
            if(folderScope.indexOf('/')===0){ folderScope=folderScope.substring(1); }
            if(folderScope.lastIndexOf('/')===folderScope.length-1){ 
                folderScope=folderScope.substring(0, folderScope.lastIndexOf('/')); 
            }
            var folderDepth=folderScope.split('/').length;
            var relUp='../';
            //make sure there are correct amount of ../ relative path 
            for(var r=0;r<folderDepth;r++){ relUp+='../'; }
            //set the path
            var setRelPath=function(el, attr){
                var val=el.attr(attr);
                val=val.substring(val.lastIndexOf('../')+'../'.length);
                el.attr(attr, relUp+val);
            };
            cloneHeadEl.children('link[href^="../"]').each(function(){
                setRelPath(jQuery(this), 'href');
            });
            cloneHeadEl.children('script[src^="../"]').each(function(){
                setRelPath(jQuery(this), 'src');
            });
        }
        //wrap head tags around html
        html+='<head>\n    '+cloneHeadEl.html().trim()+'\n</head>\n';
        return html;
    };
    
    //get a brand new blank html page content
    var getHtmlSaveContentNew=function(folderScope){
        var html='<html>\n';
        html+=getHtmlSaveContentHead(folderScope);
        html+='<body>\n\n\n\n';
        var newPageJson=getNewPageJson();
        for(var s=0;s<newPageJson.sections.length;s++){
            var section=newPageJson.sections[s];
            html+='<div id="'+section.id+'"></div>\n\n\n\n';
        }
        html+='</body>\n';
        html+='</html>';
        return html;
    };
    
    //get the page content to save into file
    var getHtmlSaveContent=function(){
        var html='';
        //header
        html+=getHtmlSaveContentHead();
        //body
        html+='<body>\n\n\n\n';
        var tokenWrap=jQuery('.token-wrap:first');
        var pageData=pagesHelper.getPageData();
        for(var s=0;s<pageData['sections'].length;s++){
            var sectionData=pageData['sections'][s];
            var id=sectionData['id'];
            html+='<div id="'+id+'">\n';
            var sectionDiv=tokenWrap.find('div[id="'+id+'"][name="'+id+'"]');
            var textarea=sectionDiv.find('textarea[data-edit="section_content"]:first');
            html+=textarea.val().trim();
            html+='\n</div>\n\n\n\n';
        }
        html+='</body>\n';
        return '<html>\n'+html+'</html>';
    };
    
    //get json for a brand new page
    var getNewPageJson=function(){
        var defaultSectionTitle=getDefaultSectionTitle(0);
        return {
            "sections":[
                {
                    "id":"s1",
                    "title":defaultSectionTitle
                }
            ]
        };
    };
    
    //get a cleaned up and shortened version of the pages json data to write to file
    var getPagesJsSaveJson=function(){
        var clonePages=getClonePagesJson();
        for(var pageId in clonePages){
            if(clonePages.hasOwnProperty(pageId)){
                //for one page object
                
                //remove redundant page id
                if(clonePages[pageId].hasOwnProperty('id')){
                    delete clonePages[pageId]['id'];
                }
                
                //for the fields of this page
                if(clonePages[pageId].hasOwnProperty('fields')){
                   for(var f=0;f<clonePages[pageId]['fields'].length;f++){
                       //if field is NOT already in string form
                       if(typeof clonePages[pageId]['fields'][f]!='string'){
                           //if this is plain html
                           if(clonePages[pageId]['fields'][f].hasOwnProperty('html')){
                                //simplify this html object field into string data
                                clonePages[pageId]['fields'][f]='html:'+clonePages[pageId]['fields'][f]['html'];
                           }else{ //not a plain html field
                               
                                var fieldId=clonePages[pageId]['fields'][f]['id'];
                               
                                //remove the type if default type
                                if(clonePages[pageId]['fields'][f].hasOwnProperty('type')){
                                    if(clonePages[pageId]['fields'][f]['type']==fieldsHelper.getDefaultFieldType()){
                                        delete clonePages[pageId]['fields'][f]['type'];
                                    }
                                }
                               
                                //remove the label if default label
                                if(clonePages[pageId]['fields'][f].hasOwnProperty('label')){
                                    if(clonePages[pageId]['fields'][f]['label']==fieldsHelper.getDefaultFieldLabel()){
                                        delete clonePages[pageId]['fields'][f]['label'];
                                    }
                                }
                               
                                //remove the summary if default summary
                                if(clonePages[pageId]['fields'][f].hasOwnProperty('summary')){
                                    if(clonePages[pageId]['fields'][f]['summary']==fieldsHelper.getDefaultFieldSummary()){
                                        delete clonePages[pageId]['fields'][f]['summary'];
                                    }
                                }
                               
                                //remove the class if blank
                                if(clonePages[pageId]['fields'][f].hasOwnProperty('class')){
                                    if(clonePages[pageId]['fields'][f]['class'].trim().length<1){
                                        delete clonePages[pageId]['fields'][f]['class'];
                                    }
                                }
                               
                                //remove the values if empty
                                if(clonePages[pageId]['fields'][f].hasOwnProperty('values')){
                                    if(clonePages[pageId]['fields'][f]['values'].length<1){
                                        delete clonePages[pageId]['fields'][f]['values'];
                                    }
                                }
                               
                                //remove the variations if empty
                                if(clonePages[pageId]['fields'][f].hasOwnProperty('variations')){
                                    if(Object.keys(clonePages[pageId]['fields'][f]['variations']).length<1){
                                        delete clonePages[pageId]['fields'][f]['variations'];
                                    }
                                }
                               
                                //if there are no properties left in this object, except for the field ID
                               if(Object.keys(clonePages[pageId]['fields'][f]).length<2){
                                   if(fieldId!=undefined){
                                        //convert to a string instead of an object
                                        clonePages[pageId]['fields'][f]=fieldId;
                                   }
                               }
                           }
                       }
                   }
                }
            }
        }
        return clonePages;
    };
    
    //get a cleaned up and shortened version of the fields json data to write to file
    var getFieldsJsSaveJson=function(){
        var cloneFields=getCloneFieldsJson();
        for(var fieldId in cloneFields){
            if(cloneFields.hasOwnProperty(fieldId)){
                
                //remove redundant field ID
                if(cloneFields[fieldId].hasOwnProperty('id')){
                    delete cloneFields[fieldId]['id'];
                }
                
                //remove the type if default type
                if(cloneFields[fieldId].hasOwnProperty('type')){
                    if(cloneFields[fieldId]['type']==fieldsHelper.getDefaultFieldType()){
                        delete cloneFields[fieldId]['type'];
                    }
                }

                //remove the label if default label
                if(cloneFields[fieldId].hasOwnProperty('label')){
                    if(cloneFields[fieldId]['label']==fieldsHelper.getDefaultFieldLabel()){
                        delete cloneFields[fieldId]['label'];
                    }
                }

                //remove the summary if default summary
                if(cloneFields[fieldId].hasOwnProperty('summary')){
                    if(cloneFields[fieldId]['summary']==fieldsHelper.getDefaultFieldSummary()){
                        delete cloneFields[fieldId]['summary'];
                    }
                }

                //remove the class if blank
                if(cloneFields[fieldId].hasOwnProperty('class')){
                    if(cloneFields[fieldId]['class'].trim().length<1){
                        delete cloneFields[fieldId]['class'];
                    }
                }

                //remove the values if empty
                if(cloneFields[fieldId].hasOwnProperty('values')){
                    if(cloneFields[fieldId]['values'].length<1){
                        delete cloneFields[fieldId]['values'];
                    }
                }

                //remove the variations if empty
                if(cloneFields[fieldId].hasOwnProperty('variations')){
                    if(Object.keys(cloneFields[fieldId]['variations']).length<1){
                        delete cloneFields[fieldId]['variations'];
                    }
                }
            }
        }
        return cloneFields;
    };
    
    //get a page name that doesn't exist yet
    var getUniqueFileNameInScope=function(suggestName, callback, folderScope){
        var startWith='page';
        suggestName=suggestName.trim();
        if(suggestName.length<1){ suggestName=startWith; }
        var currentScope=pagesHelper.getPageScopeFolder();
        if(folderScope==undefined){ folderScope=currentScope; }
        //if need to get unique name for the current scope
        if(currentScope==folderScope){
            if(pages.hasOwnProperty(suggestName)){
                var num=1;
                while(pages.hasOwnProperty(startWith+'_'+num)){
                    num++;
                }
                //return the unique file name for the current scope
                callback(startWith+'_'+num);
            }else{
                //return the unique file name for the current scope
                callback(suggestName);
            }
        }else{ //need to get a unique file name in a different scope
            
            //check with the server to see what unique file name can be suggested for the given scope
            serverHelper.getUniqueFileNameInScope(suggestName, callback, folderScope, startWith);
        }
    };
    
    //exclude illegal file name characters from the given name value
    var getCleanFileNameVal=function(val){
        val=val.trim();
        if(val.length>0){
            val=val.replace(/\.html/gi, '');
            val=val.trim();
            val=val.replace(/ /g, '_');
            val=val.replace(/[^a-z0-9_]/gi, '');
        }
        return val;
    };
    
    //get the data/pages.js content to save into file
    var getPagesJsSaveContent=function(){
        var clonePages=getPagesJsSaveJson();
        //convert the json to a string
        var pagesStr=helper.jsonToString(clonePages);
        return 'var pages = '+pagesStr+';';
    };

    //get the data/fields.js content to save into file
    var getFieldsJsSaveContent=function(){
        var cloneFields=getFieldsJsSaveJson();
        //convert the json to a string
        var fieldsStr=helper.jsonToString(cloneFields);
        return 'var fields = '+fieldsStr+';';
    };
    
    //open the screen where it's possible to create a new page
    var openNew=function(){
        var isEditMode=pagesHelper.isPageMode('edit');
        if(isEditMode){
            //popup HTML
            var getScreenHtml=function(){
                var html='';
                
                html+='<div class="browse-wrap">'; //start browse-wrap
                html+='loading...';
                html+='</div>'; //end browse-wrap
                
                html+='<div class="path-wrap">'; //start path-wrap
                
                html+='<div class="path-row folder-scope">'; //start folder-scope
                html+='<label>Scope'+pagesHelper.getDirectorySvg()+'</label>';
                html+='<input placeholder="select directory" readonly="readonly" spellcheck="false" type="text" />';
                html+='</div>'; //end folder-scope
                
                html+='<div class="path-row page-id">'; //start page-id
                html+='<label>File'+pagesHelper.getHtmlFileSvg()+'</label>';
                html+='<input placeholder="new file name" spellcheck="false" type="text" />';
                html+='<div title="create new page" class="create-file-btn">Create</div>';
                html+='</div>'; //end page-id
                
                html+='</div>'; //end browse-wrap
                
                return html;
            };
            //popup events
            var initOpenNewEvents=function(){
                var popupWrap=getPopupWrap('create_new_page');
                var browseWrap=popupWrap.find('.browse-wrap:first');
                //enable create button method
                var enableCreateBtn=function(){
                    var btn=popupWrap.find('.create-file-btn:first');
                    btn.addClass('enabled');
                };
                //disable create button method
                var disableCreateBtn=function(){
                    var btn=popupWrap.find('.create-file-btn:first');
                    btn.removeClass('enabled');
                };
                //set create button disabled/enabled
                var validateCreateBtn=function(){
                    //reset create button
                    disableCreateBtn();
                    //get folder and file inputs
                    var folderInput=popupWrap.find('.path-row.folder-scope input:first');
                    var fileInput=popupWrap.find('.path-row.page-id input:first');
                    //if there is a folder selected
                    if(folderInput.val().length>0){
                        //make sure the file value is valid
                        var validVal=getCleanFileNameVal(fileInput.val());
                        //make sure the file name is unique within the selected scope
                        getUniqueFileNameInScope(validVal, function(uniqueFile){
                            //set unique value
                            if(fileInput.val()!=uniqueFile){ fileInput.val(uniqueFile); }
                            //enable create button
                            enableCreateBtn();
                        }, folderInput.val());
                    }
                };
                //show the browse folders
                serverHelper.initContentFolders(browseWrap, '', {
                    replace_contents:true,
                    allow_goto_page:false,
                    select_folders:true,
                    right_click:{
                        create_folder:true,
                        rename_folder:true
                    },
                    on_select_folder:function(dirSpan){
                        var contentWrap=dirSpan.parents('.popup-content:first');
                        var scopeInput=contentWrap.find('.path-row.folder-scope input:first');
                        //get the folder path
                        var scopePath='';
                        dirSpan.parents('li').each(function(){
                            var nameSpan=jQuery(this).children('.file-name:first');
                            var dirName=nameSpan.text();
                            if(scopePath.length>0){
                               dirName+='/'; 
                            }
                            scopePath=dirName+scopePath;
                        });
                        //update the scope with the folder path
                        scopeInput.val(scopePath);
                        //enable/disable create button
                        validateCreateBtn();
                    }
                });
                //file input events
                var fileInput=popupWrap.find('.path-row.page-id input:first');
                fileInput.keydown(function(){
                    disableCreateBtn();
                });
                fileInput.blur(function(){
                    validateCreateBtn();
                });
                fileInput.keyup(function(e) {
                    switch (e.keyCode) {
                        case 13: //enter key press
                            e.preventDefault();
                            validateCreateBtn();
                            break;
                    }
                });
                //button click event
                var btn=popupWrap.find('.create-file-btn:first');
                btn.click(function(e){
                    e.preventDefault();
                    if(jQuery(this).hasClass('enabled')){
                        //get folder and file inputs
                        var folderInput=popupWrap.find('.path-row.folder-scope input:first');
                        var fileInput=popupWrap.find('.path-row.page-id input:first');
                        var folderScope=folderInput.val();
                        var fileName=fileInput.val();
                        serverHelper.createNewPage(folderScope, fileName);
                    }
                });
                //set default new file name
                validateCreateBtn();
            };
            //open popup
            openPopup({
                id:'create_new_page',
                title:'Create New Page',
                summary:'Right click to add or rename folders. Select folder. Name the new page. Create.',
                sets_data:false,
                noscroll:true,
                persist_content:false,
                html:getScreenHtml,
                html_events:initOpenNewEvents
            });
        }
    };
    
    //open the see data screen to review raw data that should be saved into files
    var openSeeData=function(){
        var isEditMode=pagesHelper.isPageMode('edit');
        if(isEditMode){
            //popup HTML
            var getScreenHtml=function(){
                var html='';

                var createFileHtml=function(key, path){
                    html+='<div data-file="'+key+'" class="see-file">'; //start file
                    html+='<h3 class="file-path">'+path+'</h3>';
                    html+='<div class="data-wrap">';
                    //html+='<textarea spellcheck="false" class="file-content">'+content+'</textarea>';
                    html+='</div>';
                    html+='</div>'; //end file
                };

                var pageName=pagesHelper.getPageId() + '.html';
                var pageFolder=pagesHelper.getPageScopeFolder();

                html+='<h2 class="root-dir">'+pageFolder+'/...</h2>';

                createFileHtml('fields', 'data/fields.js');
                createFileHtml('pages', 'data/pages.js');
                createFileHtml('content', pageName);

                return html;
            };
            //popup events
            var initSeeDataEvents=function(){
                var popupWrap=getPopupWrap('see_data');
                //accordion sections
                popupWrap.find('.see-file .file-path').not('.init-accordion').each(function(){
                    jQuery(this).addClass('init-accordion');
                    jQuery(this).click(function(e){
                        e.preventDefault(); e.stopPropagation();
                        var wrap=jQuery(this).parents('.see-file:first');
                        if(wrap.hasClass('open')){
                            wrap.removeClass('open');
                        }else{
                            wrap.addClass('open');
                            var dataWrap=wrap.children('.data-wrap:first');
                            var loadedData=dataWrap.children('.see-data:first');
                            if(loadedData.length<1){
                                var highlighter, func_highlighter, data_highlighter;
                                var html='<textarea spellcheck="false" class="file-content">';
                                switch(wrap.attr('data-file')){
                                    case 'fields':
                                        var fieldsStr=getFieldsJsSaveContent();
                                        html+=fieldsStr;
                                        highlighter=highlightHelper.hl_js();
                                        func_highlighter=highlightHelper.funcs_js();
                                        data_highlighter=highlightHelper.data_js();
                                        break;
                                    case 'pages':
                                        var pagesStr=getPagesJsSaveContent();
                                        html+=pagesStr;
                                        highlighter=highlightHelper.hl_jsTokens();
                                        func_highlighter=highlightHelper.funcs_jsTokens();
                                        data_highlighter=highlightHelper.data_jsTokens();
                                        break;
                                    case 'content':
                                        var contentStr=getHtmlSaveContent();
                                        html+=contentStr;
                                        highlighter=highlightHelper.hl_fullHtmlTokens();
                                        func_highlighter=highlightHelper.funcs_fullHtmlTokens();
                                        data_highlighter=highlightHelper.data_fullHtmlTokens();
                                        break;
                                }
                                html+='</textarea>';
                                dataWrap.append(html);
                                //init the textarea line numbers and heights
                                syntaxlerHelper.init({
                                    textareas:dataWrap.children('.file-content:first'),
                                    classes:['see-data'],
                                    tab_spaces:tabSpaces,
                                    line_numbers:true,
                                    readonly:true,
                                    highlight_data:data_highlighter,
                                    highlight_functions:func_highlighter,
                                    highlights:highlighter
                                });
                            }
                        }
                    });
                });
            };
            //open popup
            openPopup({
                id:'see_data',
                title:'See Data',
                summary:'Preview save data here. Manually copy and paste if you are not connected to a node server for file-writing.',
                buttons:[
                    {
                        label:'Back',
                        class:'btn-cancel'
                    }
                ],
                sets_data:false,
                noscroll:false,
                persist_content:false,
                html:getScreenHtml,
                html_events:initSeeDataEvents
            });
        }
    };
    
    var setValuesClickBarContent=function(clickBar, editArea, valArray){
        var clickBarVal='';
        var editAreaVal='';
        if(valArray.length>0){
            //for each value
            for(var s=0;s<valArray.length;s++){
                var val=valArray[s];
                val=val.trim();
                if(val.length>0){
                    var cVal=val;
                    //add to the clickbar
                    cVal=helper.replaceAll(cVal, "\\\"", ".|__q__|.");
                    cVal=helper.replaceAll(cVal, "\"", ".|__q__|.");
                    cVal=helper.replaceAll(cVal, "|__q__|.", "\\\"");
                    if(clickBarVal.length>0){ clickBarVal+=', '; }
                    clickBarVal+='"'+cVal+'"';
                    //add to the edit area
                    if(editAreaVal.length>0){ editAreaVal+='\n'; }
                    editAreaVal+=val;
                }
            }
        }
        clickBar.val(clickBarVal);
        editArea.val(editAreaVal);
    };
    
    //get the wrapper for the given scope
    var getScopeWrap=function(scope){
        return jQuery('.popups [popup-id="add_edit_field"] [data-scope="'+scope+'"]:first');
    };
    
    //get the scope, "local" or "global" for property edit control wrap
    var getFieldItemScope=function(fieldItemWrap){
        var scope;
        //if this is a page field on the home screen
        if(fieldItemWrap.attr('data-token')!=undefined){
            scope='local';
        }else if(fieldItemWrap.hasClass('plain-html')){
            scope='local';
        }else{ //this is a field in the admin popup screen
            //get scope, global or local
            var scopeEl=fieldItemWrap.parents('[data-scope]:first');
            scope=scopeEl.attr('data-scope');
        }
        return scope;
    };
    
    var getVariationScreenWrap=function(varId){
        var screenWrap=jQuery('.popups [popup-id="add_edit_variations"] .var-screens .var-screen[data-id="'+varId+'"]:first');
        return screenWrap;
    };
    
    var getVariationListBtn=function(varId){
        var btn=jQuery('.popups [popup-id="add_edit_variations"] .var-listing .variation-list-btn[data-id="'+varId+'"]:first');
        return btn;
    };
    
    //select which variation to view
    var viewVariation=function(varId){
        var screenWrap=getVariationScreenWrap(varId);
        if(screenWrap.length>0){
            if(!screenWrap.hasClass('active')){
                var btnWrap=getVariationListBtn(varId);
                //remove other active
                screenWrap.parent().children('.var-screen.active').removeClass('active');
                btnWrap.parent().children('.variation-list-btn.active').removeClass('active');
                //set active
                btnWrap.addClass('active');
                screenWrap.addClass('active');
                //update variation postfix in header
                var scopeWrap=screenWrap.parents('[data-scope]:first');
                var varSpan=scopeWrap.find('.titleWrap .variation-key:first');
                varSpan.html(varId);
                //init text highlighting for this variation's code, if needed
                initVariationSyntaxler();
            }
        }
    };
    
    //get the field item, given it's id
    var getFieldItemWrap=function(fieldId, scope){
        var wrap=jQuery('.popups .editFieldsWrap:first');
        if(scope!=undefined){
            wrap=getScopeWrap(scope);
        }
        var fieldItemWrap=wrap.find('.field-item[data-id="'+fieldId+'"]:first');
        return fieldItemWrap;
    };
    
    //update the property scope classes for a field
    var updateFieldPropScopes=function(fieldItemWrap){
        var scope=getFieldItemScope(fieldItemWrap);
        var fieldId=fieldItemWrap.attr('data-id');
        //for each field property
        fieldItemWrap.find('[data-prop]').not('[data-prop="id"]').each(function(){
            //remove classes
            removeFieldCtlClasses(jQuery(this));
            //get updated classes
            var propName=jQuery(this).attr('data-prop');
            var locAndGlobData=getLocalAndGlobalVals(fieldId, propName);
            var pageVal=locAndGlobData['local_page'];
            var globalVal=locAndGlobData['global'];
            var ctlClass=getFieldCtlClass(scope, fieldId, propName, pageVal, globalVal);
            //set updated classes
            var classArr=ctlClass.split(' ');
            for(var c=0;c<classArr.length;c++){
                jQuery(this).addClass(classArr[c]);
            }
        });
    };
    
    //show or hide field properties depending on the selected field type
    var updateShownFieldPropsForType=function(fieldItemWrap){
        var typeSelect=fieldItemWrap.find('select[data-edit="field_type"]:first');
        var typeId=typeSelect.val();
        var typeData=fieldsHelper.getFieldType(typeId);
        if(typeData!=undefined){
            var disable_props=[];
            if(typeData.hasOwnProperty('disable_props')){
                disable_props=typeData['disable_props'];
            }
            fieldItemWrap.find('.field-config [data-prop]').each(function(){
                var propWrap=jQuery(this);
                var propKey=propWrap.attr('data-prop');
                if(disable_props.indexOf(propKey)!=-1){ //if this property is disabled for this field type
                    propWrap.addClass('disabled');
                }else{
                    propWrap.removeClass('disabled');
                }
            });
        }
    };
    
    //get the opposite twin in the mirror scope for this property edit control wrap
    var getTwinFieldItem=function(fieldItemWrap){
        var scope=getFieldItemScope(fieldItemWrap);
        var otherScope='global';
        if(scope=='global'){
            otherScope='local';
        }
        var fieldId=fieldItemWrap.attr('data-id');
        var twinFieldItemWrap=getFieldItemWrap(fieldId, otherScope);
        return twinFieldItemWrap;
    };
    
    //get both the local and the global values for a given field's property
    var getLocalAndGlobalVals=function(fieldId, propName){
        var pageVal=undefined, globalVal=undefined;
        var pageFieldData=fieldsHelper.getFieldData(fieldId, {global:false, page:true, default:false});
        var globalFieldData=fieldsHelper.getFieldData(fieldId, {global:true, page:false, default:false});
        if(globalFieldData.hasOwnProperty(propName)){
            globalVal=globalFieldData[propName];
        }
        if(pageFieldData!=undefined){
            if(pageFieldData.hasOwnProperty(propName)){
                pageVal=pageFieldData[propName];
            }
        }
        return {
            local_page:pageVal,
            global:globalVal
        }
    };
    
    //force a local property to inherit from the global property
    var setAsInheritedValue=function(propWrap){
        if(propWrap.hasClass('local')){
            var fieldItemWrap=propWrap.parents('.field-item:first');
            var scope=getFieldItemScope(fieldItemWrap);
            if(scope=='local'){
                var twinItemWrap=getTwinFieldItem(fieldItemWrap);
                if(twinItemWrap.length>0){
                    var fieldId=fieldItemWrap.attr('data-id');
                    var propName=propWrap.attr('data-prop');
                    var twinPropWrap=twinItemWrap.find('[data-prop="'+propName+'"].prop:first');
                    //modify data
                    setData('inherit-global', 'pages', 'fields('+fieldId+').'+propName, function(){
                        var pageData=pagesHelper.getPageData();
                        var fieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
                        delete pageData['fields'][fieldIndex][propName];
                    });
                    //make sure the correct value is reflected on the frontend
                    switch(propName){
                        case 'variations':
                            var varBtn=propWrap.find('.variations-btn:first');
                            var twinVarBtn=twinPropWrap.find('.variations-btn:first');
                            varBtn.val(twinVarBtn.val());
                            break;
                        case 'values':
                            var fieldEl=propWrap.find('.ctl [data-edit]:first');
                            var twinFieldEl=twinPropWrap.find('.ctl [data-edit]:first');
                            fieldEl.val(twinFieldEl.val());

                            var clickBar=propWrap.find('.ctl .click-bar:first');
                            var twinClickBar=twinPropWrap.find('.ctl .click-bar:first');
                            clickBar.val(twinClickBar.val());
                            break;
                        default:
                            var fieldEl=propWrap.find('.ctl [data-edit]:first');
                            var twinFieldEl=twinPropWrap.find('.ctl [data-edit]:first');
                            fieldEl.val(twinFieldEl.val());
                            break;
                    }
                    //update the scope classes
                    updateFieldPropScopes(fieldItemWrap);
                    updateFieldPropScopes(twinItemWrap);
                    //update visible properties if the field type is what changed
                    if(propName=='type'){
                        updateShownFieldPropsForType(fieldItemWrap);
                    }
                }
            }
        }
    };
    
    //open the configuration for a field (create the property edit controls in the config, if not already created)
    var openFieldItem=function(fieldItemWrap, triggerTwinOpen){
        if(triggerTwinOpen==undefined){ triggerTwinOpen=true; }
        if(!fieldItemWrap.hasClass('accordion-open')){
            fieldItemWrap.addClass('accordion-open');
            //get scope, global or local
            var scope=getFieldItemScope(fieldItemWrap);
            //get field id
            var fieldId=fieldItemWrap.attr('data-id');
            //create the config properties, if not already there
            var fieldConfigDiv=fieldItemWrap.find('.field-config:first');
            if(fieldConfigDiv.length<1){
                fieldItemWrap.append('<div class="field-config"></div>');
                fieldConfigDiv=fieldItemWrap.find('.field-config:first');
                var props=fieldsHelper.getAllPropTypes();
                for(var p=0;p<props.length;p++){
                    var propData=props[p];
                    var propName=propData.id;
                    fieldConfigDiv.append('<div data-prop="'+propName+'" class="prop"></div>');
                    var propWrap=fieldConfigDiv.children('.prop:last');
                    propWrap.append('<label>'+propName+'</label><div class="ctl"></div><div title="scope at which this is defined" class="scope"></div>');
                    var scopeWrap=propWrap.children('.scope:last');
                    //click event for scope
                    scopeWrap.click(function(){
                        var propDiv=jQuery(this).parents('[data-prop].prop:first');
                        setAsInheritedValue(propDiv);
                    });
                    scopeWrap.append(getLocalSvg());
                    scopeWrap.append(getGlobalSvg());
                    scopeWrap.append('<span title="default" class="not-set"></span>');
                    var ctlWrap=propWrap.children('.ctl:last');
                    //get both the global and local field data
                    var locAndGlobData=getLocalAndGlobalVals(fieldId, propName);
                    var pageVal=locAndGlobData['local_page'];
                    var globalVal=locAndGlobData['global'];
                    
                    //set the html for this property
                    var fieldTypes=fieldsHelper.getAllFieldTypes();
                    var setArgs={
                        ctlWrap:ctlWrap, 
                        scope:scope,
                        pageVal:pageVal, 
                        globalVal:globalVal,
                        options:fieldTypes,
                        propName:propName,
                        getFieldCtlClass:getFieldCtlClass,
                        getSetVal:getSetVal
                    };
                    propData.set_html(setArgs);
                    
                    //init the events of this property field (if there are any events)
                    if(propData.hasOwnProperty('init_events')){
                        propData.init_events(setArgs);
                    }
                }
                //hide properties depending on field type
                updateShownFieldPropsForType(fieldItemWrap);
            } 
            //finished building the html and events
            if(triggerTwinOpen){
                //close all other open fields
                var scopeWrap=fieldItemWrap.parents('[data-scope]:first').parent();
                scopeWrap.find('.field-item.accordion-open').not(fieldItemWrap).each(function(){
                    closeFieldItem(jQuery(this), false);
                });
                //open the twin field listed in the opposite scope
                var twinFieldItemWrap=getTwinFieldItem(fieldItemWrap);
                if(twinFieldItemWrap.length>0){
                    openFieldItem(twinFieldItemWrap, false);
                }
            }
            //attach edit data events for capturing data changes
            initEditDataEvents();
        }
    };
    
    //close the properties config for a field
    var closeFieldItem=function(fieldItemWrap, triggerTwinClose){
        if(triggerTwinClose==undefined){ triggerTwinClose=true; }
        if(fieldItemWrap.hasClass('accordion-open')){
            fieldItemWrap.removeClass('accordion-open');
            if(triggerTwinClose){
                var twinFieldItemWrap=getTwinFieldItem(fieldItemWrap);
                if(twinFieldItemWrap.length>0){
                    closeFieldItem(twinFieldItemWrap, false);
                }
            }
        }
    };
    
    //init the ability for a field button to open/close its properties config
    var initAccordionBtn_Events=function(btn){
        if(!isInitEvent(btn)){
            btn.click(function(e){
                e.preventDefault(); e.stopPropagation();
                var fieldItemWrap=btn.parents('.field-item:first');
                if(fieldItemWrap.hasClass('accordion-open')){
                    closeFieldItem(fieldItemWrap);
                }else{
                    openFieldItem(fieldItemWrap);
                }
            });
        }
    };
    
    //init the events on a "+ add/edit fields" screen
    var initAddEditField_Events=function(){
        var popWrap=jQuery('.popups [popup-id="add_edit_field"]:first').find('.popup-content .editFieldsWrap:first');
        
        //init open field config options accordion buttons
        var accordianBtns=popWrap.find('.accordian-btn');
        accordianBtns.each(function(){
            initAccordionBtn_Events(jQuery(this));
        });
        
        //if this is an edit screen for an individual field
        var whichFieldEl=popWrap.parents('.popup-body:first').find('.popup-head .popup-summary .which-field:first');
        if(whichFieldEl.length>0){
            //open the field config options
            var fieldItemWrap=accordianBtns.eq(0).parents('.field-item:first');
            if(fieldItemWrap.length>0){
                openFieldItem(fieldItemWrap, true);
            }
        }else{ //this screen is dealing with all fields
            
            //attach data-add events
            initAddDataEvents();
            
            //attach data-move events
            initMoveDataEvents();
        }

        //attach data-delete events
        initDeleteDataEvents();
    };
    
    //init the text highlighting for variation code (only for active variation)
    var initVariationSyntaxler=function(){
        var popWrap=jQuery('.popups [popup-id="add_edit_variations"]:first');
        var activeVarTextarea=popWrap.find('[data-id].var-screen.active textarea[data-edit="field_variation"]:first');
        if(activeVarTextarea.length>0){
            //init the syntax highlighting for the code area
            syntaxlerHelper.init({
                textareas:activeVarTextarea,
                classes:['field-variation'],
                tab_spaces:tabSpaces,
                line_numbers:true,
                highlight_data:highlightHelper.data_js(),
                highlight_functions:highlightHelper.funcs_js(),
                highlights:highlightHelper.hl_js()
            });
        }
    };
    
    //hook up the events that will allow previewing output for the variation script
    var initVariationUpdateOutput=function(popWrap){
        var updateBtns=popWrap.find('.update-preview-btn');
        updateBtns.each(function(){
            var updateBtn=jQuery(this);
            if(!isInitEvent(updateBtn, 'init-update-event')){
                updateBtn.click(function(){
                    var screenWrap=jQuery(this).parents('.var-screen:first');
                    var textValInput=screenWrap.find('.var-field.var-val input[type="text"]:first');
                    var scriptTextarea=screenWrap.find('textarea[data-edit="field_variation"]:first');
                    var previewTextarea=screenWrap.find('.preview-content textarea:first');
                    previewTextarea.removeClass('error');
                    enableOkForSreen();

                    var val=textValInput.val();
                    var scriptStr=scriptTextarea.val();

                    //get the test output
                    var outputVal='';
                    try{
                        var varFunc=new Function('val', scriptStr+'\n\nreturn val;'); 
                        outputVal=varFunc(val);
                    }catch(err){
                        outputVal=err.stack;
                        previewTextarea.addClass('error');
                        disableOkForSreen();
                    }

                    previewTextarea.addClass('updated');
                    previewTextarea.val(outputVal);
                    setTimeout(function(){
                       previewTextarea.removeClass('updated'); 
                    },500);
                });

                //trigger update through changing the test value
                var screenWrap=updateBtn.parents('.var-screen:first');
                var textValInput=screenWrap.find('.var-field.var-val input[type="text"]:first');
                var updateTestValChange=function(valInput){
                    valInput.parents('.var-screen:first').find('.update-preview-btn:first').click();
                };
                textValInput.keyup(function(e) {
                    switch (e.keyCode) {
                        case 13: //enter key press
                            e.preventDefault();
                            updateTestValChange(jQuery(this));
                            break;
                    }
                });
                textValInput.blur(function(){
                    updateTestValChange(jQuery(this));
                });
            }
        });
    };
    
    var initAddEditVariation_Events=function(){
        var popWrap=jQuery('.popups [popup-id="add_edit_variations"]:first');

        //switch active variation
        popWrap.find('.variation-list-btn').not('.init-select-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-select-event')){
                el.click(function(e){
                    e.preventDefault(); e.stopPropagation();
                    var varId=el.attr('data-id');
                    viewVariation(varId);
                });
            }
        });
        
        //attach events to capture edit data
        initEditDataEvents();
        
        //attach data-add events
        initAddDataEvents();
        
        //attach data-delete events
        initDeleteDataEvents();
        
        //init the text highlighting for variation code (only for active variation)
        initVariationSyntaxler();
        
        //create event for testing the output of the variation's code
        initVariationUpdateOutput(popWrap);
    };
    
    //POPUP
    
    var getPopupWrap=function(id){
        var popWrap;
        if(typeof id=='string'){
            var popupsWrap=jQuery('body:first').children('.popups:last');
            if(popupsWrap.length>0){
                popWrap=popupsWrap.children('.popup[popup-id="'+id+'"]:first');
            }
        }else{
            if(id.hasClass('popup')){
                popWrap=id;
            }
        }
        return popWrap;
    };
    
    var closePopup=function(id){
        var popWrap=getPopupWrap(id);
        if(popWrap.hasClass('open')){
            popWrap.removeClass('open');
            //get rid of save-pending, if it's there
            var screenId=popWrap.attr('popup-id');
            setSavePendingIndicator(screenId, false);
            //is there a referrer to unhide?
            var referrerWrap=popWrap.parent().find('.popup[data-referrer="'+screenId+'"]:first');
            if(referrerWrap.length>0){
                //restore the referring popup
                referrerWrap.removeAttr('data-referrer');
            }else{ //no referrers to go back to
                popWrap.parents('body:first').removeClass('open-popup');
            }
        }
    };
    
    var hidePopupReferrer=function(fromId, toId){
        var fromPopWrap=getPopupWrap(fromId);
        var toPopWrap=getPopupWrap(toId);
        fromPopWrap.attr('data-referrer', toPopWrap.attr('popup-id'));
    };
    
    var openPopup=function(args){
        if(!args.hasOwnProperty('sets_data')){
           args['sets_data']=true;
        }
        var bodEl=jQuery('body:first');
        var popupsWrap=bodEl.children('.popups:last');
        //create the popups wrap
        if(popupsWrap.length<1){
            bodEl.append('<div class="popups"></div>');
            popupsWrap=bodEl.children('.popups:last');
        }
        var popWrap=popupsWrap.children('.popup[popup-id="'+args['id']+'"]:last');
        //create popup HTML for the first time
        if(popWrap.length<1){
            popupsWrap.append('<div popup-id="'+args['id']+'" class="popup"></div>');
            popWrap=popupsWrap.children('.popup[popup-id="'+args['id']+'"]:last');
            popWrap.append('<div class="popup-body"></div>');
            var popupBod=popWrap.children('.popup-body:first');
            popupBod.append('<div class="popup-head"></div>');
            var popupHead=popupBod.children('.popup-head:last');
            popupHead.append('<div class="popup-title"></div>');
            popupHead.append('<div class="popup-summary"></div>');
            popupHead.append('<div class="popup-btns top"></div>');
            var noscrollClass='';
            if(args.hasOwnProperty('noscroll')){
                if(args['noscroll']){
                    noscrollClass=' noscroll';
                }
            }
            popupBod.append('<div class="popup-content'+noscrollClass+'"></div>');
            popupBod.append('<div class="popup-btns bottom"></div>');
        }
        //if not open
        if(!popWrap.hasClass('open')){
            var persist_referrer=false;
            if(args.hasOwnProperty('persist_referrer')){
                persist_referrer=args['persist_referrer'];
            }
            //close all other open popups
            popupsWrap.children('.popup.open').each(function(){
                //if an open popup should persist as the referrer
                if(persist_referrer){
                    //persist referrer, but hide it
                    hidePopupReferrer(jQuery(this), args['id']);
                }else{
                    //close open popup
                    closePopup(jQuery(this));
                }
            });
            //set this popup to open
            popWrap.addClass('open');
            popWrap.parents('body:first').addClass('open-popup');
            //helper function to set content
            var setContent=function(params){
                var val=params['default'];
                if(args.hasOwnProperty(params['key'])){
                    val=args[params['key']]
                }
                popWrap.find('.popup-'+params['key']+':first').html(val);
            };
            //set popup title
            var defaultTitle='[Title]';
            if(args.hasOwnProperty('trigger')){
                if(args['trigger']!=undefined){
                    defaultTitle=args['trigger'].html();
                }
            }
            setContent({
                key:'title',
                default:defaultTitle
            });
            //set popup title
            setContent({
                key:'summary',
                default:''
            });
            //set buttons HTML
            var btnTopWrap=popWrap.find('.popup-btns.top:first');
            var btnBottomWrap=popWrap.find('.popup-btns.bottom:first');
            var getButtonHtml=function(btnData){
                var btnClass='';
                if(btnData.hasOwnProperty('class')){
                    btnClass=' '+btnData['class'];
                }
                var btnSvg='';
                if(btnClass.split(' ').indexOf('btn-ok')!=-1){
                    btnSvg='<span class="btn-icon">'+getCheckSvg()+'</span>';
                }
                var btnHtml='<div class="btn'+btnClass+'">'+btnSvg+'<span>'+btnData['label']+'</span></div>';
                return btnHtml;
            };
            var ifAllowClick=function(btn){
                var yesAllow=true;
                if(btn.hasClass('btn-ok')){
                    if(!btn.parents('[popup-id]:first').hasClass('save-pending')){
                        yesAllow=false;
                    }
                }
                if(btn.hasClass('disabled')){
                    yesAllow=false;
                }
                return yesAllow;
            };
            var setLastButtonEvents=function(callback){
                popWrap.find('.popup-btns').each(function(){
                    jQuery(this).find('.btn:last').click(function(e){
                        if(ifAllowClick(jQuery(this))){
                            callback(jQuery(this));
                        }else{
                            e.preventDefault(); e.stopPropagation();
                        }
                    });
                });
            };
            btnTopWrap.html(''); btnBottomWrap.html('');
            if(args.hasOwnProperty('buttons')){
                for(var b=0;b<args['buttons'].length;b++){
                    var btnData=args['buttons'][b];
                    var btnHtml=getButtonHtml(btnData);
                    btnTopWrap.append(btnHtml); btnBottomWrap.append(btnHtml);
                    if(btnData.hasOwnProperty('action')){
                        setLastButtonEvents(btnData['action']);
                    }
                }
            }
            //add default ok and cancel buttons html
            var addDefaultBtn=function(label, classStr, pos, callback){
                var btnTop=btnTopWrap.children('.'+classStr+':first');
                var btnBottom=btnBottomWrap.children('.'+classStr+':first');
                if(btnTop.length<1){
                    var btnHtml=getButtonHtml({label:label, class:classStr});
                    if(pos=='first'){
                        btnTopWrap.prepend(btnHtml); btnBottomWrap.prepend(btnHtml);
                    }else{
                        btnTopWrap.append(btnHtml); btnBottomWrap.append(btnHtml);
                    }
                    btnTop=btnTopWrap.children('.'+classStr+':first');
                    btnBottom=btnBottomWrap.children('.'+classStr+':first');
                }
                btnTop.click(function(e){
                    if(ifAllowClick(jQuery(this))){
                        callback(jQuery(this));
                    }else{
                        e.preventDefault(); e.stopPropagation();
                    }
                });
                btnBottom.click(function(e){
                    if(ifAllowClick(jQuery(this))){
                        callback(jQuery(this));
                    }else{
                        e.preventDefault(); e.stopPropagation();
                    }
                });
            };
            addDefaultBtn('Cancel', 'btn-cancel', 'first', function(btn){
                if(args['sets_data']){
                    revertDataChanges();
                }
                closePopup(btn.parents('[popup-id]:first').attr('popup-id'));
            }); 
            addDefaultBtn('Ok', 'btn-ok', 'after', function(btn){
                if(args['sets_data']){
                    clearDataChanges();
                }
                closePopup(btn.parents('[popup-id]:first').attr('popup-id'));
            }); 
            //add the popup content
            var popContent=popWrap.find('.popup-content:first');
            if(args.hasOwnProperty('html')){
                var persist_content=false;
                if(args.hasOwnProperty('persist_content')){
                    persist_content=args['persist_content'];
                }
                var doRecreate=!(popContent.hasClass('created') && persist_content);
                if(doRecreate){
                    popContent.addClass('created');
                    popContent.html(args['html']());
                    if(args.hasOwnProperty('html_events')){
                        args['html_events']();
                    }
                }
            }
        }
    };
    
    var getDefaultSectionTitle=function(index){
        var title='[Section ';
        if(index!=undefined){ title+=(index+1)+']'; }
        return title;
    };
    
    //auto-update the default numbered sections that don't have custom titles or ids, so the new order is reflected accurately
    var updateSectionNumbering=function(){
        var listUl=jQuery('.overview ul:first');
        var tokenWrap=jQuery('.token-wrap:first');
        listUl.children('li').each(function(i){
            var li=jQuery(this);
            var sectionId=li.attr('name');
            if(sectionId.indexOf('s')===0){
                var postfix=sectionId.substring(1);
                postfix=parseInt(postfix);
                if(!isNaN(postfix)){
                    if(postfix!=(i+1)){
                        var newSectionId='s'+(i+1);
                        var idInput=li.find('input.section-id:first');
                        //change id value
                        idInput.val(newSectionId);
                        editData(idInput);
                    }
                }
            }
            //see if the title also needs updating to follow default numbering
            var titleInput=li.find('input.section-title:first');
            var currentTitle=titleInput.val();
            var defaultTitle=getDefaultSectionTitle();
            //if update needed
            if(currentTitle.indexOf(defaultTitle)===0){
                //if not already numbered correctly
                if(getDefaultSectionTitle(i)!=titleInput.val()){
                    var newSectionTitle=getDefaultSectionTitle(i);
                    //change title value
                    titleInput.val(newSectionTitle);
                    editData(titleInput);
                }
            }
        });
    };
    
    var confirmDelete=function(deleteWhat){
        return confirm(deleteWhat + '\n\nAre you sure? Delete?');
    };
    
    var deleteField=function(deleteBtn){
        var didDelete=false;
        var wrapAttr='data-id';
        var screenId=getCurrentScreenId();
        if(screenId=='home'){
            wrapAttr='data-token';
        }
        fieldWrap=deleteBtn.parents('['+wrapAttr+']:first');
        if(fieldWrap.length<1){
            wrapAttr='data-html-id';
            fieldWrap=deleteBtn.parents('['+wrapAttr+']:first');
        }
        if(fieldWrap.attr('data-html-id')!=undefined){
            wrapAttr='data-html-id';
        }
        fieldId=fieldWrap.attr(wrapAttr);
        var scope=getFieldItemScope(fieldWrap);
        if(confirmDelete(fieldId+' \nscope = '+scope)){
            //if deleting a global field
            var localFieldWrap;
            if(scope=='global'){
                localFieldWrap=getTwinFieldItem(fieldWrap);
                //if this global field has a local twin on the current page
                if(localFieldWrap!=undefined && localFieldWrap.length>0){
                    //modify data, transfer all of the global properties over to the local
                    setData('edit', 'pages', 'fields('+fieldId+')', function(){
                        var pageData=pagesHelper.getPageData();
                        //make sure this field is stored as JSON
                        var localFieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
                        if(typeof pageData['fields'][localFieldIndex]=='string'){
                            pageData['fields'][localFieldIndex]=fieldsHelper.fieldToJson(pageData['fields'][localFieldIndex]);
                        }
                        //transfer all of the global properties over to the local
                        for(var f in fields[fieldId]){
                            if(fields[fieldId].hasOwnProperty(f)){
                                if(!pageData['fields'][localFieldIndex].hasOwnProperty(f)){
                                    if(fields[fieldId][f]!=undefined){
                                        //transfer this global property over to the local data
                                        pageData['fields'][localFieldIndex][f]=fields[fieldId][f];
                                    }
                                }
                            }
                        }
                    });
                }
                //modify data
                setData('delete', 'fields', '('+fieldId+')', function(){
                    delete fields[fieldId];
                });
            }else{
                //modify data
                setData('delete', 'pages', 'fields('+fieldId+')', function(){
                    var pageData=pagesHelper.getPageData();
                    var fieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
                    pageData['fields'].splice(fieldIndex, 1);
                });
            }
            //remove field
            fieldWrap.remove();
            //update the local version of this field
            if(localFieldWrap!=undefined && localFieldWrap.length>0){
                //update local field scopes
                updateFieldPropScopes(localFieldWrap);
            }
            didDelete=true;
        }
        return didDelete;
    }
    
    var deleteSection=function(deleteBtn){
        var didDelete=false;
        var li=deleteBtn.parents('li[name]:first');
        var sectionId=li.attr('name');
        var sectionTitle=li.find('input.section-title:first').val();
        if(confirmDelete(sectionId + ': ' + sectionTitle)){
            var tokenWrap=jQuery('.token-wrap:first');
            var sectionWrap=li[0]['linked_div'];
            //modify data
            var sectionIndex=li.index();
            setData('delete', 'pages', 'sections('+li.attr('name')+')', function(){
                var pageData=pagesHelper.getPageData();
                pageData['sections'].splice(sectionIndex, 1);
            });
            //remove section
            sectionWrap.remove();
            li.remove();
            //make sure the default-numbered sections are still numbered correctly
            updateSectionNumbering();
            didDelete=true;
        }
        return didDelete;
    };
    
    var deleteVariation=function(deleteBtn){
        var didDelete=false;
        
        var scopeWrap=deleteBtn.parents('[data-scope]:first');
        var fieldId=scopeWrap.find('.field-id').html();
        var prefix=scopeWrap.find('.prefix').html();
        var varId=scopeWrap.find('.variation-key').html();
        var scope=scopeWrap.attr('data-scope');
        
        if(confirmDelete(fieldId+prefix+varId+' \nscope = '+scope)){
            if(scope=='global'){
                //modify data
                setData('delete', 'fields', '('+fieldId+')variations('+varId+')', function(){
                    delete fields[fieldId]['variations'][varId];
                    if(Object.keys(fields[fieldId]['variations']).length<1){
                        delete fields[fieldId]['variations'];
                    }
                });
            }else{
                //modify data
                setData('delete', 'pages', 'fields('+fieldId+').variations('+varId+')', function(){
                    var pageData=pagesHelper.getPageData();
                    var fieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
                    if(pageData['fields'][fieldIndex].hasOwnProperty('variations')){
                        if(pageData['fields'][fieldIndex]['variations'].hasOwnProperty(varId)){
                            delete pageData['fields'][fieldIndex]['variations'][varId];
                            if(Object.keys(pageData['fields'][fieldIndex]['variations']).length<1){
                                delete pageData['fields'][fieldIndex]['variations'];
                            }
                        }
                    }
                });
            }
            //get elements
            var varBtn=getVariationListBtn(varId);
            var varScreen=getVariationScreenWrap(varId);
            var varWrap=varScreen.parent();
            //remove elements
            varBtn.remove();
            varScreen.remove();
            //find next first element
            var firstScreen=varWrap.find('.var-screen:first');
            if(firstScreen.length>0){
                //show the first variation as active
                var firstVarId=firstScreen.attr('data-id');
                viewVariation(firstVarId);
            }else{
                //no variations left
                scopeWrap.addClass('no-variations');
            }
            didDelete=true;
        }
        
        return didDelete;
    }
    
    var editPageId=function(input){
        if(!input[0].hasOwnProperty('original_page_id')){
            var oldId=pagesHelper.getPageId();
            input[0]['original_page_id']=oldId;
        }
        if(input[0]['original_page_id']!=input.val()){
            input.addClass('rename_pending');
            setSavePendingIndicator();
        }else{
            input.removeClass('rename_pending');
        }
    };
    
    var editPageTitle=function(textarea){
        setData('edit', 'pages', 'title', function(){
            var newTitle=textarea.val();
            var pageData=pagesHelper.getPageData();
            pageData['title']=newTitle;
        });
    };
    
    var editPageOutcome=function(textarea){
        setData('edit', 'pages', 'outcome', function(){
            var newOutcome=textarea.val();
            var pageData=pagesHelper.getPageData();
            pageData['outcome']=newOutcome;
        });
    };
    
    var editPageSearch=function(textarea){
        setData('edit', 'pages', 'search', function(){
            var newSearch=textarea.val();
            var pageData=pagesHelper.getPageData();
            pageData['search']=[];
            var terms=newSearch.split(',');
            var termsStr='';
            for(var t=0;t<terms.length;t++){
                var term=terms[t]; term=term.trim();
                if(term.length>0){
                    if(pageData['search'].indexOf(term)==-1){
                        if(termsStr.length>0){ termsStr+=', '; }
                        termsStr+=term;
                        pageData['search'].push(term);
                    }
                }
            }
            textarea.val(termsStr);
        });
    };
    
    var editPlainHtmlField=function(textarea){
        var fieldWrap=textarea.parents('[data-html-id]:first');
        var htmlId=fieldWrap.attr('data-html-id');
        setData('edit', 'pages', 'html_field('+htmlId+')', function(){
            var newHtml=textarea.val();
            //get field index
            var fieldIndex=fieldsHelper.getIndexOfPageField(htmlId);
            //get the json data for the page
            var pageData=pagesHelper.getPageData();
            //json field index should match the frontend index
            pageData['fields'][fieldIndex]={id:htmlId, html:newHtml};
        });
    };
    
    var isUnusedFieldId=function(fieldId){
        var isUnused=true;
        if(fields.hasOwnProperty(fieldId)){
            isUnused=false;
        }else{
            var fieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
            if(fieldIndex!=-1){
                isUnused=false;
            }
        }
        return isUnused;
    };
    
    var editFieldId=function(input){
        var newFieldId=input.val();
        if(isUnusedFieldId(newFieldId)){
            var fieldItemWrap=input.parents('.field-item:first');
            var fieldId=fieldItemWrap.attr('data-id');
            //modify local data
            setData('edit', 'pages', 'fields('+fieldId+').id', function(){
                var pageData=pagesHelper.getPageData();
                var fieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
                if(typeof pageData['fields'][fieldIndex]=='string'){
                    pageData['fields'][fieldIndex]=newFieldId;
                }else{
                    pageData['fields'][fieldIndex]['id']=newFieldId;
                }
            });
            //modify global data
            setData('edit', 'fields', '('+fieldId+').id', function(){
                var copiedFieldJson=helper.copyJson(fields[fieldId]);
                copiedFieldJson['id']=newFieldId;
                delete fields[fieldId];
                fields[newFieldId]=copiedFieldJson;
            });
            //update listing ID
            var modFieldIdListing=function(fieldWrap){
                fieldWrap.attr('data-id', newFieldId);
                fieldWrap.find('.field-id .id:first').html(newFieldId);
                fieldWrap.find('.delete-btn:first').attr('title', 'delete '+newFieldId);
                var idInput=fieldWrap.find('.ctl input:first');
                idInput.attr('value', newFieldId);
                if(idInput.val()!=newFieldId){
                    idInput.val(newFieldId);
                }
            };
            var twinFieldItemWrap=getTwinFieldItem(fieldItemWrap);
            modFieldIdListing(fieldItemWrap);
            modFieldIdListing(twinFieldItemWrap);
            //update the reference to the token id in the header
            var whichFieldEl=fieldItemWrap.parents('.popup-body:first').find('.popup-head .popup-summary .which-field:first');
            if(whichFieldEl.length>0){
                whichFieldEl.html(newFieldId);
            }
        }else{ //field ID is already being used
            input.val(input[0]['latest_value']); //restore previous value
        }
    };
    
    var isSameAsGlobalVal=function(locVal, globVal){
        var isSame=false;
        if(typeof locVal=='string'){
            if(locVal==globVal){
                isSame=true;
            }
        }else{
            var locValStr=locVal;
            var globValStr=globVal;
            if(locValStr!=undefined){ locValStr=locValStr.toString(); }
            if(globValStr!=undefined){ globValStr=globValStr.toString(); }
            if(locValStr==globValStr){
                isSame=true;
            }
        }
        return isSame;
    };
    
    var editFieldProp=function(el, propName, getNewValData){
        var newVal=el.val();
        var fieldItemWrap=el.parents('.field-item:first');
        var twinFieldWrap=getTwinFieldItem(fieldItemWrap);
        var fieldId=fieldItemWrap.attr('data-id');
        var scope=getFieldItemScope(fieldItemWrap);
        if(scope=='global'){ //edit global scope 
            //modify data
            setData('edit', 'fields', '('+fieldId+').'+propName, function(){
                if(getNewValData!=undefined){
                    newVal=getNewValData(newVal);
                }
                if(newVal.length<1){
                    delete fields[fieldId][propName];
                }else{
                    fields[fieldId][propName]=newVal;
                }
            });
            //get the local twin property
            var propDiv=twinFieldWrap.find('[data-prop="'+propName+'"]:first');
            //if the local property is set as inheriting from the global scope value'
            if(propDiv.hasClass('global') || propDiv.hasClass('not-set')){
                //copy over the global value to the local property
                var localEl=propDiv.find('.ctl [data-edit]:first');
                localEl.val(newVal);
            }
        }else{ //edit local scope 
            //modify data
            setData('edit', 'pages', 'fields('+fieldId+').'+propName, function(){
                //make sure this field is in JSON form
                var fieldIndex=fieldsHelper.setPageFieldToJson(fieldId);
                //set the new propName on the field's JSON
                var pageData=pagesHelper.getPageData();
                if(getNewValData!=undefined){
                    newVal=getNewValData(newVal);
                }
                if(newVal.length<1){
                    delete pageData['fields'][fieldIndex][propName];
                }else{
                    pageData['fields'][fieldIndex][propName]=newVal;
                }
                //if the local value is the same as the global value
                if(fields.hasOwnProperty(fieldId)){
                    if(isSameAsGlobalVal(pageData['fields'][fieldIndex][propName], fields[fieldId][propName])){
                        delete pageData['fields'][fieldIndex][propName]; //just allow local value to inherit global value
                    }
                }
            });
        }
        //update the indicated scope of this property for both local and global
        updateFieldPropScopes(fieldItemWrap);
        updateFieldPropScopes(twinFieldWrap);
        //update shown properties, if needed
        updateShownFieldPropsForType(fieldItemWrap);
        updateShownFieldPropsForType(twinFieldWrap);
    };
    
    var editFieldType=function(select){ 
        editFieldProp(select, 'type');
    };
    
    var editFieldLabel=function(input){
        editFieldProp(input, 'label');
    };
    
    var editFieldClass=function(input){
        editFieldProp(input, 'class');
    };
    
    var editFieldSummary=function(input){
        editFieldProp(input, 'summary');          
    };
    
    var editFieldValues=function(textarea){
        var propName='values';
        
        //setData...
        editFieldProp(textarea, propName, 
        function(newVal){ //getNewValData(newVal)
            //turn newline separated list to array
            var valArr=newVal.split('\n');
            newVal=[];
            for(var v=0;v<valArr.length;v++){
                var val=valArr[v]; val=val.trim();
                if(val.length>0){
                    newVal.push(val);
                }
            }
            return newVal;
        }); 
        
        var fieldItemWrap=textarea.parents('[data-id]:first');
        var twinItemWrap=getTwinFieldItem(fieldItemWrap);
        var fieldId=fieldItemWrap.attr('data-id');
        var scope=getFieldItemScope(fieldItemWrap);

        //get both the global and local field data
        var locAndGlobData=getLocalAndGlobalVals(fieldId, propName);
        var pageVal=locAndGlobData['local_page'];
        var globalVal=locAndGlobData['global'];
        
        //get scope class
        var ctlClass=getFieldCtlClass(scope, fieldId, propName, pageVal, globalVal);
        //get the set value
        var setVal=getSetVal(ctlClass, pageVal, globalVal);
        
        //copy over the data change to the property
        var propDiv=textarea.parents('[data-prop]:first');
        var clickBar=propDiv.find('.click-bar:first');
        var editArea=propDiv.find('[data-edit]:first');
        setValuesClickBarContent(clickBar, editArea, setVal);
        
        if(scope=='global'){
            //copy over the data change to the twin property
            var twinPropDiv=twinItemWrap.find('[data-prop="values"]:first');
            var twinClickBar=twinPropDiv.find('.click-bar:first');
            var twinEditArea=twinPropDiv.find('[data-edit]:first');
            setValuesClickBarContent(twinClickBar, twinEditArea, setVal);
        }
    };
    
    var editFieldVariationId=function(input){
        var scopeWrap=input.parents('[data-scope]:first');
        var fieldIdSpan=scopeWrap.find('.titleWrap .field-id:first');
        var varIdSpan=scopeWrap.find('.titleWrap .variation-key:first');
        var fieldId=fieldIdSpan.html();
        var varId=varIdSpan.html();
        var newVarId=input.val();
        var listingBtn=scopeWrap.find('.var-listing .variation-list-btns [data-id="'+varId+'"]:first');
        var screenWrap=input.parents('[data-id].var-screen:first');
        var testValDiv=screenWrap.find('.var-field.var-val:first');
        var funcNameSpan=screenWrap.find('.code-wrap .var-id:first');
        var scriptTextarea=screenWrap.find('textarea[data-edit="field_variation"]:first')
        var scope=scopeWrap.attr('data-scope');
        
        if(scope=='global'){
            //modify data
            setData('edit', 'fields', '('+fieldId+').variations('+varId+').id', function(){
                //change the ID of this json data
                delete fields[fieldId]['variations'][varId];
                fields[fieldId]['variations'][newVarId]=scriptTextarea.val();
            });
        }else{ //local
            //modify data
            setData('edit', 'pages', 'fields('+fieldId+').variations('+varId+').id', function(){
                var fieldIndex=fieldsHelper.setPageFieldToJson(fieldId);
                var pageData=pagesHelper.getPageData();
                //if the local field doesn't already have variations of its own
                if(!pageData['fields'][fieldIndex].hasOwnProperty('variations')){
                    var varJson={};
                    //if the inherited global scope can be copied to local
                    if(fields.hasOwnProperty(fieldId)){
                        if(fields[fieldId].hasOwnProperty('variations')){
                            varJson=helper.copyJson(fields[fieldId]['variations']);
                        }
                    }
                    //set local json
                    pageData['fields'][fieldIndex]['variations']=varJson;
                }
                if(pageData['fields'][fieldIndex]['variations'].hasOwnProperty(varId)){
                    delete pageData['fields'][fieldIndex]['variations'][varId];
                }
                pageData['fields'][fieldIndex]['variations'][newVarId]=scriptTextarea.val();
            });
            //the variations should no longer be inherited from global scope
            screenWrap.parents('.var-screens:first').children('.inherited-variation').removeClass('inherited-variation');
        }
        
        //update the header
        varIdSpan.html(newVarId);
        
        //update the button
        listingBtn.attr('data-id', newVarId);
        listingBtn.find('span:first').html(newVarId);
        
        //update the screen wrap
        screenWrap.attr('data-id', newVarId);
        
        //update the function name
        funcNameSpan.html(newVarId);
        
        //update the id input value attribute
        input.attr('value', newVarId);
        
        //update the datalist
        var newListId=fieldId+'_'+newVarId+'_values';
        testValDiv.find('input[list]:first').attr('list', newListId);
        testValDiv.find('datalist[id]:first').attr('id', newListId);
    };
    
    var editFieldVariation=function(textarea){
        var newScript=textarea.val();
        var scopeWrap=textarea.parents('[data-scope]:first');
        var fieldIdSpan=scopeWrap.find('.titleWrap .field-id:first');
        var fieldId=fieldIdSpan.html();
        var scope=scopeWrap.attr('data-scope');
        var varIdSpan=scopeWrap.find('.titleWrap .variation-key:first');
        var varId=varIdSpan.html();
        
        if(scope=='global'){
            //modify data
            setData('edit', 'fields', '('+fieldId+').variations('+varId+').script', function(){
                //change the script text
                fields[fieldId]['variations'][varId]=newScript;
            });
        }else{ //local
            //modify data
            setData('edit', 'pages', 'fields('+fieldId+').variations('+varId+').script', function(){
                var fieldIndex=fieldsHelper.setPageFieldToJson(fieldId);
                var pageData=pagesHelper.getPageData();
                //if the local field doesn't already have variations of its own
                if(!pageData['fields'][fieldIndex].hasOwnProperty('variations')){
                    var varJson={};
                    //if the inherited global scope can be copied to local
                    if(fields.hasOwnProperty(fieldId)){
                        if(fields[fieldId].hasOwnProperty('variations')){
                            varJson=helper.copyJson(fields[fieldId]['variations']);
                        }
                    }
                    //set local json
                    pageData['fields'][fieldIndex]['variations']=varJson;
                }
                //set variation script
                pageData['fields'][fieldIndex]['variations'][varId]=newScript;
            });
            //the variations should no longer be inherited from global scope
            textarea.parents('.var-screens:first').children('.inherited-variation').removeClass('inherited-variation');
        }
        
        //trigger update preview
        scopeWrap.find('.update-preview-btn:first').click();
    };

    var editSectionTitle=function(trigger){
        var nameParent=trigger.parents('[name]:first');
        var li, div, editInput;
        if(nameParent[0].hasOwnProperty('linked_div')){
            div=nameParent[0]['linked_div'];
            li=div[0]['linked_li'];
            editInput=div.find('input.section-title:first');
        }else{
            li=nameParent[0]['linked_li'];
            div=li[0]['linked_div'];
            editInput=li.find('input.section-title:first');
        }
        var newTitle=trigger.val();
        editInput.val(newTitle);
        //modify the data
        var sectionIndex=li.index();
        setData('edit', 'pages', 'sections['+sectionIndex+'].title', function(){
            //get the json data for the page
            var pageData=pagesHelper.getPageData();
            //json field index should match the frontend index
            pageData['sections'][sectionIndex]['title']=newTitle;
        });
    };
    
    var editSectionId=function(idInput){
        var sectionId=idInput[0]['latest_value'];
        var newSectionId=idInput.val();
        //get elements
        var tokenWrap=jQuery('.token-wrap:first');
        var li=idInput.parents('li[name]:first');
        var idInput=li.find('input.section-id:first');
        var sectionDiv=li[0]['linked_div'];
        //change all of the instances of this ID in the dom
        idInput.val(newSectionId);
        li.attr('name', newSectionId);
        sectionDiv.attr('id', newSectionId).attr('name', newSectionId);
        //modify the data
        var sectionIndex=li.index();
        setData('edit', 'pages', 'sections['+sectionIndex+'].id', function(){
            //get the json data for the page
            var pageData=pagesHelper.getPageData();
            //json field index should match the frontend index
            pageData['sections'][sectionIndex]['id']=newSectionId;
        });
    };
        
    var editSectionContent=function(textarea){
        var newVal=textarea.val();
    };
    
    var highlightPastedField=function(fieldItemWrap){
        fieldItemWrap.addClass('dropped-field');
        setTimeout(function(){
            fieldItemWrap.removeClass('dropped-field');
        },(revertDuration*2));
    }
    
    var copyFieldIntoScope=function(scope, fieldItemWrap){
        //get wraps
        var scopeWrap=getScopeWrap(scope);
        var itemsWrap=scopeWrap.find('.field-items:first');
        //create field html
        var fieldId=fieldItemWrap.attr('data-id');
        var fieldHtml=getEditFieldItemHtml(fieldId);
        //append copied field
        itemsWrap.append(fieldHtml);
        //highlight new pasted field
        var copiedItemWrap=itemsWrap.find('.field-item[data-id="'+fieldId+'"]:last');
        highlightPastedField(copiedItemWrap);
        //attach new events to html
        initAddEditField_Events();
        //update the scopes of each of this field's properties
        updateFieldPropScopes(fieldItemWrap);
        updateFieldPropScopes(getTwinFieldItem(fieldItemWrap));
    };
    
    //create a reference of a global field in the local page scope
    var copyFieldIntoLocal=function(fieldItemWrap){
        //modify data
        var fieldId=fieldItemWrap.attr('data-id');
        setData('copy-field-into', 'pages', 'fields('+fieldId+')', function(){
            var pageData=pagesHelper.getPageData();
            if(!pageData.hasOwnProperty('fields')){
                pageData['fields']=[];
            }
            //moving a field into local, just pushes its id into the array as a reference to global values
            pageData['fields'].push(fieldId); 
        });
        //update field listing
        copyFieldIntoScope('local', fieldItemWrap);
    };
    
    var copyFieldIntoGlobal=function(fieldItemWrap){
        //modify data
        var fieldId=fieldItemWrap.attr('data-id');
        setData('copy-field-into', 'fields', '('+fieldId+')', function(){
            //transfer all of the local properties over to global
            var pageFieldData=fieldsHelper.getFieldData(fieldId, {global:false, page:true, default:false});
            var copiedFieldData=helper.copyJson(pageFieldData);
            fields[fieldId]=copiedFieldData;
            //turn the existing local field into a reference (by ID) to the global properties
            var localFieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
            var pageData=pagesHelper.getPageData();
            pageData['fields'][localFieldIndex]=fieldId;
        });
        //update field listing
        copyFieldIntoScope('global', fieldItemWrap);
    };
    
    //scroll to and hightlight an existing field wrap
    var highlightExistingField=function(fieldItemWrap){
        //function to set highlight
        var setHighlight=function(){
            fieldItemWrap.addClass('highlight');
            setTimeout(function(){
                fieldItemWrap.removeClass('highlight');
            },800);
        };
        
        //if scrolled out of view
        var fieldTop=fieldItemWrap.position().top;
        if(fieldTop<0){
            //get the scroll wrap
            var scrollDiv=fieldItemWrap.parents('.contentWrap:first');
            var newScrollTop=scrollDiv.scrollTop()+fieldTop;
            
            //scroll into view
            scrollDiv.animate({
                scrollTop: newScrollTop
            }, 300, function(){
                setHighlight();
            });
        }else{ //not scrolled out of view
            setHighlight();
        }
    };
    
    var addField=function(addFieldInput){
        var didAdd=false;
        var newFieldId=addFieldInput.val();
        //check to see if this field already exists
        var fieldItemWrap=getFieldItemWrap(newFieldId);
        //if this field doesn't already exist
        if(fieldItemWrap.length<1){
            //add new html
            var newFieldHtml=getEditFieldItemHtml(newFieldId);
            var fieldsWrap=addFieldInput.parents('.contentWrap:first').find('.field-items:first');
            fieldsWrap.append(newFieldHtml);
            //attach new events to html
            initAddEditField_Events();
            didAdd=true;
            //modify the json data
            var newFieldItemWrap=fieldsWrap.find('[data-id="'+newFieldId+'"].field-item:last');
            var scope=getFieldItemScope(newFieldItemWrap);
            if(scope=='local'){ //local scope
                setData('add', 'pages', 'fields('+newFieldId+')', function(){
                    var pageData=pagesHelper.getPageData();
                    if(!pageData.hasOwnProperty('fields')){
                        pageData['fields']=[];
                    }
                    pageData['fields'].push(newFieldId);
                });
            }else{ //global scope
                setData('add', 'fields', '('+newFieldId+')', function(){
                    fields[newFieldId]={id:newFieldId};
                });
            }
        }else{ //field already exists
            var scope=addFieldInput.parents('[data-scope]:first').attr('data-scope');
            var fieldWrapToHighlight=getFieldItemWrap(newFieldId, scope);
            if(fieldWrapToHighlight.length<1){
                fieldWrapToHighlight=fieldItemWrap;
            }
            highlightExistingField(fieldWrapToHighlight);
        }
        return didAdd;
    };
    
    //add new plain html field
    var addHtmlField=function(addBtn){
        var didAdd=true;
        var contentWrap=addBtn.parents('.contentWrap:first');
        var fieldsWrap=contentWrap.find('.field-items:first');
        var newHtml=getEditFieldItemHtml('html:');
        fieldsWrap.append(newHtml);
        //attach new events to html
        initAddEditField_Events();
        //modify json data
        var htmlId=getNextHtmlFieldId() - 1;
        setData('add', 'pages', 'fields('+htmlId+')', function(){
            var pageData=pagesHelper.getPageData();
            pageData['fields'].push('html:');
        });
        return didAdd;
    };
    
    //scroll a button into view and highlight the button
    var highlightExistingVariation=function(varBtn){
        
        //function to set highlight
        var setHighlight=function(){
            varBtn.addClass('highlight');
            setTimeout(function(){
                varBtn.removeClass('highlight');
            },800);
        };
        
        //if scrolled out of view
        var btnTop=varBtn.position().top;
        if(btnTop<0){
            //get the scroll wrap
            var scrollDiv=varBtn.parents('.var-listing:first');
            var newScrollTop=scrollDiv.scrollTop()+btnTop;
            
            //scroll into view
            scrollDiv.animate({
                scrollTop: newScrollTop
            }, 300, function(){
                setHighlight();
            });
        }else{ //not scrolled out of view
            setHighlight();
        }
    };
    
    //update the number of variations shown on this field's variation property
    var updateVarCount=function(varBtn, variationData){
        var varCount=0;
        if(variationData!=undefined){
            varCount=Object.keys(variationData).length;
        }
        varBtn.val('count ['+varCount+']');
    };
    
    var triggerVariationsClick=function(varBtn){
        var fieldWrap=varBtn.parents('[data-id]:first');
        var fieldId=fieldWrap.attr('data-id');
        var scope=getFieldItemScope(fieldWrap);

        var getScreenHtml=function(){
            return getAddEditVariations_HTML(fieldId, scope);
        };

        openPopup({
            id:'add_edit_variations',
            trigger:varBtn,
            title:'+ add/edit variations',
            summary:'Variations apply to token values. Variation values can be individually called in content just like any other token value.',
            buttons:[
                {
                    label:'cancel',
                    class:'btn-cancel',
                    action:function(){

                    }
                },
                {
                    label:'Ok',
                    class:'btn-ok',
                    action:function(){
                        //make sure the variation changes on the previous screen are indicated by enabling ok button
                        setSavePendingIndicator('add_edit_field');

                        //var fieldItemWrap=ctlWrap.parents('.field-item:first');
                        var twinItemWrap=getTwinFieldItem(fieldWrap);

                        var propName='variations';
                        var locAndGlobData=getLocalAndGlobalVals(fieldId, propName);
                        var pageVal=locAndGlobData['local_page'];
                        var globalVal=locAndGlobData['global'];

                        //update the variations data count
                        var ctlClass=getFieldCtlClass(scope, fieldId, propName, pageVal, globalVal);
                        var setVal=getSetVal(ctlClass, pageVal, globalVal);
                        updateVarCount(varBtn, setVal);

                        //update the indicated scope for this field
                        updateFieldPropScopes(fieldWrap);

                        var otherScope='local';
                        if(scope=='local'){ otherScope='global'; }

                        if(twinItemWrap.length>0){
                            var otherVarBtn=twinItemWrap.find('[data-prop="variations"].prop .variations-btn:first');
                            //update the other variations data count
                            var otherCtlClass=getFieldCtlClass(otherScope, fieldId, propName, pageVal, globalVal);
                            var otherSetVal=getSetVal(otherCtlClass, pageVal, globalVal);
                            updateVarCount(otherVarBtn, otherSetVal);

                            //update the other indicated scope for this field
                            updateFieldPropScopes(twinItemWrap);
                        }
                    }
                }
            ],
            sets_data:true,
            noscroll:true,
            persist_referrer:true,
            persist_content:false,
            html:getScreenHtml,
            html_events:initAddEditVariation_Events
        });
    };
    
    var addVariation=function(addInput){
        var didAdd=false;
        var newVarId=addInput.val();
        //check to see if this variation already exists
        var varBtn=getVariationListBtn(newVarId);
        //if this variation doesn't already exist
        if(varBtn.length<1){
            var scopeWrap=addInput.parents('[data-scope]:first');
            var fieldId=scopeWrap.find('.field-id').html();
            var scope=scopeWrap.attr('data-scope');
            //get new html
            var newScreenHtml=getVariationScreen_HTML(fieldId, scope, newVarId, false);
            var newBtnHtml=getVariationListItem_HTML(newVarId, false);
            //get wraps
            var btnsWrap=scopeWrap.find('.variation-list-btns:first');
            var screensWrap=scopeWrap.find('.var-screens:first');
            //add new html
            btnsWrap.append(newBtnHtml);
            screensWrap.append(newScreenHtml);
            //attach new events to html
            initAddEditVariation_Events();
            //open the newest variation
            btnsWrap.find('.variation-list-btn:last').click();
            //make sure the no-variations class is gone
            scopeWrap.removeClass('no-variations');
            didAdd=true;
            //modify the json data
            if(scope=='local'){ //local scope
                //modify data
                setData('add', 'pages', 'fields('+fieldId+').variations('+newVarId+')', function(){
                    var pageData=pagesHelper.getPageData();
                    //get the index of this page field
                    var fieldIndex=fieldsHelper.getIndexOfPageField(fieldId);
                    //make sure the page field is represented as a JSON
                    var fieldJson=fieldsHelper.fieldToJson(pageData['fields'][fieldIndex]);
                    pageData['fields'][fieldIndex]=fieldJson;
                    //if the local field doesn't already have variations of its own
                    if(!pageData['fields'][fieldIndex].hasOwnProperty('variations')){
                        var varJson={};
                        //if the inherited global scope can be copied to local
                        if(fields.hasOwnProperty(fieldId)){
                            if(fields[fieldId].hasOwnProperty('variations')){
                                varJson=helper.copyJson(fields[fieldId]['variations']);
                            }
                        }
                        //set local json
                        pageData['fields'][fieldIndex]['variations']=varJson;
                    }
                    //set the new variation as a string on the variation object
                    pageData['fields'][fieldIndex]['variations'][newVarId]=getDefaultVariationContent();
                });
                //the variations should no longer be inherited from global scope
                screensWrap.children('.inherited-variation').removeClass('inherited-variation');
            }else{ //global scope
                setData('add', 'fields', '('+fieldId+').variations('+newVarId+')', function(){
                    //make sure there is a variations property
                    if(!fields[fieldId].hasOwnProperty('variations')){
                        fields[fieldId]['variations']={};
                    }
                    //set the new variation as a string on the variation object
                    fields[fieldId]['variations'][newVarId]=getDefaultVariationContent();
                });
            }
        }else{ //field already exists
            highlightExistingVariation(varBtn);
        }
        return didAdd;
    };
    
    //add a new section to the end
    var addSection=function(args){
        var didAdd=true;
        var listUl=jQuery('.overview ul:first');
        var tokenWrap=jQuery('.token-wrap:first');
        var addSectionBtn=tokenWrap.find('.add-section:last');
        
        var sectionCount=listUl.children('li').length;
        //build the default attributes for the new section, if needed
        if(args==undefined){
            args={};
        }
        if(!args.hasOwnProperty('id')){
            args['id']='s'+(sectionCount+1);
        }
        if(!args.hasOwnProperty('title')){
            args['title']=getDefaultSectionTitle(sectionCount);
        }
        if(!args.hasOwnProperty('content')){
            args['content']='';
        }
        //get the HTML for the new section
        var newLiHtml=pagesHelper.getEditSectionLiHtml(args['id'], args['title']);
        var newContentHtml=pagesHelper.getEditSectionHtml(args['title'], args['content'], args['id']);
        //add the HTML to the page
        listUl.append(newLiHtml);
        if(addSectionBtn.length>0){
            addSectionBtn.before(newContentHtml);
        }else{
            tokenWrap.append(newContentHtml);
        }
        //init the events for the new section
        initEdit();
        //set pages data
        setData('add', 'pages', 'sections('+args['id']+')', function(){
            var pageData=pagesHelper.getPageData();
            pageData['sections'].push({
                id:args['id'],
                title:args['title']
            });
        });
        return didAdd;
    };
    
    var moveField=function(btn){
        //get field wrap
        var fieldWrap=btn.parents('.field-item:first');
        if(fieldWrap.length<1){
            fieldWrap=btn.parents('.field:first');
        }
        //get index positions
        var prevIndex=btn[0]['latest_index'];
        var newIndex=fieldWrap.index();
        //modify data
        setData('move', 'pages', 'fields['+prevIndex+']', function(){
            var pageData=pagesHelper.getPageData();
            var fieldData=pageData['fields'][prevIndex];
            //remove field from array
            pageData['fields'].splice(prevIndex, 1);
            //add field to array at new index
            pageData['fields'].splice(newIndex, 0, fieldData);
        });
    };
    
    var moveSection=function(btn){
        var li=btn.parents('li[name]:first');
        var newIndex=li.index();
        var tokenWrap=jQuery('.token-wrap:first');
        var sectionWrap=li[0]['linked_div'];
        var sections=tokenWrap.children('div[id][name]');
        if(sections.length<=newIndex+1){
            //move last
            var lastSectionWrap=sections.filter(':last');
            lastSectionWrap.after(sectionWrap);
        }else{
            //figure out at which item to move the section based on the new index
            sections.not(sectionWrap).each(function(i){
                if(i==newIndex){
                    jQuery(this).before(sectionWrap);
                    return false;
                }
            });
        }
        //update data
        var previousIndex=li.find('a.move-section:first')[0]['latest_index'];
        setData('move', 'pages', 'sections('+li.attr('name')+')', function(){
            var pageData=pagesHelper.getPageData();
            //get section json
            var sectionJson=helper.copyJson(pageData['sections'][previousIndex]);
            //remove section from array
            pageData['sections'].splice(previousIndex, 1);
            //add section to array at new index
            pageData['sections'].splice(newIndex, 0, sectionJson)
        });
    };
    
    //handle when user drag and drops a field from one scope into another
    var dropFieldInto=function(intoScope, fieldItemWrap){
        if(fieldItemWrap.length>0){
            if(!fieldItemWrap.hasClass('plain-html')){
                //if drag into a different scope
                var fromScope=getFieldItemScope(fieldItemWrap);
                if(intoScope!=fromScope){
                    var fieldId=fieldItemWrap.attr('data-id');
                    var alreadyExistsItem=getFieldItemWrap(fieldId, intoScope);
                    //if field doesn't already exist in this scope
                    if(alreadyExistsItem.length<1){
                        copyFieldData(fromScope, fieldItemWrap);
                    }
                    setTimeout(function(){
                        //open the field
                        openFieldItem(fieldItemWrap, true);
                    },revertDuration);
                }
            }
        }
    };
    
    //INIT EVENTS
    
    var isInitEvent=function(el, initClass){
        var isInit=true;
        if(initClass==undefined){ initClass='init-event'; }
        if(!el.hasClass(initClass)){
            el.addClass(initClass);
            isInit=false;
        }
        return isInit;
    };
    
    var okEditFieldChanges=function(){
        fieldsHelper.redrawFrontendPageFields();
        initEdit();
        setSavePendingIndicator('home');
    };
    
    var initOpenFieldEdit_Event=function(btn){
        if(!isInitEvent(btn)){
            btn.click(function(e){
                e.preventDefault(); e.stopPropagation();
                
                var summary='Choose and edit fields for this page';
                
                //get current field ID, if this is for a particular field
                var theBtn=jQuery(this);
                var fieldId=undefined;
                if(theBtn.hasClass('edit-field')){
                    fieldId=theBtn.parents('[data-token]:first').attr('data-token');
                    summary='Edit one particular field, <strong class="which-field">'+fieldId+'</strong>';
                }
                
                var getScreenHtml=function(){
                    return getAddEditField_HTML(fieldId);
                };

                openPopup({
                    id:'add_edit_field',
                    trigger:theBtn,
                    summary:summary,
                    buttons:[
                        {
                            label:'cancel',
                            class:'btn-cancel',
                            action:function(){
                                
                            }
                        },
                        {
                            label:'Ok',
                            class:'btn-ok',
                            action:function(){
                                okEditFieldChanges();
                            }
                        }
                    ],
                    sets_data:true,
                    noscroll:true,
                    persist_content:false,
                    html:getScreenHtml,
                    html_events:initAddEditField_Events
                });
                
            });
        }
    };
    
    //attach data-delete handlers
    var initDeleteDataEvents=function(){
        jQuery('[data-delete]').not('.init-delete-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-delete-event')){
                el.click(function(e){
                    e.preventDefault(); e.stopPropagation();
                    deleteData(jQuery(this));
                })
            }
        });
    };
    
    //attach data-move handlers
    var initMoveDataEvents=function(){
        
        var tokenWrap=jQuery('.token-wrap:first');

        var setMemoryValue=function(el, index){
            el[0]['original_index']=index;
            el[0]['latest_index']=index;
        };
        
        //move buttons in the fields-wrap
        jQuery('.fields-wrap .move-field[data-move]').not('.init-move-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-move-event')){
                //get the parent for the sortable items
                var fieldsWrap=el.parents('.fields:first');
                
                //for each field
                fieldsWrap.children('.field').each(function(i){
                    if(!isInitEvent(jQuery(this), 'init-mem')){
                        //save latest_index and original_index
                        setMemoryValue(jQuery(this).find('a.move-field:first'), jQuery(this).index());
                    }
                });
                
                //if the sort for this wrap hasn't been set
                if(!isInitEvent(fieldsWrap, 'init-sort-event')){
                    fieldsWrap.find('a.move-field').click(function(e){
                        e.preventDefault(); e.stopPropagation();
                    });
                    //make the fields drag-sortable on the main page
                    fieldsWrap.sortable({
                        axis:'y',
                        handle:'a.move-field',
                        stop:function(e, ui){
                            var fieldDiv=jQuery(ui.item[0]);
                            var btn=fieldDiv.find('a.move-field:first');
                            moveData(btn, '.field:first');
                        }
                    });
                }
            }
        });
        
        //move fields in the local page edit screen
        jQuery('.localWrap .field-item [data-move]').not('.init-move-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-move-event')){
                //get the parent for the sortable items
                var fieldWrap=el.parents('.field-item:first');
                var fieldsWrap=fieldWrap.parents('.field-items:first');
                var scopeWrap=fieldsWrap.parents('[data-scope]:first');
                
                //for each field item
                fieldsWrap.children('.field-item').each(function(i){
                    if(!isInitEvent(jQuery(this), 'init-mem')){
                        //save latest_index and original_index
                        setMemoryValue(jQuery(this).find('[data-move]:first'), jQuery(this).index());
                    }
                });
                
                //if the sort for this wrap hasn't been set
                if(!isInitEvent(fieldsWrap, 'init-sort-event')){
                    //make the fields drag-sortable in the fields list
                    fieldsWrap.sortable({
                        //axis:'y',
                        containment:'document',
                        handle:'[data-move]',
                        revert:true,
                        revertDuration:revertDuration,
                        start:function(e, ui){
                            var fieldWrap=jQuery(ui.item[0]);
                            var popWrap=fieldWrap.parents('[popup-id]:first');
                            popWrap.addClass('dragging');
                            popWrap.addClass('drag-local');
                        },
                        stop:function(e, ui){
                            var fieldWrap=jQuery(ui.item[0]);
                            var popWrap=fieldWrap.parents('[popup-id]:first');
                            popWrap.removeClass('dragging').removeClass('drag-local');
                            var btn=fieldWrap.find('[data-move]:first');
                            moveData(btn, '[data-id].field-item:first');
                        }
                    });
                }
            }
        });
        
        //copy fields from the global edit screen
        jQuery('.globalWrap .field-item [data-move]').not('.init-move-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-move-event')){
                //get the parent elements
                var fieldWrap=el.parents('.field-item:first');
                var fieldsWrap=fieldWrap.parents('.field-items:first');
                var scopeWrap=fieldsWrap.parents('[data-scope]:first');
                
                //if the drag for this wrap hasn't been set
                if(!isInitEvent(fieldWrap, 'init-drag-event')){
                    //make the fields draggable to copy to local scope
                    fieldWrap.draggable({
                        //axis:'y',
                        containment:'document',
                        handle:'[data-move]',
                        revert:true,
                        revertDuration:revertDuration,
                        start:function(e, ui){
                            var fieldWrap=jQuery(this);
                            var popWrap=fieldWrap.parents('[popup-id]:first');
                            popWrap.addClass('dragging');
                            popWrap.addClass('drag-global');
                        },
                        stop:function(e, ui){
                            var fieldWrap=jQuery(this);
                            var popWrap=fieldWrap.parents('[popup-id]:first');
                            popWrap.removeClass('dragging').removeClass('drag-global');
                        }
                    });
                }
            }
        });
        
        //either global or local droppable wraps
        jQuery('.editFieldsWrap [data-scope]').each(function(){
            var scopeWrap=jQuery(this);
            if(!isInitEvent(scopeWrap, 'init-drop-event')){
                var scope=scopeWrap.attr('data-scope');
                if(scope=='global'){
                    
                    scopeWrap.droppable({
                        hoverClass:'drop-hover',
                        drop:function(event, ui){
                            var fieldItemWrap=jQuery('.field-item.ui-sortable-helper:first');
                            if(fieldItemWrap.length<1){
                                fieldItemWrap=jQuery('.field-item.ui-draggable-dragging:first');
                            }
                            dropFieldInto('global', fieldItemWrap);
                        }
                    });
                    
                }else{ //local
                    
                    scopeWrap.droppable({
                        hoverClass:'drop-hover',
                        drop:function(event, ui){
                            var fieldItemWrap=jQuery('.field-item.ui-draggable-dragging:first');
                            if(fieldItemWrap.length<1){
                                fieldItemWrap=jQuery('.field-item.ui-sortable-helper:first');
                            }
                            dropFieldInto('local', fieldItemWrap);
                        }
                    });
                    
                }
            }
        });
        
        //move sections
        jQuery('.overview .move-section[data-move]').not('.init-move-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-move-event')){
                //get the parent for the sortable items
                var ul=el.parents('ul:first');
                
                //for each overview item
                ul.children('li').each(function(i){
                    if(!isInitEvent(jQuery(this), 'init-mem')){
                        //save latest_index and original_index
                        setMemoryValue(jQuery(this).find('a.move-section:first'), jQuery(this).index());
                        //link the overview item to the content item
                        var sectionId=jQuery(this).attr('name');
                        jQuery(this)[0]['linked_div']=tokenWrap.find('div#'+sectionId+'[name="'+sectionId+'"]:first');
                        jQuery(this)[0]['linked_div'][0]['linked_li']=jQuery(this);
                    }
                });
                
                //if the sort for this ul hasn't been set
                if(!isInitEvent(ul, 'init-sort-event')){
                    ul.find('a.move-section').click(function(e){
                        e.preventDefault(); e.stopPropagation();
                    });
                    //make the sections drag-sortable in the overview
                    ul.sortable({
                        axis:'y',
                        handle:'a.move-section',
                        stop:function(e, ui){
                            var li=jQuery(ui.item[0]);
                            var btn=li.find('a.move-section:first');
                            moveData(btn, 'li[name]:first');
                        }
                    });
                }
            }
        });
    };
    
    //attach data-add handlers
    var initAddDataEvents=function(){
        
        var doInputAddData=function(input){
            var val=input.val();
            if(val.length>0){
                addData(input);
                input.val('');
            }
        };
        
        jQuery('input[type="text"][data-add]').not('.init-add-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-add-event')){
                //blur event
                el.blur(function() {
                    doInputAddData(jQuery(this));
                });
                //keyup event
                el.keyup(function(e) {
                    switch (e.keyCode) {
                        case 13: //enter key press
                            e.preventDefault();
                            doInputAddData(jQuery(this));
                            break;
                        case 27: //escape key press
                            e.preventDefault();
                            jQuery(this).val('');
                            break;
                    }
                });
            }
        });
        
        jQuery('span[data-add], div[data-add], a[data-add]').not('.init-add-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-add-event')){
                el.click(function(e){
                    e.preventDefault(); e.stopPropagation();
                    addData(jQuery(this));
                });
            }
        });

    };
    
    //attach data-edit handlers
    var initEditDataEvents=function(){
        //attach events to textarea and input text
        var initForText=function(txtEl){
            //click event
            txtEl.click(function() {
                jQuery(this).addClass('editing');
            });
            //blur event
            txtEl.blur(function() {
                editData(jQuery(this));
                jQuery(this).removeClass('editing');
            });
            //keyup event
            txtEl.keyup(function(e) {
                jQuery(this).addClass('editing');
            });
        };
        
        var setMemoryValue=function(el){
            var val=el.val();
            el[0]['original_value']=val;
            el[0]['latest_value']=val;
        };
        
        jQuery('textarea[data-edit]').not('.init-edit-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-edit-event')){
                setMemoryValue(el);
                initForText(el);
                //ctl+enter keypress
                el.keyup(function(e) {
                    switch (e.keyCode) {
                        case 13: //enter key press
                            if(e.ctrlKey){
                                editData(jQuery(this));
                                e.preventDefault();
                            }
                            break;
                    }
                });
            }
        });
        
        jQuery('input[type="text"][data-edit]').not('.init-edit-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-edit-event')){
                setMemoryValue(el);
                initForText(el);
                //enter keypress
                el.keyup(function(e) {
                    switch (e.keyCode) {
                        case 13: //enter key press
                            editData(jQuery(this));
                            e.preventDefault();
                            break;
                    }
                });
            }
        });
        
        jQuery('select[data-edit]').not('.init-edit-event').each(function(){
            var el=jQuery(this);
            if(!isInitEvent(el, 'init-edit-event')){
                setMemoryValue(el);
                el.change(function(){
                    editData(jQuery(this));
                });
            }
        });
    };
    
    var copyToClipboard=function(txt, callback){
        if (!navigator.clipboard){
            var textArea=document.createElement("textarea");
            textArea.value=txt;
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            
            try {
                var copied=document.execCommand('copy');
                if(callback!=undefined){
                    var stat='error, failed to copy';
                    if(copied!=undefined && copied.length>0){
                        stat='ok';
                    }
                    callback({status:stat});
                }
            } catch (err) {
                if(callback!=undefined){
                    callback({status:'error, '+err.stack});
                }
            }

            document.body.removeChild(textArea);
        }else{
            navigator.clipboard.writeText(txt).then(function(){
                if(callback!=undefined){
                    callback({status:'ok'});
                }
            },function(err){
                if(callback!=undefined){
                    callback({status:'error, '+err.stack});
                }
            });
        }
    };
    
    var initCopyToken_Event=function(btn){
        if(!isInitEvent(btn)){
            btn.click(function(e){
                e.preventDefault(); e.stopPropagation();
                var tokenKey=jQuery(this).html();
                //copy the token key to the clipboard
                copyToClipboard(tokenKey, function(res){
                    if(res.status=='ok'){
                        var floatId='copied_token_key';
                        //open indication that the token key was copied
                        syntaxlerHelper.openFloatMenu({
                            id:floatId,
                            options:[
                               {
                                    key:'success_copy',
                                    label:tokenKey+' Copied!'
                               }
                            ],
                            trigger:btn,
                            event:e
                        });
                        //wait a little bit then auto-close the float menu
                        setTimeout(function(){
                            syntaxlerHelper.closeFloatMenu(floatId);
                        },1500);
                    }
                });
            });
        }
    };
    
    //loop through each of the edit-button elements and attach their events
    var initEditBtnEvents=function(){
        jQuery('.edit-btn').not('.init-event').each(function(){
            var btn=jQuery(this); 
            if(btn.hasClass('add-token')){
                initOpenFieldEdit_Event(btn); //+ add/edit fields button
            }else if(btn.hasClass('edit-field')){
                initOpenFieldEdit_Event(btn);
            }else if(btn.hasClass('copy-token')){
                initCopyToken_Event(btn);
            }
        });
    };
    
    //SYNTAXLER
    
    //init textarea syntax highlighting elements
    var initSyntaxTextareas=function(){
        
        var textareas=jQuery('.token-wrap textarea.section-content');
        
        //section content areas
        syntaxlerHelper.init({
            textareas:textareas,
            classes:['section-syntax-highlight'],
            tab_spaces:tabSpaces,
            line_numbers:true,
            highlight_data:highlightHelper.data_fullHtmlTokens(),
            highlight_functions:highlightHelper.funcs_fullHtmlTokens(),
            highlights:highlightHelper.hl_fullHtmlTokens(),
            suggest:highlightHelper.suggest_content()
        });
        
        //allow indicating save needs to happen to keep changes made in content textareas
        textareas.each(function(){
            jQuery(this).keyup(function(){
                setSavePendingIndicator();
            });
            jQuery(this).blur(function(){
                setSavePendingIndicator();
            });
        });
        
        
    };
    
    var hasSavePending=function(){
        var hasSavePending=false;
        var bodEl=jQuery('body:first');
        if(bodEl.hasClass('save-pending')){
            hasSavePending=true;
        }else{
            var screenId=getCurrentScreenId();
            if(screenId!='home'){
                var screenWrap=jQuery('.popup[popup-id="'+screenId+'"]:first');
                if(screenWrap.hasClass('save-pending')){
                    hasSavePending=true;
                }
            }
        }
        return hasSavePending;
    };
    
    var initLossOfChangesWarning=function(){
        if(!window.hasOwnProperty('init_lose_changes_warning')){
            window['init_lose_changes_warning']=true;
            window.onbeforeunload=function(e){
                if(hasSavePending()){
                    e.preventDefault(); e.stopPropagation();
                    return true;
                }
            };
        }
    };
    
    var initShortcutKeys=function(){
        jQuery(window).bind('keydown',function(e){
            if(e.ctrlKey || e.metaKey){
                var charPressed=String.fromCharCode(e.which).toLowerCase();
                switch(charPressed){
                    case 's': 
                        e.preventDefault();
                        //saveChanges(function(){return false;});
                        saveChanges();
                        break;
                }
            }
        });
    };
    
    var initRenamePage=function(){
        if(pagesHelper.isPageMode('edit')){
            serverHelper.isServerUp(function(res){
                if(res!=undefined){
                    //allow this field to be edited because the server is on
                    var fileIdInput=jQuery('.file-id-wrap [data-edit="file_id"]:first');
                    fileIdInput.attr('readonly', '');
                    fileIdInput.prop('readonly', false);
                    fileIdInput.removeAttr('readonly');
                    //validate file name input
                    var validateFileName=function(input){
                        //make sure the file name doesn't have illegal characters
                        var val=getCleanFileNameVal(input.val());
                        //if the page name has been changed from the current page name
                        if(val!=pagesHelper.getPageId()){
                            //make sure the file name is unique within the current scope
                            getUniqueFileNameInScope(val, function(uniqueFile){
                                if(input.val()!=uniqueFile){ input.val(uniqueFile); }
                            });
                        }else{
                            if(input.val()!=val){ input.val(val); }
                        }
                    };
                    //input events to validate file name
                    fileIdInput.blur(function(){
                        validateFileName(jQuery(this));
                    });
                    fileIdInput.keyup(function(e) {
                        switch (e.keyCode) {
                            case 13: //enter key press
                                e.preventDefault();
                                validateFileName(jQuery(this));
                                break;
                            case 27: //escape key press
                                e.preventDefault();
                                if(jQuery(this)[0].hasOwnProperty('original_page_id')){                                                 jQuery(this).val(jQuery(this)[0]['original_page_id']);
                                }
                                break;
                        }
                    });
                }
            });
        }
    };
    
    var initDeletePage=function(){
        if(pagesHelper.isPageMode('edit')){
            serverHelper.isServerUp(function(res){
                if(res!=undefined){
                    var deleteBtn=jQuery('.save-as-wrap .delete-page-btn:first');
                    deleteBtn.removeClass('disabled');
                    deleteBtn.click(function(e){
                        var pageId=pagesHelper.getPageId();
                        if(confirmDelete('Page: \n'+pageId+'.html')){
                            //delete the current page
                            serverHelper.deletePage();
                        }
                    });
                }
            });
        }
    }
    
    //INIT
    
    // private
    var initEdit=function(){
        var isEditMode=false;
        isEditMode=pagesHelper.isPageMode('edit');
        if(isEditMode){
    
            //init textarea syntax highlighting elements
            initSyntaxTextareas();
            
            //attach edit-btn events
            initEditBtnEvents();
            
            //attach data-edit events
            initEditDataEvents();
            
            //attach data-add events
            initAddDataEvents();
            
            //attach data-delete events
            initDeleteDataEvents();
            
            //attach data-move events
            initMoveDataEvents();
            
            //leave page without saving changes?
            initLossOfChangesWarning();
        }
        return isEditMode;
    };
    
    var saveChanges=function(callback){
        var canSave=false;
        //if in edit mode
        if(pagesHelper.isPageMode('edit')){
            //if on the home screen
            var screenId=getCurrentScreenId();
            if(screenId=='home'){
                //if there are save edits pending
                if(hasSavePending()){

                    //get current page id and current scope
                    var pageId=pagesHelper.getPageId();
                    var dirScope=pagesHelper.getPageScopeFolder();
                    
                    //args to send back to callback();
                    var args={};

                    //first save to the html file
                    var contentStr=getHtmlSaveContent();
                    serverHelper.saveData(dirScope, pageId, 'index.html', contentStr, function(res){
                        args['index.html']=res;
                    
                        //if there is a rename needed for the html file
                        var doRename=false, newPageId=undefined;
                        var fileIdInput=jQuery('.file-id-wrap [data-edit="file_id"]:first');
                        if(fileIdInput.hasClass('rename_pending')){
                            doRename=true;
                            //update the data with the new page ID
                            var oldId=fileIdInput[0]['original_page_id'];
                            var pagesCopy=helper.copyJson(pages[oldId]);
                            delete pages[oldId];
                            newPageId=fileIdInput.val().trim();
                            pages[newPageId]=pagesCopy;
                        }
                    
                        //save to data/pages.js (if there was a page rename, it should have updated the pages json by now)
                        var pagesStr=getPagesJsSaveContent();                    
                        serverHelper.saveData(dirScope, pageId, 'pages.js', pagesStr, function(res){
                            args['pages.js']=res;
                            //save to data/fields.js
                            var fieldsStr=getFieldsJsSaveContent();
                            serverHelper.saveData(dirScope, pageId, 'fields.js', fieldsStr, function(res){
                                args['fields.js']=res;
                                //finish save
                                var finishSave=function(){
                                    setSavePendingIndicator('home', false);
                                    var doRefresh=true;
                                    if(callback!=undefined){
                                        doRefresh=callback(args);
                                    }
                                    //if the page was renamed, then a page refresh is required
                                    if(doRename){
                                        pagesHelper.setPageId(newPageId); //redirect to renamed page
                                    }else if(doRefresh){ //page was not renamed, only refresh if callback function was not given or returns true
                                        pagesHelper.reloadPage();
                                    }
                                };
                                //if there was a rename
                                if(doRename){
                                    var newPageId=fileIdInput.val();
                                    serverHelper.renameContentFile(dirScope, pageId, newPageId, function(res){
                                        args['rename']=res;
                                        fileIdInput.removeClass('rename_pending');
                                        finishSave();
                                    });
                                }else{ //no rename needed
                                   finishSave();
                                }

                            });
                        });
                    });
                    
                    canSave=true;
                }
            }
        }
        return canSave;
    };

    //public 
    return {
        setValuesClickBarContent:function(clickBar, editArea, valArray){
            setValuesClickBarContent(clickBar, editArea, valArray);
        },
        updateShownFieldPropsForType:function(fieldItemWrap){
            updateShownFieldPropsForType(fieldItemWrap);
        },
        triggerVariationsClick:function(varBtn){
            triggerVariationsClick(varBtn);
        },
        updateVarCount:function(varBtn, variationData){
            updateVarCount(varBtn, variationData);
        },
        getHtmlSaveContent:function(){
            return getHtmlSaveContent();
        },
        getHtmlSaveContentHead:function(folderScope){
            return getHtmlSaveContentHead(folderScope);
        },
        getHtmlSaveContentNew:function(folderScope){
            return getHtmlSaveContentNew(folderScope);
        },
        getNewPageJson:function(){
            return getNewPageJson();
        },
        getFieldsJsSaveContent:function(){
            return getFieldsJsSaveContent();
        },
        getPagesJsSaveContent:function(){
            return getPagesJsSaveContent();
        },
        openNew:function(){
            openNew();
        },
        openSeeData:function(){
            openSeeData();
        },
        hasSavePending:function(){
            return hasSavePending();
        },
        saveChanges:function(callback){
            return saveChanges(callback);
        },
        //figure out if an event is already init
        isInitEvent:function(el, initClass){
            return isInitEvent(el, initClass);
        },
        getTabSpaces:function(){
            return tabSpaces;
        },
        init:function(){
            initShortcutKeys();
            initRenamePage();
            initDeletePage();
            return initEdit();
        }
    };
}());