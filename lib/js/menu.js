var menuHelper=(function(){
    // private
    var getMenuData=function(){
        var pageMode=pagesHelper.getPageMode();
        var pageModeLabel=pageMode;
        pageModeLabel=pageModeLabel.substring(0, 1).toUpperCase() + pageModeLabel.substring(1);
        
        var saveSvg=getSaveSvg();
        
        return [
            {
                id:'file',
                label:'File',
                svg_icons:{
                    save_pending:saveSvg
                },
                children:[
                    {
                        id:'new',
                        label:'New',
                        hide_in_modes:['view'],
                        action:function(li, data){
                            editHelper.openNew();
                        }
                    },
                    {
                        id:'open',
                        label:'Open',
                        action:function(li, data){
                            pagesHelper.goHome();
                        }
                    },
                    {
                        id:'save',
                        label:'Save',
                        hide_in_modes:['view'],
                        svg_icons:{
                            save_pending:saveSvg
                        },
                        action:function(li, data){
                            editHelper.saveChanges();
                        }
                    },
                    {
                        id:'duplicate_page',
                        label:'Duplicate Page',
                        hide_in_modes:['view'],
                        action:function(li, data){
                            serverHelper.duplicatePage();
                        }
                    },
                    {
                        id:'see_data',
                        label:'See Data',
                        hide_in_modes:['view'],
                        action:function(li, data){
                            editHelper.openSeeData();
                        }
                    }
                ]
            },
            {
                id:'toggle_mode',
                label:pageModeLabel,
                state:pageMode, //view / edit
                action:function(li, data){
                    if(pagesHelper.isPageMode('edit')){
                        pagesHelper.setPageMode('view');
                    }else{
                         pagesHelper.setPageMode('edit');
                    }
                },
                children:[
                    {
                        id:'view',
                        label:'View',
                        hide_in_modes:['view'],
                        action:function(li, data){
                            pagesHelper.setPageMode('view');
                        }
                    },
                    {
                        id:'edit',
                        label:'Edit',
                        hide_in_modes:['edit'],
                        action:function(li, data){
                            pagesHelper.setPageMode('edit');
                        }
                    }
                ]
            }
        ];
    };
    
    var hideMenuLi=function(which){
        var li=getMenuLi(which);
        li.addClass('hidden');
    };
    
    var showMenuLi=function(which){
        var li=getMenuLi(which);
        li.removeClass('hidden');
    };
    
    //get an li element based on an id path like, id1.id2.id3
    var getMenuLi=function(idPath){
        var li;
        if(typeof idPath=='string'){
            var wrap=jQuery('.menu .menu-ul:first');
            var idArr=idPath.split('.');
            for(var i=0;i<idArr.length;i++){
                var id=idArr[i];
                var nextLi=wrap.children('li[data-id="'+id+'"]:first');
                if(nextLi.length>0){
                    if(i+1==idArr.length){
                       li=nextLi;
                    }else{
                        wrap=nextLi.children('ul:last');
                        if(wrap.length<1){
                            break;
                        }
                    }
                }else{
                    break;
                }
            }
        }else if(idPath.attr('data-id')!=undefined){
            li=idPath;   
        }
        return li;
    };
    
    var getSaveSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 448"><path d="M96 384h192v-96h-192v96zM320 384h32v-224c0-4.75-4.25-15-7.5-18.25l-70.25-70.25c-3.5-3.5-13.25-7.5-18.25-7.5v104c0 13.25-10.75 24-24 24h-144c-13.25 0-24-10.75-24-24v-104h-32v320h32v-104c0-13.25 10.75-24 24-24h208c13.25 0 24 10.75 24 24v104zM224 152v-80c0-4.25-3.75-8-8-8h-48c-4.25 0-8 3.75-8 8v80c0 4.25 3.75 8 8 8h48c4.25 0 8-3.75 8-8zM384 160v232c0 13.25-10.75 24-24 24h-336c-13.25 0-24-10.75-24-24v-336c0-13.25 10.75-24 24-24h232c13.25 0 31.5 7.5 41 17l70 70c9.5 9.5 17 27.75 17 41z"></path></svg>';
    };
    
    var initMenu=function(){
        var pageMode=pagesHelper.getPageMode();
        var bodyEl=jQuery('body:first');
        bodyEl.addClass('has-menu');
        var menuW=bodyEl.children('.menu:last');
        //init menu wrap
        if(menuW.length<1){
            bodyEl.append('<div class="menu"></div>');
            menuW=bodyEl.children('.menu:last');
        }
        //clear menu contents
        menuW.html('<ul class="menu-ul"></ul>');
        var ul=menuW.children('.menu-ul:first');
        //populate menu wrap with menu items
        var menuData=getMenuData();
        var initMenuLevel=function(arr, wrap, level){
            for(var a=0;a<arr.length;a++){
                var itemJson=arr[a];
                if(itemJson.hasOwnProperty('id')){
                    wrap.addClass('l'+level);
                    wrap.append('<li class="l'+level+'" data-id="'+itemJson['id']+'"></li>');
                    var li=wrap.children('li[data-id="'+itemJson['id']+'"]:last');
                    li[0]['menu_item_data']=itemJson;
                    if(itemJson.hasOwnProperty('state')){
                        li.attr('data-state', itemJson['state']);
                    }
                    var svgHtml='';
                    var svgIconClasses='';
                    if(itemJson.hasOwnProperty('svg_icons')){
                        for(var v in itemJson['svg_icons']){
                            if(itemJson['svg_icons'].hasOwnProperty(v)){
                                svgIconClasses+=' '+v;
                                svgHtml+='<span class="menu-icon '+v+'">'+itemJson['svg_icons'][v]+'</span>';
                            }
                        }
                    }
                    if(svgHtml.length>0){ 
                        svgHtml='<span class="menu-icons'+svgIconClasses+'">'+svgHtml+'</span>';
                    }
                    var label='[label]';
                    if(itemJson.hasOwnProperty('label')){
                        label=itemJson['label'];
                    }
                    li.append('<span class="label">'+svgHtml+label+'</span>');
                    var spanLabel=li.children('span.label:last');
                    if(itemJson.hasOwnProperty('children')){
                        if(itemJson['children'].length>0){
                            li.addClass('has-children');
                            li.append('<ul></ul>');
                            var childUl=li.children('ul:last');
                            //recursive children
                            initMenuLevel(itemJson['children'], childUl, (level+1));
                        }
                    }
                    if(itemJson.hasOwnProperty('hide_in_modes')){
                        if(itemJson['hide_in_modes'].indexOf(pageMode)!=-1){
                            hideMenuLi(li);
                        }
                    }
                    if(itemJson.hasOwnProperty('action')){
                        spanLabel.click(function(e){
                            e.preventDefault();e.stopPropagation();
                            var data=jQuery(this).parent()[0]['menu_item_data'];
                            var argData={};
                            for(var d in data){
                                if(data.hasOwnProperty(d)){
                                    if(d!='action'){
                                        argData[d]=data[d];
                                    }
                                }
                            }
                            //call the action function
                            data['action'](jQuery(this).parent(), argData);
                        });
                    }
                }
            }
        };
        initMenuLevel(menuData, ul, 0);
    };
    
    
    //public 
    return {
        showMenuLi:function(whichLi){
            showMenuLi(whichLi);
        },
        hideMenuLi:function(whichLi){
            hideMenuLi(whichLi);
        },
        getMenuLi:function(idPath){
            return getMenuLi(idPath);
        },
        getMenuData:function(){
            return getMenuData();
        },
        init:function(){
            initMenu();
        }
    };
}());