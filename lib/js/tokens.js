var tokensHelper=(function(){
    // private
    
    var originalHtml;
    
    var getFieldToken=function(field){
        return field.attr('data-token');
    };
    
    var getLocalStoreKey=function(token) {
        //get the scope folder for this token
        var scopeFolder=pagesHelper.getPageScopeFolder();
        //replace / with _
        scopeFolder=helper.replaceAll(scopeFolder, '/', '_');
        //assemble token key
        token = 'token--' + escape(scopeFolder) + '--' + escape(token);
        return token;
    };
    
    var saveLocalStoreVal=function(field){
        var token=getFieldToken(field);
        var key=getLocalStoreKey(token);
        var val=localStorage.getItem(key);
        var newVal=field[0]['get_value']();
        if (val!=undefined){ //local store item already exists
            if(val!=newVal){ //new value is different from old saved value
                //remove existing local store item
                localStorage.removeItem(key);
                localStorage.setItem(key, newVal);
            }
        }else{ //no value saved yet
            localStorage.setItem(key, newVal);
        }
    };
    
    //iterator over every field on this page
    var eachField=function(itemCallback){
        if(itemCallback!=undefined){
            jQuery('.field[data-token] [data-token]').each(function(){
                itemCallback(jQuery(this));
            });
        }
    };
    
    //see public method call (below) for more info
    var replaceTokensInStr=function(args){
        var str=args['str'];
        var retData={};
        //for each format
        for(var f=0;f<args.formats.length;f++){
            retData[args.formats[f]]=str;
        }
        eachField(function(field){
            retData=replaceOneToken(field,args,retData);
        });
        return retData;
    };
    
    var replaceOneToken=function(field,args,retData){
        var token=getFieldToken(field);
        var tokenData=fieldsHelper.getFieldData(token);
        var triggerField;
        if(args.hasOwnProperty('trigger')){
            triggerField=args['trigger'];
        }
        if(tokenData!=undefined){
            var newVal=field[0]['get_value']();
            if(token!=newVal){
                //set the local store for this token
                saveLocalStoreVal(field);

                //escape tokens that start with \\
                var escapeTokenStr='<(__ecaped^token__)>';
                var escapeAToken=function(htmlStr, tKey){
                    if(htmlStr.indexOf('\\\\'+tKey)!=-1){
                        htmlStr=helper.replaceAll(htmlStr, '\\\\'+tKey, escapeTokenStr);
                    }
                    return htmlStr;
                };

                //restore tokens that started with \\
                var restoreAToken=function(htmlStr, tKey){
                    if(htmlStr.indexOf(escapeTokenStr)!=-1){
                        htmlStr=helper.replaceAll(htmlStr, escapeTokenStr, tKey);
                    }
                    return htmlStr;
                };

                //replace one token value
                var replaceOneVal=function(replaceThisKey, withThisVal){
                    var elemAttrs=[], elemAttrsSel='';
                    //if the str_values format is being called
                    if(retData.hasOwnProperty('str_values')){
                        var htmlWrap=jQuery.parseHTML(retData['str_values']);
                        jQuery(htmlWrap).find('*').each(function(){
                            if(jQuery(this)[0].attributes.length>0){
                                for(var a=0;a<jQuery(this)[0].attributes.length;a++){
                                    var attrKey=jQuery(this)[0].attributes[a].name;
                                    if(elemAttrs.indexOf(attrKey)==-1){
                                        //add to the array of attributes that appear
                                        elemAttrs.push(attrKey);
                                    }
                                }
                            }
                        });
                        for(var e=0;e<elemAttrs.length;e++){
                            if(elemAttrsSel.length>0){ elemAttrsSel+=', '; }
                            elemAttrsSel+='['+elemAttrs[e]+']';
                        }
                    }
                    //for each format
                    for(var f=0;f<args['formats'].length;f++){
                        var format=args['formats'][f];
                        //escape tokens that start with \
                        retData[format]=escapeAToken(retData[format], replaceThisKey);

                        if(retData[format].indexOf(replaceThisKey)!=-1){
                            if(replaceThisKey!=withThisVal){
                                if(format=='str_args'){
                                    if(!retData.hasOwnProperty('arg_tokens')){
                                        retData['start_arg_token']='[${#';
                                        retData['end_arg_token']='}!]';
                                        retData['arg_tokens']=[];
                                    }
                                    var argIndex=retData['arg_tokens'].length;
                                    var argToInsert=retData['start_arg_token']+argIndex+retData['end_arg_token'];
                                    retData['arg_tokens'].push({
                                        arg:argToInsert,
                                        token:token,
                                        variation:replaceThisKey,
                                        data:tokenData,
                                        value:withThisVal
                                    });
                                    //replace the token with a placeholder value so tokens can easily be identified as arguments
                                    retData[format]=helper.replaceAll(retData[format],replaceThisKey,argToInsert);
                                }else{
                                    //if there are any html elements with attributes (replace these first so they don't get markup)
                                    if(elemAttrs.length>0){
                                        var replacedAttr=false;
                                        var htmlWrap=jQuery.parseHTML(retData[format]);
                                        jQuery(htmlWrap).find(elemAttrsSel).each(function(){
                                            for(var a=0;a<jQuery(this)[0].attributes.length;a++){
                                                var attrKey=jQuery(this)[0].attributes[a].name;
                                                var attrVal=jQuery(this).attr(attrKey);
                                                if(attrVal.indexOf(replaceThisKey)!=-1){
                                                    attrVal=helper.replaceAll(attrVal,replaceThisKey,withThisVal);
                                                    jQuery(this).attr(attrKey, attrVal);
                                                    replacedAttr=true;
                                                }
                                            }
                                        });
                                        //if any attributes were replaced, then update retData[format]
                                        if(replacedAttr){
                                            var updatedHtml='';
                                            jQuery(htmlWrap).each(function(){
                                                updatedHtml+=jQuery(this)[0].outerHTML;
                                            });
                                            retData[format]=updatedHtml;
                                        }
                                    }
                                    var encodeKey=encodeURIComponent(replaceThisKey);
                                    var startSpan='<token data-key="'+encodeKey+'">'; 
                                    var endSpan='</token>';
                                    var valueToInsert=startSpan+withThisVal+endSpan;
                                    retData[format]=helper.replaceAll(retData[format],replaceThisKey,valueToInsert);
                                }
                            }
                        }

                        //restore tokens that started with \
                        retData[format]=restoreAToken(retData[format], replaceThisKey);
                    }
                };

                //if not a checkbox (checkboxes don't have values to replace in the same way)
                if(!field.is('[type="checkbox"]')){
                    
                    //get the token variations in sorted order from longest to shortest
                    var varData=tokensHelper.getTokenVariations(tokenData, newVal);
                    var sortedKeys=varData.keys;
                    var variations=varData.vals;

                    //for each variation of this token to replace
                    for(var n=sortedKeys.length-1;n>-1;n--){
                        var key=sortedKeys[n];
                        var val=variations[key];
                        
                        //for each built-in casing variation
                        var casings=tokensHelper.getLetterTokenCasings();
                        for(var c=0;c<casings.length;c++){
                            var casing=casings[c];
                            //replace one of the casing variations on the page
                            replaceOneVal(key+casing.key, casing.set(val));
                        }

                        //standard variation
                        replaceOneVal(key, val);
                    }

                }else{ //this is a checkbox field
                    replaceOneVal(token, newVal);
                }
            }
        }
        return retData;
    };
    
    var pageLoadLocalStoreValues=function(){
        eachField(function(field){
            var token=getFieldToken(field);
            var key=getLocalStoreKey(token);
            var val=localStorage.getItem(key);
            if (val!=undefined){
                var defaultVal=field[0]['get_value']();
                if(defaultVal!=val){
                    field[0]['set_value'](val);
                }
            }
        });
    };
    
    var selectText=function(node) {
        var txt;
        var range;
        node=node[0];
        if (document.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(node);
            range.select();
        } else if (window.getSelection) {
            const selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(node);
            selection.removeAllRanges();
            selection.addRange(range);
        } else {
            console.warn("Could not select text in node: Unsupported browser.")
        }
        if(range!=undefined){
            if(range.startContainer){
                if(range.startContainer.innerText){
                    txt=range.startContainer.innerText;
                }
            }
        }
        return txt;
    };
    
    var initCodeElementEvents=function(){
        jQuery('.token-wrap code').each(function(){
            var codeEl=jQuery(this);
            codeEl.dblclick(function(){
                var codeEl=jQuery(this);
                var txt=selectText(codeEl);
                if(txt!=undefined){
                    document.execCommand('copy');
                    codeEl.addClass('copied');
                    setTimeout(function(){
                        codeEl.removeClass('copied'); 
                    },500);
                }
            });
        });
    };
    
    //public 
    return {
/*
    replaceTokensInStr(args)
    
    insert tokens into a give string...

    args for this function 

    {
        str: "full string to insert tokens into", 
        trigger: triggerElem, (optional)
        formats:[
            'str_values',      <-- just inserts values
            'str_args'         <-- inserts placeholder arguments and creates an array of actual values
        ]
    }
*/
        replaceTokensInStr:function(args){
            return replaceTokensInStr(args);
        },
        //replace tokens on the page
        replacePageTokens:function(triggerElem){
            if(!pagesHelper.isPageMode('edit')){
                var tokensWrap=jQuery('.token-wrap:first');
                if(originalHtml==undefined){
                    originalHtml=tokensWrap.html();
                }
                var html=originalHtml;
                
                //handle dynamic block chains including if/elseif/else
                html=dynamicHelper.beforeTokenReplace(html);

                //replace all of the tokens in html
                var data=replaceTokensInStr({str:html, trigger:triggerElem, formats:['str_values']});
                html=data['str_values'];

                tokensWrap.html(html);
                
                //add &nbsp; and <br /> inside code elements
                pagesHelper.escapeInsideCode();
                
                initCodeElementEvents();
            }
        },
        replaceSectionTokens:function(){
            if(!pagesHelper.isPageMode('edit')){
                var titleToken='@{section.title}';
                
                var tokensWrap=jQuery('.token-wrap:first');
                var sections=tokensWrap.children('div[id][name]');

                //for each section
                sections.each(function(){
                    var sectionTokenFound=false;
                    var sectHtml=jQuery(this).html();
                    
                    //if titleToken is used in this section
                    if(sectHtml.indexOf(titleToken)!=-1){
                        sectionTokenFound=true;
                        var titleEl=jQuery(this).find('.section-title:first');
                        var titleTxt=titleEl.text();
                        sectHtml=helper.replaceAll(sectHtml, titleToken, titleTxt);
                    }
                    
                    if(sectionTokenFound){
                        jQuery(this).html(sectHtml);
                    }
                });
                
                pagesHelper.updateFileOutput();
            }
        },
        //get all of the keys for a given token
        getTokenVariations:function(tokenData, testVal){
            var tokenId=tokenData.id;
            
            var sortedKeys=[]; sortedKeys.push(tokenId);
            var variations={}; 
            
            //if also getting the value for the variations
            if(testVal!=undefined){
               variations[tokenId]=testVal;
            }
            
            //go through each variation if there are any
            if(tokenData.hasOwnProperty('variations')){
                var varPrefix=this.getVariationPrefix();
                for(var varKey in tokenData.variations){
                    if(tokenData.variations.hasOwnProperty(varKey)){
                        sortedKeys.push(tokenId+varPrefix+varKey);
                        //if also getting the value for the variations
                        if(testVal!=undefined){
                            var varVal=undefined;
                            if(typeof tokenData.variations[varKey]=='string'){
                                //evaluate the string as a function
                                var varFunc=new Function('val', tokenData.variations[varKey]+'\n\nreturn val;'); 
                                varVal=varFunc(testVal);
                            }
                            //add this value to the list of variations to replace
                            variations[tokenId+varPrefix+varKey]=varVal;
                        }
                    }
                }
            }

            sortedKeys.sort();
            
            return {
                keys:sortedKeys,
                vals:variations
            }
        },
        //get an array of different casings to apply to a token
        getLetterTokenCasings:function(){
            return [
                {
                    key:'(ab)',
                    set:function(val){
                        return val.toLowerCase();
                    }
                },
                {
                    key:'(AB)',
                    set:function(val){
                        return val.toUpperCase();
                    }
                },
                {
                    key:'(Ab)',
                    set:function(val){
                        if(val.length>1){
                            val=val.substring(0, 1).toUpperCase() + val.substring(1);
                        }else{
                            val.toUpperCase();
                        }
                        return val;
                    }
                },
                {
                    key:'(aB)',
                    set:function(val){
                        if(val.length>1){
                            val=val.substring(0, 1).toLowerCase() + val.substring(1);
                        }else{
                            val.toLowerCase();
                        }
                        return val;
                    }
                }
            ];
        },
        //get a list of all token variation keys/casings to pass to syntax highlighter
        getSerializedTokens:function(){
            var serialTokens=[];
            var pageData=pagesHelper.getPageData();
            if(pageData.hasOwnProperty('fields')){
                //get the letter casings
                var casings=this.getLetterTokenCasings();
                //for each field
                for(var f=0;f<pageData.fields.length;f++){
                    var fieldData=fieldsHelper.getFieldData(pageData.fields[f]);
                    //if this is not a plain html field
                    if(!fieldData.hasOwnProperty('html')){
                        //get the token variations in sorted order from longest to shortest
                        var varData=this.getTokenVariations(fieldData);
                        //for each variation of this token to replace
                        for(var k=varData.keys.length-1;k>-1;k--){
                            var key=varData.keys[k];
                            //for each of the built-in casing variations
                            for(var c=0;c<casings.length;c++){
                                serialTokens.push(key+casings[c].key);
                            }
                            //no casing
                            serialTokens.push(key);
                        }
                    }
                }
            }
            return serialTokens;
        },
        //get the string or character that begins the variation after the token
        getVariationPrefix:function(){
            return ':';
        },
        getLocalStoreKey:function(token){
            return getLocalStoreKey(token);
        },
        init:function(){
            pageLoadLocalStoreValues();
            
            this.replacePageTokens();
            this.replaceSectionTokens();
        }
    };
}());