var fieldsHelper=(function(){
    // private
    
    //get all field definitions
    var getAllFieldTypes=function(){
        return [
            {
                id:'text',
                get_html:getTextFieldHtml,
                init_events:initTextFieldEvents
            },
            {
                id:'select',
                get_html:getSelectFieldHtml,
                init_events:initSelectFieldEvents
            },
            {
                id:'checkbox',
                get_html:getCheckboxFieldHtml,
                init_events:initCheckboxFieldEvents,
                disable_props:['variations', 'values']
            }
        ];
    };

    //get all field properties (for admin screens)
    var getAllPropTypes=function(){
        return [
            {
                id:'id',
                set_html:setIdPropHtml
            },
            {
                id:'type',
                set_html:setTypePropHtml,
                init_events:initTypePropEvents
            },
            {
                id:'label',
                set_html:setLabelPropHtml
            },
            {
                id:'class',
                set_html:setClassPropHtml
            },
            {
                id:'summary',
                set_html:setSummaryPropHtml
            },
            {
                id:'values',
                set_html:setValuesPropHtml,
                init_events:initValuesPropEvents
            },
            {
                id:'variations',
                set_html:setVariationsPropHtml,
                init_events:initVariationsPropEvents
            }
        ];
    };
    
    //get the field defition by type ID
    var getFieldType=function(typeId){
        var type;
        var types=getAllFieldTypes();
        for(var t=0;t<types.length;t++){
            if(types[t]['id']==typeId){
                type=types[t];
                break;
            }
        }
        return type;
    };
    
    //get the prop defition by type ID
    var getPropType=function(typeId){
        var type;
        var types=getAllPropTypes();
        for(var t=0;t<types.length;t++){
            if(types[t]['id']==typeId){
                type=types[t];
                break;
            }
        }
        return type;
    };
    
    //init all the events for the fields
    var initFieldEvents=function(){
        
        //get all the field types
        var fieldTypes=getAllFieldTypes();
        
        //init the events of each field type
        for(var f=0;f<fieldTypes.length;f++){
            var fieldType=fieldTypes[f];
            if(fieldType.hasOwnProperty('init_events')){
                fieldType.init_events();
            }
        }
        
        //add syntaxler highlighting to plain text areas
        var fields=jQuery('.fields:first');
        var plainHtmlTextareas=fields.find('textarea.plain-html-field');
        
        //plain html fields
        syntaxlerHelper.init({
            textareas:plainHtmlTextareas,
            classes:['field-html'],
            tab_spaces:editHelper.getTabSpaces(),
            line_numbers:true,
            highlight_data:highlightHelper.data_fullHtml(),
            highlight_functions:highlightHelper.funcs_fullHtml(),
            highlights:highlightHelper.hl_fullHtml()
        });
    };
    
    //get the default value for a field
    var getFieldDefaultValue=function(fieldData){
        var defaultVal='[values:0]';
        if(fieldData.hasOwnProperty('values')){
            if(fieldData.values.length>0){
                if(typeof fieldData.values == 'string'){
                    defaultVal=fieldData.values;
                }else{
                    defaultVal=fieldData.values[0];
                }
            }
        }
        return defaultVal;
    };
    
    //TEXT type html
    var getTextFieldHtml=function(fieldData){
        var defaultVal=getFieldDefaultValue(fieldData);
        var html='<input spellcheck="false" name="field-'+fieldData.id+'" data-token="'+fieldData.id+'" type="text" value="'+defaultVal+'" />';
        return html;
    };
    
    //TEXT type events
    var initTextFieldEvents=function(){
        var fields=jQuery('.fields:first');
        fields.find('input[type="text"][data-token]').each(function(){
            var input=jQuery(this);
            if(!editHelper.isInitEvent(input, 'init-text-events')){
            //SPECIAL METHODS USED IN DIFFERENT AREAS

                //trigger the value enter to replace tokens
                input[0]['trigger_replace_tokens']=function(){
                    tokensHelper.replacePageTokens(jQuery(this));
                    tokensHelper.replaceSectionTokens();
                }
                //the get value function for this field
                input[0]['get_value']=function(){
                    return jQuery(this).val();
                }
                //the set value function for this field
                input[0]['set_value']=function(v){
                    jQuery(this).val(v);
                    return jQuery(this)[0]['get_value']();
                }
                input[0]['original_val']=input[0].get_value(); //remove original value

            //NORMAL EVENTS

                //click event
                input.click(function() {
                    jQuery(this).addClass('editing');
                });
                //blur event
                input.blur(function() {
                    jQuery(this)[0]['trigger_replace_tokens']();
                    jQuery(this).removeClass('editing');
                });
                //enter keypress
                input.keyup(function(e) {
                    switch (e.keyCode) {
                        case 13: //enter key press
                            jQuery(this)[0]['trigger_replace_tokens']();
                            e.preventDefault();
                            break;
                        case 27: //escape key press
                            e.preventDefault();
                            //if there is an original value for this input
                            if (jQuery(this)[0].hasOwnProperty('original_val')) {
                                var origVal = jQuery(this)[0]['original_val'];
                                if (origVal != undefined) {
                                    //if the original value is not already in the input
                                    if (jQuery(this)[0].get_value() != origVal) {
                                        //restore the original value
                                        jQuery(this)[0].set_value(origVal);
                                        jQuery(this)[0]['trigger_replace_tokens']();
                                    }
                                }
                            }
                            break;
                        default: //any other key press
                            jQuery(this).addClass('editing');
                            break;
                    }
                });
            }
        });
    };
    
    //SELECT type html
    var getSelectFieldHtml=function(fieldData){
        var defaultVal=getFieldDefaultValue(fieldData);
        var html='<select name="field-'+fieldData.id+'" data-token="'+fieldData.id+'">';
        var valuesHtml='<option>[values]</option>';
        if(fieldData.hasOwnProperty('values')){
            for(var v=0;v<fieldData.values.length;v++){
                if(v==0){ valuesHtml=''; }
                valuesHtml+='<option>'+fieldData.values[v]+'</option>';
            }
        }
        html+=valuesHtml;
        html+='</select>';
        return html;
    };
    
    //SELECT type events
    var initSelectFieldEvents=function(select){
        var fields=jQuery('.fields:first');
        fields.find('select[data-token]').each(function(){
            var select=jQuery(this);
            if(!editHelper.isInitEvent(select, 'init-select-events')){
            //SPECIAL METHODS USED IN DIFFERENT AREAS

                //trigger the value enter to replace tokens
                select[0]['trigger_replace_tokens']=function(){
                    tokensHelper.replacePageTokens(jQuery(this));
                    tokensHelper.replaceSectionTokens();
                }
                //the get value function for this field
                select[0]['get_value']=function(){
                    return jQuery(this).val();
                }
                //the set value function for this field
                select[0]['set_value']=function(v){
                    jQuery(this).val(v);
                    return jQuery(this)[0]['get_value']();
                }
                select[0]['original_val']=select[0].get_value(); //remove original value

            //NORMAL EVENTS

                select.change(function(){
                    jQuery(this)[0]['trigger_replace_tokens']();
                });
            }
        });
    };
    
    //CHECKBOX type html
    var getCheckboxFieldHtml=function(fieldData){
        var defaultVal=getFieldDefaultValue(fieldData);
        var html='<input name="field-'+fieldData.id+'" data-token="'+fieldData.id+'" type="checkbox" />';
        return html;
    };
    
    //CHECKBOX type events
    var initCheckboxFieldEvents=function(checkbox){
        var fields=jQuery('.fields:first');
        fields.find('input[type="checkbox"][data-token]').each(function(){
            var checkbox=jQuery(this);
            if(!editHelper.isInitEvent(checkbox, 'init-check-events')){
            //SPECIAL METHODS USED IN DIFFERENT AREAS

                //trigger the value enter to replace tokens
                checkbox[0]['trigger_replace_tokens']=function(){
                    tokensHelper.replacePageTokens(jQuery(this));
                    tokensHelper.replaceSectionTokens();
                }
                //the get value function for this field
                checkbox[0]['get_value']=function(){
                    return jQuery(this).is(':checked');
                }
                //the set value function for this field
                checkbox[0]['set_value']=function(v){
                    v=helper.convertToBool(v);
                    //set checked/unchecked
                    jQuery(this).prop('checked', v); 
                    if(v){
                        jQuery(this).attr('checked', 'checked');
                    }else{
                        jQuery(this).removeAttr('checked', 'checked');
                    }
                    //return 
                    return jQuery(this)[0]['get_value']();
                }
                checkbox[0]['original_val']=checkbox[0].get_value(); //remove original value

            //NORMAL EVENTS

                checkbox.change(function(){
                    jQuery(this)[0]['trigger_replace_tokens']();
                });
            }
        });
    };
    
    //ID prop HTML
    var setIdPropHtml=function(args){
        var html='';
        if(args.pageVal==undefined){
            //when new fields are created, this will have to do
            args.pageVal=args.ctlWrap.parents('[data-id]:first').attr('data-id');
        }
        //get the set value
        html+='<input data-edit="field_id" placeholder="[required]" spellcheck="false" type="text" value="'+args.pageVal+'" />';
        args.ctlWrap.append(html);
    };
    
    //TYPE prop HTML
    var setTypePropHtml=function(args){
        var html='';
        //set the class to indicate where the value came from
        var fieldId=args.ctlWrap.parents('[data-id]:first').attr('data-id');
        var ctlClass=args.getFieldCtlClass(args.scope, fieldId, 'type', args.pageVal, args.globalVal);
        args.ctlWrap.parents('.prop:first').addClass(ctlClass);
        //get the set value
        var setVal=args.getSetVal(ctlClass, args.pageVal, args.globalVal);
        html+='<select data-edit="field_type">';
        for(var p=0;p<args.options.length;p++){
            var isSelected='';
            var optionId=args.options[p]['id'];
            if(setVal==optionId){
                isSelected=' selected="selected"';
            }
            html+='<option'+isSelected+'>'+optionId+'</option>';
        }
        html+='</select>';
        args.ctlWrap.append(html);
    };
    
    //TYPE prop Events
    var initTypePropEvents=function(args){
        var select=args.ctlWrap.children('select:first');
        select.change(function(){
            editHelper.updateShownFieldPropsForType(jQuery(this).parents('.field-item:first'));
        });
    };
    
    //REUSABLE TEXT prop HTML
    var setStandardTextPropHtml=function(args){
        var html='';
        //set the class to indicate where the value came from
        var fieldId=args.ctlWrap.parents('[data-id]:first').attr('data-id');
        //var propName=args.ctlWrap.parents('[data-prop]:first').attr('data-prop');
        var ctlClass=args.getFieldCtlClass(args.scope, fieldId, args.propName, args.pageVal, args.globalVal);
        args.ctlWrap.parents('.prop:first').addClass(ctlClass);
        //get the set value
        var setVal=args.getSetVal(ctlClass, args.pageVal, args.globalVal);
        html+='<input data-edit="field_'+args.propName+'" placeholder="[none]" spellcheck="false" type="text" value="'+setVal+'" />';
        args.ctlWrap.append(html);
    };
    
    //LABEL prop HTML
    var setLabelPropHtml=function(args){
        setStandardTextPropHtml(args);
    };
    
    //CLASS prop HTML
    var setClassPropHtml=function(args){
        setStandardTextPropHtml(args);
    };
    
    //SUMMARY prop HTML
    var setSummaryPropHtml=function(args){
        setStandardTextPropHtml(args);
    };
    
    //VALUES prop HTML 
    var setValuesPropHtml=function(args){
        var html='';
        //set the class to indicate where the value came from
        var fieldId=args.ctlWrap.parents('[data-id]:first').attr('data-id');
        var ctlClass=args.getFieldCtlClass(args.scope, fieldId, 'values', args.pageVal, args.globalVal);
        args.ctlWrap.parents('.prop:first').addClass(ctlClass);
        //create inner control
        args.ctlWrap.append('<div class="edit-list-wrap"><input type="text" placeholder="[none]" readonly="readonly" spellcheck="false" class="click-bar" /><textarea data-edit="field_values" spellcheck="false" class="edit-area"></textarea></div>');
        var clickBar=args.ctlWrap.find('.click-bar:first');
        var editArea=args.ctlWrap.find('.edit-area:first');
        //get the set value
        var setVal=args.getSetVal(ctlClass, args.pageVal, args.globalVal);
        //set the values for the clickbar and editArea
        editHelper.setValuesClickBarContent(clickBar, editArea, setVal);
    };
    
    //VALUES prop Events
    var initValuesPropEvents=function(args){
        var editWrap=args.ctlWrap.find('.edit-list-wrap:first');
        var editArea=args.ctlWrap.find('.edit-area:first');
        
        editWrap.hover(function(){
            jQuery(this).addClass('show');
        }, function(){
            jQuery(this).removeClass('show');
        });
        editWrap.mousemove(function(){
            jQuery(this).addClass('show');
        });
        editWrap.mouseleave(function(){
            jQuery(this).removeClass('show');
        });
        editArea.blur(function(){
            jQuery(this).parents('.edit-list-wrap:first').removeClass('show');
        });
        editArea.focus(function(){
            jQuery(this).parents('.edit-list-wrap:first').addClass('show');
        });
    };

    //VARIATIONS prop HTML
    var setVariationsPropHtml=function(args){
        var html='';
        //set the class to indicate where the value came from
        var fieldId=args.ctlWrap.parents('[data-id]:first').attr('data-id');
        var ctlClass=args.getFieldCtlClass(args.scope, fieldId, 'variations', args.pageVal, args.globalVal);
        args.ctlWrap.parents('.prop:first').addClass(ctlClass);
        //append control html
        args.ctlWrap.append('<div class="variations-ctl"></div>');
        var varCtl=args.ctlWrap.find('.variations-ctl:first');
        varCtl.append('<input title="+ add/edit variations" type="text" placeholder="[none]" readonly="readonly" spellcheck="false" class="variations-btn" />');
        var varBtn=varCtl.find('.variations-btn:first');
        //get the set value
        var setVal=args.getSetVal(ctlClass, args.pageVal, args.globalVal);
        //update the variations count
        editHelper.updateVarCount(varBtn, setVal); 
    };
    
    //VARIATIONS prop Events ctlWrap, scope, pageVal, globalVal
    var initVariationsPropEvents=function(args){
        var varCtl=args.ctlWrap.find('.variations-ctl:first');
        var varBtn=varCtl.find('.variations-btn:first');
        //click event
        varBtn.click(function(e){
            e.preventDefault(); e.stopPropagation();
            editHelper.triggerVariationsClick(jQuery(this));
        });
    };
    
    var getFieldHelpHtml=function(fieldData){
        var html='';
        if(fieldData.hasOwnProperty('summary')){
            html+='<span class="popup help">';
            html+='<span class="icon"></span>';
            html+='<span class="content"><span class="inner-content">';
            html+=fieldData['summary'];
            if(fieldData['type']=='text'){
                if(fieldData.hasOwnProperty('values') && fieldData.values.length>0){
                    html+='<span class="value-examples">';
                    if(fieldData.values.length==1){
                        html+='<em>Example</em>: ';
                    }else{
                        html+='<em>Examples</em>: ';
                    }
                    for(var e=0;e<fieldData.values.length;e++){
                        html+='<em>"<a href="#">'+fieldData.values[e]+'</a>"</em> ';
                    }
                    html+='</span>';
                }
            }
            html+='</span></span>';
            html+='</span>';
        }
        return html;
    };
    
    var initHelpInfoEvents=function(){
        jQuery('.fields:first').find('.value-examples a').each(function(){
            if(!editHelper.isInitEvent(jQuery(this), 'init-info-events')){
                jQuery(this).click(function(e){
                    e.preventDefault(); e.stopPropagation();
                    var val=jQuery(this).html();
                    var fieldWrap=jQuery(this).parents('.field:first');
                    var field=fieldWrap.find('[data-token]:first');
                    var oldVal=field[0]['get_value']();
                    if(oldVal!=val){
                        field[0]['set_value'](val);
                    }
                    field[0]['trigger_replace_tokens']();
                    setTimeout(function(){
                        field.addClass('editing');
                    },100);
                    setTimeout(function(){
                        field.removeClass('editing');
                    },800);
                });
            }
        });
    };
    
    var getMoveFieldBtnHtml=function(){
        return '<a data-move="field" title="move" href="#" class="edit-btn move-field">&lt; &gt;</a>';
    };
    
    var getEditFieldBtnHtml=function(){
        return '<a title="edit" href="#" class="edit-btn edit-field">edit</a>';
    };
    
    var getRemoveFieldBtnHtml=function(){
        return '<a data-delete="field" title="delete" href="#" class="edit-btn remove-field">X</a>';
    };
    
    var getCopyTokenBtnHtml=function(id){
        return '<a title="copy token" href="#" class="edit-btn copy-token">'+id+'</a>';
    };
    
    //get the HTML for a field
    var getFieldHtml=function(fieldData){
        var isEditMode=pagesHelper.isPageMode('edit');
        var html='';
        var editClass='';
        if(isEditMode){
            editClass=' edit';
        }
        if(fieldData.hasOwnProperty('class')){
            html+='<div data-token="'+fieldData.id+'" class="field '+fieldData.class+editClass+'">';
        }else{
            html+='<div data-token="'+fieldData.id+'" class="field'+editClass+'">';
        }
        
        if(isEditMode){
            html+=getMoveFieldBtnHtml();
            html+=getEditFieldBtnHtml();
            html+=getCopyTokenBtnHtml(fieldData.id);
        }
        
        if(fieldData.hasOwnProperty('label')){
            html+='<label for="field-'+fieldData.id+'"><span>'+fieldData.label+'</span>'+getFieldHelpHtml(fieldData)+'</label>';
        }
        
        var fieldType='text';
        if(fieldData.hasOwnProperty('type')){
            fieldType=fieldData.type;
        }
        var typeData=getFieldType(fieldType);
        html+=typeData.get_html(fieldData);
        
        if(isEditMode){
            html+=getRemoveFieldBtnHtml();
        }
        
        return html + '</div>';
    };
    
    //make sure the field is in json form, {id:"{token}"}
    var fieldToJson=function(rawField){
        var field;
        if(typeof rawField=='string'){
            if(rawField.indexOf('html:')==0){
                rawField=rawField.substring('html:'.length);
                rawField=rawField.trim();
                field={html:rawField};
            }else{
                field={id:rawField};
            }
        }else if(rawField.hasOwnProperty('id')){
            for(var i in rawField){
                if(rawField.hasOwnProperty(i)){
                    if(field==undefined){ field={}; }
                    field[i]=rawField[i];
                }
            }
        }
        return field;
    };
    
    //make changes to the stored page field to make sure it's in jSON form
    var setPageFieldToJson=function(fieldId){
        var pageData=pagesHelper.getPageData();
        var fieldIndex=getIndexOfPageField(fieldId);
        if(fieldIndex>-1){
            if(typeof pageData['fields'][fieldIndex]=='string'){
                var fieldJson=fieldToJson(fieldId);
                pageData['fields'][fieldIndex]=fieldJson;
            }
        }
        return fieldIndex;
    };
    
    //given a field id, or a field JSON, figure out the index of this field in the page's array of fields
    var getIndexOfPageField=function(rawField){
        var fieldIndex=-1;
        var givenField=fieldToJson(rawField);
        var pageData=pagesHelper.getPageData();
        if(pageData.hasOwnProperty('fields')){
            for(var p=0;p<pageData['fields'].length;p++){
                var pageField=fieldToJson(pageData['fields'][p]);
                if(pageField['id']==givenField['id']){
                    fieldIndex=p;
                    break;
                }
            }
        }
        return fieldIndex;
    };
    
    //given a field, {id:"{token}"}, add the field properties to this json, defined for this page in data/pages.js
    var addFieldPageProps=function(fieldJson){
        var pageField;
        var pageData=pagesHelper.getPageData();
        if(pageData!=undefined){
            if(pageData.hasOwnProperty('fields')){
                //find the correct page field by its id
                for(var p=0;p<pageData.fields.length;p++){
                    var thisField=fieldToJson(pageData.fields[p]);
                    if(thisField!=undefined && thisField.id==fieldJson.id){
                        pageField=thisField;
                        break;
                    }
                }
            }
        }
        return pageField;
    };
    
    //given a field, {id:"{token}",...} with page properties, add the field's token properties defined in data/fields.js
    var addFieldDefProps=function(fieldJson){
        if(fieldJson!=undefined){
            fieldJson=helper.copyJson(fieldJson); //prevent altering saved data object
            if(fieldJson.hasOwnProperty('id')){
                //if this field definition is defined under data/fields.js
                if(fields.hasOwnProperty(fieldJson.id)){
                    //get all of the definition's default properties 
                    for(var d in fields[fieldJson.id]){
                        if(!fieldJson.hasOwnProperty(d)){ //leave page-overrides for this field alone
                            if(fields[fieldJson.id].hasOwnProperty(d)){
                                fieldJson[d]=fields[fieldJson.id][d];
                            }
                        }
                    }
                }
            }
        }
        return fieldJson;
    };
    
    var getDefaultFieldType=function(){
        return 'text';
    };
    
    var getDefaultFieldLabel=function(){
        return '[label]';
    };
    
    var getDefaultFieldSummary=function(){
        return '[summary]';
    };
    
    //add the default properties, if missing for a field's json
    var addDefaultFieldProps=function(fieldJson){
        if(fieldJson!=undefined){
            fieldJson=helper.copyJson(fieldJson); //prevent altering saved data object
            if(!fieldJson.hasOwnProperty('type')){
                fieldJson['type']=getDefaultFieldType();
            }
            if(!fieldJson.hasOwnProperty('label')){
                fieldJson['label']=getDefaultFieldLabel();
            }
            if(!fieldJson.hasOwnProperty('summary')){
                fieldJson['summary']=getDefaultFieldSummary();
            }
        }
        return fieldJson;
    };
    
    //get field data with the correct page orverrides, if any (see public function below)
    var getFieldData=function(args, includeArgs){
        if(includeArgs==undefined){ includeArgs={}; }
        if(!includeArgs.hasOwnProperty('global')){ includeArgs['global']=true; }
        if(!includeArgs.hasOwnProperty('page')){ includeArgs['page']=true; }
        if(!includeArgs.hasOwnProperty('default')){ includeArgs['default']=true; }
        var fieldData;
        if(args!=undefined){
            //first, make sure the field is in json form, {id:"{token}",...}
            if(typeof args=='string'){
                fieldData={id:args};
                if(includeArgs['page']){
                    fieldData=addFieldPageProps(fieldData);
                }
            }else{
                if(args.hasOwnProperty('page_field')){
                    if(typeof args['page_field']=='string'){
                        fieldData={id:args['page_field']};
                        if(includeArgs['page']){
                            fieldData=addFieldPageProps(fieldData);
                        }
                    }else{
                        fieldData=args['page_field'];
                    }
                }else if(args.hasOwnProperty('id')){
                    fieldData=args;
                    if(includeArgs['page']){
                        fieldData=addFieldPageProps(fieldData);
                    }
                }
            }
            if(includeArgs['global']){
                //given a field, {id:"{token}",...} with page properties, add the field's token properties defined in data/fields.js
                fieldData=addFieldDefProps(fieldData);
            }
            if(includeArgs['default']){
                //add the default properties, if missing for a field's json
                fieldData=addDefaultFieldProps(fieldData);
            }
        }
        return fieldData;
    };
    
    var getPlainHtmlFieldHtml=function(html, isEditMode, plainHtmlIndex){
        var ret='';
        ret+='<div data-html-id="'+plainHtmlIndex+'" class="field plain-html">';
        if(isEditMode){
            ret+=getMoveFieldBtnHtml();
            ret+=getCopyTokenBtnHtml('HTML ['+plainHtmlIndex+']');
            ret+=getRemoveFieldBtnHtml();
            ret+='<textarea data-edit="plain_html_field" class="plain-html-field">'+html+'</textarea>';
        }else{
            ret+=html;
        }
        ret+='</div>';
        return ret;
    };
    
    //reset the fields listing depending on current fields data for the page
    var redrawFrontendPageFields=function(pageData){
        if(pageData==undefined){ pageData=pagesHelper.getPageData(); }
        var isEditMode=pagesHelper.isPageMode('edit');
        var fieldsWrap=jQuery('.fields:first');
        if(fieldsWrap.length>0){
            fieldsWrap.html(''); //clear fields wrap out
            if(pageData.hasOwnProperty('fields')){
                var plainHtmlIndex=0;
                for(var f=0;f<pageData.fields.length;f++){
                    var rawPageField=pageData.fields[f];
                    if(typeof rawPageField == 'string' && rawPageField.indexOf('html:')===0){
                        rawPageField=rawPageField.substring('html:'.length);
                        rawPageField=rawPageField.trim();
                        fieldsWrap.append(getPlainHtmlFieldHtml(rawPageField, isEditMode, plainHtmlIndex));
                        pageData.fields[f]=fieldToJson(pageData.fields[f]);
                        pageData.fields[f]['id']=plainHtmlIndex+'';
                        plainHtmlIndex++;
                    }else if(rawPageField.hasOwnProperty('html')){
                        var plainHtmlId=plainHtmlIndex;
                        if(pageData.fields[f].hasOwnProperty('id')){
                            plainHtmlId=pageData.fields[f]['id'];
                        }else{
                            pageData.fields[f]['id']=plainHtmlIndex+'';
                        }
                        fieldsWrap.append(getPlainHtmlFieldHtml(rawPageField.html, isEditMode, plainHtmlId));
                        plainHtmlIndex++;
                    }else{
                        //get the field with the page-specific overrides
                        var fieldData=getFieldData({page_field:rawPageField});
                        if(fieldData!=undefined){
                            if(fieldData.hasOwnProperty('id')){
                                var fieldHtml=getFieldHtml(fieldData);
                                fieldsWrap.append(fieldHtml+'</div>');
                            }else{
                                console.log('field id required for field, '+rawPageField); 
                            }
                        }else{ 
                            console.log('undefined field, '+rawPageField+'. if you want to print as HTML, use the "html:" prefix.'); 
                        }
                    }
                }
            }
        }else{ console.log('missing fields wrap'); }
    };
                  
    //loop through each field
    var initFields=function(pageData){
        var isEditMode=pagesHelper.isPageMode('edit');
        var fieldsWrap=jQuery('.fields:first');
        //create the fields listing
        redrawFrontendPageFields(pageData);
        //add additional button elements, etc...
        var addNewEditHtml='';
        if(isEditMode){
            addNewEditHtml+=' <a title="add field" class="edit-btn add-token" href="#">+ add/edit fields</a> ';
        }
        fieldsWrap.after('<div class="fields-wrap"><h2>Configuration'+addNewEditHtml+'</h2></div>');
        var outerWrap=fieldsWrap.next('.fields-wrap:first');
        outerWrap.append(fieldsWrap);
        outerWrap.append(addNewEditHtml);
    };
    
    //public 
    return {
        /*
            get field data with the correct page orverrides, if any
            args:
                getFieldData("{myToken}")
                getFieldData({page_field:"{myToken}"})
                getFieldData({page_field:{id:"{myToken}",...}})
        */
        getFieldData:function(args, includeArgs){
            return getFieldData(args, includeArgs);
        },
        getDefaultFieldType:function(){
            return getDefaultFieldType();
        },
        getDefaultFieldLabel:function(){
            return getDefaultFieldLabel();
        },
        getDefaultFieldSummary:function(){
            return getDefaultFieldSummary();
        },
        fieldToJson:function(rawField){
            return fieldToJson(rawField);
        },
        setPageFieldToJson:function(fieldId){
            return setPageFieldToJson(fieldId);
        },
        getIndexOfPageField:function(rawField){
            return getIndexOfPageField(rawField);
        },
        getFieldHtml:function(args){
            return getFieldHtml(args);
        },
        getFieldType:function(typeId){
            return getFieldType(typeId);
        },
        getAllFieldTypes:function(){
            return getAllFieldTypes();
        },
        getAllPropTypes:function(){
            return getAllPropTypes();
        },
        redrawFrontendPageFields:function(pageData){
            redrawFrontendPageFields(pageData);
            initFieldEvents();
            initHelpInfoEvents();
        },
        //init a page
        init:function(pageData){
            var self=this;
            
            //not home page
            initFields(pageData);
            initFieldEvents();
            initHelpInfoEvents();
        }
    };
}());