var dynamicHelper=(function(){
    // private
    
    //used to evaluate if-conditions, etc...
    var evalCondition=function(str){
        var evalThing=eval(str);
        var isTrue=helper.convertToBool(evalThing);
        return isTrue;
    };
    
    //public 
    return {
        getBlockTypeData:function(name){
            //define the data for each of the dynamic block types
            var data={
                if:{
                    resolve_chain:this.resolveIfChain,
                    chain:[
                        {
                            name:'if',
                            min:1, max:1
                        },
                        {
                            name:'elseif',
                            min:0, max:-1

                        },
                        {
                            name:'else',
                            min:0, max:1,
                            has_condition:false
                        }
                    ]
                }
            };
            //decide what to return
            var ret;
            if(name!=undefined){
                ret=data[name];
            }else{
                ret=data;
            }
            return ret;
        },
        //get the string from the if block chain
        resolveIfChain:function(blockChain){
            var html='';
            //if-block eval condition function
            var ifBlockHeadTrue=function(headStr){
                var ret=false;
                if(headStr.trim().indexOf('[else]')===0){
                    ret=true;
                }else{
                    headStr=headStr.substring(headStr.indexOf('(')+'('.length);
                    headStr=headStr.substring(0, headStr.lastIndexOf(')'));
                    //replace tokens inside headStr
                    var tokenData=dynamicHelper.captureTokensInStr(headStr);
                    headStr=tokenData['str_values'].trim();
                    //see if this condition is true
                    ret=evalCondition(headStr);
                }
                return ret;
            };
            //every other part is the block body
            for(var p=0;p<blockChain.parts.length;p+=2){
                var blockHead=blockChain.parts[p];
                var blockBody=blockChain.parts[p+1];
                if(ifBlockHeadTrue(blockHead)){
                    blockBody=blockBody.substring('{'.length);
                    blockBody=blockBody.substring(0, blockBody.lastIndexOf('}'));
                    //recursively handle any dynamic blocks inside this blockBody
                    html=dynamicHelper.resolveBlockChains(blockBody); //***
                    break;
                }
            }
            return html;
        },
        //get the token-replace data for a given string that could be filled with tokens, or not
        captureTokensInStr:function(str){
            var tokenData=tokensHelper.replaceTokensInStr({str:str, formats:['str_args']});
            var str_noArgs=tokenData['str_args'];
            var str_values=tokenData['str_args'];
            //if any tokens were returned (if any tokens to replace in the given string)
            if(tokenData.hasOwnProperty('arg_tokens')){
                for(var s=0;s<tokenData['arg_tokens'].length;s++){
                    var argToken=tokenData['arg_tokens'][s];
                    str_noArgs=helper.replaceAll(str_noArgs, argToken['arg'], '');
                    str_values=helper.replaceAll(str_values, argToken['arg'], argToken['value']);
                }
            }
            tokenData['str_noArgs']=str_noArgs;
            tokenData['str_values']=str_values;
            return tokenData;
        },
        //get an array of all of the dynamic blocks (ignore nested blocks in bodies)
        getBlockChains:function(inStr, namesData, getAllBetween){
            if(getAllBetween==undefined){ getAllBetween=helper.getAllBetween; }
            //match this part: "[if]('{letter}'=='A')"
            var matchNextBlockHead=function(bName){
                var reg;
                var regStr='\\['+bName+'\\]\\s*?\\((.|\\n)*?\\)';
                var tempStr=inStr;
                reg=new RegExp(regStr);
                var match=tempStr.match(reg);
                if(match==undefined){ match=''; }
                else{ 
                    match=match[0]; 
                    //make sure the match doesn't end too soon, should be properly nested ( ( ) )
                    tempStr=tempStr.substring(tempStr.indexOf(match));
                    var startMatch=tempStr.substring(0, tempStr.indexOf('('));
                    match=getAllBetween(tempStr, '(', ')', 1);
                    match=startMatch+'('+match[0]+')';
                }
                return match;
            };
            //match this part: "[else] {"
            var matchNextBlockHeadNoCondition=function(bName){
                var reg;
                var regStr='\\['+bName+'\\]\\s*?\\{';
                reg=new RegExp(regStr);
                var match=inStr.match(reg);
                if(match==undefined){ match=''; }
                else{ 
                    match='['+bName+']';
                }
                return match;
            };
            //match block body after head
            var matchNextBlockBody=function(headStr){
                var bodyStr='';
                if(headStr.length>0){
                    var tempStr=inStr;
                    tempStr=tempStr.substring(tempStr.indexOf(headStr)+headStr.length);
                    var matchedBody=getAllBetween(tempStr, '{', '}', 1);
                    if(matchedBody.length>0){
                        bodyStr='{'+matchedBody[0]+'}';
                    }
                }
                return bodyStr;
            };
            //get the gap between head and body, for example
            var getNextStrBetween=function(str1, str2){
                var between=inStr.substring(inStr.indexOf(str1)+str1.length);
                between=between.substring(0, between.indexOf(str2));
                return between;
            };
            //get the next head/body string
            var getNextHeadAndBody=function(bname, bData, nameData){
                if(bData==undefined){
                    bData={
                        append:false,
                        str:'',
                        part_count:{},
                        parts:[]
                    }
                }
                //init for this iteration
                bData['append']=false;
                if(!bData['part_count'].hasOwnProperty(bname)){ bData['part_count'][bname]=0; }
                if(!nameData.hasOwnProperty('has_condition')){ nameData['has_condition']=true; }
                //get next block head...
                var blockHead='';
                if(nameData['has_condition']){
                    blockHead=matchNextBlockHead(bname);
                }else{
                    blockHead=matchNextBlockHeadNoCondition(bname);
                }
                //if found next head section
                if(blockHead.length>0){
                    //if this is not the first head/body in the chain
                    if(bData['parts'].length>0){
                        var between=inStr.substring(0, inStr.indexOf(blockHead));
                        //only whitespace characters between previous body and the next head!
                        if(between.trim().length>0){
                            blockHead=''; //this block head is NOT connected to the previous body
                        }
                    }
                    var blockBody=matchNextBlockBody(blockHead);
                    if(blockBody.length>0){
                        var between=getNextStrBetween(blockHead, blockBody);
                        //only whitespace characters between head and body!
                        if(between.trim().length<1){
                            //add whitespace to head
                            blockHead+=between;
                            //if this is not the first head/body in the chain
                            if(bData['parts'].length>0){
                                //bridge the gap between this head/body and the previous head/body
                                var gapBetween=inStr.substring(0, inStr.indexOf(blockHead));
                                if(gapBetween.length>0){
                                    bData['str']+=gapBetween;
                                    bData['parts'][bData['parts'].length-1]+=gapBetween;
                                }
                            }
                            //block data
                            bData['str']+=blockHead+blockBody;
                            bData['parts'].push(blockHead);
                            bData['parts'].push(blockBody);
                            //flag that a new block was appended to this block's data
                            bData['append']=true;
                            bData['part_count'][bname]=bData['part_count'][bname]+1;
                            //TRIM this block from the string
                            inStr=inStr.substring(inStr.indexOf(blockHead+blockBody)+(blockHead+blockBody).length);
                        }
                    }
                }
                return bData;
            };
            
            //GATHER ONE BLOCK CHAIN DATA, eg: "[if]('{letter}'=='A') [elseif]{ ... } [else] { ... }"
            var getNextBlockChain=function(){
                var blockData=undefined;
                var stillValid=true;
                //for each name in the chain, eg: if, elseif, else
                for(var b=0;b<namesData.length;b++){
                    var bname=namesData[b].name;
                    var stayOnThisName=true;
                    //get the same block name multiple times, if possible
                    while(stayOnThisName && stillValid){
                        //max number of this block type?
                        var atMaxNum=false;
                        if(blockData!=undefined){
                            if(!blockData['part_count'].hasOwnProperty(bname)){
                               blockData['part_count'][bname]=0;
                            }
                            if(namesData[b].max>0 && blockData['part_count'][bname]>=namesData[b].max){
                                atMaxNum=true;
                            }
                        }
                        //if not reached max
                        if(!atMaxNum){
                            //GET NEXT head/body, if it is next in string
                            blockData=getNextHeadAndBody(bname, blockData, namesData[b]);
                            //if the next block was not found
                            if(!blockData['append']){
                                stayOnThisName=false;
                                //if below the minimum
                                if(blockData['part_count'][bname]<namesData[b].min){
                                    stillValid=false;
                                }
                            }
                        }else{ //reached max
                            stayOnThisName=false;
                        }
                    }

                    if(!stillValid){
                        blockData=undefined;
                        break;
                    }
                }

                if(blockData!=undefined){
                    delete blockData['append'];
                }
                return blockData;
            };
            
            //GATHER ALL BLOCK CHAINS
            var blockChains=[];
            var findNextChain=true;
            while(findNextChain){
                var oneBlockChain=getNextBlockChain();
                //if found another block chain
                if(oneBlockChain!=undefined){
                    blockChains.push(oneBlockChain);
                }else{
                    findNextChain=false;
                }
            }
            return blockChains;
        },
        //resolve all of the dynamic blocks (can be used recursively inside block bodies)
        resolveBlockChains:function(html){
            var chains_ph=[];
            //for each block type
            var blockTypes=this.getBlockTypeData();
            for(var blockType in blockTypes){
                if(blockTypes.hasOwnProperty(blockType)){
                    //get the block-chains for this type
                    var chains=this.getBlockChains(html, blockTypes[blockType].chain);
                    var ph='__b___BLO0_Ck____C_hA1rN_';
                    if(chains!=undefined && chains.length>0){
                        for(var c=0;c<chains.length;c++){
                            var chain=chains[c];
                            var uniquePh=ph+blockType+'_'+c;
                            //temporarily switch this block chain out with a placeholder
                            html=html.replace(chain.str, uniquePh);
                            //figure out what new string to resolve the block chain with
                            var newStr=blockTypes[blockType].resolve_chain(chain);
                            //keep track of the placeholder used
                            chains_ph.push({ph:uniquePh, resolved:newStr});
                        }
                    }
                }
            }
            //for each block chain to resolve
            for(var r=0;r<chains_ph.length;r++){
                html=html.replace(chains_ph[r].ph, chains_ph[r].resolved);
            }
            return html;
        },
        //before all tokens are replaced on the page
        beforeTokenReplace:function(html){
            html=this.resolveBlockChains(html);
            return html;
        }
    };
}());