var homeHelper=(function(){
    // private
    
    //public 
    return {
        init:function(){
            var browseWrap=jQuery('#browse-content:first');
            serverHelper.initContentFolders(browseWrap, '', {
                replace_contents:true,
                allow_goto_page:true,
                select_folders:false
            });
        }
    };
}());

jQuery(document).ready(function(){
    homeHelper.init();
});