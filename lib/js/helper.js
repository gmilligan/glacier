var helper=(function(){
    // private

    //public 
    return {
        //get an arg value from a json and provide a default if the value doesn't exist
        getArgVal:function(args, key, defaultVal){
            var ret=defaultVal;
            if(args!=undefined){
                if(args.hasOwnProperty(key)){
                    ret=args[key];
                }
            }
            return ret;
        },
        //sets the default value if there is no existing value
        getSetArgVal:function(args, key, defaultVal){
            if(args==undefined){
                args={};
            }
            if(!args.hasOwnProperty(key)){
                args[key]=defaultVal; //set the default value
            }
            return args[key];
        },
        //loop through each key in a json object
        eachKey:function(inJson, itemCallback, args){
            if(inJson!=undefined && itemCallback!=undefined){
                var skip=this.getArgVal(args, 'skip', []);
                var itemNum=0;
                for(var key in inJson){
                    if(inJson.hasOwnProperty(key)){
                        if(skip.indexOf(key)==-1){
                            var ret=itemCallback(key, itemNum);
                            itemNum++;
                            if(ret!=undefined){
                                if(!ret){
                                    break; //break this for loop if return=false from itemCallback
                                }else{
                                    continue; //continue this for loop if return=true from itemCallback
                                }
                            }
                        }
                    }
                }
            }
        },
        //get just the extension from a file name or path
        getExt:function(path){
            var ext='';
            var dotIndex=path.lastIndexOf('.');
            if(dotIndex!==-1){
                ext=path.substring(dotIndex+'.'.length);
                ext=ext.toLowerCase(); ext=ext.trim();
            }
            return ext;
        },
        //get the path without the extension
        getNoExt:function(path){
            var ret=path;
            if(ret.indexOf('.')!=-1){
                ret=ret.substring(0, ret.lastIndexOf('.'));
            }
            return ret;
        },
        getParentDir:function(path){
            var parentDir='';
            if(path.indexOf('/')!=-1){
                parentDir=path.substring(0, path.lastIndexOf('/')+'/'.length);
            }
            return parentDir;
        },
        //get just the name of the file or folder with no parent
        getNoParentDir:function(path){
            var name=path;
            if(path.indexOf('/')!=-1){
                name=path.substring(path.lastIndexOf('/')+'/'.length);
            }
            return name;
        },
        endPathWithSlash:function(path){
            if(path.length>0){
                if(path.lastIndexOf('/')!=path.length-'/'.length){
                    path+='/';
                }
            }
            return path;
        },
        /*
            isContentSame(
                str1, 
                str2
            )
        */
        isContentSame:function(str1, str2){
            var same=true;
            if(str1.length == str2.length){
                if(str1!=str2){
                    same=false;
                }
            }else{
                same=false;
            }
            return same;
        },
        jsonToString:function(json){
            var tabSpaces=editHelper.getTabSpaces();
            return JSON.stringify(json, null, tabSpaces);
        },
        //get a backup clone copy of copiedJson
        copyJson:function(copiedJson){
            return jQuery.extend(true, {}, copiedJson);
        },
        convertToBool:function(v){
            if(typeof v!='boolean'){
                if(v==undefined){
                    v=false;
                }else if(typeof v=='string'){
                    v=v.toLowerCase();
                    if(v.length>=1){
                        var firstLetter=v;
                        if(firstLetter.length>1){
                            firstLetter=firstLetter.substring(0, 1);
                        }
                        switch(firstLetter){
                            case 't': v=true; break; case 'f': v=false; break;
                            case 'y': v=true; break; case 'n': v=false; break;  
                            case '1': v=true; break; case '0': v=false; break;
                            default: v=false; break;
                        }
                    }else{
                        v=false;
                    }
                }else if(!isNaN(v)){
                    if(v>=1){
                        v=true;
                    }else{
                        v=false;
                    }
                }else{
                    v=false;
                }
            }
            return v;
        },
        //removes start and end from str, ignoring white space
        unwrapString:function(str, startStr, endStr){
            //trim off the surrounding start and end tags
            if(str.trim().indexOf(startStr)===0){
                str=str.substring(str.indexOf(startStr)+startStr.length);
            }
            var lastIndex=str.lastIndexOf(endStr);
            if(lastIndex!=-1){
                var remaining=str.substring(lastIndex+endStr.length);
                remaining=remaining.trim();
                if(remaining.length==0){
                    str=str.substring(0, lastIndex);
                }
            }
            return str;
        },
        replaceAll:function(theStr, charToReplace, replaceWith){
            if (theStr != undefined) {
                if (charToReplace == undefined) {
                    charToReplace = '';
                }
                if (replaceWith == undefined) {
                    replaceWith = '';
                }
                if (charToReplace != replaceWith) {
                    while (theStr.indexOf(charToReplace) != -1) {
                        theStr = theStr.replace(charToReplace, replaceWith);
                    }
                }
            } else {
                theStr = '';
            }
            return theStr;
        },
        replaceAllArray:function(theStr, array){
            for(var a=0;a<array.length;a+=2){
                theStr=this.replaceAll(theStr, array[a], array[a+1]);
            }
            return theStr;
        },
        wrapLines:function(str, startLineWrap, endLineWrap){
            var ret=str;
            var lines=ret.split('\n');
            if(lines.length>0){
                ret='';
                for(var n=0;n<lines.length;n++){
                    if(ret.length>0){ ret+='\n'; }
                    ret+=startLineWrap+lines[n]+endLineWrap;
                }
            }
            return ret;
        },
        /*
            getAllBetween(
                "some string that {contains {tokens} } or open/close {statements}", 
                "{",
                "}",
                [optional] max number of chunks to return 
            )

            return:

            ["contains {tokens} ", "statements"]
        */
        getAllBetween:function(fullStr, start, end, limitChunkCount){
            var ret=[];
            if(limitChunkCount==undefined){ limitChunkCount=-1; }
            //"some string that {contains {tokens} } or open/close {statements}"
            var nextIndex=function(v){
                return fullStr.indexOf(v);
            };
            var getTrimToNext=function(v){
                var str; var i=nextIndex(v);
                if(i!=-1){
                    if(i===0){ str=''; }
                    else{
                        str=fullStr.substring(0, i);
                    }
                    fullStr=fullStr.substring(str.length+v.length);
                } return str;
            };
            var trimToNext=function(v){
                var didTrim=false; var str=getTrimToNext(v);
                if(str!=undefined){ didTrim=true; }
                return didTrim;
            };
            var nextTagType=function(){
                var ret; var iEnd=nextIndex(end); var iStart=nextIndex(start);
                if(iEnd!=-1){
                    if(iStart!=-1){
                        if(iEnd<iStart){ ret='end'; }
                        else if(iEnd==iStart){ //if both the start and end tags are the same (therefore, at the same index)
                            ret='end'; //always return end, expecting that the start has already been trimmed off at this point
                        }
                        else{ ret='start'; }
                    }else{ ret='end'; }
                }else if(iStart!=-1){ ret='start'; }
                return ret;
            };
            //"contains {tokens} } or open/close {statements}"
            if(trimToNext(start)){
                var starts=1, ends=0, chunk='';
                var processNextChunk=function(){
                    var doNext=false;
                    var nextType=nextTagType();
                    switch(nextType){
                        case 'start': starts++; doNext=true; 
                            chunk+=getTrimToNext(start) + start; break;
                        case 'end': ends++; doNext=true; 
                            chunk+=getTrimToNext(end); 
                            if(starts!=ends){ chunk+=end; }
                            break;
                    }
                    if(starts==ends){
                        ret.push(chunk);
                        chunk=''; starts=1; ends=0;
                        if(!trimToNext(start)){
                            doNext=false; 
                        }
                    }
                    return doNext;
                };
                var doNext=true;
                while(doNext){
                    doNext=processNextChunk();
                    if(limitChunkCount!=-1){
                        if(ret.length>=limitChunkCount){
                            doNext=false;
                        }
                    }
                }
            }
            return ret;
        }
    };
}());

if(typeof exports != 'undefined'){
    exports.helper=helper;
}