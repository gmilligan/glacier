jQuery(document).ready(function(){
    var pageData=pagesHelper.init();
    
    var isEditMode=pagesHelper.isPageMode('edit');
    
    //not home page
    fieldsHelper.init(pageData);
    
    if(!isEditMode){
        tokensHelper.init();
    }
    
    //any page
    menuHelper.init();
    
    editHelper.init();
});