var serverHelper=(function(){
    // private
    
    var ok=function(){
        var ret=undefined;
        if(document.hasOwnProperty('server_data')){
            ret=document['server_data']['status'];
        }
        return ret;
    };
    
    //remove an array of project paths (and related data) from recent_projects.json
    function ajaxPost(path, sendArgs, callback){
        // construct an HTTP request
        var xhr = new XMLHttpRequest();
        xhr.open('POST', path, true);
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        xhr.onloadend=function(res){
            //if the server responded with ok status
            var res=JSON.parse(this.responseText);
            //callback, if any
            if(callback!=undefined){
                callback(res);
            }
        };
        // send the collected data as JSON
        xhr.send(JSON.stringify(sendArgs));
    }

    function ajaxGet(path, callback){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                var str=xmlhttp.responseText;
                var json=JSON.parse(str);
                if(callback!=undefined){
                    callback(json);
                }
            }
        };
        xmlhttp.open("GET",path,true);
        xmlhttp.send();
    }
    
    var isServerUp=function(callback){
        //if server state hasn't been cached
        if(!document.hasOwnProperty('server_data')){
            //see if it's possible to communicate with the server
            ajaxGet('/is-server-up', function(res){
                if(res!=undefined && res.hasOwnProperty('status')){
                    //cache values from the server
                    document['server_data']={
                        status:res.status,
                        content_root:res.content_root,
                        default_encoding:res.default_encoding
                    };
                    if(callback!=undefined){
                        //callback
                        callback(res); 
                    }
                }
            });
        }else{ //server was already pinged
            if(callback!=undefined){
                //callback
                callback(document['server_data']);
            }
        }
    };
    
    var getDirClosedSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M224 64l64 64h224v352h-512v-416z"></path></svg>';
    };
    
    var getAddDirSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 128l-64-64h-224v416h512v-352h-224zM352 352h-64v64h-64v-64h-64v-64h64v-64h64v64h64v64z"></path></svg>';
    };
    
    var getDirOpenedSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M416 480l96-256h-416l-96 256zM64 192l-64 288v-416h144l64 64h208v64z"></path></svg>';
    };
    
    var getHtmlFileSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024"><path d="M608 832v64.082c0 35.408-28.617 63.918-63.918 63.918h-480.165c-35.408 0-63.918-28.759-63.918-64.235v-735.531c0-35.488 28.776-64.235 64.273-64.235h319.727v192.061c0 35.565 28.739 63.939 64.189 63.939h159.811v64h-415.781c-53.14 0-96.219 42.952-96.219 95.961v224.078c0 52.998 42.752 95.961 96.219 95.961h415.781zM416 96v191.906c0 17.725 14.431 32.094 31.705 32.094h160.295l-192-224zM192.235 448h735.531c35.488 0 64.235 28.539 64.235 63.745v224.511c0 34.939-28.759 63.745-64.235 63.745h-735.531c-35.488 0-64.235-28.539-64.235-63.745v-224.511c0-34.939 28.759-63.745 64.235-63.745zM928 704h-128v-192h-32v224h160v-32zM448 544v192h32v-192h64v-32h-160v32h64zM320 608h-96v-96h-32v224h32v-96h96v96h32v-224h-32v96zM656 608l-48-96h-32v224h32v-160l32 64h32l32-64v160h32v-224h-32l-48 96z"></path></svg>';
    };
    
    var getEditSvg=function(){
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 640"><path d="M470.080 85.152c-60.608-44.128-103.744-43.168-120.128-40.512-4.608 0.736-8.48 3.392-11.2 7.136l-220.256 303.904c-8.864 12.224-13.984 26.752-14.784 41.824l-9.472 179.968c-0.672 12.96 12.224 22.336 24.32 17.696l168.192-64.32c14.176-5.44 26.496-14.88 35.392-27.168l220.16-303.808c2.848-3.936 4-8.736 3.2-13.536-2.688-16.832-15.584-57.664-75.424-101.184zM287.264 494.88l-65.376 25.248c-2.56 0.992-5.408 0.192-7.072-1.984-8.416-10.72-18.432-21.344-34.4-32.96-15.968-11.584-29.152-17.856-41.92-22.592-2.56-0.96-4.192-3.392-4.032-6.144l3.904-69.952 17.568-24.16c0 0 39.328-5.408 90.656 31.936 51.264 37.312 58.272 76.416 58.272 76.416l-17.6 24.192z"></path></svg>';
    };
    
    //get the full folder path to the current fileName element
    var getFullPathFromEl=function(el){
        var path='';
        if(el.hasClass('file-name')){
            el.parents('li[data-type]').each(function(){
                var li=jQuery(this);
                var fileNameEl=li.children('.file-name:first');
                var name='';
                if(li.hasClass('new-folder')){
                    name=fileNameEl.val().trim();
                }else{
                    name=fileNameEl.text();
                }
                if(path.length>0){ name+='/'; }
                path=name+path;
            });
        }
        return path;
    };
    
    //validate a folder name
    var getValidDirName=function(input){
        var val=input.val();
        var newVal=val.trim();
        newVal=newVal.replace(/ /g, '_');
        newVal=newVal.replace(/[^a-z0-9_]/gi, '');
        if(val!=newVal){
            input.val(newVal);
        }
        return newVal;
    };
    
    //start rename content 
    var renameContentFolderEvent=function(fileNameSpan, args){
        fileNameSpan.addClass('renaming');
        fileNameSpan.parent().addClass('renaming');
        fileNameSpan.after('<input class="set-file-name file-name rename" type="text" spellcheck="false" />');
        var input=fileNameSpan.next('input:first');
        input.val(fileNameSpan.text().trim());
        //handle remove new html
        var removeNewHtml=function(inEl){
            inEl.parent().children('.renaming').removeClass('renaming');
            inEl.parent().removeClass('renaming');
            inEl.remove();
        };
        //handle cancel rename
        var cancelRename=function(inEl){
            removeNewHtml(inEl);
        };
        //handle rename
        var doRename=function(inEl){
            var val=inEl.val().trim();
            if(val.length>0){
                var dirVal=getValidDirName(inEl);
                //if the directory value didn't need to change to valid
                if(dirVal==val){
                    var oldPath=getFullPathFromEl(inEl);
                    var newPath=oldPath;
                    var newDirName=inEl.val();
                    if(newPath.indexOf('/')!=-1){
                        newPath=newPath.substring(0, newPath.lastIndexOf('/'));
                        newPath+='/'+newDirName;
                    }else{
                       newPath=newDirName;
                    }
                    if(newPath!=oldPath){
                        //call the server to create the new directory
                        renameDir(oldPath, newPath, function(res){
                            if(res.status==ok()){
                                var nameSpan=inEl.prev('.file-name:first');
                                nameSpan.text(newDirName);
                                refreshContentFolders(inEl,args);
                            }else{
                                removeNewHtml(inEl);
                            }
                        });
                    }else{ //the directory name didn't change
                        cancelRename(inEl);
                    }
                }else{ //dir value needed to be changed to be valid
                    inEl.focus();
                }
            }else{
                removeNewHtml(inEl);
            }
        };
        //events
        input.blur(function(e){
            cancelRename(jQuery(this));
        });
        input.keyup(function(e) {
            switch (e.keyCode) {
                case 13: //enter key press
                    e.preventDefault();
                    doRename(jQuery(this));
                    break;
                case 27: //escape key press
                    e.preventDefault();
                    cancelRename(jQuery(this)); 
                    break;
            }
        });
        //put cursor into input
        input.select();
    };
    
    //start add new folder
    var addNewContentFolderEvent=function(el, args){
        var newLi;
        var newLiHtml='<li class="new-folder" data-type="d" unselectable="on" style="user-select: none;"></li>';
        //if this is a folder under the root (not the root)
        if(el.hasClass('file-name')){
            var parentLi=el.parent();
            //if this folder is not already a parent
            if(!parentLi.hasClass('parent')){
                //create the child ul element
                el.after('<ul></ul>');
            }
            var ul=parentLi.children('ul:last');
            //prepend a new folder li
            ul.prepend(newLiHtml);
            newLi=ul.children('li:first');
        }else if(el.hasClass('wrap')){ //if this is the root
            el.append(newLiHtml);
            newLi=el.children('li:last');
        }
        if(newLi!=undefined){
            //create new folder name input
            newLi.append('<span class="file-icon"></span><input class="set-file-name file-name" type="text" spellcheck="false" />');
            //create folder icon
            var fileIconSpan=newLi.children('.file-icon:first');
            fileIconSpan.append(getDirClosedSvg());
            //get the folder input
            var input=newLi.children('input:first');
            //remove new folder html
            var removeNewHtml=function(inEl){
                inEl.parents('li.new-folder:first').remove();
            };
            //handle cancel add
            var cancelAdd=function(inEl){
                removeNewHtml(inEl);
            };
            //handle add folder
            var addFolder=function(inEl){
                var val=inEl.val().trim();
                if(val.length>0){
                    var dirVal=getValidDirName(inEl);
                    //if the directory value didn't need to change to valid
                    if(dirVal==val){
                        var path=getFullPathFromEl(inEl);
                        //call the server to create the new directory
                        createNewDir(path, function(res){
                            if(res.status==ok()){
                                refreshContentFolders(inEl,args);
                            }else{
                                removeNewHtml(inEl);
                            }
                        });
                    }else{ //dir value needed to be changed to be valid
                        inEl.focus();
                    }
                }else{
                    removeNewHtml(inEl);
                }
            };
            //events
            input.blur(function(e){
                cancelAdd(jQuery(this));
            });
            input.keyup(function(e) {
                switch (e.keyCode) {
                    case 13: //enter key press
                        e.preventDefault();
                        addFolder(jQuery(this));
                        break;
                    case 27: //escape key press
                        e.preventDefault();
                        cancelAdd(jQuery(this)); 
                        break;
                }
            });
            //put cursor into input
            input.focus();
        }
    };
    
    //select folder event
    var selectDirEvent=function(selectSpan, args){
        selectSpan.parents('ul.loaded-content:last').find('.selected-dir').removeClass('selected-dir');
        selectSpan.addClass('selected-dir');
        if(args.hasOwnProperty('on_select_folder') && args['on_select_folder']!=undefined){
            args['on_select_folder'](selectSpan);
        }
    };
    
    //open directories in a given path
    var openDirsEvent=function(wrap, path, args){
        if(path.length>0){
            var pathArray=path.split('/');
            var dirName=pathArray[0];
            var li=wrap.find('li[data-dir="'+dirName+'"]:first');
            if(li.length>0){
                var nameSpan=li.children('.file-name:first');
                var fullPath=getFullPathFromEl(nameSpan);
                openDirEvent(nameSpan, args, args['path'], {name:fullPath}, function(){
                    //function to remove first dir/file in the path
                    var trimOffNextPathDir=function(){
                        if(path.indexOf('/')!=-1){
                            path=path.substring(path.indexOf('/')+'/'.length);
                        }else{
                            path='';
                        }
                    };
                    //remove first dir/file in the path
                    trimOffNextPathDir();
                    if(path.length>0){
                        var newDir=path;
                        if(newDir.indexOf('/')!=-1){
                            newDir=newDir.substring(0, newDir.indexOf('/'));
                        }
                        var newWrap=li.find('li[data-dir="'+newDir+'"]:first');
                        if(newWrap.length>0){
                            var nameSpan=newWrap.children('.file-name:first');
                            if(newWrap.hasClass('parent')){
                                //recursive open child contents
                                var copyArgs=helper.copyJson(args);
                                copyArgs['replace_contents']=false;
                                openDirsEvent(newWrap.parent(), path, copyArgs);
                            }else{
                                selectDirEvent(nameSpan, args);
                            }
                        }
                    }
                });
            }
        }
    };
    
    //open directory function
    var openDirEvent=function(openSpan, args, path, file, callback){
        //select at the same time as open/close
        selectDirEvent(openSpan, args);
        var parentLi=openSpan.parents('li:first');
        if(parentLi.hasClass('open')){
            //close
            parentLi.removeClass('open');
        }else{
            //open
            parentLi.addClass('open');
            var ul=parentLi.children('ul:first');
            if(ul.length<1){
                //recursive open child contents
                var copyArgs=helper.copyJson(args);
                copyArgs['replace_contents']=false;
                initContentFolders(parentLi, path+file.name, copyArgs, callback);
            }
        }
    };
    
    //clear out the browse section and recreate it
    var refreshContentFolders=function(trigger, args){
        //get path to the trigger element
        var pathToOpen=getFullPathFromEl(trigger);
        //get the browse wrapper
        var wrapUl=trigger.parents('.loaded-content.wrap:last');
        var wrap=wrapUl.parent();
        //force replace contents
        args['replace_contents']=true;
        //recreate folders
        initContentFolders(wrap, '', args, function(){
            openDirsEvent(wrap, pathToOpen, args);
        });
    };
    
    //place the contents of a folder into a dom element
    var initContentFolders=function(wrap, root, args, callback){
        //set defaults to args
        helper.getSetArgVal(args, 'replace_contents', true);
        helper.getSetArgVal(args, 'allow_goto_page', true);
        helper.getSetArgVal(args, 'select_folders', false);
        helper.getSetArgVal(args, 'on_select_folder');
        helper.getSetArgVal(args, 'right_click', {});
        helper.getSetArgVal(args['right_click'], 'create_folder', false);
        helper.getSetArgVal(args['right_click'], 'rename_folder', false);
        //handle right click, if enabled
        var initRightClickFolderActions=function(rcEl, selectFunc){
            //if right click is enabled 
            var rcOptions=[];
            if(args['right_click']['create_folder']){
                var newDirSvg=getAddDirSvg();
                rcOptions.push({
                    svg:newDirSvg,
                    key:'new_folder',
                    label:'New Folder',
                    click:function(option, settings, clickedEl, e){
                        var browseEl=settings.trigger;
                        addNewContentFolderEvent(browseEl,args);
                    }
                });
            }
            if(!rcEl.hasClass('wrap')){
                //make sure editing part of the current page's folder path isn't possible, while in this path
                var folderPath=getFullPathFromEl(rcEl);
                var currentScopeFolder=pagesHelper.getPageScopeFolder();
                currentScopeFolder='/'+currentScopeFolder+'/';
                folderPath='/'+folderPath+'/';
                if(currentScopeFolder.indexOf(folderPath)==-1){
                    if(args['right_click']['rename_folder']){
                        var editSvg=getEditSvg();
                        rcOptions.push({
                            svg:editSvg,
                            key:'rename_folder',
                            label:'Rename Folder',
                            click:function(option, settings, clickedEl, e){
                                var browseEl=settings.trigger;
                                renameContentFolderEvent(browseEl,args);
                            }
                        });
                    }
                }
            }
            if(rcOptions.length>0){
                if(!rcEl.hasClass('init-right-click')){
                    rcEl.addClass('init-right-click');
                    rcEl[0]['contextmenu_options']=rcOptions;
                    rcEl.contextmenu(function(e){
                        e.preventDefault(); e.stopPropagation();
                        if(selectFunc!=undefined){
                            selectFunc(jQuery(this));
                        }
                        var options=jQuery(this)[0]['contextmenu_options'];
                        syntaxlerHelper.openFloatMenu({
                            id:'folder-context-menu',
                            options:options,
                            trigger:jQuery(this),
                            event:e
                        });
                    });
                }
            }
        };
        //communicate with the server to load folder list
        loadContentFiles(root, function(fileData){
            var outerWrapClass='';
            if(root.length<1){
                outerWrapClass=' wrap';
            }
            if(args['replace_contents']){
                wrap.html('<ul class="loaded-content'+outerWrapClass+'"></ul>');
            }else{
                wrap.append('<ul class="loaded-content'+outerWrapClass+'"></ul>');
            }
            var contentWrap=wrap.children('.loaded-content:last');
            //if this is the first outer wrap
            if(outerWrapClass.length>0){
                initRightClickFolderActions(contentWrap);
            }
            
            //for each content folder
            eachContentFile(fileData, function(file, type, typeIndex, path){
                args['path']=path;
                //add li and file name
                contentWrap.append('<li><span class="file-name">'+file.name+'</span></li>');
                var li=contentWrap.children('li:last');
                //add class labels
                if(file.hasOwnProperty('labels')){
                    for(var l=0;l<file.labels.length;l++){
                        li.addClass(file.labels[l]);
                    }
                }
                //add type, file or dir
                li.attr('data-type', type);
                //prevent text selection
                li.attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false);
                //folder events
                var fileNameSpan=li.children('.file-name:first');
                fileNameSpan.prepend('<span class="file-icon"></span>');
                var iconSpan=fileNameSpan.children('.file-icon:first');
                //if this is a directory
                if(type=='d'){
                    li.attr('data-dir', file.name);
                    //add the folder icon
                    iconSpan.append('<span class="open">'+getDirOpenedSvg()+'</span><span class="close">'+getDirClosedSvg()+'</span>');
                    //if you can select folders, than single click selects and double click opens
                    if(args['select_folders']){
                        //click event
                        fileNameSpan.click(function(){
                            selectDirEvent(jQuery(this), args);
                        });
                    }
                    var rightClickFolder=function(labelSpan){
                        selectDirEvent(labelSpan, args);
                        if(!labelSpan.parent().hasClass('open')){
                            openDirEvent(labelSpan, args, path, file);
                        }
                    };
                    //init right click actions, if enabled
                    initRightClickFolderActions(fileNameSpan, rightClickFolder);
                    //if this is a parent folder
                    if(li.hasClass('parent')){
                        if(!args['select_folders']){ //no selectable folders, single click to open
                            //click event
                            fileNameSpan.click(function(){
                                openDirEvent(jQuery(this), args, path, file);
                            });
                        }else{ //it's possible to select the folder
                            //double click to open selectable folder
                            fileNameSpan.dblclick(function(){
                                openDirEvent(jQuery(this), args, path, file);
                            });
                        }
                    }
                }
                //if this is a file
                if(type=='f'){ //click go to file event
                    //add the file icon
                    iconSpan.append(getHtmlFileSvg());
                    //add the data-url attribute
                    var url=path+file.name;
                    if(url.indexOf('.')===0){ url=url.substring('.'.length); }
                    li.attr('data-url', url);
                    //go to page click event
                    if(args['allow_goto_page']){
                        fileNameSpan.addClass('link');
                        fileNameSpan.click(function(){
                            var parentLi=jQuery(this).parents('li:first');
                            var dataUrl=parentLi.attr('data-url')+'.html';
                            var url=document.location.protocol+'//'+document.location.host+dataUrl;
                            document.location.href=url;
                        });
                    }
                    //disable right click on files
                    fileNameSpan.parents('ul:first').contextmenu(function(e){
                        e.preventDefault(); e.stopPropagation();
                    });
                }
            });
            
            //if there is a callback
            if(callback!=undefined){
                callback(args);
            }

        });
    };
    
    //load one level of files for a given path that will extend under the root content directory
    var loadContentFiles=function(path, callback){
        isServerUp(function(){
            ajaxPost('/load-content-files', {path:path}, function(res){
                if(res!=undefined && res.status==ok()){
                    if(callback!=undefined){
                        callback(res);
                    }
                }
            });
        });
    };
    
    //loop through each of the file objects at a given content path
    var eachContentFile=function(fileData, callback){
        if(fileData.status==ok()){
           if(fileData.hasOwnProperty('matches')){
               //for each parent path (only one at a time because recursive is turned off in server browse())
               for(var p=0;p<fileData.matches.length;p++){
                   var root=fileData.matches[p]['path'];
                   //if there are any directories
                   if(fileData.matches[p].hasOwnProperty('d')){
                       //for each directory
                       for(var d=0;d<fileData.matches[p]['d'].length;d++){
                           callback(fileData.matches[p]['d'][d], 'd', d, root);
                       }
                   }
                   //if there are any files
                   if(fileData.matches[p].hasOwnProperty('f')){
                       //for each file
                       for(var f=0;f<fileData.matches[p]['f'].length;f++){
                            callback(fileData.matches[p]['f'][f], 'f', f, root);
                       }
                   }
               }
           }
        }
    };
    
    var saveData=function(dirScope, pageId, fileKey, content, callback){
        isServerUp(function(){
            ajaxPost('/write-to-file', {dir_scope:dirScope, page_id:pageId, file_key:fileKey, content:content}, function(res){
                if(res!=undefined){
                    if(res.status==ok()){
                        console.log('saved --> '+res.path);
                    }else{
                        var msg=res.status;
                        if(res.hasOwnProperty('path')){
                            msg+=' --> '+res.path;
                        }
                        console.log(msg);
                    }
                    if(callback!=undefined){
                        callback(res);
                    }
                }
            });
        });
    };
    
    var renameContentFile=function(dirScope, pageId, newPageId, callback){
        isServerUp(function(){
            ajaxPost('/rename-content-file', {dir_scope:dirScope, page_id:pageId, new_page_id:newPageId}, function(res){
                if(res!=undefined){
                    if(res.status==ok()){
                        console.log('rename: '+res.path+' --> '+res.new_path);
                    }else{
                        console.log('rename: '+res.status);
                    }
                    if(callback!=undefined){
                        callback(res);
                    }
                }
            });
        });
    };
    
    //duplicate the current page (like save-as)
    var duplicatePage=function(callback){
        isServerUp(function(){
            var dirScope=pagesHelper.getPageScopeFolder();
            var pageId=pagesHelper.getPageId();
            var fileKey='index.html';
            //figure out the new page ID
            var newPageId=pageId, copyPrefix='_Copy';
            if(newPageId.indexOf(copyPrefix)!=-1){
                newPageId=newPageId.substring(0, newPageId.lastIndexOf(copyPrefix));
            }
            var copyNum=1;
            while(pages.hasOwnProperty(newPageId+copyPrefix+copyNum)){
                copyNum++;
            }
            newPageId=newPageId+copyPrefix+copyNum;
            //save the index.html
            var args={};
            var content=editHelper.getHtmlSaveContent();
            saveData(dirScope, newPageId, fileKey, content, function(res){
                args[fileKey]=res;
                //copy the page data for pages.js
                var clonePage=helper.copyJson(pages[pageId]);
                pages[newPageId]=clonePage;
                //save the updated pages.js
                fileKey='pages.js';
                var pagesStr=editHelper.getPagesJsSaveContent();
                saveData(dirScope, newPageId, fileKey, pagesStr, function(res){
                    args[fileKey]=res;
                    if(callback!=undefined){
                        callback(args);
                    }
                    pagesHelper.setPageId(newPageId); //redirect to duplicate page
                });
            });
        });  
    };
    
    var deletePage=function(callback){
        isServerUp(function(){
            var dirScope=pagesHelper.getPageScopeFolder();
            var pageId=pagesHelper.getPageId();
            //delete the html file
            var args={};
            ajaxPost('/delete-content-file', {dir_scope:dirScope, page_id:pageId}, function(res){
                args['delete']=res;
                //modify the data
                delete pages[pageId];
                //save the updated pages.js
                var pagesStr=editHelper.getPagesJsSaveContent();
                saveData(dirScope, pageId, 'pages.js', pagesStr, function(res){
                    args['pages.js']=res;
                    if(callback!=undefined){
                        callback(args);
                    }
                    pagesHelper.goHome(); //redirect since this page doesn't exist anymore
                });
            });
        }); 
    };
    
    //figure out if a suggested file name already exists within a given scope, then return a unique name, if it already exists
    var getUniqueFileNameInScope=function(suggestName, callback, folderScope, startWith){
        if(startWith==undefined){ startWith='page'; }
        if(suggestName==undefined || suggestName.length<1){ suggestName=startWith; }
        isServerUp(function(){
            ajaxPost('/get-unique-content-file-name', {suggest_name:suggestName, dir_scope:folderScope, start_with:startWith},
            function(res){
                if(res.status==ok()){
                    callback(res.unique_name);
                }
            });
        }); 
    };
    
    //get JSON from a file that may be out of scope
    var loadJsonForFolderScope=function(folderScope, pageKey, callback){
        var currentScope=pagesHelper.getPageScopeFolder();
        if(currentScope==folderScope){
            switch(pageKey){
                case 'pages.js':
                    callback(pages);
                    break;
                case 'fields.js':
                    callback(fields);
                    break;
            }
        }else{ //out of the current scope
            ajaxPost('/get-data-json', {dir_scope:folderScope, page_id:pageKey}, function(res){
                if(res.status==ok()){
                    var json=JSON.parse(res.content);
                    callback(json);
                }else if(res!=undefined){ //the json file may not exist
                    //just send back empty object, because this file may not exist
                    callback({});    
                }
            });
        }
    };
    
    var createNewDir=function(folderScope, callback){
        isServerUp(function(){
            //call the server
            ajaxPost('/create-new-content-dir', {dir_scope:folderScope}, function(res){
                if(res.status==ok()){
                    if(callback!=undefined){
                        callback(res);
                    }
                }
            });
        }); 
    };
    
    var renameDir=function(oldDirPath, newDirPath, callback){
        isServerUp(function(){
            //call the server
            ajaxPost('/rename-content-dir', {old_path:oldDirPath, new_path:newDirPath}, function(res){
                if(res.status==ok()){
                    if(callback!=undefined){
                        callback(res);
                    }
                }
            });
        }); 
    };
    
    var createNewPage=function(folderScope, fileName, callback){
        isServerUp(function(){
            loadJsonForFolderScope(folderScope, 'pages.js', function(pagesJson){
                if(!pagesJson.hasOwnProperty(fileName)){
                    var content=editHelper.getHtmlSaveContentNew(folderScope);
                    //add new page json to the existing or new pages.js content
                    var newPageJson=editHelper.getNewPageJson();
                    pagesJson[fileName]=newPageJson;
                    //convert the json to a string
                    var pagesJsonStr=helper.jsonToString(pagesJson);
                    pagesJsonStr='var pages = '+pagesJsonStr+';';
                    //call the server
                    ajaxPost('/create-new-page', {dir_scope:folderScope, page_id:fileName, pages_json:pagesJsonStr, content:content}, 
                        function(res){
                        if(res.status==ok()){
                            if(callback!=undefined){
                                callback(res);
                            }
                            //redirect to the new page
                            pagesHelper.goToContentPage(folderScope, fileName, 'edit');
                        }
                    });
                }else{
                    callback({status:'error, '+fileName+' already exists in scope, '+folderScope});
                }
            });
        });    
    };
    
    var rootDirExists=function(path, callback){
        isServerUp(function(){
            //call the server
            ajaxPost('/root-dir-exists', {path:path}, function(res){
                if(callback!=undefined){
                    callback(res);
                }
            });
        });    
    };
    
    var writeOutputFile=function(root, path, content, callback){
        isServerUp(function(){
            //call the server
            ajaxPost('/write-output-file', {root:root, path:path, content:content}, function(res){
                if(callback!=undefined){
                    if(res.status==ok()){
                        res['valid']=true;
                    }
                    callback(res);
                }
            });
        });    
    };
    
    //public 
    return {
        rootDirExists:function(path, callback){
            rootDirExists(path, callback);
        },
        writeOutputFile:function(root, path, content, callback){
            writeOutputFile(root, path, content, callback);
        },
        createNewPage:function(folderScope, fileName, callback){
            createNewPage(folderScope, fileName, callback);
        },
        getUniqueFileNameInScope:function(suggestName, callback, folderScope, startWith){
            getUniqueFileNameInScope(suggestName, callback, folderScope, startWith);
        },
        initContentFolders:function(wrap, root, args){
            initContentFolders(wrap, root, args);
        },
        deletePage:function(callback){
            deletePage(callback);
        },
        duplicatePage:function(callback){
            duplicatePage(callback);
        },
        saveData:function(dirScope, pageId, fileKey, content, callback){
            saveData(dirScope, pageId, fileKey, content, callback);
        },
        renameContentFile:function(dirScope, pageId, newPageId, callback){
            renameContentFile(dirScope, pageId, newPageId, callback);
        },
        eachContentFile:function(fileData, callback){
            eachContentFile(fileData, callback);
        },
        loadContentFiles:function(root, callback){
            loadContentFiles(root, callback);
        },
        isServerUp:function(callback){
            isServerUp(callback);
        }
    };
}());