var highlightHelper=(function(){
    // private
    
    //regex
    var reg_startTag=/&lt;\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'"&gt;\s]+))?)+\s*|\s*)[\s?\/?]?&gt;/g;
    var reg_attr=/(\w+\s*=\s*(?:".*?"|'.*?'|[\^'"&gt;\s]+))/g;
    var reg_attrKey=/\w+\s*=/g;
    var reg_endTag=/&lt;\/\w+&gt;/g;
    var reg_htmlComment=/&lt;!--([\s\S]*?)--&gt;/g;
    var reg_jsMlComment=/\/\*(.|\n)*?\*\//g;
    var reg_jsComment=/\/\/.*/g;
    var reg_singleQuote=/\'.*?\'/g;
    var reg_doubleQuote=/\".*?\"/g;
    var reg_regx=/\/.+\/\w*/g;
    var reg_afterDot=/\.(\w+)/g;
    
    //highlight items
    var hl_startTag={
        wrap:'start_tag',
        match:reg_startTag,
        redraw:true,
        recursive:[
            {
                wrap:'attr',
                match:reg_attr,
                recursive:[
                    {
                        wrap:'attr_key',
                        match:reg_attrKey
                    }
                ]
            }
        ]
    };
    var hl_endTag={
        wrap:'end_tag',
        match:reg_endTag,
        redraw:true
    };
    var hl_htmlComment={
        wrap:'html_comment',
        match:reg_htmlComment,
        placeholders:true,
        recursive:[
            {
                wrap:'start_html_comment',
                match:'&lt;!--',
                index:0,
                redraw:true
            },
            {
                wrap:'end_html_comment',
                match:'--&gt;',
                index:-1,
                redraw:true
            }
        ]
    };
    var hl_jsMlComment={
        wrap:'script_ml_comment',
        match:reg_jsMlComment,
        placeholders:true,
        recursive:[
            {
                wrap:'start_script_ml_comment',
                match:'/*',
                index:0,
                redraw:true
            },
            {
                wrap:'end_script_ml_comment',
                match:'*/',
                index:-1,
                redraw:true
            }
        ]
    };
    var hl_jsComment={
        wrap:'script_comment',
        match:reg_jsComment,
        placeholders:true,
        recursive:[
            {
                wrap:'start_script_comment',
                match:'//',
                index:0,
                redraw:true
            }
        ]
    };
    var hl_singleQuote={
        wrap:'lit',
        match:reg_singleQuote,
        placeholders:true,
        recursive:[
            {
                wrap:'start_lit',
                match:"'",
                index:0,
                redraw:true
            },
            {
                wrap:'end_lit',
                match:"'",
                index:-1,
                redraw:true
            }
        ]
    };
    var hl_doubleQuote={
        wrap:'lit',
        match:reg_doubleQuote,
        placeholders:true,
        recursive:[
            {
                wrap:'start_lit',
                match:'"',
                index:0,
                redraw:true
            },
            {
                wrap:'end_lit',
                match:'"',
                index:-1,
                redraw:true
            }
        ]
    };
    var hl_valKeyword={
        wrap:'word_val',
        match:/\b(val)\b/g,
        redraw:true
    };
    var hl_regx={
        wrap:'reg_str',
        match:reg_regx,
        placeholders:true,
        redraw:true
    };
    var hl_afterDot={
        wrap:'dot_prop',
        match:reg_afterDot,
        redraw:true
    };
    var hl_startCurly={
        wrap:'start_curly',
        match:/\{/g,
        redraw:true
    };
    var hl_endCurly={
        wrap:'end_curly',
        match:/\}/g,
        redraw:true
    };
    var hl_startParen={
        wrap:'start_paren',
        match:/\(/g,
        redraw:true
    };
    var hl_endParen={
        wrap:'end_paren',
        match:/\)/g,
        redraw:true
    };
    var hl_startBrack={
        wrap:'start_brack',
        match:/\[/g,
        redraw:true
    };
    var hl_endBrack={
        wrap:'end_brack',
        match:/\]/g,
        redraw:true
    };
    //function to add tokens to the highlights
    var hl_tokens=function(str, data){
        var arr=[];
        var tokens=data.tokens;
        for(var t=0;t<tokens.length;t++){
            //basic token
            var tokenJson={match:tokens[t], reg_flags:'g', wrap:'token', placeholders:true, redraw:true};
            //token variation
            var rec=undefined;
            if(tokens[t].indexOf(':')!=undefined){
                rec=[];
                var varId=tokens[t].substring(tokens[t].indexOf(':')+1);
                if(varId.indexOf('(')!=-1){ varId=varId.substring(0, varId.indexOf('(')); }
                rec.push({
                    match:':'+varId,
                    wrap:'var_id'
                });
            }
            //casing
            if(tokens[t].toLowerCase().lastIndexOf('(ab)')==tokens[t].length-'(ab)'.length){
                if(rec==undefined){ rec=[]; }
                var casing=tokens[t].substring(tokens[t].lastIndexOf('('));
                rec.push({
                    match:casing,
                    wrap:'casing'
                });
            }
            //add the recursive variation strings
            if(rec!=undefined){
                tokenJson['recursive']=rec;
            }
            arr.push(tokenJson);
        }
        return arr;
    };

    //highlighter for block chains
    var hl_dynamic=function(str, data){
        //function to loop the dynamic block types
        var eachBlockType=function(callback){
            //for each block type
            var blockTypes=getBlockTypeData();
            for(var blockType in blockTypes){
                if(blockTypes.hasOwnProperty(blockType)){
                    callback(blockTypes[blockType], blockType);
                }
            }
        };
        //function to loop each block chain within each block type
        var eachBlockChain=function(html, callback){
            eachBlockType(function(block, type){
                //get the block-chains for this type
                var chains=getBlockChains(html, block.chain, getAllBetween);
                if(chains!=undefined && chains.length>0){
                    for(var c=0;c<chains.length;c++){
                        var chain=chains[c];
                        callback(block, type, chain);
                    }
                }
            });
        };
        
        //for each block type
        var arr=[];
        eachBlockChain(str, function(block, type, chain){
            var parts=[];
            //for each block in the chain
            for(var p=0;p<chain.parts.length;p+=2){
                parts.push({
                    match:chain.parts[p],
                    wrap:'block_head',
                    consume:['right'],
                    recursive:[
                        {
                            match:/\[\w+\][\s]*\(?/,
                            wrap:'block_type',
                            redraw:true
                        },
                        {
                            match:/[\s]*\(/,
                            wrap:'start_cond',
                            redraw:true,
                            index:0,
                        },
                        {
                            match:/\)[\s]*/,
                            wrap:'end_cond',
                            redraw:true,
                            index:-1,
                            consume:['right']
                        }
                    ]
                });
                //recursive call to chains insided the body of this part
                var rec=resolveDynamicInStr(chain.parts[p+1], data);
                rec.push({
                    wrap:'start_block', match:'{', index:0, redraw:true
                });
                //if this block is connected to another block in the chain
                if(p+2<chain.parts.length){
                    rec.push({
                        wrap:'end_block', match:/}[\s]*/, index:-1, redraw:true, consume:['right']
                    });
                }else{ //this is the last block in the chain
                    rec.push({
                        wrap:'end_block', match:'}', index:-1, redraw:true
                    });
                }
                parts.push({
                    match:chain.parts[p+1],
                    recursive:rec
                });
            }
            //add to the return array
            arr.push({
                match:chain.str, 
                recursive:parts
            });
        });
        return arr;
    };
    //js keyword highlighting
    var addJsKeywords=function(arr){
        var words=[
            'function', 'var', 
            'case', 'break', 
            'for', 'while', 
            'if', 'undefined'
        ];
        for(var w=0;w<words.length;w++){
            var word=words[w];
            var reg=new RegExp('\\b('+word+')\\b', 'g');
            arr.push({
                match:reg,
                wrap:'key_word',
                redraw:true
            });
        }
        return arr;
    };
    //public 
    return {
        suggest_content:function(){
            //some HTML suggestions
            var ret=[
                {
                    svg:false,
                    min_chars_left:2,
                    break:false,
                    options:[
                        {
                            label:'<code>', 
                            write:'<code write="{\'enabled\':true,\'path\':\'@{section.title}\'}"><!--\n${select:0}\n--></code>', 
                            match_left:['<code><!--', 'code']
                        },
                        {
                            label:'<p>', 
                            write:'<p>${select:0}</p>', 
                            match_left:['<p']
                        },
                        {
                            label:'<a>', 
                            write:'<a href="${href:1}" target="_blank">${select:0}</a>', 
                            match_left:['<a']
                        },
                        {
                            label:'<img>', 
                            write:'<img src="img/${src:1}" alt="${alt:0}" />', 
                            match_left:['img', '<img']
                        },
                        {
                            label:'<ul>', 
                            write:'<ul>\n<li>${select:0}</li>\n${tab:1}</ul>', 
                            match_left:['<ul>', 'ul']
                        },
                        {
                            label:'<li>', 
                            write:'<li>${select:0}</li>\n${tab:1}', 
                            match_left:['<li>', 'li']
                        }
                    ]
                }
            ];
            //section tokens
            var sectionTokens={
                svg:false,
                min_chars_left:2,
                break:false,
                options:[
                    {
                        label:'@{section.title}', 
                        write:'@{section.title}', 
                        match_left:['@{', 'section.title']
                    }
                ]
            }
            ret.push(sectionTokens);
            //token suggestions
            var tokens={
                svg:false,
                min_chars_left:1,
                break:false,
                options:[]
            };
            var tokensList=tokensHelper.getSerializedTokens();
            for(var t=0;t<tokensList.length;t++){
                tokens.options.push({
                    label:tokensList[t],
                    write:tokensList[t]
                });
            }
            ret.push(tokens);
            //dynamic block suggestions
            var dynamic={
                svg:false,
                min_chars_left:2,
                break:false,
                options:[
                    {
                        label:'[if]', 
                        write:'[if]( ${condition:0} ){\n${select:1}\n}', 
                        match_left:['if']
                    },
                    {
                        label:'[elseif]', 
                        write:'[elseif]( ${condition:0} ){\n${select:1}\n}', 
                        match_left:['elseif']
                    },
                    {
                        label:'[else]', 
                        write:'[else]{\n${select:0}\n}', 
                        match_left:['else']
                    },
                    {
                        label:'[if][else]', 
                        write:'[if]( ${condition:0} ){\n${select:1}\n}[else]{\n${default:2}\n}', 
                        match_left:['if-else']
                    },
                    {
                        label:'[if][elseif][else]', 
                        write:'[if]( ${condition:0} ){\n${select:1}\n}[elseif]( ${condition:2} ){\n${content:3}\n}[else]{\n${default:4}\n}', 
                        match_left:['if-elseif-else']
                    }
                ]
            };
            ret.push(dynamic);
            return ret;
        },
        data_tokens:function(ret){
            if(ret==undefined){ ret={}; }
            ret['tokens']=function(){ return tokensHelper.getSerializedTokens(); };
            return ret;
        },
        funcs_tokens:function(ret){
            if(ret==undefined){ ret={}; }
            ret['resolveTokensInStr']=hl_tokens;
            return ret;
        },
        funcs_dynamic:function(ret){
            if(ret==undefined){ ret={}; }
            ret['resolveDynamicInStr']=hl_dynamic;
            ret['getBlockTypeData']=dynamicHelper.getBlockTypeData;
            ret['getBlockChains']=dynamicHelper.getBlockChains;
            ret['getAllBetween']=helper.getAllBetween;
            return ret;
        },
        data_fullHtml:function(){},
        funcs_fullHtml:function(){},
        hl_fullHtml:function(ret){
            if(ret==undefined){ ret=[]; }
            ret.push(hl_htmlComment);
            ret.push(hl_startTag);
            ret.push(hl_endTag);
            return ret;
        },
        data_fullHtmlTokens:function(){
            var ret={}
            ret=this.data_tokens(ret);
            return ret;
        },
        funcs_fullHtmlTokens:function(){
            var ret={};
            ret=this.funcs_tokens(ret);
            ret=this.funcs_dynamic(ret);
            return ret;
        },
        hl_fullHtmlTokens:function(ret){
            if(ret==undefined){ ret=[]; }
            ret.push({call:'resolveTokensInStr'});
            ret.push({call:'resolveDynamicInStr'});
            return this.hl_fullHtml(ret);
        },
        data_js:function(){},
        funcs_js:function(){},
        hl_js:function(ret){
            if(ret==undefined){ ret=[]; }
            ret.push(hl_jsMlComment);
            ret.push(hl_jsComment);
            ret.push(hl_singleQuote);
            ret.push(hl_doubleQuote);
            ret.push(hl_regx);
            ret=addJsKeywords(ret);
            ret.push(hl_valKeyword);
            ret.push(hl_afterDot);
            ret.push(hl_startCurly);
            ret.push(hl_endCurly);
            ret.push(hl_startParen);
            ret.push(hl_endParen);
            ret.push(hl_startBrack);
            ret.push(hl_endBrack);
            return ret;
        },
        data_jsTokens:function(){
            var ret={}
            ret=this.data_tokens(ret);
            return ret;
        },
        funcs_jsTokens:function(){
            var ret={};
            ret=this.funcs_tokens(ret);
            return ret;
        },
        hl_jsTokens:function(ret){
            if(ret==undefined){ ret=[]; }
            ret.push({call:'resolveTokensInStr'});
            return this.hl_js(ret);
        }
    };
}());