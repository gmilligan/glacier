## What is Glacier?

Glacier is a configuration framework, with a graphical-user-interface (gui), __for building boilerplate token-based templates__.

Glacier's interface runs through a browser (google chrome recommended) and interacts with the filesystem via a lightweight node.js server. 

__Prerequisite__:

* If you haven't yet, you will need to [download and install node.js](https://nodejs.org/en/)

## Glacier Installation

First, [download the .zip source files](https://bitbucket.org/gmilligan/glacier/get/master.zip)

* On Mac, double click the downloaded .zip, to unzip
* On Windows, extract the downloaded .zip, to unzip

Find server.js inside a extraced folder. 
Copy all of the files and folders, at this level, to a new directory called "__glacier/__" at a location of your choosing.

The __glacier/__ directory should parent the __server.js__, and __package.json__ files.

```shell
cd [your-path]/glacier
npm install
```

## Run Glacier

Inside the root __glacier/__ directory

```shell
npm start
```
or 

```shell
node server.js
```

whichever you prefer.

## Getting started

A browser tab will open when you run glacier.

In the browser interface, navigate to the __Sample/Sample1__ page. Review this page in edit mode to get a sense of the features available to you.

## Philosophy

I'm not interested in becoming a virtuoso to move mountains. 
I'm simply interested in moving mountains. 

You may think that I'm undisciplined and undeserving of such accomplishment. 
But I disagree. Because I will pay for this privilege through my creations, instead of investing in the narrow scope of raw, finite, talent. 

My creations will be the true virtuoso, that will do the heavy lifting while I am free to focus on a bigger picture. 
To be able to lay down a solid foundation quickly so that we may explore beyond, 'just the basics,' is a privilege. 

I will also pay for the privilege of glory by sharing my methods and tools with you; 'the rising tide may raise all ships.'  

The care and effort that I have spent may be repaid if I can save you from similar effort. 
The struggle that I experience can serve to save you from similar struggle so that you may set your sights even higher, beyond my initial efforts that lay at the foundation of your path.

Where I leave-off, you begin, where you stop, I start. 
Repeat. 

Whether I ever reach the next summit, at least I can try seeking one foothold at a time. Maybe together, we can move mountains. 